import os, sys
from plistlib import *

path = sys.argv[1]
plist_path = path + "/YukonMobile/info.plist"

pl = readPlist(plist_path)
bundle_version_key = 'CFBundleVersion'
version = int(pl[bundle_version_key]) + 1
pl[bundle_version_key] = str(version)

bundle_short_version_key = 'CFBundleShortVersionString'
version = pl[bundle_short_version_key]
version_splited = version.split('.')
last_index = len(version_splited)-1
version_splited[last_index] = str(int(version_splited[last_index]) + 1)
updated_version = '.'.join(version_splited)
pl[bundle_short_version_key] = updated_version

writePlist(pl, plist_path)