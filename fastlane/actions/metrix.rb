# Copyright (C) Ciklum, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Artem Andrusenko <arand@ciklum.com>, April 2015


module Fastlane
  module Actions
    class MetrixAction < Action
      def self.is_supported?(platform)
        platform == :ios
      end
      
      def self.run(params)
        run_sonar_command = 'sh /usr/local/bin/run-sonar.sh -v'
        Actions.sh run_sonar_command
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        'Run Sonar and Metrix analysis'
      end

      def self.author
        'artemand'
      end
    end
  end
end