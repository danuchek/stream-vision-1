import os
import argparse
import localizable
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

parser = argparse.ArgumentParser()
parser.add_argument('--loco')
args = parser.parse_args()

strings = localizable.parse_strings(filename=args.loco)

result = {}
for x in strings:
    key = x['key']
    value = x['value']
    for z in strings:
        zkey = z['key']
        zvalue = z['value']

        if not key == zkey:
            if value == zvalue:
                string = result.get(zkey,None)
                found = ''
                if string == None:
                    print 'NOOOOOONE'
                    found = 'KEY:' + zkey + ' Value:' + zvalue + '\n'
                found += 'With KEY:' + key    
                if string == None:
                    result[zkey] = found
                else:
                    string += '\n' + found
                    result[zkey] = string

for sss in result:
    print result[sss]
    print '---'

file = open('output.txt', 'w')
for x in result:
    file.write(result[x] + '\n-----\n')
file.close()
