#!/bin/sh
PROJECT_KEY=-7scFuL6RnpYzb2T6W-eKZf7PQgzcMnf
TEMP_ARC=temp.zip
TEMP_FOLDER=tempFolder
curl "https://localise.biz:443/api/export/archive/strings.zip?key=$PROJECT_KEY&fallback=en" >> $TEMP_ARC
unzip $TEMP_ARC -d $TEMP_FOLDER
cd $TEMP_FOLDER/*
cp -r *.lproj ../../StreamVision/Resources/Localization
cd ../..
rm -rf $TEMP_FOLDER
rm $TEMP_ARC
python localize-plist.py