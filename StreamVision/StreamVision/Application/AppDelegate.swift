//
//  AppDelegate.swift
//  YukonMobile
//
//  Created by Anton Pavlyuk on 8/13/15.
//  Copyright (c) 2015 Ciklum. All rights reserved.
//

import UIKit
import Localize_Swift
import RealmSwift
import GoogleSignIn
import UserNotifications
import Firebase
import FileKit
import FirebaseCrashlytics
import FacebookCore

extension AppDelegate {
    func realmMigration() {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 17,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 17) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                    migration.enumerateObjects(ofType: "DeviceModel", { oldDevice, _ in
                        let device = VirtualDevice()
                        
                        let deviceInfoModel = oldDevice?["deviceInfoModel"] as? DynamicObject
                        let oldModelVersion = deviceInfoModel?["firmwareVersion"] as? DynamicObject
                        
                        if let id = oldDevice?["id"] as? String,
                            let sku = deviceInfoModel?["sku"] as? String,
                            let serial = deviceInfoModel?["serial"] as? String,
                            let hw = deviceInfoModel?["hardwareVersion"] as? String,
                            let connectionDate = deviceInfoModel?["connectionDate"] as? Date,
                            let disconnectionDate = deviceInfoModel?["disconnectionDate"] as? Date {
                            device.id = id
                            device.connectionDate = connectionDate
                            device.disconnectionDate = disconnectionDate
                            
                            if let versionString = oldModelVersion?["versionString"] as? String,
                                let major = oldModelVersion?["major"] as? Int,
                                let minor = oldModelVersion?["minor"] as? Int,
                                let patch = oldModelVersion?["patch"] as? Int,
                                let latter = oldModelVersion?["latter"] as? String {
                                let version = FirmwareVersion()
                                version.versionString = versionString
                                version.major = major
                                version.minor = minor
                                version.patch = patch
                                version.latter = latter
                                device.info.version = version
                            }
                            device.info.sku = sku
                            device.info.hw = hw
                            device.info.serial = serial
                        }
                        migration.create("VirtualDevice", value: device)
                    })
                    migration.deleteData(forType: "DeviceModel")
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        _ = try! Realm()
    }
}

// Change root VC extension
extension AppDelegate {
    func resetToMain() {
        _ = window?.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func changeRootViewController(_ toViewController: UIViewController, withAnimation animation: Bool?) {
        changeRootViewController(toViewController, withAnimation: animation, completion: nil)
    }
    
    func changeRootViewController(_ toViewController: UIViewController, withAnimation animation: Bool?, completion: (() -> Void)? ) {
        guard self.window?.rootViewController != toViewController else {
            completion?()
            return
        }
        
        window?.subviews.forEach() { view in
            view.removeFromSuperview()
        }
        
        if animation == true {
            UIView.transition(with: self.window!,
                              duration: 0.35,
                              options:UIView.AnimationOptions.transitionCrossDissolve, animations: {
                let animationState = UIView.areAnimationsEnabled;
                UIView.setAnimationsEnabled(false)
                self.window?.rootViewController = toViewController
                UIView.setAnimationsEnabled(animationState)
                }, completion: {(finished: Bool) -> () in
                    completion?()
            })
        } else {
            self.window?.rootViewController = toViewController
            completion?()
        }
    }
}

extension AppDelegate {
    // MARK: Notifications
    func setupLocalNotificationActions() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge],
                                                                completionHandler: { granted, error in
        })
        let laterAction = UNNotificationAction(identifier: laterActionIdentifier,
                                               title: "Notification.File.Converted.Later".localized(), options: [.foreground])
        let shareAction = UNNotificationAction(identifier: shareActionIdentifier,
                                               title: "Notification.File.Converted.Share".localized(), options: [.foreground])
        
        let quizCategory = UNNotificationCategory(identifier: notificationIdentifier,
                                                  actions: [laterAction, shareAction],
                                                  intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([quizCategory])
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, StoreManager, DeviceManager {
    // Private keys
    private let kCrashlyticsAPIKey = "15276aa0c1ad6916b056e94845971536a5f321eb"
    private let kDBAPPKey = "rpckw44i48zjncc"
    private let kDBAPPSecret = "p7c6mzglveh868v"
    private let kGoogleAnalytics = "UA-76449191-2"
    
    #if DEVBUILD
    private let kGSNClientID = "917495371193-reve35jbm33fe4rtlii71pb57fos2asg.apps.googleusercontent.com"
    #else
    private let kGSNClientID = "303623125836-837jk7gmdgnmkfed1jusi31728ppcfs7.apps.googleusercontent.com"
    #endif
    
    private let kGMSProdKey = "AIzaSyD4UPhl6Ac8foAv0XvN04-V566UGcRUoo0"
    private let kGMSStageKey = "AIzaSyABeSlqInpbxyMm8_lOcFGnEwINUDY-9yw"
    
    
    // Notification identifier
    let notificationIdentifier = "shareNotificationIdentifier"
    let laterActionIdentifier = "laterActionIdentifier"
    let shareActionIdentifier = "shareActionIdentifier"

    var window: UIWindow?
    var connectionAlertShowed: Bool = false
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Migrate DB if needed
        realmMigration()
        
        // Analytics
        configureFirebaseAnalytics()
        
        // Setup Facebook
        configureFB(application: application, options: launchOptions)
        
        // Setup Google sign in
        configureGSN()
        
        // Setup push notifications
        registerUserPushToken(application)
        
        // Setup application style
        Appearence.setupApplicationStyle()
        
        // Setup connection notification
        subscribeConnectionNotification()
        
        // Setup local notifications
        setupLocalNotificationActions()
        
        // Rate app
        setFirstLaunchDate()
        
        // Make delay for launch screen
        Thread.sleep(forTimeInterval: 2.0)
        
        // Subscribe to catch crashes in log file
        
//        NSSetUncaughtExceptionHandler { exception in
//            Logging.log(message: "<<============= CRASH ===============>>")
//            Logging.log(message: "Crash: " + exception.description)
//            Logging.log(message: exception.callStackSymbols.reduce("Crash stack: ", {
//                return $1 + "\n" + $0
//            }))
//        }
        
        return true
    }
    
    // MARK: - Rate app
    
    fileprivate func setFirstLaunchDate() {
        if storeManager.firstLaunchDate == nil {
            storeManager.firstLaunchDate = Date()
        }
    }
    
    // MARK: What's new
    fileprivate func checkWhatsNew() {
        let version =
            Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        
        if storeManager.lastAppVersion != version {
            let alert = UIAlertController(title: "iOS.Notifications.WhatsNewAlert.Title".localized(),
                                          message: "iOS.Notifications.WhatsNewAlert.Text".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default,
                                          handler: nil))
            self.window?.rootViewController?.present(alert,
                                                     animated: true,
                                                     completion: nil)
        }
        
        storeManager.lastAppVersion = version
    }
    
    // MARK: Update localization
    fileprivate func checkUpdates(completion: (() -> ())?) {
        Logging.log(message: "[Global] check for updates")
        
        let lastCheckDate = storeManager.lastCheckingUpdates
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard formatter.string(from: lastCheckDate) != formatter.string(from: Date()) else {
            completion?()
            return
        }
        
        let firmwareService = FirmwareUpdateService()
        firmwareService.checkForUpdatesForDevices(storeManager.devices) { devices in
            let devices = devices.compactMap() { return !$0.isInvalidated ? $0 : nil }
            var hasUpdates = 0
            devices.forEach() { device in
                hasUpdates += device.hasNewestFirmware ? 1 : 0
            }
            if hasUpdates > 0 {
                Logging.log(message: "[Global] update exist - devices \(devices)")
                
                var message = String(format: "Notifications.UpdateExists.OneDevice".localized(),
                                     devices[0].info.name)
                if hasUpdates > 1 {
                    message = ""
                    for item in devices {
                        message += item.info.name + ", "
                    }
                    message += "Notifications.UpdateExists.ManyDevices".localized()
                }
                
                let alert = UIAlertController(title: "Notifications.UpdateExists.Title".localized(),
                                              message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                              style: .default) { action in
                                                completion?()
                })
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            } else {
                completion?()
            }
        }
        storeManager.lastCheckingUpdates = Date()
    }
    
    // MARK: Connection notification
    func subscribeConnectionNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(deviceDidDisconnect),
            name: NSNotification.Name(rawValue: "DeviceDidDisconnectNotification"),
            object: nil)
    }
    
    func registerUserPushToken(_ application: UIApplication) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge]) { (granted, error) in
            DispatchQueue.main.async {
                application.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let convertedToken = deviceToken.convertPushTokenToString()
        ConnectionService.sendPushToken(convertedToken)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()
        checkUpdates() {
            self.checkWhatsNew()
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        SocketManager.send(.getDeviceInfo)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint("Couldn't register: \(error)")
    }

    // MARK: FB configuration
    func configureFB(application: UIApplication, options: [UIApplication.LaunchOptionsKey: Any]?) {
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: options)
    }
    
    // MARK: Google configuration
    func configureGSN() {
        guard let gsn = GIDSignIn.sharedInstance() else {
            assert(false, "Google SignIn not configured correctly")
            return
        }
        
        gsn.clientID = kGSNClientID
    }

    func configureFirebaseAnalytics() {
        FirebaseApp.configure()
    }
    
    // MARK: Device disconnect
    @objc func deviceDidDisconnect() {
        DispatchQueue.main.async { () -> Void in
            let main: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
            self.changeRootViewController(main, withAnimation: true)
        }
    }
    
    // MARK: Handle URLs
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url) ||
            ApplicationDelegate.shared.application(application,
                                                   open: url,
                                                   sourceApplication: sourceApplication,
                                                   annotation: annotation)
    }
    
    @available(iOS 9.0, *)
    internal func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url) ||
        ApplicationDelegate.shared.application(app, open: url, options: options)
    }
}

