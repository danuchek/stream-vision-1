//
//  DeviceNetworkService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/15/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import Alamofire
import FileKit

extension DeviceNetworkService: DeviceManager {
    typealias CompletionHandler = (_ imagePath: String?) -> Void
    
    func checkDevicesImages(devices: [VirtualDeviceTemplate], completionHandler: (() -> Void)? = nil) {
        let group = DispatchGroup()
        
        for device in devices {
            group.enter()
            let file = File<Data>(path: Path(device.info.imagePath) )
            if !file.exists {
                if let url = URL(string: device.info.imagePath), url.isValid {
                    downloadImage(name: device.id, url: url) { [weak self] (path) in
                        if let path = path {
                            self?.deviceManager.update {
                                device.info.imagePath = path
                                Logging.log(message: path)
                            }
                        }
                        group.leave()
                    }
                } else {
                    group.leave()
                }
            } else {
                group.leave()
            }
        }
        group.notify(queue: .main) {
            completionHandler?()
        }
    }
    
    fileprivate func downloadImage(name: String, url: URL?, completionHandler: CompletionHandler?) {
        guard let url = url else {
            DispatchQueue.main.async {
                completionHandler?(nil)
            }
            return
        }
        
        let fileName = "/images/" + "\(name).png"
        let destination: DownloadRequest.Destination = { _, _ in
            let fileURL: URL = (Path.userLibrary + fileName).url
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        AF.download(url, to: destination).response { response in
            if response.error == nil, let imagePath = response.fileURL, let _ = Path(url: imagePath) {
                DispatchQueue.main.async {
                    completionHandler?(fileName)
                }
            } else {
                Logging.log(message: response.error?.localizedDescription ?? "Download device image - unrecognized")
                Logging.log(message: response.fileURL?.relativePath ?? "Download device image - File url invalid")
                completionHandler?(nil)
            }
        }
    }
}

class DeviceNetworkService {
    
    fileprivate let baseUrl = NetworkConnection().baseUrl
    
    func fetchDevices(by timestamp: Int, completionHandler: @escaping (_ networkDevices: [NetworkDevice]) -> Void ) {
        let parameters: Parameters = ["offsetTimestamp": timestamp]
        AF.request("\(baseUrl)/api/v2/device/list",
                          method: .get,
                          parameters: parameters,
                          encoding: URLEncoding(destination: .queryString),
                          headers: ["accept": "application/json"]
        ).responseJSON { (response) in
            switch response.result {
            case let .success(data):
                if let devicesArray = data as? [[String: Any]] {
                    completionHandler(devicesArray.compactMap({ self.parseToNetworkDevice(deviceDict: $0) }))
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
    private func parseToNetworkDevice(deviceDict: [String: Any]) -> NetworkDevice? {
        guard let description: Description = Description.from(dict: deviceDict),
            let brand: Brand = Brand.from(dict: deviceDict["brand"] as? [String : Any]),
            let project: Project = Project.from(dict: deviceDict["project"] as? [String : Any]) else {
                return nil
        }
        
        guard let sku = deviceDict["sku"] as? String,
              let lastChangedAt = deviceDict["lastChangedAt"] as? Int,
              let name = deviceDict["name"] as? String
            else {
            return nil
        }
        
        // TODO: Add default image
        var logo: String = (deviceDict["logo"] as? String) ?? ""
        logo = logo.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        return NetworkDevice(sku: sku,
                             lastChangedAt: lastChangedAt,
                             name: name,
                             description: description,
                             logo: logo,
                             project: project,
                             brand: brand)
    }
}
