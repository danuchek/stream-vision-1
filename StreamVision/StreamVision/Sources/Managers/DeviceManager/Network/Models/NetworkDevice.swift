//
//  NetworkDevice.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/15/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

struct Brand {
    let id: Int
    
    static func from(dict: [String: Any]?) -> Self? {
        guard let dict = dict, let id = dict["id"] as? Int else {
            return nil
        }
        return Brand(id: id)
    }
}

struct Project {
    let id: Int
    let name: String
    
    static func from(dict: [String: Any]?) -> Self? {
        guard let dict = dict, let id = dict["id"] as? Int, let name = dict["name"] as? String else {
            return nil
        }
        return Project(id: (id - 1), name: name)
    }
}

struct NetworkTranslation {
    let locale: String
    let translation: String
    
    static func from(dict: [String: Any]?) -> Self? {
        guard let dict = dict, let locale = dict["locale"] as? String, let translation = dict["translation"] as? String, translation != "" else {
            return nil
        }
        return NetworkTranslation(locale: locale, translation: translation)
    }
}

struct Description {
    let assetId: String
    let translations: [NetworkTranslation]
    
    static func from(dict: [String: Any]?) -> Self? {
        if let descriptionDict = dict?["description"] as? [String: Any],
           let assetId = descriptionDict["assetId"] as? String {
            if let translationsArray = descriptionDict["translations"] as? [[String: Any]] {
                let translations: [NetworkTranslation] = translationsArray.compactMap { translation in
                    NetworkTranslation.from(dict: translation)
                }
                return Description(assetId: assetId, translations: translations)
            }
        }
        return Description(assetId: "", translations: [])
    }
}

struct NetworkDevice {
    var sku: String
    var lastChangedAt: Int
    var name: String
    var description: Description
    var logo: String
    var project: Project
    var brand: Brand
}
