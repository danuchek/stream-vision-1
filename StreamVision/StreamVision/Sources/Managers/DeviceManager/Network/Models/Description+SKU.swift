//
//  Description+SKU.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/11/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

extension Description {
    static func by(sku: String, onlyKey: Bool) -> String {
        var description = "DeviceDetails.Description.\(sku)"
        
        switch sku {
        case "77350", "77352", "77354", "77360", "77362", "77364", "77368":
            description = "DeviceDetails.Description.OracleLQF"
            
        case "77351", "77353", "77355", "77361", "77363", "77365", "77369", "77381":
            description = "DeviceDetails.Description.OracleXQF"
            
        case "77356", "77358", "77366":
            description = "DeviceDetails.Description.OracleLP"
            
        case "77357", "77359", "77367":
            description = "DeviceDetails.Description.OracleXP"
            
        // Helion
        case "77391", "77392", "77393", "77394", "77395", "77398", "77399":
            description = "DeviceDetails.Description.HelionXQF"
            
        case "77403", "77404", "77405", "77408", "77409":
            description = "DeviceDetails.Description.HelionXP"
            
        // Helion 2
        case "77402":
            description = "DeviceDetails.Description.Helion2.XP50"
        case "77397":
            description = "DeviceDetails.Description.Helion2.XQ50F"
            
        // Trail 2
        case "76558":
            description = "DeviceDetails.Description.Trail2.LRF.XQ50"
        case "76559":
            description = "DeviceDetails.Description.Trail2.LRF.XP50"
            
        // Lexion
        case "77443", "77444", "77445":
            description = "DeviceDetails.Description.LexionXP"
            
        case "77441", "77442":
            description = "DeviceDetails.Description.LexionXQ"
            
            // Digex
            
        case "76641", "76642":
            description = "DeviceDetails.Description.Digex"
            
        // Signal
        case "78063", "78062", "78061", "28062", "28063", "28061":
            description = "DeviceDetails.Description.SignalRT"
            
        // Trail
        case "76500", "76502", "76510":
            description = "DeviceDetails.Description.TrailLQ"
            
        case "76501", "76503", "76511", "76513", "76512":
            description = "DeviceDetails.Description.TrailXQ"
            
        // Trail LRF
        case "76514", "76516", "76517", "76518", "76519":
            description = "DeviceDetails.Description.TrailLRF"
            
        case "76506", "76508":
            description = "DeviceDetails.Description.TrailLP"
            
        case "77415", "77416", "77417", "77418":
            description = "DeviceDetails.Description.Accolade.LRF"
            
        case "76507", "76509":
            description = "DeviceDetails.Description.TrailXP"
            
        case "26381", "26382", "26383", "26384", "26385", "26386", "76381", "76383", "76384", "76386":
            description = "DeviceDetails.Description.SightlineRT"
            
        case "26391", "26392", "26393", "26394", "76391", "76392", "76393", "76394", "SM18015", "SM18016", "SM18017", "SM18018":
            description = "DeviceDetails.Description.PhotonRT"
            
            // Forward
            
        case "78136", "78135", "78134", "78133", "78132", "78131", "78106", "78105", "78104", "78103", "78102", "78101":
            description = "DeviceDetails.Description.Forward"
            
        case "78107", "78138", "78110", "78109", "78108":
            description = "DeviceDetails.Description.ForwardFN"
            
        case "78186", "78182":
            description = "DeviceDetails.Description.Forward.F455"
            
        case "78196", "78192":
            description = "DeviceDetails.Description.Forward.FN455"
            
            // Digisight
            
        case "76360", "76370":
            description = "DeviceDetails.Description.DigisightULTRA350"
            
        case "76627", "76628":
            description = "DeviceDetails.Description.Digisight.N450.LRF"
            
        // Accolade
        case "77411", "77412", "77413", "77414":
            description = "DeviceDetails.Description.Accolade"
            
        // Accolade LRF 2
        case "77410":
            description = "DeviceDetails.Description.Accolade.LRF.2"
            
        // Digisignt
        case "76617", "76618":
            description = "DeviceDetails.Description.Digisight"
            
        // Axion
        case "77421", "77422", "77437", "77438", "77420", "77423":
            description = "DeviceDetails.Description.Axion"
            
        // Thermion
        case "76524", "76525", "76526":
            description = "DeviceDetails.Description.ThermionXM"
            
        case "76542", "76543":
            description = "DeviceDetails.Description.ThermionXP"
            
        case "76522":
            description = "DeviceDetails.Description.ThermionXQ.38"
            
        case "76523":
            description = "DeviceDetails.Description.ThermionXQ.50"
            
        // Demo
        case "00000":
            description = "DeviceDetails.Description.78093"
            
        default:
            description = "DeviceDetails.Description.\(sku)"
        }
        
        return onlyKey ? description : description.localized()
    }
}
