//
//  DeviceManager.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/20/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift
import FileKit

fileprivate let shared: DeviceManagerImplementation = DeviceManagerImplementation()

protocol DeviceManager: StoreManager {
    var deviceManager: DeviceManagerProtocol { get }
}

extension DeviceManager {
    var deviceManager: DeviceManagerProtocol {
        return shared
    }
}

protocol DeviceManagerProtocol: class {
    func getDevice(by sku: String, version: FirmwareVersion) -> VirtualDeviceTemplate?
    
    func add(device: VirtualDeviceTemplate)
    func add(completion: () -> VirtualDeviceTemplate)
    func update(completionHandler: @escaping () -> Void)
    func fetch(completionHandler: (() -> Void)?)
    func reload()
    func deviceFPS(by: String) -> Int
}

/*
 - Main idea of this service it's storing device teplates for different versions and use it for creating
 models for real connected devices.
 */
fileprivate final class DeviceManagerImplementation: DeviceManager,
                                                     DeviceManagerProtocol {
//    private lazy var realm = try! Realm(fileURL: URL(string: (Path.userLibrary + "/devices.realm").rawValue)! )
    private var lastTimeStamp: Int {
        let realm = try! Realm()
        return realm.objects(VirtualDeviceTemplate.self).sorted(by: { lhs, rhs in
            lhs.info.serverTimeStamp > rhs.info.serverTimeStamp
        }).first?.info.serverTimeStamp ?? 0
    }
    
    let network: DeviceNetworkService = DeviceNetworkService()
    
    /*
     - Get device template from DB for feature convertation to VirtualDevice
     */
    func getDevice(by sku: String, version: FirmwareVersion) -> VirtualDeviceTemplate? {
        let realm = try! Realm()
        let storedDevices = realm.objects(VirtualDeviceTemplate.self)
        
        let devices = storedDevices.filter ({ (device) -> Bool in
            return device.info.sku == sku
        }).sorted { (lhs, rhs) -> Bool in
            lhs.info.version >= rhs.info.version
        }
        
        let device = devices.filter { (device) -> Bool in
            device.info.version <= version
        }

        return device.first ?? devices.first ?? getDevice(by: "0", version: version) ?? nil
    }
    
    func add(device: VirtualDeviceTemplate) {
        let realm = try! Realm()
        try? realm.write({
            realm.add(device, update: .all)
        })
    }
    
    func add(completion: () -> VirtualDeviceTemplate) {
        let realm = try! Realm()
        try? realm.write({
            realm.add(completion(), update: .all)
        })
    }
    
    func update(completionHandler: @escaping () -> Void) {
        DispatchQueue.main.async {
            let realm = try! Realm()
            try? realm.write {
                completionHandler()
            }
        }
    }
    
    /*
     - Fetch new devices from server and check images for stored devices
     */
    func fetch(completionHandler: (() -> Void)? = nil) {
        let realm = try! Realm()
        let localDevices = realm.objects(VirtualDeviceTemplate.self).sorted { $0.id < $1.id }
        network.fetchDevices(by: lastTimeStamp) { [weak self] devices in
            devices.forEach { (device) in
                if let localDevice = localDevices.filter ({ $0.info.sku == device.sku }).last {
                    _ = try? realm.write {
                        localDevice.update(by: device)
                    }
                } else {
                    self?.add() {
                        let device = VirtualDeviceTemplate().update(by: device)
                        device.id = device.deviceString
                        return device
                    }
                }
            }
            let templates = realm.objects(VirtualDeviceTemplate.self).sorted (by: { $0.id < $1.id })
            self?.network.checkDevicesImages(devices: templates, completionHandler: completionHandler)
        }
    }
    
    /*
     - Hack for convertation for different devices with different fps
     */
    func deviceFPS(by sku: String) -> Int {
        switch sku {
        case "78093",
             "28047",
             "28048",
             "18056",
             "28061",
             "SM18023",
             "28062",
             "SM18024",
             "28063",
             "26391",
             "26392",
             "26393",
             "26394",
             "SM18015",
             "SM18016",
             "SM18017",
             "SM18018",
             "SM18025":
            return 20
        default:
            return 24
        }
    }
}
