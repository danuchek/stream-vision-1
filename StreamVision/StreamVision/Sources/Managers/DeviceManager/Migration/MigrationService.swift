//
//  MigrationService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/14/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import UIKit
import Localize_Swift
import RealmSwift

/**
  Migration service need for migrate plist with devices to DB with templates.
*/
class MigrationService: DeviceManager {
    
    /**
      General method for migration plist devices to templates in db
    */
    func migrateFromPlistToDB(completion: @escaping () -> Void ) {
        guard let devices = MigrationService.getDevicesFromBundle() else {
            DispatchQueue.main.async {
                completion()
            }
            return
        }
        
        for plistDevice in devices {
            addDeviceWithVersionsCorrection(plistDevice: plistDevice,
                                            version: FirmwareVersion().versionString,
                                            correction: nil,
                                            correctionParams: nil)
            
            if var versions = plistDevice["version"] as? [String: Any] {
                
                let orderedKeys: [String] = versions.keys.map({ $0 }).sorted()
                fix(versions: &versions)
                for version in orderedKeys {
                    let correction: [String: Any]? = getCorrectedParams(plistVersion: versions[version] as? [String: Any])
                    let correctionParams: [Parameter] = getCorrectedParams(plistVersion: versions[version] as? [String: Any])
                    
                    addDeviceWithVersionsCorrection(plistDevice: plistDevice,
                                                    version: version,
                                                    correction: correction,
                                                    correctionParams: correctionParams)
                }
            }
        }
        
        let realm = try! Realm()
        let storedDevices = realm.objects(VirtualDevice.self)
        
        storedDevices.forEach { (device) in
            if let template = deviceManager.getDevice(by: device.info.sku, version: device.info.version) {
                device.update(with: template)
            }
        }
        
        UserDefaults.standard.set(true, forKey: "isMigrated")
        DispatchQueue.main.async {
            completion()
        }
    }
    
    private func addDeviceWithVersionsCorrection(plistDevice: [String: Any],
                                                 version: String,
                                                 correction: [String: Any]?,
                                                 correctionParams: [Parameter]?) {
        
        if let details = plistDevice["details"] as? [[String: Any]] {
            for info in details {
                autoreleasepool {
                    let deviceModel = VirtualDeviceTemplate()
                    var virtualParameters = deviceModel.templateParameters
                    
                    let sku: String = info[PlistKeys.sku.rawValue] as! String
                    let name: String = info[PlistKeys.name.rawValue] as! String
                    
                    // Get zoom from versioning
                    let zoom = parameterByPlistKey(plistKey: PlistKeys.zoom.rawValue,
                                                   parameters: &virtualParameters)
                    setup(parameter: zoom, function: info[PlistKeys.zoom.rawValue] as? [String : AnyObject])
                    
                    var parameters: [Parameter?] = []
                    
                    // Add parameters from functions
                    if let functions = plistDevice["functions"] as? [String: Any] {
                        for key in functions.keys {
                            let param = parameterByPlistKey(plistKey: key,
                                                            parameters: &virtualParameters)
                            setup(parameter: param, function: functions[key] as? [String : AnyObject],
                                  correction: correction)
                            parameters.append(param)
                        }
                        if functions.keys.count < 3 {
                            deviceModel.templateParameters.api = .new
                        }
                    }
                    
                    // Add parameters from settings
                    if let settings = plistDevice["settings"] as? [String: Any] {
                        for key in settings.keys {
                            let param = parameterByPlistKey(plistKey: key, parameters: &virtualParameters)
                            setup(parameter: param, function: settings[key] as? [String : AnyObject],
                                  correction: correction)
                            parameters.append(param)
                        }
                    }
                    
                    // Add parameters from versioning
                    if let correctionParams = correctionParams {
                        for param in correctionParams {
                            if !parameters.contains(param) {
                                let virtualParameter = getParameter(by: type(of: param), parameters: &virtualParameters)
                                virtualParameter.update(with: param)
                            }
                        }
                    }
                    
                    // Update max value for zoom
                    if let limits = virtualParameters?.zoom?.limits {
                        limits.max = limits.min * limits.multiplier
                    }
                    
                    deviceModel.id = "\(sku)+\(version)"
                    deviceModel.info.sku = sku
                    deviceModel.info.name = name
                    deviceModel.info.imagePath = getImageName(by: sku) ?? ""
                    
                    let transltionArray: [Translation] = Localize.availableLanguages().map {
                        let translation = Translation()
                        translation.locale = $0
                        translation.translation =
                            Description.by(sku: deviceModel.info.sku, onlyKey: true).localized(for: $0)
                        return translation
                    }
                    deviceModel.info.specification.translations.append(objectsIn: transltionArray)
                    deviceModel.firmware.version = FirmwareVersion.instanceFrom(version)
                    deviceModel.info.version = FirmwareVersion.instanceFrom(version)
                    
                    deviceModel.templateParameters = virtualParameters
                    deviceManager.add(device: deviceModel)
                }
            }
        }
        
    }
    
    // Used for versioning
    private func fix(versions: inout [String: Any]) {
        let orderedKeys: [String] = versions.keys.map({ $0 }).sorted()
        
        for key in orderedKeys {
            for updateKey in orderedKeys {
                if var version = versions[key] as? [String: Any] {
                    if FirmwareVersion.instanceFrom(updateKey) < FirmwareVersion.instanceFrom(key) {
                        if var current: [String: Any] = getCorrectedParams(plistVersion: version) {
                            if let correction: [String: Any] = getCorrectedParams(plistVersion: versions[updateKey] as? [String : Any]) {
                                for corKey in correction.keys {
                                    current[corKey] = correction[corKey]
                                }
                            }
                            setCorrectedParams(plistVersion: &version, update: current, key: key)
                            versions[key] = version
                        }
                    }
                }
            }
        }
    }
    
    private func setCorrectedParams(plistVersion: inout [String: Any], update: [String: Any], key: String) {
        guard var brand = plistVersion["brand"] as? [String: Any] else {
            return;
        }
        guard var params = brand["params"] as? [String: Any] else {
            return;
        }
        
        for key in update.keys {
            params[key] = update[key]
        }
        
        brand["params"] = params
        plistVersion["brand"] = brand
    }
    
    private func getCorrectedParams(plistVersion: [String: Any]?) -> [String: Any]? {
        guard let brand = plistVersion?["brand"] as? [String: Any] else {
            return nil
        }
        guard let params = brand["params"] as? [String: Any] else {
            return nil
        }
        
        return params
    }
    
    private func getCorrectedParams(plistVersion: [String: Any]?) -> [Parameter] {
        guard let brand = plistVersion?["brand"] as? [String: Any] else {
            return []
        }
        guard let params = brand["params"] as? [String: Any] else {
            return []
        }
        
        var result: [Parameter?] = []
        for key in params.keys {
            var stub: TemplateParameters? = nil
            let parameter = parameterByPlistKey(plistKey: key, parameters: &stub)
            setup(parameter: parameter, function: params[key] as? [String : AnyObject])
            result.append(parameter)
        }
        
        return result.compactMap({ $0 })
    }
    
    private enum PlistKeys: String {
        case sku = "sku"
        case name = "name"
        case zoom = "zoom"
        case recognition = "recognition"
        case shutter = "shutter"
        case overlay = "overlay"
        case distances = "distances"
        case marker = "marker"
        case markerColor = "markerColor"
        case markerBrightness = "markerBrightness"
        case pip = "pip"
        case capture = "capture"
        case contrast = "contrast"
        case brightness = "brightness"
        case colorsPallet = "colorsPallet"
        case language = "language"
        case units = "units"
        case calibration = "calibration"
        case autoOff = "autoOff"
        case profile = "profile"
        case format = "format"
        case dateFormat = "dateFormat"
        case blockage = "blockage"
        case time = "time"
        case date = "date"
        case live = "live"
        case ir = "ir"
        case lcd = "lcd"
        case stream = "stream"
    }
    
    private func parameterByPlistKey<T: VirtualParametersProtocol>(plistKey: String, parameters: inout T?) -> Parameter? {
        switch PlistKeys(rawValue: plistKey) {
        case .ir: return getParameter(by: IR.self, parameters: &parameters)
        case .lcd: return getParameter(by: DisplayOff.self, parameters: &parameters)
        case .pip: return getParameter(by: PiP.self, parameters: &parameters)
        case .zoom: return getParameter(by: Zoom.self, parameters: &parameters)
        case .time: return getParameter(by: DateTime.self, parameters: &parameters)
        case .date: return getParameter(by: DateTime.self, parameters: &parameters)
        case .live: return getParameter(by: Live.self, parameters: &parameters)
        case .stream: return getParameter(by: Stream.self, parameters: &parameters)
        case .units: return getParameter(by: Units.self, parameters: &parameters)
        case .format: return getParameter(by: Format.self, parameters: &parameters)
        case .marker: return getParameter(by: ReticleType.self, parameters: &parameters)
        case .autoOff: return getParameter(by: AutoOff.self, parameters: &parameters)
        case .profile: return getParameter(by: Preset.self, parameters: &parameters)
        case .shutter: return getParameter(by: Shutter.self, parameters: &parameters)
        case .overlay: return getParameter(by: Overlay.self, parameters: &parameters)
        case .capture: return getParameter(by: CaptureMode.self, parameters: &parameters)
        case .blockage: return getParameter(by: Blockage.self, parameters: &parameters)
        case .contrast: return getParameter(by: Contrast.self, parameters: &parameters)
        case .language: return getParameter(by: Language.self, parameters: &parameters)
        case .distances: return getParameter(by: Distance.self, parameters: &parameters)
        case .dateFormat: return getParameter(by: TimeFormat.self, parameters: &parameters)
        case .brightness: return getParameter(by: Brightness.self, parameters: &parameters)
        case .recognition: return getParameter(by: Recognition.self, parameters: &parameters)
        case .markerColor: return getParameter(by: ReticleColor.self, parameters: &parameters)
        case .calibration: return getParameter(by: Calibration.self, parameters: &parameters)
        case .colorsPallet: return getParameter(by: ColorsPalette.self, parameters: &parameters)
        case .markerBrightness: return getParameter(by: ReticleBrightness.self, parameters: &parameters)
            
        default: return nil
        }
    }
    
    func getParameter<T: Parameter, P: VirtualParametersProtocol>(by paramType: T.Type, parameters: inout P?) -> T {
        var result: T = paramType.empty()
        
        if let parameters = parameters {
            for child in Mirror(reflecting: parameters).children {
                let matchResult = "\(type(of:child.value))".match(regex: "Optional<(.*)>")
                if matchResult.count > 1, matchResult[1] == "\(paramType)" {
                    if let param = child.value as? T {
                        result = param
                    } else {
                        parameters.setValue(result, forKey: child.label!)
                    }
                }
            }
        }
        
        return result
    }
    
    func setup<T: Parameter>(parameter: T?,
                             function: [String: AnyObject]?,
                             correction: [String: Any]? = nil) {
        guard let parameter = parameter else {
            return;
        }
        
        guard var function = function else {
            return;
        }
        
        if let correction = correction {
            for key in correction.keys {
                function[key] = correction[key] as AnyObject
            }
        }
        
        parameter.limits = parameter.limits ?? ParameterLimit()
        
        var value: Float = parameter.value
        var min: Float = parameter.limits!.min
        var max: Float = parameter.limits!.max
        var step: Float = parameter.limits!.step
        var multiplier: Float = parameter.limits!.multiplier
        var localizedArray: [String]? = parameter.limits!.localizedValues.map({ $0 })
        
        if let number = function["value"] as? NSNumber {
            value = number.floatValue
        } else if let number = function["value"] as? String, let floatNumber = Float(number) {
            value = floatNumber
        }
        
        if let number = function["min"] as? NSNumber {
            min = number.floatValue
        } else if let number = function["min"] as? String, let floatNumber = Float(number) {
            min = floatNumber
        }
        
        if let number = function["max"] as? NSNumber {
            max = number.floatValue
        } else if let number = function["max"] as? String, let floatNumber = Float(number) {
            max = floatNumber
        }
        
        if let number = function["step"] as? NSNumber {
            step = number.floatValue
        } else if let number = function["step"] as? String, let floatNumber = Float(number) {
            step = floatNumber
        }
        
        if let number = function["multiplier"] as? NSNumber {
            multiplier = number.floatValue
        } else if let number = function["multiplier"] as? String, let floatNumber = Float(number) {
            multiplier = floatNumber
        }
        
        if let valuesArray = function["values"] as? [String] {
            max = Float( valuesArray.count - 1 )
            localizedArray = valuesArray
        } else if let intValuesArray = function["values"] as? [Int] {
            max = Float( intValuesArray.count - 1 )
            localizedArray = intValuesArray.map {
                return String($0)
            }
        }
        
        parameter.value = value
        parameter.limits?.max = max
        parameter.limits?.min = min
        parameter.limits?.step = step
        parameter.limits?.multiplier = multiplier
        if let array = localizedArray {
            let setupValues = setupLocalizedValues(parameter: parameter, values: array)
            parameter.limits?.localizedValues.removeAll()
            parameter.limits?.localizedValues.append(objectsIn: setupValues)
        }
    }
    
    private func setupLocalizedValues<T: Parameter>(parameter: T, values: [String]?) -> [String] {
        guard let values = values, values.count > 0 else {
            return []
        }
        
        var ordered: [String] = []
        
        switch parameter {
        case is ColorsPalette: ordered = Palette.ordered
        case is Recognition: ordered = RecognitionEnum.ordered
        case is ReticleColor: ordered = ReticleColor18.ordered
        case is ReticleType: ordered = ReticleType18.ordered
        case is IR: ordered = IREnum.ordered
        case is DateTime: ordered = DeviceTime.ordered
        case is Language: ordered = DeviceLanguage.ordered
        case is Units: ordered = DeviceUnits.ordered
        case is Calibration: ordered = ShutterMode.ordered
        case is Preset: ordered = Profile.ordered
        case is AutoOff: ordered = DeviceAutoOff.ordered
            
        default: return []
        }
        
        return values.map {
            return setupLocalizedValue(value: $0, values: ordered)
        }
    }
    
    func setupLocalizedValue(value: String, values: [String]) -> String {
        guard let intValue = Int(value), intValue < values.count else { return value }
        return values[intValue]
    }
    
    private class func getDevicesFromBundle() -> Array<Dictionary<String,AnyObject>>? {
        guard let pathToPlist =  Bundle.main.path(forResource: "DevicesInfo", ofType: "plist") else {
            return nil
        }
        guard let devicePlistDictionary = NSDictionary(contentsOfFile: pathToPlist) else {
            return nil
        }
        guard let devices = devicePlistDictionary["Devices"] as? Array<Dictionary<String,AnyObject>> else {
            return nil
        }
        
        return devices
    }
    
    func getImageName(by sku: String) -> String? {
        let deviceInfoDict = getDeviceContentInfoBySku(sku)
        guard let deviceInfoContent = deviceInfoDict["details"] as? [[String:String]] else {
            return deviceInfoDict["deviceImage"] as? String
        }
        
        return deviceInfoContent.compactMap ({ return $0["deviceImage"] }).last ?? deviceInfoDict["deviceImage"] as? String
    }
    
    private func getDeviceContentInfoBySku(_ sku: String) -> Dictionary<String, AnyObject> {
        guard let devices = getDevicesFromBundle() else {
            return getDeviceContentInfoBySku("78093")
        }
        
        var device: Dictionary<String, AnyObject> = [:]
        devices.forEach() { groupedDeivces in
            let deivcesDetails = groupedDeivces["details"] as! Array< Dictionary<String, AnyObject> >
            
            let deviceIndex = deivcesDetails.firstIndex() { deviceDetails in
                let _sku = deviceDetails["sku"] as! String
                return _sku == sku
            }
            
            if let index = deviceIndex {
                device = groupedDeivces
                device["details"] = [deivcesDetails[index]] as AnyObject
            }
        }
        return device
    }
    
    private func getNameBySku(_ sku: String) -> String {
        guard let devices = getDevicesFromBundle() else {
            return "Unowned"
        }
        
        var name: String?
        devices.forEach() { groupedDeivces in
            let deivcesDetails = groupedDeivces["details"] as! Array< Dictionary<String, AnyObject> >
            
            let deviceIndex = deivcesDetails.firstIndex() { deviceDetails in
                let _sku = deviceDetails["sku"] as! String
                return _sku == sku
            }
            
            if let index = deviceIndex {
                name = deivcesDetails[index]["name"] as? String
            }
        }
        return name ?? "Unowned"
    }
    
    private func getSkuByName(_ name: String) -> String? {
        guard let devices = getDevicesFromBundle() else {
            return nil
        }
        
        var sku: String? = nil
        devices.forEach() { groupedDeivces in
            let deivcesDetails = groupedDeivces["details"] as! Array< Dictionary<String, AnyObject> >
            
            let deviceIndex = deivcesDetails.firstIndex() { deviceDetails in
                let _name = deviceDetails["name"] as! String
                return _name == name
            }
            
            if let index = deviceIndex {
                sku = deivcesDetails[index]["sku"] as? String
            }
        }
        return sku
    }
    
    private func getDevicesFromBundle() -> Array<Dictionary<String,AnyObject>>? {
        guard let pathToPlist =  Bundle.main.path(forResource: "DevicesInfo", ofType: "plist") else {
            return nil
        }
        guard let devicePlistDictionary = NSDictionary(contentsOfFile: pathToPlist) else {
            return nil
        }
        guard let devices = devicePlistDictionary["Devices"] as? Array<Dictionary<String,AnyObject>> else {
            return nil
        }
        
        return devices
    }
}

protocol DeviceFunctionsProtocol {
    static var ordered: [String] { get }
}

extension MigrationService {
    enum Palette: String, DeviceFunctionsProtocol {
        case HotWhite = "Viewfinder.Parameters.ColorSchema.HotWhite"
        case HotBlack = "Viewfinder.Parameters.ColorSchema.HotBlack"
        case HotRed = "Viewfinder.Parameters.ColorSchema.HotRed"
        case RedMonochrome = "Viewfinder.Parameters.ColorSchema.RedMonochrome"
        case Rainbow = "Viewfinder.Parameters.ColorSchema.Rainbow"
        case Ultramarine = "Viewfinder.Parameters.ColorSchema.Ultramarine"
        case Violet = "Viewfinder.Parameters.ColorSchema.Violet"
        case Sepia = "Viewfinder.Parameters.ColorSchema.Sepia"
        
        static var ordered: [String] {
            return [Palette.HotWhite,
                    Palette.HotBlack,
                    Palette.HotRed,
                    Palette.RedMonochrome,
                    Palette.Rainbow,
                    Palette.Ultramarine,
                    Palette.Violet,
                    Palette.Sepia].map() { return $0.rawValue }
        }
    }
    
    enum RecognitionEnum: String, DeviceFunctionsProtocol {
        case Rocks = "Viewfinder.Parameters.WorkMode.Rocks"
        case Forest = "Viewfinder.Parameters.WorkMode.Forest"
        case Identification = "Viewfinder.Parameters.WorkMode.Identification"
        case Custom = "Viewfinder.Parameters.WorkMode.Custom"
        
        static var ordered: [String] {
            return [RecognitionEnum.Rocks,
                    RecognitionEnum.Forest,
                    RecognitionEnum.Identification,
                    RecognitionEnum.Custom
                ].map() { return $0.rawValue }
        }
    }
    
    enum MarkerColor: String, DeviceFunctionsProtocol {
        case WhiteBlack = "Viewfinder.Parameters.MarkerColor.WhiteBlack"
        case BlackWhite = "Viewfinder.Parameters.MarkerColor.BlackWhite"
        case White = "Viewfinder.Parameters.MarkerColor.White"
        case Black = "Viewfinder.Parameters.MarkerColor.Black"
        case Red = "Viewfinder.Parameters.MarkerColor.Red"
        case Green = "Viewfinder.Parameters.MarkerColor.Green"
        
        static var ordered: [String] {
            return [MarkerColor.WhiteBlack,
                    MarkerColor.BlackWhite,
                    MarkerColor.White,
                    MarkerColor.Black,
                    MarkerColor.Red,
                    MarkerColor.Green
                ].map() { return $0.rawValue }
        }
    }
    
    enum ReticleType18: String, DeviceFunctionsProtocol {
        case Reticle0 = "Viewfinder.Parameters.MarkerType.New.0"
        case Reticle1 = "Viewfinder.Parameters.MarkerType.New.1"
        case Reticle2 = "Viewfinder.Parameters.MarkerType.New.2"
        case Reticle3 = "Viewfinder.Parameters.MarkerType.New.3"
        case Reticle4 = "Viewfinder.Parameters.MarkerType.New.4"
        case Reticle5 = "Viewfinder.Parameters.MarkerType.New.5"
        case Reticle6 = "Viewfinder.Parameters.MarkerType.New.6"
        case Reticle7 = "Viewfinder.Parameters.MarkerType.New.7"
        case Reticle8 = "Viewfinder.Parameters.MarkerType.New.8"
        case Reticle9 = "Viewfinder.Parameters.MarkerType.New.9"
        
        static var ordered: [String] {
            return [ReticleType18.Reticle0,
                    ReticleType18.Reticle1,
                    ReticleType18.Reticle2,
                    ReticleType18.Reticle3,
                    ReticleType18.Reticle4,
                    ReticleType18.Reticle5,
                    ReticleType18.Reticle6,
                    ReticleType18.Reticle7,
                    ReticleType18.Reticle8,
                    ReticleType18.Reticle9
                ].map() { return $0.rawValue }
        }
    }
    
    enum ReticleColor18: String, DeviceFunctionsProtocol {
        case BlackRed = "Viewfinder.Parameters.MarkerColor.BlackRed"
        case WhiteRed = "Viewfinder.Parameters.MarkerColor.WhiteRed"
        case BlackGreen = "Viewfinder.Parameters.MarkerColor.BlackGreen"
        case WhiteGreen = "Viewfinder.Parameters.MarkerColor.WhiteGreen"
        case Red = "Viewfinder.Parameters.MarkerColor.Red"
        case Green = "Viewfinder.Parameters.MarkerColor.Green"
        case Yellow = "Viewfinder.Parameters.MarkerColor.Yellow"
        case Blue = "Viewfinder.Parameters.MarkerColor.Blue"
        case Orange = "Viewfinder.Parameters.MarkerColor.Orange"
        case BlackWhite = "Viewfinder.Parameters.MarkerColor.BlackWhite"
        case WhiteBlack = "Viewfinder.Parameters.MarkerColor.WhiteBlack"
        
        static var ordered: [String] {
            return [ReticleColor18.BlackRed,
                    ReticleColor18.WhiteRed,
                    ReticleColor18.BlackGreen,
                    ReticleColor18.WhiteGreen,
                    ReticleColor18.Red,
                    ReticleColor18.Green,
                    ReticleColor18.Yellow,
                    ReticleColor18.Blue,
                    ReticleColor18.Orange,
                    ReticleColor18.BlackWhite,
                    ReticleColor18.WhiteBlack
                ].map() { return $0.rawValue }
        }
    }
    
    enum IREnum: String, DeviceFunctionsProtocol {
        case Off = "Viewfinder.Parameters.IR.Off"
        case Min = "Viewfinder.Parameters.IR.Min"
        case Mid = "Viewfinder.Parameters.IR.Mid"
        case Max = "Viewfinder.Parameters.IR.Max"
        
        static var ordered: [String] {
            return [IREnum.Off,
                    IREnum.Min,
                    IREnum.Mid,
                    IREnum.Max].map() { return $0.rawValue }
        }
    }
    
    enum DeviceTime: String, DeviceFunctionsProtocol {
        case Manual = "Settings.DateTime.SetManually"
        case iPhone = "Settings.DateTime.PhoneTime"
        
        static var ordered: [String] {
            return [
                DeviceTime.Manual,
                DeviceTime.iPhone
                ].map() { return $0.rawValue }
        }
    }
    
    enum DeviceLanguage: String, DeviceFunctionsProtocol {
        case English = "Settings.Language.English"
        case German = "Settings.Language.Deutsch"
        case Spanish = "Settings.Language.Español"
        case French = "Settings.Language.French"
        case Russian = "Settings.Language.Russian"
        
        static var ordered: [String] {
            return [DeviceLanguage.English,
                    DeviceLanguage.German,
                    DeviceLanguage.Spanish,
                    DeviceLanguage.French,
                    DeviceLanguage.Russian
                ].map() { return $0.rawValue }
        }
    }
    
    enum DeviceUnits: String, DeviceFunctionsProtocol {
        case Metrs = "Settings.Units.Meters"
        case Yards = "Settings.Units.Yards"
        
        static var ordered: [String] {
            return [DeviceUnits.Metrs,
                    DeviceUnits.Yards].map() { return $0.rawValue }
        }
    }
    
    enum ShutterMode: String, DeviceFunctionsProtocol {
        case Manual = "Settings.ShutterMode.Manual"
        case Hybrid = "Settings.ShutterMode.SemiAutomatic"
        case Auto = "Settings.ShutterMode.Auto"
        
        static var ordered: [String] {
            return [ShutterMode.Manual,
                    ShutterMode.Hybrid,
                    ShutterMode.Auto
                ].map() { return $0.rawValue }
        }
    }
    
    enum DeviceAutoOff: String, DeviceFunctionsProtocol {
        case min1 = "Settings.AutoPowerOff.OneMinute"
        case min3 = "Settings.AutoPowerOff.ThreeMinutes"
        case min5 = "Settings.AutoPowerOff.FiveMinutes"
        case off = "Settings.AutoPowerOff.Off"
        
        static var ordered: [String] {
            return [
                DeviceAutoOff.min1,
                DeviceAutoOff.min3,
                DeviceAutoOff.min5,
                DeviceAutoOff.off
                ].map() { return $0.rawValue }
        }
    }
    
    enum Profile: String, DeviceFunctionsProtocol {
        case A = "Viewfinder.Parameters.Preset.A"
        case B = "Viewfinder.Parameters.Preset.B"
        case C = "Viewfinder.Parameters.Preset.C"
        case D = "Viewfinder.Parameters.Preset.D"
        case E = "Viewfinder.Parameters.Preset.E"
        
        static var ordered: [String] {
            return [
                Profile.A,
                Profile.B,
                Profile.C,
                Profile.D,
                Profile.E
                ].map() { return $0.rawValue }
        }
    }
}
