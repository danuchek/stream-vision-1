//
//  VirtualParameters.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/28/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift

class Parameter: Object, ParameterProtocol {
    typealias UpdateCompletion = ((Bool) -> ())?
    
    var command: String {
        SocketManager.connectedDevice?.templateParameters.api.type.command(for: self) ?? "None"
    }
    @objc dynamic var value: Float = 0.0
    @objc dynamic var limits: ParameterLimit! = ParameterLimit()
    
    class func empty() -> Self {
        return self.init()
    }
    
    fileprivate func directUpdate(value: Float) {
        self.value = value
    }

    fileprivate func directUpdate(value: Int) {
        self.value = Float(value)
    }

    fileprivate func directUpdate(value: Bool) {
        self.value = NSNumber(booleanLiteral: value).floatValue
    }

    fileprivate func directUpdate(value: Double) {
        self.value = NSNumber(value: value).floatValue
    }
    
    func update(value: Float, completion: UpdateCompletion = nil) {
        SocketManager.send("set" + command,
                           params: ["newValue" : "\( Int(value) )" ]).response(handler(value: value,
                                                                                       completion: completion))
    }
    
    // TODO: Return result with error for inform user
    func handler(value: Float,
                 completion: UpdateCompletion) -> SocketResponse.SocketResponseCallback {
        return { _, error in
            if error == nil {
                let realm = try! Realm()
                try? realm.write {
                    self.value = value
                }
                DispatchQueue.main.async {
                    completion?(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion?(false)
                }
            }
        }
    }
    
    override class func ignoredProperties() -> [String] {
        ["value"]
    }
}

//// MARK: Operator for compare parameters
infix operator <>

extension VirtualParameters {
    
    func validate() -> Bool {
        return Mirror(reflecting: self).children.reduce(true, {
            if let param = $1.value as? Parameter {
                if param.value > param.limits.max {
                    Logging.log(message: "Invalid:\(param) | Current value:\(param.value) | Max:\(param.limits.max)")
                }
                return $0 && param.value <= param.limits.max
            }
            return $0
        })
        
    }

    var hashValues: String {
        Logging.log(message: "[DeviceParameters] - Get Hash Values: \(self)", toFabric: true)
        let mirror = Mirror(reflecting: self)

        Logging.log(message: "[DeviceParameters] - Mirror: \(mirror)", toFabric: true)

        let map = mirror.children.reduce("Result:", {
            if let param = $1.value as? Parameter {
                return $0 + "\(param.value)"
            }
            return $0
        })

        return map
    }

    var hashArray: [String] {
        let map = Mirror(reflecting: self).children.compactMap({ child -> String? in
            if let _ = child.value as? Parameter,
                child.label != "stream",
                child.label != "dateTime",
                child.label != "format",
                child.label != "shutter",
                child.label != "overlay",
                child.label != "live"
                {
                return child.label
            }
            return nil
        })
        return map
    }
//
//    func hash(into hasher: inout Hasher) {
//        hasher.combine(hashArray)
//    }

    static func == (lhs: VirtualParameters, rhs: VirtualParameters) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    static func <> (lhs: VirtualParameters, rhs: VirtualParameters) -> Bool {
        let rhsHashArray = rhs.hashArray
        let count = lhs.hashArray.reduce(0) { l, r in
            if rhsHashArray.contains(r) {
                return l + 1
            } else {
                Logging.log(message: "Parameter missed: \(r)")
                return l
            }
        }
        return count >= lhs.hashArray.count
    }
}

extension VirtualParameters {
    private func getValue(_ value: AnyObject?) -> Float? {
        if let value = value as? Int {
            return Float(value)
        }
        if let value = value as? Float {
            return value
        }
        if let value = value as? String {
            return Float(value)
        }
        return nil
    }
    
    private func getValue(_ value: AnyObject?) -> Int? {
        if let value: Float = getValue(value) {
            return Int(value)
        }
        return nil
    }
    
    func update(_ rawParametersData: Data) -> [String: AnyObject]? {
        let object = try? JSONSerialization.jsonObject(with: rawParametersData,
                                                       options: .mutableContainers)
        let parametersObjectDictionary = object as? [String: [String: AnyObject]]
        let deviceParameters = parametersObjectDictionary?["Parameters"]
        
        
        if let param: Float = getValue(deviceParameters?["Brightness"]) {
            brightness?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["Contrast"]) {
            contrast?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["RecTime"]) {
            recordTime?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["Zoom"]) {
            var value = Float(param)
            if let limits = zoom?.limits {
                value = Zoom.viseVersaCalculate(with: value, limits: limits)
            }
            zoom?.directUpdate(value: value)
        }
        
        if let param: Float = getValue(deviceParameters?["IR"]) {
            ir?.directUpdate(value: param)
        }
        
        if let param = deviceParameters?["Battery"] {
            if let param = param as? Int {
                battery?.directUpdate(value: param)
            } else if let param = param as? String, let value = Int(param) {
                battery?.directUpdate(value: value)
            }
        }
        
        if let param: Float = getValue(deviceParameters?["LCD"]) {
            displayOff?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["GunAngle"]) {
            blockage?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["RecMode"]) {
            captureMode?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["RecStatus"]) {
            record?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["IRStatus"]) {
            irStatus?.directUpdate(value: param)
        }
        
        if let param = deviceParameters?["DateTime"] as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "en_GB")
            
            if let date = dateFormatter.date(from: param) {
                dateTime?.directUpdate(value: date.timeIntervalSince1970)
            }
        }
        
        if let param: Float = getValue(deviceParameters?["AccessLevel"]) {
            access?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["TimeFormat"]) {
            timeFormat?.directUpdate(value: param)
        }
        
        // 640
        
        if let param = deviceParameters?["Profile"] as? [String: AnyObject] {
            if let index: Int = getValue(param["Index"]),
                let selected: Int = getValue(param["SelectedDistance"]),
                let distances: [Int] = param["Distances"] as? [Int] {
                
                self.distances?.selectedDistance = selected
                self.distances?.distances = distances
                self.distances?.index = index
                
                preset?.directUpdate(value: index)
            }
        }
        
        if let param: Float = getValue(deviceParameters?["AutoOff"]) {
            autoOff?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["Mode"]) {
            recognition?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["ReticleColor"]) {
            reticleColor?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["ReticleBrightness"]) {
            reticleBrightness?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["ReticleType"]) {
            reticleType?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["Palette"]) {
            colorsPalette?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["PiP"]) {
            pip?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["Language"]) {
            language?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["Units"]) {
            units?.directUpdate(value: param)
        }
        
        if let param: Float = getValue(deviceParameters?["ModeShutter"]) {
            calibration?.directUpdate(value: param)
        }
        
        if let param = deviceParameters?["ExtDC"] {
            if let param = param as? Int {
                extDc?.directUpdate(value: param)
            } else if let param = param as? String, let value = Int(param) {
                extDc?.directUpdate(value: value)
            }
        }
        
        if deviceParameters == nil {
            Logging.log(message: "[\(type(of: self))] - Fail parsing device vision parameters")
        }
        return deviceParameters
    }
}

extension VirtualParameters {
    func update(with template: TemplateParameters) {
        Mirror(reflecting: template).children.forEach {
            let value = template.value(forKey: $0.label!)
            if value != nil {
                setValue(value, forKey: $0.label!)
            }
        }
        api = template.api
    }
}

class VirtualParameters: NSObject, VirtualParametersProtocol {
    
    @objc private var apiStored: Int = 0
    var api: ApiProtocol {
        get {
            ApiProtocol(rawValue: apiStored) ?? .new
        }
        set {
            apiStored = newValue.rawValue
        }
    }
    
    // Viewfinder
    @objc var zoom: Zoom?
    @objc var brightness: Brightness?
    @objc var captureMode: CaptureMode?
    @objc var recordTime : RecordTime?
    @objc var ir: IR?
    @objc var irStatus: IRStatus?
    @objc var pip: PiP?
    @objc var displayOff: DisplayOff?
    @objc var recognition: Recognition?
    @objc var colorsPalette: ColorsPalette?
    @objc var reticleColor: ReticleColor?
    @objc var reticleBrightness: ReticleBrightness?
    @objc var contrast: Contrast?
    @objc var distances: Distance?
    @objc var reticleType: ReticleType?
    @objc var shutter: Shutter?
    
    // Pre initialized
    @objc var record : Record!
    @objc var access: Guest!
    @objc var battery: Battery!
    @objc var extDc: ExtDc!
    @objc var visual: VisualParameter!

    // Settings
    @objc var units: Units?
    @objc var autoOff: AutoOff?
    @objc var preset: Preset?
    @objc var format: Format?
    @objc var timeFormat: TimeFormat?
    @objc var blockage: Blockage?
    @objc var dateTime: DateTime?
    @objc var language: Language?
    @objc var calibration: Calibration?

    // Basic
    @objc var overlay: Overlay?
    @objc var stream: Stream?
    @objc var live: Live?
    
    @objc var motionAccuracy: MotionAccuracy!
    @objc var motionActive: MotionActive!
}

extension VirtualParameters {
    typealias Constraints =  [ String : [String: [Parameter?]] ]
    
    enum DeviceBrand {
        case Digiforce
        case Digisight
        case Oracle
        case Helion
        case Helion18
        case Lexion
        case Forward
        case Forward18
        case Sightline
        case Signal
        case Accolade
        case TrailLRF
        case Trail
        case Trail18
        case Axion
        case Digisight18
        case Digex
        case Thermion
        case Demo
        case None
    }
    
    /*
     - constraints: We should use formula for setuping viewfinder. You can add unlimited count parameters for priority array.
     */
    var constraints: Constraints {
        let priority: [[Parameter?]] = [[captureMode], [zoom, ir], [brightness], [ir, zoom], [contrast], [recognition], [colorsPalette], [reticleColor], [reticleBrightness], [reticleType], [distances]]
        var right0: [Parameter?] = []
        var right1: [Parameter?] = []
        
        for parameter in priority {
            let param = parameter.compactMap({ $0 }).first
            if right0.count < 3 {
                if let parameter = param {
                    right0.append(parameter)
                }
            } else {
                if let parameter = param {
                    if parameter is IR, right0.filter({ $0 is IR }).count > 0 {
                        continue
                    } else if parameter is Zoom, right0.filter({ $0 is Zoom }).count > 0 {
                        continue
                    } else {
                        right1.append(parameter)
                    }
                }
            }
        }
        return [ "right" :
            ["level0": right0,
             "level1": right1 ],
                 "left" :
                    ["level0": [displayOff, shutter] ] ]
    }
}
