//
//  VirtualDevice.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/11/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift

extension VirtualDevice {
    var hasDownloadedFirmware: Bool {
        get {
            return FilesManager.shared.firmwareExistFromDevice(self)
        }
    }
    var hasNewestFirmware: Bool {
        get {
            guard let _ = firmware.version else {
                return false
            }
            return firmware.version > info.version
        }
    }
    
    static func ==(lhs: VirtualDevice, rhs: VirtualDevice) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func !=(lhs: VirtualDevice, rhs: VirtualDevice) -> Bool {
        return lhs.id != rhs.id
    }
    
    override public func isEqual(_ object: Any?) -> Bool {
        return self.id == (object as? VirtualDevice)?.id
    }
    
    override public var hash: Int {
        return id.hashValue
    }
}

extension VirtualDevice {
    class func from(template: VirtualDeviceTemplate) -> VirtualDevice {
        let device = VirtualDevice()
        device.info = VirtualDeviceInfo(value: template.info as Any)
        device.firmware = VirtuaFirmwareInfo(value: template.firmware as Any)
        device.templateParameters = TemplateParameters(value: template.templateParameters as Any)
        device.parameters.update(with: template.templateParameters)
        device.id = template.deviceString
        return device
    }
    
    func update(with template: VirtualDeviceTemplate) {
        let realm = try! Realm()
        try? realm.write {
            info.imagePath = template.info.imagePath
            info.serverTimeStamp = template.info.serverTimeStamp
            info.name = template.info.name
            info.specification = template.info.specification
        }
    }
    
    func update(with device: VirtualDevice) {
            info.version = FirmwareVersion(value: device.info.version)
            info.hw = device.info.hw
            info.serial = device.info.serial
            info.invalidSku = device.info.invalidSku
            parameters = device.parameters
            firmware = VirtuaFirmwareInfo(value: device.firmware as Any)
    }
}

class VirtualDevice: VirtualDeviceTemplate {
    @objc dynamic var connectionDate: Date = Date()
    @objc dynamic var disconnectionDate: Date = Date()
    var parameters: VirtualParameters! = VirtualParameters()
    
    required init(_ data: Data) {
        super.init()
        
        guard let response = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers),
            let convertedResponse = response as? [String: AnyObject],
            let sku = convertedResponse["sku"] as? String,
            let firmware = convertedResponse["softwareVersion"] as? String,
            let hardware = convertedResponse["hardwareVersion"] as? String
        else { return; }
        
        var deviceParameters = convertedResponse["Parameters"] as? [String: AnyObject]
        deviceParameters?["magnification"] = convertedResponse["magnification"]
        if let commandsArray = convertedResponse["Commands"] as? [String] {
            for command in commandsArray {
                deviceParameters?[command] = true as AnyObject
            }
        }
        
        // Safe check if serial number has incorrect type
        if let serial = convertedResponse["serialNumber"] as? Int {
            info.serial = "\(serial)"
        } else if let serial = convertedResponse["serialNumber"] as? String {
            info.serial = serial
        } else {
            info.serial = "000000"
        }
        
        info.sku = sku
        info.hw = hardware
        connectionDate = Date()
        
        let version = FirmwareVersion.instanceFrom(firmware)
        info.version = version
        
        self.firmware.version = version
        id = deviceString
    }
    
    required init() {
        super.init()
    }
    
    override class func ignoredProperties() -> [String] {
        var result = ["deviceString", "parameters"]
        result.append(contentsOf: super.ignoredProperties())
        return result
    }
    
}
