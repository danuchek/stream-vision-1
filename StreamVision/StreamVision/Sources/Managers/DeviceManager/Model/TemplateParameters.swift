//
//  DeviceParametersModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/20/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift

enum DeviceApi {
    case zoom
    case distances
    case recognition
    case marker
    case markerColor
    case markerBrightness
    case pip
    case ir
    case lcd
    case extDC
    case contrast
    case brightness
    case colorsPallet
    case capture
    case shutter
    case irStatus
    case units
    case autoOff
    case profile
    case format
    case dateFormat
    case language
    case blockage
    case calibration
    case time
    case date
    
    // App settings
    case stream
    case overlay
    
    func raw(_ value: Self, commands: DeviceApiProtocol.Type) -> String {
        switch value {
        case .zoom: return commands.zoom
        case .distances: return commands.distances
        case .recognition: return commands.recognition
        case .marker: return commands.marker
        case .markerColor: return commands.markerColor
        case .markerBrightness: return commands.markerBrightness
        case .pip: return commands.pip
        case .ir: return commands.ir
        case .lcd: return commands.lcd
        case .extDC: return commands.extDC
        case .contrast: return commands.contrast
        case .brightness: return commands.brightness
        case .colorsPallet: return commands.colorsPallet
        case .capture: return commands.capture
        case .shutter: return commands.shutter
        case .irStatus: return commands.irStatus
        case .units: return commands.units
        case .autoOff: return commands.autoOff
        case .profile: return commands.profile
        case .format: return commands.format
        case .dateFormat: return commands.dateFormat
        case .language: return commands.language
        case .blockage: return commands.blockage
        case .calibration: return commands.calibration
        case .time: return commands.time
        case .date: return commands.date
        
        // App settings
        case .stream: return commands.stream
        case .overlay: return commands.overlay
        }
    }
}

protocol DeviceApiProtocol {
    static var zoom: String { get }
    static var distances: String { get }
    static var recognition: String { get }
    static var marker: String { get }
    static var markerColor: String { get }
    static var markerBrightness: String { get }
    static var pip: String { get }
    static var ir: String { get }
    static var lcd: String { get }
    static var extDC: String { get }
    static var contrast: String { get }
    static var brightness: String { get }
    static var colorsPallet: String { get }
    static var capture: String { get }
    static var shutter: String { get }
    static var irStatus: String { get }
    static var units: String { get }
    static var autoOff: String { get }
    static var profile: String { get }
    static var format: String { get }
    static var dateFormat: String { get }
    static var language: String { get }
    static var blockage: String { get }
    static var calibration: String { get }
    static var time: String { get }
    static var date: String { get }
    
    // App settings
    static var stream: String { get }
    static var overlay: String { get }
}

extension DeviceApiProtocol {
    static func update<T: VirtualParametersProtocol>(rawParameters: [String: Any], parameters: inout T?) {
        let service = MigrationService()
        
        for key in rawParameters.keys {
            let parameter = parameterByApiKey(apiKey: key, parameters: &parameters )
            service.setup(parameter: parameter, function: rawParameters[key] as? [String : AnyObject])
        }
    }
    
    static func command(for parameter: Parameter) -> String {
        switch parameter {
        case is Zoom: return Self.zoom
        case is Distance: return Self.distances
        case is Recognition: return Self.recognition
        case is ReticleType:  return Self.marker
        case is ReticleColor: return Self.markerColor
        case is ReticleBrightness: return Self.markerBrightness
        case is PiP: return Self.pip
        case is IR: return Self.ir
        case is DisplayOff:  return Self.lcd
        case is ExtDc: return Self.extDC
        case is Contrast: return Self.contrast
        case is Brightness: return Self.brightness
        case is ColorsPalette: return Self.colorsPallet
        case is CaptureMode: return Self.capture
        case is Shutter: return Self.shutter
        case is IRStatus: return Self.irStatus
        case is Units: return Self.units
        case is AutoOff: return Self.autoOff
        case is Preset: return Self.profile
        case is Format: return Self.format
        case is TimeFormat: return Self.dateFormat
        case is Language: return Self.language
        case is Blockage: return Self.blockage
        case is Calibration: return Self.calibration
        case is DateTime: return Self.date
        case is Stream:  return Self.stream
        case is Overlay:  return Self.overlay
        default:
            return ""
        }
    }
    
    private static func parameterByApiKey<T: VirtualParametersProtocol>(apiKey: String, parameters: inout T?) -> Parameter? {
        let service = MigrationService()
        switch apiKey {
        case Self.zoom: return service.getParameter(by: Zoom.self, parameters: &parameters)
        case Self.distances: return service.getParameter(by: Distance.self, parameters: &parameters)
        case Self.recognition: return service.getParameter(by: Recognition.self, parameters: &parameters)
        case Self.marker: return service.getParameter(by: ReticleType.self, parameters: &parameters)
        case Self.markerColor: return service.getParameter(by: ReticleColor.self, parameters: &parameters)
        case Self.markerBrightness: return service.getParameter(by: ReticleBrightness.self, parameters: &parameters)
        case Self.pip: return service.getParameter(by: PiP.self, parameters: &parameters)
        case Self.ir: return service.getParameter(by: IR.self, parameters: &parameters)
        case Self.lcd: return service.getParameter(by: DisplayOff.self, parameters: &parameters)
        case Self.extDC: return service.getParameter(by: ExtDc.self, parameters: &parameters)
        case Self.contrast: return service.getParameter(by: Contrast.self, parameters: &parameters)
        case Self.brightness: return service.getParameter(by: Brightness.self, parameters: &parameters)
        case Self.colorsPallet: return service.getParameter(by: ColorsPalette.self, parameters: &parameters)
        case Self.capture: return service.getParameter(by: CaptureMode.self, parameters: &parameters)
        case Self.shutter: return service.getParameter(by: Shutter.self, parameters: &parameters)
        case Self.irStatus: return service.getParameter(by: IRStatus.self, parameters: &parameters)
        case Self.units: return service.getParameter(by: Units.self, parameters: &parameters)
        case Self.autoOff: return service.getParameter(by: AutoOff.self, parameters: &parameters)
        case Self.profile: return service.getParameter(by: Preset.self, parameters: &parameters)
        case Self.format: return service.getParameter(by: Format.self, parameters: &parameters)
        case Self.dateFormat: return service.getParameter(by: TimeFormat.self, parameters: &parameters)
        case Self.language: return service.getParameter(by: Language.self, parameters: &parameters)
        case Self.blockage: return service.getParameter(by: Blockage.self, parameters: &parameters)
        case Self.calibration: return service.getParameter(by: Calibration.self, parameters: &parameters)
        case Self.time: return service.getParameter(by: DateTime.self, parameters: &parameters)
        case Self.date: return service.getParameter(by: DateTime.self, parameters: &parameters)
        case Self.stream: return service.getParameter(by: Stream.self, parameters: &parameters)
        case Self.overlay: return service.getParameter(by: Overlay.self, parameters: &parameters)
        default: return nil
        }
    }
}

class DeviceApiOld: DeviceApiProtocol {
    static var zoom = "Zoom"
     static var distances = "Distance"
     static var recognition = "Mode"
     static var marker = "ReticleType"
     static var markerColor = "ReticleColor"
     static var markerBrightness = "ReticleBrightness"
     static var pip = "PiP"
     static var ir = "IR"
     static var lcd = "LCD"
     static var extDC = "ExtDC"
     static var contrast = "Contrast"
     static var brightness = "Brightness"
     static var colorsPallet = "Palette"
     static var capture = "RecMode"
     static var shutter = "shutter"
     static var irStatus = "IRStatus"
     static var units = "Units"
     static var autoOff = "AutoOff"
     static var profile = "Profile"
     static var format = "format"
     static var dateFormat = "TimeFormat"
     static var language = "Language"
     static var blockage = "GunAngle"
     static var calibration = "ModeShutter"
     static var battery = "Battery"
     static var time = "DateTime"
     static var date = "DateTime"
    
    // App settings
    static var stream = "stream"
    static var overlay = "overlay"
}

class DeviceApiNew: DeviceApiProtocol {
    static var zoom = "Zoom"
    static var distances = "Distance"
    static var recognition = "recognition"
    static var marker = "ReticleType"
    static var markerColor = "ReticleColor"
    static var markerBrightness = "ReticleBrightness"
    static var pip = "PiP"
    static var ir = "IR"
    static var lcd = "LCD"
    static var extDC = "ExtDC"
    static var contrast = "Contrast"
    static var brightness = "Brightness"
    static var colorsPallet = "Palette"
    static var capture = "RecMode"
    static var shutter = "shutter"
    static var irStatus = "IRStatus"
    static var units = "Units"
    static var autoOff = "AutoOff"
    static var profile = "Profile"
    static var format = "format"
    static var dateFormat = "TimeFormat"
    static var language = "Language"
    static var blockage = "GunAngle"
    static var calibration = "ModeShutter"
    static var battery = "Battery"
    static var time = "DateTime"
    static var date = "DateTime"
    
    // App settings
    static var stream = "stream"
    static var overlay = "overlay"
}

class ParameterLimitZoom: ParameterLimit {
    override var max: Float {
        get {
            min * multiplier
        }
        set {
            super.max = newValue
        }
    }
}

class ParameterLimit: Object {
    @objc dynamic var max: Float = 1.0
    @objc dynamic var min: Float = 0.0
    @objc dynamic var step: Float = 1.0
    @objc dynamic var multiplier: Float = 1.0
    let localizedValues: List<String> = List<String>()
}

protocol ParameterProtocol {
    static func empty() -> Self
}

extension Parameter {
    func update<T: Parameter>(with parameter: T) {
        Mirror(reflecting: parameter).children.forEach {
            setValue($0.value, forKey: $0.label!)
        }
        Mirror(reflecting: parameter).superclassMirror?.children.forEach {
            setValue($0.value, forKey: $0.label!)
        }
    }
}

enum ApiProtocol: Int {
    case old = 0
    case new = 1
    
    var type: DeviceApiProtocol.Type {
        switch self {
        case .old: return DeviceApiOld.self
        default: return DeviceApiNew.self
        }
    }
}

extension TemplateParameters {
    var count: Int {
        return Mirror(reflecting: self).children.reduce(0) { (result, arg1) -> Int in
            return value(forKey: arg1.label!) != nil ? result + 1 : result
        }
    }
}

protocol VirtualParametersProtocol: NSObject {

    var api: ApiProtocol { get set }
    
    // Viewfinder
    var zoom: Zoom? { get set }
    var brightness: Brightness? { get set }
    var captureMode: CaptureMode? { get set }
    var recordTime : RecordTime? { get set }
    var ir: IR? { get set }
    var irStatus: IRStatus? { get set }
    var pip: PiP? { get set }
    var displayOff: DisplayOff? { get set }
    var recognition: Recognition? { get set }
    var colorsPalette: ColorsPalette? { get set }
    var reticleColor: ReticleColor? { get set }
    var reticleBrightness: ReticleBrightness? { get set }
    var contrast: Contrast? { get set }
    var distances: Distance? { get set }
    var reticleType: ReticleType? { get set }
    var shutter: Shutter? { get set }
    
    // Pre initialized
    var record : Record! { get set }
    var access: Guest! { get set }
    var battery: Battery! { get set }
    var extDc: ExtDc! { get set }
    var visual: VisualParameter! { get set }

    // Settings
    var units: Units? { get set }
    var autoOff: AutoOff? { get set }
    var preset: Preset? { get set }
    var format: Format? { get set }
    var timeFormat: TimeFormat? { get set }
    var blockage: Blockage? { get set }
    var dateTime: DateTime? { get set }
    var language: Language? { get set }
    var calibration: Calibration? { get set }

    // Basic
    var overlay: Overlay? { get set }
    var stream: Stream? { get set }
    var live: Live? { get set }
    
    var motionAccuracy: MotionAccuracy! { get set }
    var motionActive: MotionActive! { get set }
}

class TemplateParameters: Object, VirtualParametersProtocol {
    @objc private dynamic var apiStored: Int = 0
    var api: ApiProtocol {
        get {
            ApiProtocol(rawValue: apiStored) ?? .new
        }
        set {
            apiStored = newValue.rawValue
        }
    }
    
    // Viewfinder
    @objc dynamic var zoom: Zoom?
    @objc dynamic var brightness: Brightness?
    @objc dynamic var captureMode: CaptureMode?
    @objc dynamic var recordTime : RecordTime?
    @objc dynamic var ir: IR?
    @objc dynamic var irStatus: IRStatus?
    @objc dynamic var pip: PiP?
    @objc dynamic var displayOff: DisplayOff?
    @objc dynamic var recognition: Recognition?
    @objc dynamic var colorsPalette: ColorsPalette?
    @objc dynamic var reticleColor: ReticleColor?
    @objc dynamic var reticleBrightness: ReticleBrightness?
    @objc dynamic var contrast: Contrast?
    @objc dynamic var distances: Distance?
    @objc dynamic var reticleType: ReticleType?
    @objc dynamic var shutter: Shutter?
    
    // Pre initialized
    @objc dynamic var record : Record! = Record()
    @objc dynamic var access: Guest! = Guest()
    @objc dynamic var battery: Battery! = Battery()
    @objc dynamic var extDc: ExtDc! = ExtDc()
    @objc dynamic var visual: VisualParameter! = VisualParameter()

    // Settings
    @objc dynamic var units: Units?
    @objc dynamic var autoOff: AutoOff?
    @objc dynamic var preset: Preset?
    @objc dynamic var format: Format?
    @objc dynamic var timeFormat: TimeFormat?
    @objc dynamic var blockage: Blockage?
    @objc dynamic var dateTime: DateTime?
    @objc dynamic var language: Language?
    @objc dynamic var calibration: Calibration?

    // Basic
    @objc dynamic var overlay: Overlay?
    @objc dynamic var stream: Stream?
    @objc dynamic var live: Live?
    
    @objc dynamic var motionAccuracy: MotionAccuracy! = MotionAccuracy()
    @objc dynamic var motionActive: MotionActive! = MotionActive()
}
