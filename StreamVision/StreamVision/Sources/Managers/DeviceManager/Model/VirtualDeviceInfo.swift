//
//  VirtualDeviceInfo.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/7/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift
import Localize_Swift
import FileKit

fileprivate extension VirtualDeviceInfo {
    func getBrand(by sku: String) -> DeviceBrand {
        switch sku.first! {
        case "1": return .newton
        case "2": return .yukon
        case "7", "0": return .pulsar
        default:
            if sku.contains("SM") {
                return .sightmark
            } else {
                return .yukon
            }
        }
    }
}

enum DeviceBrand: Int {
    case newton = 0
    case yukon = 1
    case pulsar = 2
    case sightmark = 3
    
    func image() -> UIImage? {
        switch self {
        case .newton: return UIImage(named: "logo_newton")
        case .yukon: return UIImage(named: "logo_yukon")
        case .pulsar: return UIImage(named: "logo_pulsar")
        case .sightmark: return UIImage(named: "logo_sightmark")
        }
    }
}

class Translation: Object {
    @objc dynamic var locale: String = ""
    @objc dynamic var translation: String = ""
}

extension Specification {
    fileprivate func getTranslation(by localeCode: String) -> String? {
        return translations.filter ({ $0.locale == localeCode }).last?.translation
    }
}

class Specification: Object {
    var current: String {
        return getTranslation(by: Localize.currentLanguage()) ?? getTranslation(by: "en") ?? ""
    }
    
    let translations: List<Translation> = List<Translation>()
    
    class func from(description: Description) -> Specification {
        let specification = Specification()
        description.translations.forEach { (netTranslation) in
            let localTranslation = Translation()
            localTranslation.locale = netTranslation.locale
            localTranslation.translation = netTranslation.translation
            specification.translations.append(localTranslation)
        }
        return specification
    }
    
    override class func ignoredProperties() -> [String] {
        ["current"]
    }
}

extension VirtualDeviceInfo {
    var image: UIImage? {
        guard imagePath != "" else {
            return nil
        }
        
        if let image = UIImage(named: imagePath) {
            return image
        } else {
            return UIImage(contentsOfFile: (Path.userLibrary + imagePath).rawValue )
        }
    }
}

/*
* Contains all device information
- invalidSku: use for store correct SKU because device with invalid sku should be created with 0 SKU
 but after device will be fetched from server we should swap the invalid sku with real SKU

NOTE: You should use actual serverTimeStamp which was in release application because user may not receive some devices
*/
class VirtualDeviceInfo: Object {
    @objc dynamic var hw: String = ""
    @objc dynamic var sku: String = ""
    @objc dynamic var invalidSku: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var serial: String = ""
    @objc dynamic var versionStored: String = ""
    var version: FirmwareVersion {
        get {
            FirmwareVersion.instanceFrom(versionStored)
        }
        set {
            versionStored = newValue.versionString
        }
    }
    @objc dynamic var specification: Specification! = Specification()
    @objc dynamic var imagePath: String = ""
    @objc dynamic var serverTimeStamp: Int = 1577829600
    var brand: DeviceBrand {
        getBrand(by: sku)
    }
    
    override class func ignoredProperties() -> [String] {
        return ["brand", "version", "fps"]
    }
}
