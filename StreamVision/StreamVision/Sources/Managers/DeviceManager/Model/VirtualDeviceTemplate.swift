//
//  VirtualDeviceModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/20/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift

extension VirtualDeviceTemplate {
    var deviceString: String {
        var result: String!
        
        if Thread.isMainThread {
            result = composedDeviceString()
        } else {
            DispatchQueue.main.sync {
                result = composedDeviceString()
            }
        }
        
        return result
    }
    
    private func composedDeviceString() -> String {
        return "\(info.name).\(info.sku).\(info.serial)"
    }
}

extension VirtualDeviceTemplate {
    
    /*
    * Update all required properties for local device.
    */
    @discardableResult
    func update(by networkDevice: NetworkDevice) -> Self {
        info.sku = networkDevice.sku
        info.name = networkDevice.name
        info.specification = Specification.from(description: networkDevice.description)
        info.serverTimeStamp = networkDevice.lastChangedAt
        templateParameters.api = ApiProtocol(rawValue: networkDevice.project.id) ?? .new
        if templateParameters.api == .new {
            templateParameters.stream = Stream()
            templateParameters.stream?.value = 1
        }
        info.imagePath = networkDevice.logo
        return self
    }
}

/*
 * Template contains all properties that uses for creating device model from real connected device
 - templateParameters: template parameters uses only for old devices bpnv and 640 project
 
 NOTE: For feature updates you should add fps to this device template
 */
class VirtualDeviceTemplate: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var info: VirtualDeviceInfo! = VirtualDeviceInfo()
    @objc dynamic var firmware: VirtuaFirmwareInfo! = VirtuaFirmwareInfo()
    @objc dynamic var templateParameters: TemplateParameters! = TemplateParameters()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
