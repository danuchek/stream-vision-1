//
//  VirtualUpdateInfo.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/7/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift

class VirtuaFirmwareInfo: Object {
    @objc dynamic var version: FirmwareVersion! = FirmwareVersion()
    @objc dynamic var lastCheckForUpdates: Date?
    @objc dynamic var sizeInBytes: Int = 0
    @objc dynamic var crc32: String = ""
    @objc dynamic var firmwareDescription: String = ""
}
