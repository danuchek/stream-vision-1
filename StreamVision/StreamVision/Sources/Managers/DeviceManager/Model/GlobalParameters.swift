//
//  Parameters.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/6/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import RealmSwift

class IRStatus: Parameter {}
class Brightness: Parameter {}
class ColorsPalette: Parameter {}
class MotionAccuracy: Parameter {}
class PiP: Parameter {}
class Recognition: Parameter {}
class Language: Parameter  {}
class Record: Parameter {}
class Guest: Parameter {}
class ReticleBrightness: Parameter {}
class Blockage: Parameter {}
class Zoom: Parameter {
    static func calculate(with value: Float, limits: ParameterLimit) -> Float {
        let value = value * limits.min
        
        let newMax = (limits.max / limits.min) * 1000.0
        let newMin: Float = 1000.0
        let oldMax = limits.max
        let oldMin = limits.min
        let oldValue = value
        
        let oldRange = (oldMax - oldMin)
        let newRange = (newMax - newMin)
        let newRangeValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin
        
        return newRangeValue
    }
    
    static func viseVersaCalculate(with value: Float, limits: ParameterLimit) -> Float {
        let value = value
        
        let oldMax = (limits.max / limits.min) * 1000.0
        let oldMin: Float = 1000.0
        let newMax = limits.max
        let newMin = limits.min
        let oldValue = Float(value)
        
        let oldRange = (oldMax - oldMin)
        let newRange = (newMax - newMin)
        let newRangeValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin
        
        return newRangeValue
    }
    
    override func update(value: Float, completion: Parameter.UpdateCompletion = nil) {
        let newValue = Zoom.calculate(with: value, limits: limits)
        let forSetupValue = Zoom.viseVersaCalculate(with: newValue, limits: limits)
        SocketManager.send("set" + command,
                           params: ["newValue" : "\( Int(newValue) )" ]).response(handler(value: forSetupValue, completion: completion))
    }
}
class Calibration: Parameter {}

class Distance: Parameter {
    override var command: String {
        "SelectedDistance"
    }
    
    var distances: [Int] = []
    var index: Int = 0
    var selectedDistance: Int = -1
    
    override class func ignoredProperties() -> [String] {
        ["distances", "index", "selectedDistance"]
    }
    
    override func update(value: Float, completion: UpdateCompletion) {
        super.update(value: Float(distances[Int(value)]), completion: completion)
    }
    
    override func handler(value: Float,
                 completion: UpdateCompletion) -> SocketResponse.SocketResponseCallback {
        return { _, error in
            if error == nil {
                let realm = try! Realm()
                try? realm.write {
                    if let index = self.distances.firstIndex(of: Int(value)) {
                        self.value = Float(index)
                    }
                }
                DispatchQueue.main.async {
                    completion?(true)
                }
            } else {
                DispatchQueue.main.async {
                    completion?(false)
                }
            }
        }
    }
}

class Format: Parameter {}
class TimeFormat: Parameter {}
class IR: Parameter {
    weak var irStatus: IRStatus? {
        return SocketManager.connectedDevice?.parameters.irStatus
    }

    override var value: Float {
        get {
            if limits.step > 0 {
                return super.value / limits.step
            }
            return super.value
        }
        set {
            super.value = newValue
        }
    }
    
    override func update(value: Float, completion: UpdateCompletion) {
        super.update(value: value * limits.step, completion: completion)
    }
}
class Contrast: Parameter {}
class Shutter: Parameter {}
class Live: Parameter {}
class CaptureMode: Parameter {}
class Overlay: Parameter {}
class RecordTime: Parameter {}
class DateTime: Parameter {
    var dateValue: Date {
        return Date(timeIntervalSince1970: TimeInterval(value))
    }
    
    var timeValue: Date {
        return dateValue
    }
}
class AutoOff: Parameter {}
class Units: Parameter {}
class ReticleColor: Parameter {}
class ReticleType: Parameter {}
class MotionActive: Parameter {}
class DisplayOff: Parameter {}
class Battery: Parameter {}
class Preset: Parameter {}
class Stream: Parameter {
    enum StreamType: Int {
        case mjpg = 0
        case h264 = 1
    }
    
    var isH264: Bool {
        return getStreamType() == .h264
    }
    
    func getStreamType() -> StreamType? {
        return StreamType(rawValue: Int(value))
    }
}
class ExtDc: Parameter {}

// TODO: Remove this hack
class VisualParameter: Parameter {}
