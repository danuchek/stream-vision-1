//
//  RatingService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 2/12/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import StoreKit
import MessageUI

fileprivate let shared: RateManagerImplementation = RateManagerImplementation()

extension RateManager {
    var rateManager: RateManagerProtocol {
        return shared
    }
}

protocol RateManager {
    var rateManager: RateManagerProtocol { get }
}

protocol ReviewAlertDelegateProtocol: class {
    func alert(presented: Bool)
}

protocol RateManagerProtocol: class, RateManagerCasesProtocol {
    
}

protocol RateManagerCasesProtocol {
    func negativeDisconnected()
    func firmwareUpdated()
    func bcCalculation()
    func mainPresented(isValid: Bool) -> (UIViewController) -> Void
    
    // Call this api when VC will apear
    func enterOnTrackingVC()
    
    // Call this api when VC will dissapear
    func leftFromTrackingVC()
}

extension RateManagerImplementation: StoreManager { }

fileprivate final class RateManagerImplementation: NSObject,
                                               RateManagerProtocol,
                                               RateManagerCasesProtocol,
                                               ReviewAlertDelegateProtocol,
                                               MFMailComposeViewControllerDelegate {
    // App id const
    private let appId: Int = 1069593770
    private var storeUrl: URL! {
        URL(string: "itms-apps://itunes.apple.com/app/\(appId)")
    }
    private let rateViewElapseTime = 182 // Time for new quiz
    
    // Variables
    private var timer: Timer?
    private let timeLimit = 5 * 60
    private var trackingTime: Int = 0
    
    private var isValid: Bool = false
    
    @objc fileprivate func windowDidBecomeVisibleNotification(notification: Notification) {
        Logging.log(message: "[RATE] windowDidBecomeVisibleNotification")
        
        if let window = notification.object as? UIWindow, "\(type(of: window))".contains("SK") {
            Logging.log(message: "[RATE] SK Detected")
            
            window.swizzle()
            window.delegate = self
        }
    }

    // MARK: - Presentation
    
    fileprivate func request(viewController: UIViewController) {
        guard isValid,
              checkIsValidTimeFromFirstLaunch(),
              checkIsValidTimeWithoutQuizDone(),
              checkIsValidTimeFromLastQuiz() else {
              return
        }
        
        ConnectionService.reachability { [weak self] (status) in
            switch status {
            case .reachable:
                self?.quizAlertView(viewController: viewController)
            case .notReachable:
                break
            }
        }
    }
    
    private func quizAlertView(viewController: UIViewController) {
        let alertController = UIAlertController(title: "Rating.InitialDialog.Title".localized(),
                                                message: "Rating.InitialDialog.Text".localized(),
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Rating.InitialDialog.Like".localized(),
                                                style: .default, handler: { [weak self] (action) in
            SKStoreReviewController.requestReview()
            
            if let weakSelf = self {
                NotificationCenter.default.addObserver(weakSelf,
                                                       selector: #selector(weakSelf.windowDidBecomeVisibleNotification),
                                                       name: UIWindow.didBecomeVisibleNotification,
                                                       object: nil)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Rating.InitialDialog.DoNotLike".localized(),
                                                style: .default, handler: { [weak self] (action) in
                                                    self?.feedbackAlertView(viewController: viewController)
                                                    self?.storeManager.rateViewsCount += 1
                                                    self?.storeManager.lastQuizDate = Date()
        }))
        alertController.addAction(UIAlertAction(title: "Rating.InitialDialog.NotNow".localized(),
                                                style: .cancel, handler: { [weak self] (action) in
            self?.holdOverRate()
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    private func feedbackAlertView(viewController: UIViewController) {
        let alertController = UIAlertController(title: "Rating.DislikeDialog.Title".localized(),
                                                message: "Rating.DislikeDialog.Text".localized(),
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "General.Alert.Give.Feedback".localized(),
                                                style: .default, handler: { [weak self] (action) in
            if let mailComposeViewController = self?.configuredMailComposeViewController() {
                if MFMailComposeViewController.canSendMail() {
                    viewController.present(mailComposeViewController, animated: true, completion: nil)
                }
            }
        }))
        alertController.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                                style: .cancel, handler: { (action) in
        
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    private func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["support@pulsar-nv.com"])
        
        return mailComposerVC
    }
    
    fileprivate func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - User Actions
    
    func enterOnTrackingVC() {
        startTrackTimeUsage()
    }
    
    func leftFromTrackingVC() {
        stopTrackTimeUsage()
    }
    
    func negativeDisconnected() {
        isValid = false
    }
    
    func firmwareUpdated() {
        isValid = true
    }
    
    func bcCalculation() {
        isValid = true
    }
    
    func mainPresented(isValid: Bool) -> (UIViewController) -> Void {
        self.isValid = self.isValid && isValid
        Logging.log(message: "[RATE] Main presented and is valid - \(self.isValid)")
        return request
    }
    
    // MARK: - Rate alert delegate
    
    fileprivate func alert(presented: Bool) {
        Logging.log(message: "[RATE] Delegate call - presented \(presented)")
        
        if !presented {
            if UIApplication.shared.canOpenURL(storeUrl) {
                Logging.log(message: "[RATE] Will open url - \(String(describing: storeUrl))")
                UIApplication.shared.open(storeUrl, options: [:], completionHandler: nil)
            } else {
                Logging.log(message: "[RATE] Url can't be open")
            }
        } else {
            storeManager.rateViewsCount += 1
        }
        NotificationCenter.default.removeObserver(self)
        storeManager.lastQuizDate = Date()
    }
    
    // MARK: - Internal logic
    
    private func holdOverRate() {
        storeManager.lastQuizDate = Date()
    }
    
    func checkIsValidTimeFromFirstLaunch() -> Bool {
        Logging.log(message: "[RATE] Check if valid from first install")
        
        if let lastQuizDate = storeManager.firstLaunchDate {
            let currentDate = Date()
            if let nextDayFromLastQuiz = Calendar.current.date(byAdding: {
                var component = DateComponents()
                component.day = 1
                return component
            }(), to: lastQuizDate) {
                if nextDayFromLastQuiz < currentDate {
                    Logging.log(message: "[RATE] True")
                    return true
                }
            }
        }
        
        Logging.log(message: "[RATE] False")
        return false
    }
    
    func checkIsValidTimeWithoutQuizDone() -> Bool {
        Logging.log(message: "[RATE] Check if valid without done")
        
        if let lastQuizDate = storeManager.lastQuizDate {
            let currentDate = Date()
            if let nextDayFromLastQuiz = Calendar.current.date(byAdding: {
                var component = DateComponents()
                component.month = 1
                return component
            }(), to: lastQuizDate) {
                if nextDayFromLastQuiz < currentDate {
                    Logging.log(message: "[RATE] True")
                    return true
                }
            }
        } else {
            Logging.log(message: "[RATE] True")
            return true
        }
        
        Logging.log(message: "[RATE] False")
        return false
    }
    
    func checkIsValidTimeFromLastQuiz() -> Bool {
        Logging.log(message: "[RATE] Check if valid from last quiz")
        
        let rateViewsCount = storeManager.rateViewsCount
        
        if let lastQuizDate = storeManager.lastQuizDate {
            let currentDate = Date()
            if let nextDayFromLastQuiz = Calendar.current.date(byAdding: {
                var component = DateComponents()
                component.day = ($0 * $1)
                return component
            }(rateViewsCount, rateViewElapseTime), to: lastQuizDate) {
                if nextDayFromLastQuiz < currentDate {
                    Logging.log(message: "[RATE] True")
                    return true
                }
            }
        } else {
            Logging.log(message: "[RATE] True")
            return true
        }
        
        Logging.log(message: "[RATE] False")
        return false
    }
    
    // MARK: - Time tracker
    
    private func startTrackTimeUsage() {
        timer = Timer(fire: Date(), interval: 1, repeats: true, block: { [weak self] (timer) in
            if let self = self {
                Logging.log(message: "[RATE] Tracking Time: \(self.trackingTime)")
                
                self.trackingTime += 1
                if self.trackingTime >= self.timeLimit {
                    timer.invalidate()
                    self.timer = nil
                    self.isValid = true
                }
            }
        })
        RunLoop.current.add(timer!, forMode: .common)
    }
    
    private func stopTrackTimeUsage() {
        timer?.invalidate()
        timer = nil
        
        trackingTime = 0
    }
}

fileprivate var isSwizzled = false
extension UIWindow  {

    private struct AssociatedKey {
        fileprivate static var delegate = "delegate"
        fileprivate static var elapsedTime = "elapsedTime"
    }

    fileprivate weak var delegate: ReviewAlertDelegateProtocol? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.delegate) as? ReviewAlertDelegateProtocol
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKey.delegate, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    fileprivate var elapsedTime: UInt64? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.elapsedTime) as? UInt64
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKey.elapsedTime, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }

    fileprivate func swizzle() {
        guard !isSwizzled,
        let addSubview_method = class_getInstanceMethod(
            object_getClass(self), #selector(addSubview(_:))
        ),
        let willRemoveSubview_method = class_getInstanceMethod(
            object_getClass(self), #selector(willRemoveSubview(_:))
        ),
        let addSubview_implementation = class_getMethodImplementation(
            object_getClass(self), #selector(swizzled_addSubview(_:))
        ),
        let willRemoveSubview_implementation = class_getMethodImplementation(
            object_getClass(self), #selector(swizzled_willRemoveSubview(_:))
        ) else {
            Logging.log(message: "[RATE] Swizzle for window exception")
            return
        }

        class_addMethod(object_getClass(self), #selector(addSubview(_:)),
                        addSubview_implementation,
                        method_getTypeEncoding(addSubview_method))
        
        class_addMethod(object_getClass(self), #selector(willRemoveSubview(_:)),
                        willRemoveSubview_implementation,
                        method_getTypeEncoding(willRemoveSubview_method))
        
        isSwizzled = true
        
        Logging.log(message: "[RATE] Swizzle for window done")
    }
    
    @objc fileprivate func swizzled_willRemoveSubview(_ subview: UIView) {
        super.willRemoveSubview(subview)
        
        Logging.log(message: "[RATE] Swizzled will remove subview func - time \(String(describing: elapsedTime))")
        
        if let elapsedTime = self.elapsedTime {
            self.elapsedTime = (DispatchTime.now().uptimeNanoseconds - elapsedTime) / 1_000_000_000
        }
        
        self.delegate?.alert(presented: self.elapsedTime != 0)
        self.delegate = nil
        
        Logging.log(message: "[RATE] Swizzled will remove subview func")
    }
    
    @objc fileprivate func swizzled_addSubview(_ view: UIView) {
        super.addSubview(view)
        self.elapsedTime = DispatchTime.now().uptimeNanoseconds
        
        Logging.log(message: "[RATE] Swizzled add subview func time - \(String(describing: elapsedTime))")
    }
}
