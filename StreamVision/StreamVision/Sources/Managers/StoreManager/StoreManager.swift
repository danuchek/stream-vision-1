//
//  ConnecionStoreService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/20/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import FileKit
import RealmSwift
import Security
import SwiftKeychainWrapper

fileprivate let shared: StoreManagerImplementation = StoreManagerImplementation()

extension StoreManager {
    var storeManager: StoreManagerProtocol {
        return shared
    }
}

protocol StoreManager {
    var storeManager: StoreManagerProtocol { get }
}

protocol StoreManagerProtocol: class {
    // Device store
    var devices: [VirtualDevice] { get }
    
    // Variables
    var nightModeColor: UIColor { get set }
    var isNightMode: Bool { get set }
    var isInvertedVideoStream: Bool { get set }
    var isRecordToPhone: Bool { get set }
    var currentLocalFileNumber: Int { get set }
    var lastCheckingUpdates: Date { get set }
    var lastAppVersion: String? { get set }
    
    // Rate app
    var firstLaunchDate: Date? { get set }
    var lastQuizDate: Date? { get set }
    var rateViewsCount: Int { get set }
    
    // Methods
    func store(_ device: VirtualDevice)
    func store(_ devices: [VirtualDevice])
    func deleteDevice(_ device: VirtualDevice)
}

fileprivate final class StoreManagerImplementation: NSObject, StoreManagerProtocol {
    // Variables
    fileprivate let userDefaults: UserDefaults = UserDefaults.standard
    fileprivate let localKeychain: KeychainWrapper = KeychainWrapper(serviceName: "SV_Keychain",
                                                                     accessGroup: "6N78XC2X26.com.yukon.realstream")
    
    // MARK: Settings
    
    // TODO: Add to config file
    var nightModeColor = UIColor.colorFromHexString("A0011B")
    
    var isNightMode: Bool {
        get {
            return userDefaults.bool(forKey: "isNightMode")
        }
        set {
            userDefaults.set(newValue, forKey: "isNightMode")
        }
    }
    
    var isInvertedVideoStream: Bool {
        get {
            return userDefaults.bool(forKey: "isInvertedVideoStream")
        }
        set {
            userDefaults.set(newValue, forKey: "isInvertedVideoStream")
        }
    }
    
    var isRecordToPhone: Bool {
        get {
            return userDefaults.bool(forKey: "isRecordToPhone")
        }
        set {
            userDefaults.set(newValue, forKey: "isRecordToPhone")
        }
    }
    
    var currentLocalFileNumber: Int {
        get {
            return userDefaults.integer(forKey: "currentLocalFileNumber")
        }
        set {
            userDefaults.set(newValue, forKey: "currentLocalFileNumber")
        }
    }
    
    var lastCheckingUpdates: Date {
        get {
            return (userDefaults.object(forKey: "lastCheckingUpdates") as? Date) ?? Date(timeIntervalSince1970: 0)
        }
        set {
            userDefaults.set(newValue, forKey: "lastCheckingUpdates")
        }
    }
    
    var lastAppVersion: String? {
        get {
            return userDefaults.object(forKey: "lastAppVersion") as? String
        }
        set {
            userDefaults.set(newValue, forKey: "lastAppVersion")
        }
    }
    
    // MARK: - Rate app
    
    var firstLaunchDate: Date? {
        get {
            return userDefaults.object(forKey: "firstLaunchDate") as? Date
        }
        set {
            userDefaults.set(newValue, forKey: "firstLaunchDate")
        }
    }

    var lastQuizDate: Date? {
        get {
            if let dateString = localKeychain.string(forKey: "lastQuizDate") {
                return dateFormatter.date(from: dateString)
            }
            return nil
        }
        set {
            if let date = newValue {
                localKeychain.set(dateFormatter.string(from: date), forKey: "lastQuizDate")
            }
        }
    }
    
    var rateViewsCount: Int {
        get {
            return localKeychain.integer(forKey: "rateViewsCount") ?? 0
        }
        set {
            localKeychain.set(newValue, forKey: "rateViewsCount")
        }
    }
    
    var neverAskAgain: Bool {
        get {
            return localKeychain.bool(forKey: "neverAskAgain") ?? false
        }
        set {
            localKeychain.set(newValue, forKey: "neverAskAgain")
        }
    }
    
    // MARK: - Date formatter

    var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.timeStyle = .full
        formatter.dateStyle = .full
        return formatter
    }
    
    // MARK: Store manager
    
    var devices: [VirtualDevice] {
        try! Realm().objects(VirtualDevice.self).sorted(by: { (lhs, rhs) -> Bool in
            return lhs.connectionDate > rhs.connectionDate
        })
    }
    
    func store(_ devices: [VirtualDevice]) {
        for device in devices where !device.isInvalidated {
            self.store(device)
        }
    }
    
    func store(_ device: VirtualDevice) {
        var firmware: VirtuaFirmwareInfo?
        let realm = try! Realm()
        if let storedDevice = realm.objects(VirtualDevice.self).filter({ d in
            d == device
        }).last {
            firmware = storedDevice.firmware
        }
        
        try! realm.write {
            if let firmware = firmware {
                device.firmware = firmware
            }
            realm.create(VirtualDevice.self, value: device, update: .all)
        }
    }
    
    func deleteDevice(_ device: VirtualDevice) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(device, update: .all)
            realm.delete(device)
        }
    }
}
