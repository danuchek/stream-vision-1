//
//  ConcurrentOperation.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/17/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

class ConcurrentOperation: Operation {
    
    override var isAsynchronous: Bool {
        return true
    }
    
    fileprivate var _executing = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    fileprivate var _finished = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    
    fileprivate var _cancelled = false {
        willSet {
            willChangeValue(forKey: "isCancelled")
        }
        
        didSet {
            didChangeValue(forKey: "isCancelled")
        }
    }
    
    override var isCancelled: Bool {
        return _cancelled
    }
    
    override var isFinished: Bool {
        return _finished
    }
    
    override func start() {
        if (isCancelled) {
            _finished = true
            return
        }
        
        _executing = true
        main()
    }
    
    func finish() {
        // Notify the completion of async task and hence the completion of the operation
        
        _executing = false
        _finished = true
    }
    
    override func cancel() {
        finish()
        _cancelled = true
        
        debugPrint("RAW CANCELL CALLED - \(self)")
    }
    
}

