//
//  SocketCommandProtocol.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 11/5/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

enum SocketInit {
    case commandWithType(type: String)
    case commandWithParams(type: String, params: [String: String?])
}

typealias OperationProtocol = ConcurrentOperation & SocketOperationDelegate
protocol SocketCommandProtocol: OperationProtocol {
    
    typealias FinishCallback = () -> ()
    typealias Socket = () -> DeviceSocket
    
    // Variables
    var commandParameters: Dictionary<String,String?>? { get set }
    var socketResponse: SocketResponse { get set }
    var deviceSocket: DeviceSocket { get set }
    
    // Private variables
    var active: Bool { get set }
    
    init(initialType: SocketInit, socket: Socket)
}
