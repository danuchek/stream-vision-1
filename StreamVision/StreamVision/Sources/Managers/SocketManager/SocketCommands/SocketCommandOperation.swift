//
//  SocketCommandOperation.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/12/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

/**
 *  SocketCommandData:

 Use init(command:) for converting data for socket

 Use init(command:params:) if need params

 - rawData: Converted command with params or not. Use for socket
 */

struct SocketData {
    var commandType: String
    var commandParams: Dictionary<String,String?>?
    var rawData: Data!
    
    init(command: String) {
        commandType = command
        rawData = self.convertToData(command.description)
    }
    
    init(command: String, params: Dictionary<String,String?>) {
        commandType = command
        commandParams = params

        var baseCommand = command
        
        let keys = params.compactMap { $0.key }
        if keys.count == 1, params[keys.first!]! == nil {
        } else {
            baseCommand += "?"
        }
        for (index, key) in keys.enumerated() {
            if let param = params[key] {
                if param == nil {
                    baseCommand += key
                } else {
                    baseCommand += key + "=" + param!
                }
                if keys.count > 1, index != keys.count - 1  {
                    baseCommand += "&"
                }
            } else {
                baseCommand += key
            }
        }
        rawData = self.convertToData(baseCommand)
    }
    func convertToData(_ commandString: String) -> Data {
        let stringWithEndMessage = commandString + "\r\n"
        return stringWithEndMessage.data(using: String.Encoding.nonLossyASCII)!
    }
}

protocol SocketFileOperationDelegate: SocketOperationDelegate {
    func finishWithSocketClosed()
}

protocol SocketOperationDelegate {
    func receivingData(_ bytesReaded: NSNumber)
    func dataSendedWithResponse(_ response: DeviceSocketResponse?)
}

/// SocketResponse: Can use for progress callback and response.
/// Functions:
/// - response: It will call after completion the operation
/// - progress: It will call during the operation
class SocketResponse {
    typealias SocketResponseCallback = (_ model: AnyObject?, _ error: NSError?) -> ()
    typealias SocketProgressCallback = (_ bytesReaded: NSNumber) -> ()
    var response: SocketResponseCallback? = nil
    var progress: SocketProgressCallback? = nil

    @discardableResult
     func response (_ r: SocketResponseCallback? = nil ) -> Self {
        response = r
        return self
    }

    func progress( _ p: SocketProgressCallback? = nil ) -> Self {
        progress = p
        return self
    }
}

/// SocketCommandOperation: Operation for sending commands via socket to device
class SocketCommandOperation:  ConcurrentOperation, SocketOperationDelegate, SocketCommandProtocol {
    // Types
    typealias FinishCallback = () -> ()
    typealias Socket = () -> DeviceSocket
    
    // Variables
    var commandType: String
    var commandParameters: Dictionary<String,String?>?
    var socketResponse: SocketResponse
    var deviceSocket: DeviceSocket
    // Private variables
    var active: Bool = false

    /**
     Base initialization with 2 requred params

     - parameter initialType: This is type of initialization this operation. You able to initialize with type command and parameters
     - parameter session:     Seesion use for executing commands via socket. All commands have dependency beetwen their completions

     - returns: Self
     */
    required init(initialType: SocketInit, socket: Socket) {
        deviceSocket = socket()
        socketResponse = SocketResponse()

        switch initialType {
        case let .commandWithType(type):
            commandType = type
            break
        case let .commandWithParams(type, params):
            commandType = type
            commandParameters = params
            break
        }
        
        Logging.log(message: "Command operation initialized - \(commandType)", toFabric: true)
    }

    override func main() {
        Logging.log(message: "Command operation started - \(commandType)", toFabric: true)
        guard !isCancelled else {
            return
        }
        deviceSocket.operationDelegate = self
        deviceSocket.connectSocket() { connected in
            if connected {
                let socketData: SocketData
                if let prarms = self.commandParameters {
                   socketData = SocketData(command: self.commandType, params: prarms)
                } else {
                   socketData = SocketData(command: self.commandType)
                }
                self.deviceSocket.sendSocketCommand(socketData)
            } else {
                self.cancel()
            }
        }
        active = true
    }

    override func cancel() {
        Logging.log(message: "[SOCKET.C] Operation cancel called - \(commandType) \(String(describing: commandParameters))", toFabric: true)
        
        socketResponse.response?(nil, NSError(domain: "Cancelled", code: 0, userInfo: nil))
        super.cancel()
    }

    func receivingData(_ bytesReaded: NSNumber) {
        socketResponse.progress?(bytesReaded)
    }

    func dataSendedWithResponse(_ response: DeviceSocketResponse?) {
        Logging.log(message: "Command operation will be finish - \(commandType)", toFabric: true)
        
        var model: AnyObject?
        var error: NSError?
        
        if let data = response?.responseData, let parsedDataInString =
            String(data: data, encoding: String.Encoding.utf8), parsedDataInString == "ok" {
            // TODO: Add success model
        } else if let rawType = response?.responseType,
            let type = String(data: rawType, encoding: String.Encoding.utf8) {
            if let _ = SocketCommand(rawValue: type), let commandType = SocketCommand(rawValue: self.commandType) {
                model = ModelParser.parseDataBy(commandType, data: response?.responseData)
                if model == nil {
                    if let data = response?.responseData, let parsedDataInString =
                        String(data: data, encoding: String.Encoding.utf8), parsedDataInString == "fail" {
                        error = NSError(domain: "Socket.command.parse.error", code: 0, userInfo: nil)
                    }
                }
            } else {
                error = NSError(domain: "Socket.command.type.error", code: 0, userInfo: nil)
            }
        } else {
            error = NSError(domain: "Socket.command.type.error", code: 0, userInfo: nil)
        }

        DispatchQueue.main.async {
            Logging.log(message: "[SOCKET.C] Response model \(String(describing: model)) or error \(String(describing: error))",
                toFabric: true)
            self.socketResponse.response?(model, error)
        }
        deviceSocket.operationDelegate = nil
        finish()
    }
}
