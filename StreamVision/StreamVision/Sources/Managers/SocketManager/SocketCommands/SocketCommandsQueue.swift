//
//  SocketCommandsQueue.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/12/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

class SocketOperationsQueue: OperationQueue {
    override init() {
        super.init()

        // Default setup
        maxConcurrentOperationCount = 1
    }
}
