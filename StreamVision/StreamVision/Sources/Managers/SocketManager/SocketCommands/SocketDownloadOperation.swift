//
//  SocketDownloadOperation.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/18/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class SocketDownloadOperation: SocketCommandOperation, SocketFileOperationDelegate {
    
    override func main() {
        Logging.log(message: "[SOCKET.C] Download command start: \(commandType) \(String(describing: commandParameters))")
        guard !isCancelled else {
            return
        }
        deviceSocket.connectSocket() { connected in
            self.deviceSocket.operationDelegate = self
            
            Logging.log(message: "[SOCKET.C] Socket \(self.deviceSocket) - connected = \(connected)")
            if connected {
                if let params = self.commandParameters {
                    let path = params["path"]
                    Logging.log(message: "[SOCKET.C] Lets send download command - \(String(describing: path))")
                    SocketManager.send(.download, params: ["path": path!]).response { model, error in
                        Logging.log(message: "[SOCKET.C] Download command received - model \(String(describing: model)) or error \(String(describing: error))")
                        if let _ = error {
                            Logging.log(message: "[SOCKET.C] Download command error")
                            self.cancel()
                        } else {
                            Logging.log(message: "[SOCKET.C] Download command ok - begin file socket communication")
                            
                            let socketData = SocketData(command: self.commandType, params: params)
                            self.deviceSocket.sendSocketCommand(socketData)
                        }
                    }
                }
            } else {
                Logging.log(message: "[SOCKET.C] Socket not connected - cancel should be called")
                self.cancel()
            }
        }
        active = true
    }
    
    override func cancel() {
        Logging.log(message: "[SOCKET.C] File cancel called - \(commandType) \(String(describing: commandParameters))")
        
        deviceSocket.operationDelegate = nil
        if let socket = deviceSocket as? SocketDeviceFiles, active {
            active = false
            socket.cancellDownloading()
            socket.disconnect { (connected) in
                Logging.log(message: "[SOCKET.C] Disconnect closure called")
                super.cancel()
            }
        }
    }
    
    override func dataSendedWithResponse(_ response: DeviceSocketResponse?) {
        Logging.log(message: "[SOCKET.C] Command operation will be finish - \(commandType) \(String(describing: commandParameters))")
        
        var model: AnyObject?
        if let commandType = SocketCommand(rawValue: self.commandType) {
            model = ModelParser.parseDataBy(commandType, data: response?.responseData)
        }
        
        DispatchQueue.global(qos: .background).async {
            self.socketResponse.response?(model, nil)
        }
        if let socket = deviceSocket as? SocketDeviceFiles, active {
            Logging.log(message: "[SOCKET.C] I want to close socket")
            socket.disconnect()
        }
    }
    
    func finishWithSocketClosed() {
        Logging.log(message: "[SOCKET.C] Finish by disconnection - \(commandType) \(String(describing: commandParameters))")
        finish()
    }
    
}
