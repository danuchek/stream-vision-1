//
//  SocketEventHandler.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/21/19.
//  Copyright © 2019 Ciklum. All rights reserved.
//

final class SocketEventHandler {
    /**
     * DSEvent - DeviceSocketEvent
     *
     * Contains list of supported events from device
     * unparsable event should be mark as invalid for
     * future handling
     **/
    enum DSEvent: String {
        case wifiOff = "ev_wifi_off"
        case powerOff = "ev_power_off"
        case invalid = "invalid"
    }
    
    static func parse(data: Data) -> DSEvent {
        guard let object = try? JSONSerialization.jsonObject(with: data,
                                                             options: .mutableContainers) else {
                                                                return .invalid
        }
        //  Example:
        //  Events\r\n{"Event":{ "ev_wifi_off":"1 sec"}}\r\n
        let eventDict = object as? [String: [String: String]]
        
        if let _ = eventDict?["Event"]?["ev_wifi_off"] {
            return .wifiOff
        } else if let _ = eventDict?["Event"]?["ev_power_off"] {
            return .powerOff
        }

        return .invalid
    }
}
