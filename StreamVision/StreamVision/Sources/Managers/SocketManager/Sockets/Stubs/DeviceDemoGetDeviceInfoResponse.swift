//
//  DeviceDemoResponse.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/12/19.
//  Copyright © 2019 Ciklum. All rights reserved.
//

enum DeviceSKUS: String {
    case digiforce = "78093"
    case ranger = "28047"
    case mission = "18056"
    case signal = "28061"
    case photon = "SM18018"
    case trail = "76511"
    case trailLRF = "76517"
    case accolade = "77412"
    case helion = "77405"
    case sightline = "28062"
    case digisightN350 = "76360"
    case forward = "78102"
    case helion2 = "76509"
    case thermion = "76524"
    case axion = "77421"
    case digisight = "76617"
    case lexion = "77444"
    case Digix = "76641"
    case demo = "00000"
    case krypton = "76655"
    case unnown = "11001"
    case unnown2 = "11002"
    
    case custom = "77394"
}

enum DeviceVersions: String {
    case v0101042F = "01.01.042.F"
    case v0203060F = "02.03.060.F"
    case v0203065F = "02.03.065.F"
    case v0300000F = "03.00.000.F"
    case v0400000F = "04.00.000.F"
    case v0000000F = "00.00.000.F"
    case v0301029F = "03.01.029.F"
}

enum DeviceSerials: String {
    case s70119308 = "70119308"
    case s9000960 = "9000960"
    case s781024227 = "781024227"
    case s7009832 = "7009832"
}

enum DeviceHardwares: String {
    case mnv000 = "MNV000"
    case mnv001 = "MNV001"
    case ptv013 = "PTV013"
    case afd006 = "AFD006.002"
    case mtv024 = "mtv024.001"
    case PTV025 = "PTV025.002"
}



class DeviceDemoGetDeviceInfoResponse: DeviceSocketResponse {
    let type: Data = "getDeviceInfo".data(using: .utf8)!
    let data: (_ sku: DeviceSKUS,
        _ serial: DeviceSerials,
        _ version: DeviceVersions,
        _ hardware: DeviceHardwares) -> Data =
        { sku, serial, version, hardware in
            let sku = "\"sku\":\"\(sku.rawValue)\""
            let serial = "\"serialNumber\":\"\(serial.rawValue)\""
            let version = "\"softwareVersion\":\"\(version.rawValue)\""
            let hardware = "\"hardwareVersion\":\"\(hardware.rawValue)\""
            
            return "{\(sku),\(serial),\(version),\(hardware)}".data(using: .utf8)!
    }
    
    
    init(sku: DeviceSKUS, serial: DeviceSerials, version: DeviceVersions, hardware: DeviceHardwares) {
        super.init(withType: type, data: data(sku, serial, version, hardware))
    }
}

class DeviceDemoGetParamsResponse: DeviceSocketResponse {
    let type: Data = "getParams".data(using: .utf8)!
    static func getData(accessLevel: Int, palette: Int, mode: Int, index: Int, selectedDistance: Int,
        distances: [Int], brightness: Int, autoOff: Int, gunAngle: Int, contrast: Int, units: Int,
        language: Int, recMode: Int, resPhoto: Int, pip: Int, resVideo: Int, zoom: Int, ir: Int,
        reticleType: Int, reticleBrightness: Int, reticleColor: Int, lcd: Int, date: Date,
        statusPort: Int, battery: Int, recStatus: Int, recTime: Int, extDC: Int) -> Data {
                    let accessLevel = "\"AccessLevel\":\"\(accessLevel)\""
                    let palette = "\"Palette\":\"\(palette)\""
                    let mode = "\"Mode\":\"\(mode)\""
                    let index = "\"Index\":\"\(index)\""
                    let selectedDistance = "\"SelectedDistance\":\"\(selectedDistance)\""
                    let distances = "\"Distances\":\(distances)"
                    let brightness = "\"Brightness\":\"\(brightness)\""
                    let autoOff = "\"AutoOff\":\"\(autoOff)\""
                    let gunAngle = "\"GunAngle\":\"\(gunAngle)\""
                    let contrast = "\"Contrast\":\"\(contrast)\""
                    let units = "\"Units\":\"\(units)\""
                    let language = "\"Language\":\"\(language)\""
                    let recMode = "\"RecMode\":\"\(recMode)\""
                    let resPhoto = "\"ResPhoto\":\"\(resPhoto)\""
                    let pip = "\"PiP\":\"\(pip)\""
                    let resVideo = "\"ResVideo\":\"\(resVideo)\""
                    let zoom = "\"Zoom\":\"\(zoom)\""
                    let ir = "\"IR\":\"\(ir)\""
                    let reticleType = "\"ReticleType\":\"\(reticleType)\""
                    let reticleBrightness = "\"ReticleBrightness\":\"\(reticleBrightness)\""
                    let reticleColor = "\"ReticleColor\":\"\(reticleColor)\""
                    let lcd = "\"LCD\":\"\(lcd)\""
                    let date = "\"DateTime\":\"\(date)\""
                    let statusPort = "\"StatusPort\":\"\(statusPort)\""
                    let battery = "\"Battery\":\"\(battery)\""
                    let recStatus = "\"RecStatus\":\"\(recStatus)\""
                    let recTime = "\"RecTime\":\"\(recTime)\""
                    let extDC = "\"ExtDC\":\"\(extDC)\""
                    let profile = "\"Profile\":{\(index),\(selectedDistance),\(distances)}"
                    
                    let parameters = [accessLevel, palette, mode, index, selectedDistance, distances,
                                      brightness, autoOff, gunAngle, contrast, units, language, recMode, resPhoto,
                                      pip, resVideo, zoom, ir, reticleType, reticleBrightness, reticleColor, lcd,
                                      date, statusPort, battery, recStatus, recTime, extDC, profile].reduce("", {
                                        return "\($0)\({ count in return count > 0 ? "," : ""}($0.count))\($1)"
                                      })
                    return "{\"Parameters\":{\(parameters)}}".data(using: .utf8)!
    }
    
    init(accessLevel: Int, palette: Int, mode: Int, index: Int, selectedDistance: Int,
         distances: [Int], brightness: Int, autoOff: Int, gunAngle: Int, contrast: Int, units: Int,
         language: Int, recMode: Int, resPhoto: Int, pip: Int, resVideo: Int, zoom: Int, ir: Int,
         reticleType: Int, reticleBrightness: Int, reticleColor: Int, lcd: Int, date: Date,
         statusPort: Int, battery: Int, recStatus: Int, recTime: Int, extDC: Int) {
        super.init(withType: type, data:
            DeviceDemoGetParamsResponse.getData(accessLevel: accessLevel, palette: palette,
                                                mode: mode, index: index, selectedDistance: selectedDistance,
                                                distances: distances, brightness: brightness,
                                                autoOff: autoOff, gunAngle: gunAngle, contrast: contrast,
                                                units: units, language: language, recMode: recMode,
                                                resPhoto: resPhoto, pip: pip, resVideo: resVideo,
                                                zoom: zoom, ir: ir, reticleType: reticleType,
                                                reticleBrightness: reticleBrightness,
                                                reticleColor: reticleColor, lcd: lcd, date: date,
                                                statusPort: statusPort, battery: battery,
                                                recStatus: recStatus, recTime: recTime, extDC: extDC))
    }
}
