//
//  Device18Response.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 10/25/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit

class Device18GetDeviceInfoResponse: DeviceSocketResponse {
    let type: Data = "getDeviceInfo".data(using: .utf8)!
    let data: Data = "{\"sku\":\"76655\",\"serialNumber\":77700385,\"softwareVersion\":\"01.00.010\",\"hardwareVersion\":\"PDV023.002\",\"magnification\":4.0,\"APIVersion\":2,\"Parameters\":{\"Brightness\": { \"mode\":\"rw\", \"min\": 0,\"max\": 20,\"step\": 1},\"Contrast\": { \"mode\":\"rw\", \"min\": 0,\"max\": 20,\"step\": 1},\"Profile\": { \"mode\":\"rw\", \"values\": [0,1,2,3,4]},\"RecMode\": { \"mode\":\"rw\", \"values\": [0,1,2]},\"Zoom\": { \"mode\":\"rw\", \"min\": 1000,\"max\": 4000,\"step\": 1},\"PiP\": { \"mode\":\"rw\", \"values\": [0,1]},\"ModeShutter\": { \"mode\":\"rw\", \"values\": [0,1,2]},\"Mode\": { \"mode\":\"rw\", \"values\": [0,1,2,3]},\"Palette\": { \"mode\":\"rw\", \"values\": [0,1,2,3,4,5,6,7]},\"ReticleType\": { \"mode\":\"rw\", \"values\": [0,1,2,3,4,5,6,7,8,9]},\"ReticleColor\": { \"mode\":\"rw\", \"values\": [0,1,2,3,4,5,6,7,8]},\"ReticleBrightness\": { \"mode\":\"rw\", \"min\": 1,\"max\": 10,\"step\": 1},\"Units\": { \"mode\":\"rw\", \"values\": [0,1]},\"AutoOff\": { \"mode\":\"rw\", \"values\": [0,1,2,3]},\"Language\": { \"mode\":\"rw\", \"values\": [0,1,2,3,4]},\"GunAngle\": { \"mode\":\"rw\", \"values\": [0,1]},\"TimeFormat\": { \"mode\":\"rw\", \"values\": [0,1]},\"DateTime\":\"yyyy-mm-dd HH:MM:SS\",\"Distance\": { \"mode\":\"rw\", \"min\": 0,\"max\": 999,\"step\": 1},\"IR\": { \"mode\":\"ro\", \"min\": 0,\"max\": 3000,\"step\": 1000},\"AccessLevel\": { \"mode\":\"ro\", \"values\": [0,1]},\"Battery\": { \"mode\":\"ro\", \"min\": 0,\"max\": 100,\"step\": 1},\"ExtDC\": { \"mode\":\"ro\", \"values\": [0,1]},\"IRStatus\": { \"mode\":\"rw\", \"values\": [0,1,2]},\"AnglePitch\": { \"mode\":\"ro\", \"min\": -90,\"max\": 90,\"step\": 1},\"AngleRoll\": { \"mode\":\"ro\", \"min\": -90,\"max\": 90,\"step\": 1},\"RecStatus\": { \"mode\":\"ro\", \"min\": 0,\"max\": 1,\"step\": 1},\"RecTime\": { \"mode\":\"ro\", \"min\": 0,\"max\": 350,\"step\": 1}},\"Commands\": [\"start_video\",\"pause_video\",\"stop_video\",\"snapshot\",\"fw_update\",\"format\",\"factory_reset\",\"download\",\"delete\",\"ls\",\"stop_operation\",\"shutter\"],\"Events\": [\"ev_saved_videofile\",\"ev_saved_snapshot\",\"ev_removed_file\",\"ev_access_media\",\"ev_wifi_off\",\"ev_power_off\",\"ev_disk_almost_full\" ]}".data(using: .utf8)!
    
    init() {
        super.init(withType: type, data: data)
    }
}

class Device18GetParamsResponse: DeviceSocketResponse {
    let type: Data = "getParams".data(using: .utf8)!
    let data: Data = "{\"Parameters\":{\"Brightness\":10,\"Contrast\":10,\"RecMode\":1,\"Zoom\":1000,\"IR\":0,\"TimeFormat\":0,\"DateTime\":\"2019-10-02 10:58:09\",\"PiP\":0,\"ModeShutter\":2,\"Mode\":1,\"ReticleType\":3,\"ReticleColor\":0,\"ReticleBrightness\":10,\"Profile\":{\"Index\":3,\"SelectedDistance\":200,\"Distances\":[823,100,200,300]},\"Units\":0,\"AutoOff\":3,\"GunAngle\":1,\"Language\":0,\"Palette\":1,\"AccessLevel\":0,\"Battery\":37,\"ExtDC\":0,\"AnglePitch\":0,\"AngleRoll\":0,\"IRStatus\":0,\"RecStatus\":0,\"RecTime\":0}}".data(using: .utf8)!
    
    init() {
        super.init(withType: type, data: data)
    }
}
