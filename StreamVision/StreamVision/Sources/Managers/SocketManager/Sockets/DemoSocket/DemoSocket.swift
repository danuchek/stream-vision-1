//
//  DemoSocket.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/8/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import FileKit

class DemoSocket: DeviceSocket {
    var operationDelegate: SocketOperationDelegate?
    var socketConnection: SocketConnection?
    
    fileprivate let filesList = DeviceSocketResponse(withType: "ls".data(using: String.Encoding.utf8),
                                                 data:("{\"curdir\":\"1:/img_0000\",\"filelist\":[{\"file\":\"demo_video.avi\",\"size\":148890,\"date\":\"14.01.16 07:54\"},{\"file\":\"demo_photo.jpg\",\"size\":97750,\"date\":\"14.01.16 07:54\"}]}".data(using: String.Encoding.utf8)))
    fileprivate let folderList = DeviceSocketResponse(withType: "ls?path=1:/".data(using: String.Encoding.utf8),
                                                 data:("{\"curdir\":\"1:/\",\"filelist\":[{\"folder\":\"img_0000\",\"size\":0,\"date\":\"28.07.13 00:00\"}]}".data(using: String.Encoding.utf8)))
    fileprivate let statisticsList = DeviceSocketResponse(withType: "ls?path=1:/".data(using: String.Encoding.utf8),
                                                      data:("{\"curdir\":\"1:/\",\"filelist\":[{\"folder\":\"SystemVolumeInformation\",\"size\":8192,\"date\":\"12.04.1913:48\"},{\"folder\":\"img_0000\",\"size\":8192,\"date\":\"15.04.1915:41\"},{\"file\":\"AFD006.002.M.00.00.012.bin.log\",\"size\":3466,\"date\":\"15.04.1915:17\"},{\"file\":\"AFD006.002.M.00.00.012.bin.UPDATED\",\"size\":41140224,\"date\":\"15.04.1915:16\"}]}".data(using: String.Encoding.utf8)))
    
    var connected: Bool = false {
        didSet {
            if !connected {
                if let connection = socketConnection {
                    connection.disconnectedFromSocket(self)
                }
            }
        }
    }
    func sendSocketCommand(_ commandData: SocketData) {
        var response: DeviceSocketResponse = DeviceSocketResponse(withType: commandData.commandType.data(using: .utf8),
                                                                  data: "ok".data(using: .utf8))
        
        if let type = SocketCommand(rawValue: commandData.commandType) {
            switch type {
            case .getDeviceInfo:
//                response = Device18GetDeviceInfoResponse()
                response = DeviceDemoGetDeviceInfoResponse(sku: .demo,
                                                           serial: .s9000960,
                                                           version: .v0101042F,
                                                           hardware: .mnv000)
 
            case .getFoldersList:
                response = folderList
            case .getFilesList:
                response = filesList
            case .getStatisticsList:
                response = statisticsList
            case .getParams:
//                response = Device18GetParamsResponse()
                response = DeviceDemoGetParamsResponse(accessLevel: 0, palette: 1, mode: 1, index: 1,
                                                       selectedDistance: 2, distances: [2,4,45,],
                                                       brightness: 0, autoOff: 0, gunAngle: 0, contrast: 0,
                                                       units: 0, language: 0, recMode: 1, resPhoto: 480,
                                                       pip: 0, resVideo: 480, zoom: 1000, ir: 0,
                                                       reticleType: 0, reticleBrightness: 0, reticleColor: 0,
                                                       lcd: 1, date: Date(), statusPort: 5008, battery: 100,
                                                       recStatus: 0, recTime: 0, extDC: 0)
            default: break
            }
        }
        
        if let data = response.responseData {
            debugPrint("[DEMO Socket] Response - \( String(data: data, encoding: .utf8)! )")
        }
        
        if let delegate = operationDelegate {
            DispatchQueue.global(qos: .background).async {
                delegate.dataSendedWithResponse(response)
            }
        }
    }
    func connectSocket(_ completion: ((_ connected: Bool) -> ())?) {
        if let _ = completion {
            completion!(true)
        }
        guard !connected else {
            return
        }
        if let connection = socketConnection {
            connection.connectedToSocket(self)
        }
        connected = true
    }
    func connectSocketWithSettings(_ settings: SocketConnectionSettings, completion: ((_ connected: Bool) -> ())?) {
        
    }
}
