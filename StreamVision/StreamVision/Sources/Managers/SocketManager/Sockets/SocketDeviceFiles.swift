//
//  SocketDeviceFiles.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/11/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//
class SocketDeviceFiles : DeviceSocketNetwork, DeviceSocket {
    // Variables
    var operationDelegate: SocketOperationDelegate?
    var socketConnection: SocketConnection?
    var isOldApi: Bool = true
    
    override func sendSocketCommand(_ commandData: SocketData) {
        if let size = commandData.commandParams?["size"] {
            Logging.log(message: "[SOCKET] Socket download file size(\(String(describing: size)))", toFile: true)
            Logging.log(message: "[SOCKET] Socket API is OLD \(isOldApi)", toFile: true)
            self.readFileFromSocket(UInt(size!)!)
        } else {
            self.dataSendedWithResponse(DeviceSocketResponse())
        }
    }
    
    @objc func socketDisconnected() {
        Logging.log(message: "[SOCKET] Socket device files - Disconnected")
        Logging.log(message: "[SOCKET] Socket API is OLD \(isOldApi)", toFile: true)
        
        if let delegate = operationDelegate as? SocketFileOperationDelegate {
            delegate.finishWithSocketClosed()
        }
    }

    @objc func dataSendedWithResponse(_ response: AnyObject) {
        if isOldApi {
            sendEND()
        } else {
            sendEND(endCommand: "END\r\nOKr\n")
        }
        
        if let delegate = operationDelegate {
            Logging.log(message: "[SOCKET] File socket dataSendedWithResponse:")
            
            delegate.receivingData(8192)
            delegate.dataSendedWithResponse(response as? DeviceSocketResponse)
        }
    }
    
    @objc func receivingData(_ bytes: NSNumber) {
        if let delegate = operationDelegate {
            delegate.receivingData(bytes)
        }
    }
    
    @objc func dataReceived() {
        if isOldApi {
            sendACK()
        }
    }

    func cancellDownloading() {
        Logging.log(message: "[SOCKET.C] Send fail to file socket")
        
        if isOldApi {
            sendFAIL()
        } else {
            sendFAIL(failCommand: "END\r\nFAIL\r")
        }
    }
}
