//
//  SocketDeviceCommands.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/11/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

protocol SocketCommandDescriptionProtocol: RawRepresentable {
    var description: String  { get }
}

enum SocketCommand18: String, SocketCommandDescriptionProtocol {
    case setGunAngle = "setGunAngle"
    case setBrightness = "setBrightness"
    case setPalette = "setPalette"
    case setIR = "setIR"
    case setRecMode = "setRecMode"
    case setReticleType = "setReticleType"
    case setUnits = "setUnits"
    case setRecStatus = "setRecStatus"
    case setReticleBrightness = "setReticleBrightness"
    case setAccess = "AccessLevel"
    case setTimeFormat = "setTimeFormat"
    case setReticleColor = "setReticleColor"
    case setZoom = "setZoom"
    case setExtDC = "setExtDC"
    case setIrStatus = "setIRStatus"
    case setProfile = "setProfile"
    case setPiP = "setPiP"
    case setContrast = "setContrast"
    case setAngleRoll = "setAngleRoll"
    case setAutoOff = "setAutoOff"
    case setBattery = "setBattery"
    case setRecTime = "setRecTime"
    case setMode = "setMode"
    case setModeShutter = "setModeShutter"
    case setAnglePitch = "setAnglePitch"
    case setLCD = "setLCD"
    case setLanguage = "setLanguage"
    case setDistance = "setSelectedDistance"
    case enableGunAngle = "enableGunAngle"
    case format = "format"
    case shutter = "shutter"
    case none = "none"
    case setDateTime = "setDateTime"
    
    var description: String {
        return self.rawValue
    }
}

enum SocketCommand : String, SocketCommandDescriptionProtocol {
    case getDeviceInfo = "getDeviceInfo"
    case getParams = "getParams"
    case setBrightness = "setBrightness"
    case setContrast = "setContrast"
    case setIR = "setIR"
    case setLCD = "setLCD"
    case setProfile = "setProfile"
    case setRecMode = "setRecMode"
    case setZoom = "setZoom"
    case setPIP = "setPiP"
    case setModeShutter = "setModeShutter"
    case setMode = "setMode"
    case setPalette = "setPalette"
    case setReticleType = "setReticleType"
    case setReticleColor = "setReticleColor"
    case setReticleBrightness = "setReticleBrightness"
    case setUnits = "setUnits"
    case setTimeFormat = "setTimeFormat"
    case setAutoOff = "setAutoOff"
    case setDateTime = "setDateTime"
    case setDistance = "setSelectedDistance"
    case enableGunAngle = "enableGunAngle"
    case setLanguage = "setLanguage"
    case startRecord = "start_video"
    case stopRecord = "stop_video"
    case doSnapshot = "snapshot"
    case shutter = "shutter"
    case fwUpdate = "fw_update"
    case fomat = "format"
    case addBad = "addBad"
    case getBitmap = "getBitmap"
    case delete = "delete"
    case getFoldersList = "ls?path=1:/"
    case getStatisticsList = "ls?path="
    case getFilesList = "ls"
    case setSlaveWifi = "setSlaveWifi"
    case stopOperation = "stop_operation"
    case download = "download"
    case setStatusPort = "setStatusPort"
    case events = "Events"
    case none = "none"

    var description: String {
        get {
            return self.rawValue
        }
    }
}

class SocketDeviceCommands: DeviceSocketNetwork, DeviceSocket {
    // Variables
    var operationDelegate: SocketOperationDelegate?
    var socketConnection: SocketConnection?

    override init(connectionSettings: SocketConnectionSettings, socketPort: UInt16) {
        super.init(connectionSettings: connectionSettings, socketPort: socketPort)
    }

    @objc func socketConnected() {
        if let connection = socketConnection {
            connection.connectedToSocket(self)
        }
        readDataFromSocket()
    }

    @objc func socketDisconnected() {
        if let connection = socketConnection {
            connection.disconnectedFromSocket(self)
        }
    }

    @objc func dataSendedWithResponse(_ response: AnyObject) {
        if let delegate = operationDelegate {
            delegate.dataSendedWithResponse(response as? DeviceSocketResponse)
        }
    }

    @objc func receivingData(_ bytes: NSNumber) {
        if let delegate = operationDelegate {
            delegate.receivingData(bytes)
        }
    }
}
