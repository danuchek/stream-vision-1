//
//  DeviceSocketNetwork.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/8/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import CocoaAsyncSocket
import RealmSwift
import FileKit

@objc protocol DeviceSocketNetworkProtocol {
    @objc optional func socketConnected()
    @objc optional func socketDisconnected()
    @objc optional func socketCannotConnect()
    @objc optional func dataSendedWithResponse(_ response: AnyObject)
    @objc optional func receivingData(_ bytes: UInt)
    @objc optional func dataReceived()
}

class DeviceSocketNetwork: NSObject, GCDAsyncSocketDelegate, DeviceSocketNetworkProtocol {
    typealias ConnectionCompletion = ((_ connected: Bool) -> ())
    typealias DisconnectionCompletion = ((_ connected: Bool) -> ())

    // Private variables
    fileprivate var connectionCompletion: ConnectionCompletion?
    fileprivate var basicSocket = GCDAsyncSocket()
    fileprivate var settings: SocketConnectionSettings
    fileprivate var port: UInt16 = 0
    fileprivate var currentBytes: UInt = 0
    fileprivate var queue: DispatchQueue
    fileprivate var dataReaded: (data: Data?, finish: Bool) = (nil, false)
    fileprivate var response: DeviceSocketResponse?
    fileprivate var needInterrupt: Bool = false
    fileprivate var currentHash: Int = 0
    fileprivate var currentTimeStamp: Int = 0
    fileprivate var fileHandle: FileHandle?
    fileprivate var disconnectionCompletion: ((_ disconnected: Bool) -> Void)?
    
    // Temp folder with file
    fileprivate let tempFolderPath = Path.userHome + "/Documents/download/temp/"
    fileprivate var tempFile: File<Data>?

    fileprivate var endMessageData: Data {
        get {
            var endSymb = NSData(data: GCDAsyncSocket.crData()) as Data
            endSymb.append(GCDAsyncSocket.lfData())
            return endSymb
        }
    }

    var isConnected : Bool{
        get {
            return basicSocket.isConnected
        }
    }

    /**
     Basic initialize with settings and port. After init you can try connect to socket

     - parameter connectionSettings: Settings with base IP
     - parameter socketPort:         Port current socket

     - returns: Self
     */
    init(connectionSettings: SocketConnectionSettings, socketPort: UInt16) {
        settings = connectionSettings
        port = socketPort
        queue = DispatchQueue(label: "socket\(socketPort)", attributes: []);
        
        _ = try? tempFolderPath.createDirectory(withIntermediateDirectories: true)

        super.init()
        basicSocket.setDelegate(self, delegateQueue: queue)
        basicSocket.readStream()
    }
    
    func disconnect(completion: DisconnectionCompletion? = nil) {
        if !isConnected {
            completion?(isConnected)
        } else {
            disconnectionCompletion = completion
        }
        basicSocket.disconnect()
    }

    func operationReadingInterrupt(_ hashValue: Int, _ timeStamp: Int) {
        if needInterrupt && hashValue == currentHash && timeStamp == currentTimeStamp {
            Logging.log(message: "[SOCKET] Socket data has not been red", toFabric: true)
            
            if self.responds(to: #selector(DeviceSocketNetworkProtocol.socketDisconnected)) {
                self.perform(#selector(DeviceSocketNetworkProtocol.socketDisconnected))
            }
            basicSocket.disconnect()
        }
    }
    
    func connectSocketWithSettings(_ settings: SocketConnectionSettings, completion: ConnectionCompletion?) {
        self.settings = settings
        connectSocket(completion)
    }

    func connectSocket(_ completion: ConnectionCompletion?) {
        Logging.log(message: "[SOCKET] Connection start to \(self)", toFabric: true)
        
        guard UserDefaults.standard.bool(forKey: "isMigrated") else {
            Logging.log(message: "[SOCKET] Fail connect - DB not migrated", toFabric: true)
            completion?(false)
            return
        }
        
        guard !isConnected else {
            Logging.log(message: "[SOCKET] Already connected", toFabric: true)
            completion?(true)
            return
        }
        
        do {
            Logging.log(message: "[SOCKET] Try connect to \(self)", toFabric: true)
            try basicSocket.connect(toHost: settings.baseSocketAddress, onPort: port, withTimeout: 5)
        } catch {
            Logging.log(message: "[SOCKET] Connect to socket ERROR - \(error)", toFabric: true)
        }
        connectionCompletion = completion
    }

    func sendSocketCommand(_ commandData: SocketData) {
        if isConnected {
            let data = commandData.rawData
            basicSocket.write(data!, withTimeout: 3, tag: data?.hashValue ?? -1)

            let logString = String(data: data!, encoding: String.Encoding.utf8)!
            Logging.log(message: "[SOCKET]Send:" + logString)
            
            needInterrupt = true
            currentHash = data!.hashValue
            let timeStamp = Int(Date().timeIntervalSince1970)
            currentTimeStamp = timeStamp
            queue.asyncAfter(deadline: .now() + 7) {
                let hash = data!.hashValue
                self.operationReadingInterrupt(hash, timeStamp)
            }
        }
    }

    func sendACK() {
        basicSocket.write("ACK\r\n".data(using: String.Encoding.ascii)!, withTimeout: -1, tag: "ACK".hash)
    }

    func sendFAIL(failCommand: String = "FAIL\r\n") {
        try? tempFile?.delete()
        tempFile = nil
        basicSocket.write(failCommand.data(using: String.Encoding.ascii)!, withTimeout: -1, tag: "FAIL".hash)
    }

    func sendEND(endCommand: String = "ACK\r\nEND\r\n") {
        basicSocket.write(endCommand.data(using: String.Encoding.ascii)!, withTimeout: -1, tag: "FAIL".hash)
    }

    func readDataFromSocket() {
        basicSocket.readData(to: endMessageData, withTimeout: -1, tag: "BasicReadingFromSocket".hash)
    }
    
    func readFileFromSocket(_ bytesToRead: UInt) {
        Logging.log(message: "[SOCKET] Socket read - \(bytesToRead)", toFile: false)
        
        currentBytes = bytesToRead
        if bytesToRead <= 8192 {
            currentBytes = 0
            basicSocket.readData(toLength: bytesToRead, withTimeout: 10, tag: "FileReadingFromSocket".hash)
        } else {
            currentBytes -= 8192
            basicSocket.readData(toLength: 8192, withTimeout: 10, tag: "FileReadingFromSocket".hash)
        }
    }

    // MARK: Delegate methods

    @objc func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        disconnectionCompletion?(isConnected)
        disconnectionCompletion = nil
        
        let error = err as NSError?
        if error?.localizedFailureReason == "Error in connect() function" || error?.code == 3 {
            if self.responds(to: #selector(DeviceSocketNetworkProtocol.socketCannotConnect)) {
                self.perform(#selector(DeviceSocketNetworkProtocol.socketCannotConnect))
            }
            Logging.log(message: "[SOCKET] Try connect to socket - Fail", toFabric: true)
            connectionCompletion?(false)
        } else {
            if error?.code == 4 {
                Logging.log(message: "[SOCKET] Disconnect Error 4 or \(String(describing: error?.localizedDescription))", toFabric: true)
                if self.responds(to: #selector(DeviceSocketNetworkProtocol.dataSendedWithResponse(_:))) {
                    self.perform(#selector(DeviceSocketNetworkProtocol.dataSendedWithResponse(_:)), with: DeviceSocketResponse() )
                }
            }
            
            if self.responds(to: #selector(DeviceSocketNetworkProtocol.socketDisconnected)) {
                Logging.log(message: "[SOCKET] Disconnect delegate call", toFabric: true)
                self.perform(#selector(DeviceSocketNetworkProtocol.socketDisconnected))
            }

            response = nil
            Logging.log(message: "[SOCKET] Socket disconnected - \(self)", toFabric: true)
        }
    }

    func socket(_ sock: GCDAsyncSocket, didReadPartialDataOfLength partialLength: UInt, tag: Int) {
        if response == nil {
            response = DeviceSocketResponse()
        }
        
        if self.responds(to: #selector(DeviceSocketNetworkProtocol.receivingData(_:))) {
            self.perform(#selector(DeviceSocketNetworkProtocol.receivingData(_:)), with: partialLength as NSNumber)
        }
    }

    @objc func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        DispatchQueue.global().async {
            if let parsedDataInString = String(data: data, encoding: String.Encoding.utf8) {
                Logging.log(message: "[SOCKET] Read: \(parsedDataInString)", toFabric: true)
            } else {
                Logging.log(message: "[SOCKET] Read error - data is null or unparsable", toFile: false)
            }
        }
        
        var mutableData: Data = data
        if tag == "BasicReadingFromSocket".hash {
            mutableData.count = mutableData.count - 2
        }
        if response == nil {
            response = DeviceSocketResponse()
        }
        
        switch tag {
        case "BasicReadingFromSocket".hash:
            if response!.responseType == nil {
                response!.responseType = mutableData as Data
            } else {
                needInterrupt = false
                response!.responseData = mutableData as Data
                if self.responds(to: #selector(DeviceSocketNetworkProtocol.dataSendedWithResponse(_:))) {
                    self.perform(#selector(DeviceSocketNetworkProtocol.dataSendedWithResponse(_:)), with: response)
                }
                response = nil
            }
            readDataFromSocket()
        case "FileReadingFromSocket".hash:
            if tempFile == nil || tempFile?.exists == false {
                Logging.log(message: "[SOCKET] New temp file created")
                
                tempFile = File<Data>(path: tempFolderPath + "file\(Date()).tmp")
                _ = try? tempFile?.create()
                
                if let path = tempFile?.path.rawValue {
                    fileHandle = FileHandle(forWritingAtPath: path)
                    Logging.log(message: "[SOCKET] File Handler created")
                }
            }
            fileHandle?.write(mutableData)
            
            if self.responds(to: #selector(DeviceSocketNetworkProtocol.dataReceived ) ) {
                self.perform(#selector(DeviceSocketNetworkProtocol.dataReceived ) )
            }
            if currentBytes > 0 {
                readFileFromSocket(currentBytes)
            } else {
                fileHandle?.synchronizeFile()
                fileHandle?.closeFile()
                Logging.log(message: "[SOCKET] Download finished size:\(String(describing: tempFile?.size))", toFabric: true)
                
                response?.responseData = tempFile?.path.rawValue.data(using: .utf8)
                needInterrupt = false
                if self.responds(to: #selector(DeviceSocketNetworkProtocol.dataSendedWithResponse(_:))) {
                    self.perform(#selector(DeviceSocketNetworkProtocol.dataSendedWithResponse(_:)), with: response )
                }
                response = nil
                tempFile = nil
                Logging.log(message: "[SOCKET] Download callback end", toFabric: true)
            }
        default: break
        }
    }

    @objc func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        Logging.log(message: "[SOCKET] Socket Connected - \(self)", toFabric: true)
        
        if self.responds(to: #selector(DeviceSocketNetworkProtocol.socketConnected)) {
            self.perform(#selector(DeviceSocketNetworkProtocol.socketConnected))
        }

        if let completion = connectionCompletion {
            completion(true)
        }
    }

    func socket(_ sock: GCDAsyncSocket, shouldTimeoutReadWithTag tag: Int, elapsed: TimeInterval, bytesDone length: UInt) -> TimeInterval {
        return 0
    }

    @objc func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        Logging.log(message: "[SOCKET] Data was sent", toFile: false)
    }

    deinit {
        basicSocket.disconnect()
    }

}
