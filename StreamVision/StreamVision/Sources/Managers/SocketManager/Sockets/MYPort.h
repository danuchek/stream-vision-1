//
//  Port.h
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/26/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MYPort : NSObject<NSNetServiceDelegate>
    
    -(uint16_t)test;

@end
