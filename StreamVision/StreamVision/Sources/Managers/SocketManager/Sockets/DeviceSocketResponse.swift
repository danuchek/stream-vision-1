//
//  DeviceSocketResponse.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/28/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

extension DeviceSocketResponse {
    var commandType: SocketCommand? {
        if let data = responseType, let type = String(data: data, encoding: .utf8) {
            return SocketCommand(rawValue: type)
        } else {
            return nil
        }
    }
}

extension DeviceSocketResponse {
    convenience init(data: Data) {
        self.init()

        let convertedData = String(data: data, encoding: String.Encoding.utf8)
        if let parsedString = convertedData?.components(separatedBy: "\r\n"), parsedString.count > 1 {
            responseType = parsedString[0].data(using: String.Encoding.utf8)
            responseData = parsedString[1].data(using: String.Encoding.utf8)
        }
    }
}

class DeviceSocketResponse {
    var responseType: Data?
    var responseData: Data?

    convenience init() {
        self.init(withType: nil, data: nil)
    }

    init(withType type: Data?, data: Data?) {
        if let _ = type {
            responseType = (type as NSData?)?.copy() as? Data
        }
        if let _ = data {
            responseData = data
        }
    }
}
