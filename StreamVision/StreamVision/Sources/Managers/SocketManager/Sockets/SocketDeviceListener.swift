//
//  SocketDeviceUpdater.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/11/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import CocoaAsyncSocket
import Foundation
import CFNetwork
import CoreFoundation

class SocketDeviceListener: GCDAsyncUdpSocketDelegate {
    // Variables
    fileprivate let notificationCenter = NotificationCenter.default
    fileprivate var basicSocket: GCDAsyncUdpSocket!
    var socketConnection: SocketConnection!

    var assignedPort: UInt16 {
        get {
            Logging.log(message: "[SOCKET CONNECTION] - Get local port = \(basicSocket.localPort())", toFabric: true)
            Logging.log(message: "[SOCKET CONNECTION] - Get local_ip4 port = \(basicSocket.localPort_IPv4())", toFabric: true)
            return basicSocket.localPort()
        }
    }
    var assignedIP: String? {
        get {
            Logging.log(message: "[SOCKET CONNECTION] - Get local ip = \(String(describing: basicSocket.localHost()))", toFabric: true)
            return basicSocket.localHost()
        }
    }

    // MARK: Initialize
    init() {
        basicSocket = GCDAsyncUdpSocket(delegate: self, delegateQueue: DispatchQueue.global())
        Logging.log(message: "[SOCKET CONNECTION] - Listner Socket initial", toFabric: true)
    }
    
    func disconnect() {
        Logging.log(message: "[SOCKET CONNECTION] - Disconnect", toFabric: true)
        basicSocket.close()
    }
    
    func connect() {
        Logging.log(message: "[SOCKET CONNECTION] - Connection start", toFabric: true)
        
        let port: MYPort = MYPort()
        let localport = port.test()
        
        Logging.log(message: "[SOCKET CONNECTION] - Port [\(localport)] generated", toFabric: true)
        
        do {
            try basicSocket.bind(toPort: localport)
            Logging.log(message: "[SOCKET CONNECTION] - bind success", toFabric: true)
        } catch {
            Logging.log(message: "[SOCKET ERROR] - Bind to port - \(error)", toFabric: true)
        }
        
        do {
            try basicSocket.enableBroadcast(true)
            Logging.log(message: "[SOCKET CONNECTION] - enableBroadcast success", toFabric: true)
        } catch {
            
            Logging.log(message: "[SOCKET ERROR] - Initial broadcast - \(error)", toFabric: true)
        }
        
        do {
            try basicSocket.beginReceiving()
            Logging.log(message: "[SOCKET CONNECTION] - beginReceiving success", toFabric: true)
        } catch {
            Logging.log(message: "[SOCKET ERROR] - Initial receiving - \(error)", toFabric: true)
        }
        
        SocketManager.send(.setStatusPort, params: ["newValue": "\(SocketManager.listnerPort)"])
        Logging.log(message: "Set Status Port - \(SocketManager.listnerPort)", toFabric: true)
    }

    func udpSocket(_ sock: GCDAsyncUdpSocket,
                   didReceive data: Data,
                   fromAddress address: Data,
                   withFilterContext filterContext: Any?) {
        Logging.log(message: "[SOCKET LISTNER] - Received params", toFabric: true)
        
        if let message = String(data: data, encoding: String.Encoding.utf8) {
            Logging.log(message: "[SOCKET LISTNER] - Received \(message)", toFabric: true)
        }
        
        let response = DeviceSocketResponse(data: data)
        if let paramsData = response.responseData, response.commandType == .getParams {
            DispatchQueue.main.async {
                self.socketConnection.updateDeviceParameters(paramsData)
            }
        } else if response.commandType == .events {
            switch SocketEventHandler.parse(data: data) {
            case .wifiOff:
                // TODO: Add analythics
                SocketManager.disconnect()
                break
            case .powerOff:
                // TODO: Add analythics
                SocketManager.disconnect()
                break
            default:
                // TODO: Add analythics
                break
            }
        } else {
            Logging.log(message: "[SOCKET LISTNER] - Parameters invalid", toFabric: true)
        }
    }
}
