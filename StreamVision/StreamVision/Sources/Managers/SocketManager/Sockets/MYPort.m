//
//  Port.m
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/26/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

#import "MYPort.h"
@import Darwin.sys.sysctl;
@import Darwin.POSIX.netinet.in;

@interface MYPort()

@property NSNetService *service;

@end

@implementation MYPort
    
    -(uint16_t)test {
        
        return [self getPort];
    }

- (void)netService:(NSNetService *)sender didAcceptConnectionWithInputStream:(NSInputStream *)inputStream outputStream:(NSOutputStream *)outputStream {
    
    printf("");
}

- (int) getPort {
    CFSocketContext socketCtxt = {0, (__bridge void *)(self), NULL, NULL, NULL};
    
    // Start by trying to do everything with IPv6.  This will work for both IPv4 and IPv6 clients
    // via the miracle of mapped IPv4 addresses.
    
    CFSocketRef witap_socket = CFSocketCreate(kCFAllocatorDefault, PF_INET6, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack, nil, &socketCtxt);
    uint32_t protocolFamily;
    
    if (witap_socket != NULL)   // the socket was created successfully
    {
        protocolFamily = PF_INET6;
    } else // there was an error creating the IPv6 socket - could be running under iOS 3.x
    {
        witap_socket = CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack, nil, &socketCtxt);
//        if (witap_socket != NULL)
//        {
        protocolFamily = PF_INET;
//        }
    }
    
    /*if (NULL == witap_socket) {
     if (error) *error = [[NSError alloc] initWithDomain:TCPServerErrorDomain code:kTCPServerNoSocketsAvailable userInfo:nil];
     if (witap_socket) CFRelease(witap_socket);
     witap_socket = NULL;
     return NO;
     }*/
    
    
    int yes = 1;
    setsockopt(CFSocketGetNative(witap_socket), SOL_SOCKET, SO_REUSEADDR, (void *)&yes, sizeof(yes));
    
    // set up the IP endpoint; use port 0, so the kernel will choose an arbitrary port for us, which will be advertised using Bonjour
    if (protocolFamily == PF_INET6)
    {
        struct sockaddr_in6 addr6;
        memset(&addr6, 0, sizeof(addr6));
        addr6.sin6_len = sizeof(addr6);
        addr6.sin6_family = AF_INET6;
        addr6.sin6_port = 0;
        addr6.sin6_flowinfo = 0;
        addr6.sin6_addr = in6addr_any;
        NSData *address6 = [NSData dataWithBytes:&addr6 length:sizeof(addr6)];
        
        CFSocketSetAddress(witap_socket, (CFDataRef)address6);
        /*if (kCFSocketSuccess != CFSocketSetAddress(witap_socket, (CFDataRef)address6)) {
         if (error) *error = [[NSError alloc] initWithDomain:TCPServerErrorDomain code:kTCPServerCouldNotBindToIPv6Address userInfo:nil];
         if (witap_socket) CFRelease(witap_socket);
         witap_socket = NULL;
         return NO;
         }*/
        
        // now that the binding was successful, we get the port number
        // -- we will need it for the NSNetService
        NSData *addr = (NSData *)CFBridgingRelease(CFSocketCopyAddress(witap_socket));
        memcpy(&addr6, [addr bytes], [addr length]);
        return ntohs(addr6.sin6_port);
        
    } else {
        struct sockaddr_in addr4;
        memset(&addr4, 0, sizeof(addr4));
        addr4.sin_len = sizeof(addr4);
        addr4.sin_family = AF_INET;
        addr4.sin_port = 0;
        addr4.sin_addr.s_addr = htonl(INADDR_ANY);
        NSData *address4 = [NSData dataWithBytes:&addr4 length:sizeof(addr4)];
        
        CFSocketSetAddress(witap_socket, (CFDataRef)address4);
        /*if (kCFSocketSuccess != CFSocketSetAddress(witap_socket, (CFDataRef)address4)) {
         if (error) *error = [[NSError alloc] initWithDomain:TCPServerErrorDomain code:kTCPServerCouldNotBindToIPv4Address userInfo:nil];
         if (witap_socket) CFRelease(witap_socket);
         witap_socket = NULL;
         return NO;
         }*/
        
        // now that the binding was successful, we get the port number
        // -- we will need it for the NSNetService
        NSData *addr = (NSData *)CFBridgingRelease(CFSocketCopyAddress(witap_socket));
        memcpy(&addr4, [addr bytes], [addr length]);
        return ntohs(addr4.sin_port);
    }
    
}

@end
