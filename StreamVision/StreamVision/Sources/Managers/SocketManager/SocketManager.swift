//
//  SocketManager.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/11/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import RealmSwift

protocol DeviceSocket {
    var operationDelegate: SocketOperationDelegate? { get set }
    var socketConnection: SocketConnection? { get set }
    func sendSocketCommand(_ commandData: SocketData)
    func connectSocket(_ completion: ((_ connected: Bool) -> ())?)
    func connectSocketWithSettings(_ settings: SocketConnectionSettings, completion: ((_ connected: Bool) -> ())?)
}

enum ConnectionStatus : Int {
    case connected = 0
    case disconnected = 1
    case terminated = 2
    case notResponse = 3
}

enum ConnectionMode {
    case normal
    case demo
    case slave
    case initialize
    case slaveConnection
}

extension SocketManager {
    static func isDeviceConnected(_ device: VirtualDevice) -> Bool {
        guard let connectedDevice = SocketManager.connectedDevice, connectedDevice == device else {
            return false
        }
        return true
    }
    static func isConnection(_ connectionMode: ConnectionMode) -> Bool {
        return SocketManager.connectionMode == connectionMode
    }
    static var isDevice18: Bool {
        return SocketManager.connectedDevice?.parameters.api == .new
    }
    
    static var connectedDevice: VirtualDevice? {
        get {
            return SocketManager.socketConnection.connectedDevice
        }
        set {
            SocketManager.socketConnection.connectedDevice = newValue
        }
    }
    static var connectionMode: ConnectionMode {
        get {
            return SocketManager.socketConnection.connectionMode
        }
        set {
            if newValue != .slave {
                SocketManager.sharedManager.commandsQueue.cancelAllOperations()
            }
            if newValue != .demo {
                SocketManager.sharedManager.demoSocket.connected = false
            }
            socketConnection.connectionMode = newValue
        }
    }
    static var connectionStatus: ConnectionStatus {
        get {
            return SocketManager.socketConnection.connectionStatus
        }
        set {
            SocketManager.socketConnection.connectionStatus = newValue
        }
    }
    static var isConnected: Bool {
        get {
            return SocketManager.socketConnection.connectedDevice != nil
        }
    }
    static var isDataUpdatedBlock: ((_ updated: Bool) -> ())? {
        get {
            return SocketManager.socketConnection.isDataUpdatedBlock
        }
        set {
            SocketManager.socketConnection.isDataUpdatedBlock = newValue
        }
    }
    static var isGuestMode: Bool {
        return SocketManager.socketConnection.accessLevel == .guest
    }
    
    static var listnerPort: UInt16 {
        return SocketManager.sharedManager.listenerSocket.assignedPort
    }
    
    @discardableResult
    static func download(params: Dictionary<String, String?>) -> SocketResponse {
        return SocketManager.sharedManager.downloadCommand(.download, params: params)
    }
    
    @discardableResult
    static func send(_ commandType: String, params: [String: String?]) -> SocketResponse {
        return SocketManager.sharedManager.sendCommand(commandType: nil,
                                                       commandString: commandType, params: params)
    }
    
    @discardableResult
    static func send(_ commandType: String) -> SocketResponse {
        return SocketManager.sharedManager.sendCommand(commandType)
    }
    
    
    @discardableResult
    static func send(_ commandType: SocketCommand, params: [String: String?]) -> SocketResponse {
        return SocketManager.sharedManager.sendCommand(commandType: commandType, params: params)
    }

    @discardableResult
    static func send(_ commandType: SocketCommand) -> SocketResponse {
        return SocketManager.sharedManager.sendCommand(commandType)
    }
    
    static func connectSocket(_ completion: ((_ connected: Bool) -> ())?) {
        return SocketManager.sharedManager.connectSocket(completion)
    }
    
    /**
    * Call disconnect from command socket and reset to main screen
    **/
    static func disconnect() {
        SocketManager.sharedManager.disconnect()
    }
    
    static func cancellDownload() {
        SocketManager.sharedManager.cancellDownloading()
    }
    
    static func cancellDeleting() {
        SocketManager.sharedManager.cancellDeleting()
    }
    
    static func tryConnectionWithIP(_ ip: String,
                                    completion: @escaping (_ connected: Bool) -> Void) {
        SocketManager.sharedManager.tryConnectionWithIP(ip, completion: completion)
    }
    
    // MARK: CONNECTION
    class func subscribe(_ subscriber: SocketConnectionSubsriber, notificationType: SocketConnection.Notifications) {
        SocketManager.socketConnection.subscribe(subscriber, notificationType: notificationType)
    }
    
    class func subscribe(_ subscriber: SocketConnectionSubsriber, notificationsType: [SocketConnection.Notifications]) {
        notificationsType.forEach() {
            SocketManager.socketConnection.subscribe(subscriber, notificationType: $0)
        }
    }
    
    class func unsubscribe(_ subscriber: SocketConnectionSubsriber) {
        SocketManager.socketConnection.unsubscribe(subscriber)
    }
}

class SocketManager: SocketConnectionSubsriber, DeviceManager, StoreManager {
    //Singleton
    static let sharedManager = SocketManager()

    // Private variables
    fileprivate let commandsQueue: SocketOperationsQueue = SocketOperationsQueue()
    fileprivate let filesQueue: SocketOperationsQueue = SocketOperationsQueue()

    fileprivate var commandSocket: SocketDeviceCommands!
    fileprivate var filesSocket: SocketDeviceFiles!
    fileprivate var listenerSocket: SocketDeviceListener!
    fileprivate var demoSocket = DemoSocket()

    // Variables
    fileprivate static let socketConnection = SocketConnection()
    let settings = SocketConnectionSettings()

    init() {
        Logging.log(message: "@NEW LOG SESSION \n")
        self.setupSockets()
        
        DispatchQueue.global().async {
            SocketManager.subscribe(self, notificationsType:  [.DeviceDisconnected, .DeviceConnected])
        }
    }
    
    private func setupSockets() {
        // Socket initialization
        Logging.log(message: "[SOCKET] - Setup sockets", toFabric: true)
        listenerSocket = SocketDeviceListener()
        commandSocket = SocketDeviceCommands(connectionSettings: settings, socketPort: 5006)
        filesSocket = SocketDeviceFiles(connectionSettings: settings, socketPort: 5005)
        
        // Setup sockets
        commandSocket.socketConnection = SocketManager.socketConnection
        filesSocket.socketConnection = SocketManager.socketConnection
        listenerSocket.socketConnection = SocketManager.socketConnection
        demoSocket.socketConnection = SocketManager.socketConnection
    }
    
    func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        switch notification {
        case .DeviceDisconnected:
            commandsQueue.cancelAllOperations()
            listenerSocket.disconnect()
        case .DeviceConnected:
            if SocketManager.connectionMode != .demo {
                listenerSocket.connect()
            }
            
            if SocketManager.isDevice18 {
                Logging.log(message: "[SOCKET] Initialized with new api", toFile: true)
                filesSocket.isOldApi = false
            } else {
                Logging.log(message: "[SOCKET] Initialized with old api", toFile: true)
                filesSocket.isOldApi = true
            }
        default: break
        }
    }

    fileprivate func connectSocket(_ completion: ((_ connected: Bool) -> ())?) {
        guard UserDefaults.standard.bool(forKey: "isMigrated") else {
            Logging.log(message: "[Socket] Try to connect without migration")
            completion?(false)
            return
        }
        
        if SocketManager.connectionMode == .demo {
            demoSocket.connectSocket(completion)
        } else {
            commandSocket.connectSocket(completion)
        }
    }
    
    fileprivate func disconnect() {
        commandSocket.disconnect()
    }
    
    func tryConnectionWithIP(_ ip: String,
                             completion: @escaping (_ connected: Bool) -> Void) {
        settings.baseSocketAddress = ip
        commandSocket.connectSocketWithSettings(settings) { (connected) in
            completion(connected)
            if connected {
                self.filesSocket = SocketDeviceFiles(connectionSettings: self.settings, socketPort: 5005)
            }
        }
    }

    fileprivate func downloadCommand(_ commandType: SocketCommand, params: Dictionary<String, String?>?) -> SocketResponse {
        let initialType = SocketInit.commandWithParams(type: commandType.rawValue, params: params!)
        let queue: SocketOperationsQueue = filesQueue
        var socketCommand: SocketCommandOperation

        socketCommand = SocketDownloadOperation(initialType: initialType, socket: {
            return self.filesSocket
        })
        
        Logging.log(message: "Command operation will be add to queue - \(commandType)", toFabric: true)
        queue.addOperation(socketCommand)
        
        Logging.log(message: "Queue operations is \(queue.operations.count): ", toFabric: true)
        for operation in queue.operations {
            if let command = operation as? SocketCommandOperation {
                Logging.log(message:
                    "Operation type: \(command.commandType) params: \(String(describing: command.commandParameters))", toFabric: true)
            }
        }
        
        return socketCommand.socketResponse
    }

    func sendCommand(_ commandType: SocketCommand) -> SocketResponse {
        return sendCommand(commandType: commandType, params: nil)
    }
    
    func sendCommand(_ commandType: String) -> SocketResponse {
        return sendCommand(commandType: nil, commandString: commandType, params: nil)
    }

    fileprivate func sendCommand(commandType: SocketCommand?, commandString: String = "none",
                                 params: [String: String?]?) -> SocketResponse {
        let queue: SocketOperationsQueue = commandsQueue
        var socketCommand: SocketCommandOperation
        let socketType: SocketCommandOperation.Socket = {
            if SocketManager.connectionMode == .demo {
                return self.demoSocket
            }
            return self.commandSocket
        }
        let type: String = commandType?.rawValue ?? commandString
        var initialType = SocketInit.commandWithType(type: type)
        if let _ = params {
            initialType = SocketInit.commandWithParams(type: type, params: params!)
        }
        socketCommand = SocketCommandOperation(initialType: initialType, socket: socketType)
        
        Logging.log(message: "Begin send = \(socketCommand.commandType), Params = \(String(describing: socketCommand.commandParameters))", toFabric: true)
        queue.addOperation(socketCommand)
        Logging.log(message: "Queue operations - \(queue.operations)", toFabric: true)
        return socketCommand.socketResponse
    }

    fileprivate func cancellDownloading() {
        //filesQueue.operations.first?.cancel()
        filesQueue.cancelAllOperations()
        commandsQueue.cancelAllOperations()
        
        Logging.log(message: "Queue operations after cancel is \(filesQueue.operations.count): ", toFabric: true)
        for operation in filesQueue.operations {
            if let command = operation as? SocketCommandOperation {
                Logging.log(message:
                    "Operation type: \(command.commandType) params: \(String(describing: command.commandParameters))", toFabric: true)
            }
        }
    }
    
    fileprivate func cancellDeleting() {
        commandsQueue.cancelAllOperations()
    }
}

/*
 * Extension here need for access to socketConnection
 */
extension DeviceManager {
    func reload() {
        let devices = storeManager.devices
        
        for device in devices {
            var sku: String = device.info.sku
            var isInvalid = false
            if device.info.invalidSku != "" && device.info.sku != device.info.invalidSku {
                sku = device.info.invalidSku
                isInvalid = true
            }
            if let template = deviceManager.getDevice(by: sku, version: device.info.version) {
                let newDevice = VirtualDevice.from(template: template)
                newDevice.update(with: device)
                
                // TODO: Concrete device
                
                if let connectedDevice = SocketManager.socketConnection.connectedDevice, SocketManager.isDeviceConnected(device) {
//                if let connectedDevice = SocketManager.socketConnection.connectedDevice {
                    newDevice.update(with: connectedDevice)
                    SocketManager.socketConnection.connectedDevice = newDevice
                }
                if isInvalid {
                    storeManager.deleteDevice(device)
                }
                storeManager.store(newDevice)
            }
        }
    }
}
