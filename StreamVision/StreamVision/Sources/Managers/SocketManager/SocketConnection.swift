//
//  SocketConnection.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/21/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import STZPopupView

protocol SocketConnectionSubsriber: class {
    func didFireNotification(notification : SocketConnection.Notifications, model: Any?)
}

extension SocketConnection {
    enum Notifications {
        case DeviceConnected
        case DeviceDisconnected
        case DeviceParamsUpdated
        case DeviceAccessUpdated
        case InvalidDevice
    }
}

extension SocketConnection {
    
    // MARK: Connection norification center
    func subscribe(_ subscriber: SocketConnectionSubsriber, notificationType: Notifications) {
        let subscriberWithType: [Any] = [subscriber, notificationType]
        subscribers.append(subscriberWithType)
    }
    
    func unsubscribe(_ subscriber: SocketConnectionSubsriber) {
        let filtredSubscribers = subscribers.filter() { ($0[0] as! SocketConnectionSubsriber) === subscriber }
        filtredSubscribers.forEach() {
            let filtredSubscriber = $0[0] as! SocketConnectionSubsriber
            let index = self.subscribers.firstIndex() { s in
                filtredSubscriber === s[0] as! SocketConnectionSubsriber
            }
            if let _ = index {
                subscribers.remove(at: index!)
            }
        }
    }

    fileprivate func notifySubscribers(_ notification: Notifications, model: Any?) {
        let subscribersForNotify = subscribers.filter { ($0[1] as! Notifications) == notification }
        subscribersForNotify.forEach { ($0[0] as! SocketConnectionSubsriber).didFireNotification(notification: notification, model: model) }
    }
}

extension SocketConnection {
    enum AccessLevel: Int {
        case guest = 0
        case owner = 1
    }
}

extension SocketConnection {
    static let demoConnectionKey = "demoDeviceActive"
    class var isDemoPreviosActive: Bool {
        get {
            return UserDefaults.standard.bool(forKey: SocketConnection.demoConnectionKey)
        }
    }
}

class SocketConnection: StoreManager {
    // Private variables
    fileprivate let notificationCenter = NotificationCenter.default
    fileprivate var connectionTimer: Timer?
    fileprivate let updateTime = 3.0;
    fileprivate var checkDownlodedFirmware: Bool = false
    fileprivate var subscribers: [[Any]] = []

    // Variables
    var connectedDevice: VirtualDevice? {
        didSet {
            Logging.log(message: "[SocketConnect] Connected \(String(describing: connectedDevice?.info.name)) will be change on - \(String(describing: connectedDevice?.info.name))")
        }
    }
    
    var accessLevel: AccessLevel = .owner {
        didSet {
            if oldValue != accessLevel {
                notifySubscribers(.DeviceAccessUpdated, model: nil)
            }
        }
    }
    
    fileprivate var _connectionMode: ConnectionMode = .normal
    var connectionMode: ConnectionMode {
        get {
            return _connectionMode
        }
        set {
            _connectionMode = newValue
            if connectionMode == .demo {
                stopTimer()
                tryConnection()
                
                UserDefaults.standard.set(true, forKey: SocketConnection.demoConnectionKey)
            } else if connectionMode == .slaveConnection {
                stopTimer()
                
                UserDefaults.standard.set(false, forKey: SocketConnection.demoConnectionKey)
            } else {
                startTimer()
                
                UserDefaults.standard.set(false, forKey: SocketConnection.demoConnectionKey)
            }
        }
    }
    var connectionStatus: ConnectionStatus = .disconnected
    fileprivate var dataUpdated = false
    fileprivate var _isDataUpdatedBlock: ((_ updated: Bool) -> ())?
    var isDataUpdatedBlock: ((_ updated: Bool) -> ())? {
        get {
            return _isDataUpdatedBlock
        }
        set {
            _isDataUpdatedBlock = newValue
            if dataUpdated {
                isDataUpdatedBlock!(true)
                _isDataUpdatedBlock = nil
            }
        }
    }
    fileprivate var isReconnectionStarted = false

    fileprivate func startTimer() {
        DispatchQueue.main.async {
            self.connectionTimer?.invalidate()
            self.connectionTimer = Timer.scheduledTimer(timeInterval: self.updateTime,
                                                                          target: self,
                                                                          selector: #selector(SocketConnection.tryConnection),
                                                                          userInfo: nil,
                                                                          repeats: false)
        }
    }

    fileprivate func stopTimer() {
        DispatchQueue.main.async {
            self.connectionTimer?.invalidate()
            self.connectionTimer = nil
        }
    }

    @objc fileprivate func tryConnection() {
        guard connectionMode != .slaveConnection else {
            return
        }

        if connectedDevice == nil {
            SocketManager.connectSocket({ [weak self] (connected) in
                if connected {
                    self?.stopTimer()
                } else {
                    self?.startTimer()
                }
            })
        }
    }

    func disconnectedFromSocket(_ socket: DeviceSocket) {
        Logging.log(message: "[SocketConnect] Device disconnected", toFabric: true)
        // TODO: Rewrite this
        guard connectionMode != .slaveConnection else {
            return
        }
        
        Logging.log(message: "[SocketConnect] Disconnection event executed", toFabric: true)
        
        if socket is DemoSocket {
            if let device = connectedDevice {
                storeManager.deleteDevice(device)
            }
        }

        if socket is SocketDeviceCommands || socket is DemoSocket {
            if socket is SocketDeviceCommands && connectionMode == . demo {
                return
            }

            resetConnection()
            Logging.log(message: "[SocketConnect] Reset connection", toFabric: true)
            
            if connectionStatus == .connected && !(socket is DemoSocket) {
                Logging.log(message: "[SocketConnect] Connection status = terminated", toFabric: true)
                self.connectionStatus = .disconnected
                
                
                DispatchQueue.main.async {
                    if !self.isReconnectionStarted {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        if let vc = ((appDelegate.window?.rootViewController?.presentedViewController as? BaseNavigationController)?.frontViewController as? UINavigationController)?.topViewController as? ViewfinderViewController, type(of: vc) == ViewfinderViewController.self {
                            let reconnectView = ReconnectView(center: vc.view.center, cancell: {
                                self.isReconnectionStarted = false
                                self.resetToMainWithTermination()
                            })
                            let config = STZPopupViewConfig()
                            config.dismissTouchBackground = false
                            vc.presentPopupView(reconnectView, config: config)
                            
                            self.isReconnectionStarted = true
                        } else {
                            self.resetToMainWithTermination()
                        }
                    }
                }
            } else {
                Logging.log(message: "[SocketConnect] Connection status = disconnected", toFabric: true)
                self.connectionStatus = .disconnected
            }
            
            self.notifySubscribers(.DeviceDisconnected, model: self.connectedDevice)

        }
    }
    
    private func resetToMainWithTermination() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        connectionStatus = .terminated
        DispatchQueue.main.async {
            appDelegate.resetToMain()
        }
    }
    
    private func resetConnection() {
        startTimer()
        connectedDevice = nil
        checkDownlodedFirmware = false
        self.accessLevel = .owner
    }

    func connectedToSocket(_ socket: DeviceSocket) {
        Logging.log(message: "[SocketConnect] Try connect device", toFabric: true)
        if (socket is SocketDeviceCommands || socket is DemoSocket) && (connectionStatus != .connected || isReconnectionStarted) {
            dataUpdated = false
            Logging.log(message: "[SocketConnect] Trying to get device info", toFabric: true)
            SocketManager.send(.getDeviceInfo).response { model, error in
                Logging.log(message: "[SocketConnect] Device info was received", toFabric: true)
                guard error == nil else {
                    self.resetConnection()
                    return
                }

                if let device = model as? VirtualDevice {
                    self.connectedDevice = device
                }

                if let _ = model {
                    self.checkNewestFirmware()
                    if let device = self.connectedDevice {
                        if self.storeManager.devices.first != device {
                            FilesManager.shared.resetOperationsService()
                        }
                        self.storeManager.store(device)
                    }
                    Logging.log(message: "[SocketConnect] Device connected with - Brand:\(String(describing: self.connectedDevice?.info.brand)) Name:\(String(describing: self.connectedDevice?.info.name)) Sku:\(String(describing: self.connectedDevice?.info.sku)) Serial:\(String(describing: self.connectedDevice?.info.serial)) HW:\(String(describing: self.connectedDevice?.info.hw))", toFabric: true)
                } else {
                    Logging.log(message: "[SocketConnect] Connection ERROR!!! Model = Nil", toFabric: true)
                }
                
                Logging.log(message: "[SocketConnect] Trying to get device params", toFabric: true)
                
                _ = SocketManager.sharedManager.sendCommand(.getParams).response { model, error in
                    Logging.log(message: "[SocketConnect] Device params was received", toFabric: true)
                    
                    guard let parametersData = model as? Data,
                        let device = self.connectedDevice, error == nil else {
                        Logging.log(message: "[SocketConnect] Reset connection", toFabric: true)
                        self.resetConnection()
                        return
                    }
                    
                    var parametersDict = device.parameters.update(parametersData)
                    if let access = device.parameters.access, access.value == 1 {
                        self.accessLevel = .guest
                    } else {
                        self.accessLevel = .owner
                    }
                    
                    // Change stream type to h264
                    if let stream = device.parameters.stream {
                        stream.value = 1
                    }
                    self.notifySubscribers(.DeviceParamsUpdated, model: device.parameters )

                    if let device = self.connectedDevice {
                        var deviceParamters: VirtualParameters? = VirtualParameters()
                        if let _ = device.parameters.distances {
                            parametersDict?["Distance"] = ["value" : "0"] as AnyObject
                        }
                        
                        if let parametersDict = parametersDict {
                            device.parameters.api.type.update(rawParameters: parametersDict, parameters: &deviceParamters)
                        }
                        
                        self.notifySubscribers(.DeviceConnected, model: self.connectedDevice)

                        // TODO: How to detect missing parameters if someone not presented in getParams
//                        if ((device.parameters <> deviceParamters!) && device.parameters.validate()) || self.connectionMode == .demo {
//                            self.notifySubscribers(.DeviceConnected, model: self.connectedDevice)
//                        } else {
//                            self.notifySubscribers(.InvalidDevice, model: self.connectedDevice)
//                        }

                        self.connectionStatus = .connected
                    }

                    self.dataUpdated = true
                    if let block = self.isDataUpdatedBlock {
                        block(true)
                        self._isDataUpdatedBlock = nil
                    }
                    
                    DeviceStatisticsService.collectStatistics()
                }
            }
        }
        self.connectionStatus = .connected
        
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if let vc = ((appDelegate.window?.rootViewController?.presentedViewController as? BaseNavigationController)?.frontViewController as? UINavigationController)?.topViewController, vc is ViewfinderViewController {
                if self.isReconnectionStarted {
                    self.isReconnectionStarted = false
                    (vc as! ViewfinderViewController).setupStream()
                }
                vc.dismissPopupView()
            }
        }
    }
    
    func checkNewestFirmware() {
        Logging.log(message: "[SocketConnection] Check newest firmware", toFabric: true)
        if !checkDownlodedFirmware {
            Logging.log(message: "[SocketConnection] checkDownlodedFirmware is false", toFabric: true)
            
            if connectedDevice?.hasDownloadedFirmware == true {
                Logging.log(message: "[SocketConnection] Downloaded firmware exist", toFabric: true)
                
                let alert = UIAlertController(title: "DeviceDetails.InstallSoftware.Title.Prefix".localized(),
                                              message: "General.Alert.HasNewFirmware".localized(),
                                              preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized() , style: .default, handler: nil))

                UIApplication.shared.windows.last?.rootViewController?.present(alert, animated: true, completion: nil)
            }
            checkDownlodedFirmware = true
        }
    }

    func updateDeviceParameters(_ data: Data) {
        if let parameters = SocketManager.connectedDevice?.parameters {
            let oldValues = parameters.hashValues
            Logging.log(message: "[Listner] Update parameters - \(oldValues)", toFabric: true)

            _ = parameters.update(data)
            let newValues = parameters.hashValues
            Logging.log(message: "[Listner] Update parameters - \(newValues)", toFabric: true)

            if parameters.access?.value == 1 {
                accessLevel = .guest
            } else {
                accessLevel = .owner
            }
            if oldValues != newValues {
                self.notifySubscribers(.DeviceParamsUpdated, model: parameters )
            }
        }
    }
}
