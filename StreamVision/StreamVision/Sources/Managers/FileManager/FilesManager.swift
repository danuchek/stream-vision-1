//
//  DownloadQueue.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/23/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import FileKit
import SVProgressHUD
import AVFoundation

protocol FileManagerSubsriber {
    func didFireNotification(notification: FilesManager.FileNotification)
}

extension FilesManager: StoreManager {
    static let shared: FilesManager = FilesManager()
}

class FilesManager {
    typealias FoldersRequestCallback = (_ folders: [FolderModel]) -> Void
    typealias DownloadingProgressCallback = (_ progress: Float, _ downloadingHash: String) -> Void
    typealias DownloadingFinishCallback = (_ folder: FolderModel, _ file: FileModel, _ downloadingHash: String, _ error: Error?) -> Void
    
    enum FileNotification {
        case start
        case progress
        case finish
    }
    
    fileprivate var fileOperationsService: FileOperationsService = FileOperationsService()
    fileprivate let connection: SocketManager = SocketManager.sharedManager
    fileprivate var videoEncoder: VideoEncoder?
    fileprivate var streamService: StreamService?
    fileprivate var converterHandler: ConverterHandler = ConverterHandler()
    
    var isLocalFilesExist: Bool {
        return defaultDir.find(searchDepth: 2) { $0.pathExtension == "fileinfo" }.count > 0
    }
    
    var defaultPathToFirmware: Path {
        get {
            return self.defaultDir + "/firmware/"
        }
    }
    
    var defaultDir: Path {
        get {
            let documents: Path = Path.userHome + "/Documents"
            return documents
        }
    }
    
    var cacheFolders: [FolderModel] = []
    var remoteFolders: [FolderModel] = []
    var isDownloading: Bool {
        return fileOperationsService.isDownloading
    }
    
    var isCanBeResumed: Bool {
        return fileOperationsService.isCanBeResumed
    }
    
    var isFileConverting: (_ file: FileModel) -> Bool {
        return converterHandler.isConverting
    }
    
    var isConverting: Bool {
        return !converterHandler.isConvertingEnd
    }
    
    // MARK: - Files manager
    
    // MARK: - Files operation
    
    func download(data: [FolderModel], for device: VirtualDevice) {
        fileOperationsService.downloadFolders(folders: data, for: device)
    }
    
    func subscribeOnDonwloadingProgress(fileHash: String, callback: @escaping DownloadingProgressCallback ) {
        fileOperationsService.subscribeOnDonwloadingProgress(fileHash: fileHash, callback: callback)
    }
    
    func subscribeOnDownloadingFinish(fileHash: String, callback: @escaping DownloadingFinishCallback ) {
        fileOperationsService.subscribeOnDownloadingFinish(fileHash: fileHash, callback: callback)
    }
    
    func isDownloading(hash: String) -> Bool {
        return fileOperationsService.isDownloading(hash: hash)
    }
    
    func isInDownloadingQueue(hash: String) -> Bool {
        return fileOperationsService.isInDownloadingQueue(hash: hash)
    }
    
    func stopDownloading() {
        fileOperationsService.stopDownloading()
    }
    
    func cancelDownload(by hash: String) {
        fileOperationsService.cancelDownload(by: hash)
    }
    
    func deleteFiles(for folders: [(folder: FolderModel, withFolder: Bool)],
                     device: VirtualDevice,
                     location: FilesDataSource.FileLocation,
                     progress: @escaping FileOperationsService.DeleteProgress,
                     finish: @escaping FileOperationsService.DeleteFinish) {
        fileOperationsService.deleteFiles(folders: folders, device: device, location: location, progress: progress, finish: finish)
    }
    
    func resumeDownloading() {
        fileOperationsService.resumeDownloading()
    }
    
    func resetOperationsService() {
        fileOperationsService = FileOperationsService()
    }
    
}

extension FilesManager {
    func saveDemoConvertedFile(path: Path, fileName: String) {
        if let device = SocketManager.connectedDevice {
            if let demoFolder = getLocalFoldersFor(device: device).last {
                let videoFile = File<Data>(path: path)
                let convertedFile = FileModel(name: "demo_video.mov",
                                              size: Int(Int64(videoFile.size ?? 0)),
                                              date: Date(),
                                              path: path.rawValue)
                let aviFile = demoFolder.files.filter { (file) -> Bool in
                    file.name.contains("avi") }.first!
                
                let pathToSave = defaultDir + "/\(device.deviceString)/\(demoFolder.name)/demo_video.mov.fileinfo"
                let pathToAviFile = defaultDir + "/\(device.deviceString)/\(demoFolder.name)/\(aviFile.name + ".fileinfo")"
                
                do {
                    try File<Data>(path: pathToSave).write(convertedFile.fileToData)
                    try pathToAviFile.deleteFile()
                } catch {
                    debugPrint("Fail save model")
                }
            }
        }
    }
    
    func saveModelInfoFor(converted file: FileModel) {
        let pathForLocalInfoFile = Path(file.path! + ".fileinfo")
        
        do {
            try File<Data>(path: pathForLocalInfoFile).write(file.fileToData)
        } catch {
            debugPrint("Fail save model")
        }
    }
    
    func saveModelInfoFor(folder model: FolderModel, file: FileModel) {
        guard let device = SocketManager.connectedDevice ?? storeManager.devices.first else {
            Logging.log(message: "[File manager ERROR] - Save model info device not exist")
            return
        }
        
        let fileNameInfo = file.name + ".fileinfo"
        let folderPath = defaultDir + "/" + device.deviceString + "/" + model.name
        
        saveModelInfoFor(folder: model)
        
        let pathForLocalInfoFile = folderPath + "/" + fileNameInfo
        
        do {
            try File<Data>(path: pathForLocalInfoFile).write(file.fileToData)
        } catch {
            debugPrint("Fail save model")
        }
    }
    
    func saveModelInfoFor(folder model: FolderModel) {
        let device = SocketManager.connectedDevice ?? storeManager.devices.first!
        let folderNameInfo = model.name + ".folderinfo"
        let pathForLocalInfoFile = defaultDir + "/" + device.deviceString + "/" + folderNameInfo
        let foldetPath = defaultDir + "/" + device.deviceString + model.name
        
        _ = try? foldetPath.createDirectory()
        _ = try? File<Data>(path: pathForLocalInfoFile).write(model.folderToData)
    }
    
    func pathToFile(_ file: FileModel, folder: FolderModel, device: VirtualDevice) -> Path {
        guard device.info.sku != "00000" else {
            if file.name.contains("photo") {
                return Path(Bundle.main.path(forResource: "demo_scr", ofType: ".jpg", inDirectory: nil)!)
            }
            if file.name.contains("mov") {
                return Path(Bundle.main.path(forResource: "demo_vid", ofType: ".mov", inDirectory: nil)!)
            }
            return Path(Bundle.main.path(forResource: "demo_vid", ofType: ".avi", inDirectory: nil)!)
        }
        
        return defaultDir + "/" + device.deviceString + "/" + folder.name + "/" + file.name
    }
    
    func pathToLocalFile(_ file: FileModel, folder: FolderModel, device: VirtualDevice) -> Path {
        return defaultDir + "/" + device.deviceString + "/" + folder.name + "/" + file.name
    }
    func pathToLocalInfoFile(_ file: FileModel, folder: FolderModel, device: VirtualDevice) -> Path {
        let localFilePath = pathToLocalFile(file, folder: folder, device: device).rawValue + ".fileinfo"
        return Path(localFilePath)
    }
    func pathToLocalFolder(_ folder: FolderModel, device: VirtualDevice) -> Path {
        return defaultDir + "/" + device.deviceString + "/" + folder.name
    }
    func pathToLocalInfoFolder(_ folder: FolderModel, device: VirtualDevice) -> Path {
        let localFolderPath = pathToLocalFolder(folder, device: device).rawValue + ".folderinfo"
        return Path(localFolderPath)
    }
}

// MARK: - Encoding files
extension FilesManager {
    func commitConvertedFile(path: Path) {
        let fileName = path.fileName.components(separatedBy: ".").first! + ".mov"
        let localFilePath = path.parent + fileName
        let videoFile = File<NSData>(path: localFilePath)
        let localFile = FileModel(name: fileName, size: Int(Int64(videoFile.size ?? 0)), date: Date())
        localFile.path = localFilePath.rawValue
        
        if SocketManager.isConnection(.demo), SocketManager.isConnected, fileName.contains("demo") {
            self.saveDemoConvertedFile(path: localFilePath, fileName: fileName)
        } else {
            let fileInfo = Path(path.rawValue + ".fileinfo")
            try? path.deleteFile()
            try? fileInfo.deleteFile()
            self.saveModelInfoFor(converted: localFile)
        }
    }
    
    func commitFile(path: Path) {
        let localFolder = FolderModel(name: "local")
        let videoFile = File<NSData>(path: path)
        let localFile = FileModel(name: path.fileName, size: Int(Int64(videoFile.size ?? 0)), date: Date())
        
        self.saveModelInfoFor(folder: localFolder, file: localFile)
    }
    
    func pathToLocalFile(_ name: String, ext: String) -> Path {
        let device = SocketManager.connectedDevice ?? storeManager.devices.first
        let nameIndex = storeManager.currentLocalFileNumber
        let fileName = name + "_" + String(nameIndex) + "." + ext
        let path = defaultDir + device!.deviceString + "/local/" + fileName
        
        let folder = FolderModel(name: "local")
        if !folder.isLocal {
            saveModelInfoFor(folder: folder)
        }
        
        return path
    }
    
    // TODO: Work this
    func startConvert(with file: FileModel,
                      sku: String,
                      progress: @escaping (Double) -> Void, completion: @escaping () -> () ) {
        Logging.log(message: "[Files] Start converting")
        converterHandler.currentFile = file
        converterHandler.finished = completion
        
        streamService = nil
        streamService = StreamService(mode: .convert(path: file.path!, sku: sku,
                                                     converterHandler,
                                                     callback: { [weak self] width, height, length, fps in
                                                        Logging.log(message: "[Files] Converting callback called")
                                                        self?.startConverting(with: (width, height),
                                                                              progress: progress,
                                                                              totalTime: length,
                                                                              fps: fps,
                                                                              for: file)
                                                        self?.streamService?.start()
        }))
        
        streamService?.vlcMediaPlayer?.delegate = converterHandler
    }
    
    func startConverting(with resolution: (Int, Int),
                         progress: @escaping (Double) -> Void,
                         totalTime: Int, fps: Int, for file: FileModel) {
        endEncoding() { [weak self] in
            let resolution = SocketManager.isConnection(.demo) ? (320, 640) : resolution
            let path = Path(file.path!)
            let filePath = Path(path.rawValue.replacingOccurrences(of: "avi", with: "mov"))
            self?.videoEncoder = VideoEncoder(path: filePath.url,
                                             resolution: resolution,
                                             isStream: false,
                                             totalTime: totalTime,
                                             fps: fps,
                                             progress: progress)
        }
    }
    
    func startEncodingWithSize(_ resolution: (Int, Int)) {
        endEncoding() { [weak self] in
            if let path = self?.pathToLocalFile("video", ext: "mov").url {
                self?.videoEncoder = VideoEncoder(path: path, resolution: resolution)
            }
        }
    }
    
    func saveSnapshot(_ image: UIImage?) {
        guard let image = image else {
            return
        }
        
        let data = image.jpegData(compressionQuality: 1)
        let path = pathToLocalFile("img", ext: "jpg")
        let file = File<Data>(path: path)
        
        _ = try! file.create()
        _ = try! file.write(data!)
        
        self.commitFile(path: path)
        storeManager.currentLocalFileNumber += 1
    }
    
    func encodeFrame(_ frame: Data, pts: Int) {
        videoEncoder?.encodeFrame(frame: frame, pts: pts)
    }
    
    func encodeAudio(data: Data, channels: Int, rate: Int, samples: Int, bps: Int, pts: Int) {
        videoEncoder?.encodeAudio(data: data, channels: channels, rate: rate, samples: samples, bps: bps, pts: pts)
    }
    
    func endEncoding(completion: (() -> ())? = nil ) {
        Logging.log(message: "[CONVERT] End encoding")
        
        guard let _ = videoEncoder else {
            Logging.log(message: "[CONVERT] Video encoder empty")
            completion?()
            return
        }
        SVProgressHUD.show()
        videoEncoder?.endSession { [weak self] error in
            if let path = self?.pathToLocalFile("video", ext: "mov"), !SocketManager.isConnection(.demo) {
                self?.commitFile(path: path)
                self?.videoEncoder = nil
                self?.storeManager.currentLocalFileNumber += 1
            }
            SVProgressHUD.dismiss()
            completion?()
        }
    }
    
    func endConverting(completion: (() -> ())? = nil ) {
        Logging.log(message: "[CONVERT] End converting")
        
        guard let _ = videoEncoder else {
            Logging.log(message: "[CONVERT] Video encoder empty")
            completion?()
            return
        }
        
//        SVProgressHUD.show()
        videoEncoder?.endSession { [weak self] error in
            Logging.log(message: "[CONVERT] End session completion")
            if let path = self?.converterHandler.currentFile?.path {
                self?.commitConvertedFile(path: Path(path))
            }
            
            self?.videoEncoder = nil
            self?.streamService?.stop()
//            SVProgressHUD.dismiss()
            completion?()
        }
    }
    
    func cancellConverting() {
        Logging.log(message: "[CONVERT] Cancel converting")
        streamService?.stop()
        converterHandler.cancelConverting()
        videoEncoder?.cancellSession()
        self.videoEncoder = nil
    }
}

extension FilesManager {
    func getDeviceRemoteFolders(callback: @escaping FoldersRequestCallback) {
        SocketManager.send(.getFoldersList).response { model, error in
            if let deviceFolders = model as? DeviceFoldersModel {
                self.getFilesFor(folders: deviceFolders.folders, readyFolders: [], callback: { (folders) in
                    var resultFolders = folders
                    if let device = SocketManager.connectedDevice {
                        self.updateFolders(folders: &resultFolders, from: self.getLocalFoldersFor(device: device))
                    }
                    callback(resultFolders)
                })
            } else if let _ = error {
                callback([])
            }
            
        }
    }
        
        
    private func getFilesFor(folders: [FolderModel], readyFolders: [FolderModel] , callback: @escaping FoldersRequestCallback) {
        guard folders.count > 0 else {
            callback(readyFolders)
            return
        }
        
        var leftFolders = folders
        var currentFolder = leftFolders.removeFirst()
        SocketManager.send(.getFilesList, params: ["path":"1:/\(currentFolder.name)"]).response { response, error in
            if let deviceFileModel = response as? DeviceFilesModel {
                currentFolder.files.append(contentsOf: deviceFileModel.files)
                var resultFolders = readyFolders
                resultFolders.append(currentFolder)
                self.getFilesFor(folders: leftFolders, readyFolders: resultFolders, callback: callback)
            }
        }
    }
    
    func updateFolders(folders: inout [FolderModel], from local: [FolderModel]) {
        local.forEach { (localFolder) in
            if let folderIndex = folders.firstIndex(of: localFolder) {
                folders[folderIndex].path = localFolder.path
                localFolder.files.forEach({ (localFile) in
                    if let fileIndex = folders[folderIndex].files.firstIndex(of: localFile) {
                        // Merge file with remote for converted files
                        folders[folderIndex].files[fileIndex].name = localFile.name
                        folders[folderIndex].files[fileIndex].size = localFile.size
                        folders[folderIndex].files[fileIndex].date = localFile.date
                        folders[folderIndex].files[fileIndex].path = localFile.path
                    } else {
                        folders[folderIndex].files.append(localFile)
                    }
                })
            } else {
                folders.append(localFolder)
            }
        }
        folders = folders.map { (folder) -> FolderModel in
            var resultFolder = folder
            resultFolder.files = folder.files.sorted(by: { (lhs, rhs) -> Bool in
                if let lhsNumbers = lhs.nameWithoutType.getIntNumbers(), let rhsNumbers = rhs.nameWithoutType.getIntNumbers() {
                    return lhsNumbers > rhsNumbers
                }
                return lhs.name > rhs.name
            })
            return resultFolder
        }
        folders = folders.sorted(by: { (lhs, rhs) -> Bool in
            lhs.name > rhs.name
        })
        cacheFolders = folders
    }
    
    func update(in folders: inout [FolderModel], by folder: FolderModel, and file: FileModel) {
        if let folderIndex = folders.firstIndex(of: folder) {
            let remoteFolder = folders[folderIndex]
            folders[folderIndex].path = folder.path
            if let fileIndex = remoteFolder.files.firstIndex(of: file) {
                remoteFolder.files[fileIndex].path = file.path
            }
        }
    }
    
    func updateByLocal(folders: inout [FolderModel], device: VirtualDevice) {
        let localFolders = getLocalFoldersFor(device: device)
        
        folders = folders.compactMap { (folder) -> FolderModel? in
            var resultFolder = folder
            
            if let index = localFolders.firstIndex(of: resultFolder) {
                resultFolder.path = localFolders[index].path
                
                let localFiles = localFolders[index].files
                resultFolder.files = resultFolder.files.compactMap({ (file) -> FileModel? in
                    if let index = localFiles.firstIndex(of: file) {
                        file.name = localFiles[index].name
                        file.size = localFiles[index].size
                        file.date = localFiles[index].date
                        file.path = localFiles[index].path
                        return file
                    } else if file.isRemote {
                        file.path = nil
                        return file
                    }
                    return nil
                })
                return resultFolder
            } else if resultFolder.isRemote {
                resultFolder.path = nil
                resultFolder.files = resultFolder.files.compactMap({ (file) -> FileModel? in
                    file.path = nil
                    return file
                })
                return resultFolder
            }
            
            return nil
        }
    }
    
    func getLocalFoldersFor(device model: VirtualDevice) -> [FolderModel] {
        let deviceFolder = defaultDir + "/\(model.deviceString)/"
        
        let infoFoldersPaths = deviceFolder.find(searchDepth: 1) { path in
            path.pathExtension == "folderinfo"
        }
        var fetchedLocalFolders: [FolderModel] = []
        infoFoldersPaths.forEach() { folderInfoPath in
            NSKeyedUnarchiver.setClass(NSClassFromString("_TtCV12StreamVision11FolderModel17FolderModelHelper"), forClassName: "StreamVision.FolderModel.FolderModelHelper")
            NSKeyedUnarchiver.setClass(NSClassFromString("_TtCV12StreamVision11FolderModel17FolderModelHelper"), forClassName: "_TtCV16StreamVision_Dev11FolderModel17FolderModelHelper")
            if let unarcheviedFolder = NSKeyedUnarchiver.unarchiveObject(withFile: folderInfoPath.rawValue) as? FolderModel.FolderModelHelper {
                var folderModel = FolderModel(folderModelHelper: unarcheviedFolder)
                folderModel.path = (folderInfoPath.parent + "/\(unarcheviedFolder.name)").rawValue
                fetchedLocalFolders.append(folderModel)
            }
        }
        
        for (index, folder) in fetchedLocalFolders.enumerated() {
            fetchedLocalFolders[index].files = getLocalFilesFor(device: model, folder: folder)
        }
        fetchedLocalFolders = fetchedLocalFolders.sorted(by: { (lhs, rhs) -> Bool in
            lhs.name > rhs.name
        })
        return fetchedLocalFolders
    }
    
    func getLocalFilesFor(device model: VirtualDevice, folder: FolderModel) -> [FileModel] {
        let deviceFolder = defaultDir + "/\(model.deviceString)/\(folder.name)"
        
        let infoFilePaths = deviceFolder.find(searchDepth: 2) { path in
            path.pathExtension == "fileinfo"
        }
        var files: [FileModel] = []
        infoFilePaths.forEach() { fileInfoPath in
            if let unarcheviedFile = NSKeyedUnarchiver.unarchiveObject(withFile: fileInfoPath.rawValue) as? FileModel {
                if unarcheviedFile.name.contains("demo") {
                    unarcheviedFile.path = pathToFile(unarcheviedFile, folder: folder, device: model).rawValue
                } else {
                    unarcheviedFile.path = (fileInfoPath.parent + "/\(unarcheviedFile.name)").rawValue
                }
                files.append(unarcheviedFile)
            }
        }
        files = files.sorted(by: { (lhs, rhs) -> Bool in
            if let lhsNumbers = lhs.nameWithoutType.getIntNumbers(), let rhsNumbers = rhs.nameWithoutType.getIntNumbers() {
                return lhsNumbers > rhsNumbers
            }
            return lhs.name > rhs.name
        })
        return files
    }
}

extension FilesManager {
        func firmwareExistFromDevice(_ device: VirtualDevice) -> Bool {
            return pathToFirmwareFromDevice(device).exists
        }
    
        func pathToFirmwareFromDevice(_ device: VirtualDevice) -> Path {
            if !defaultPathToFirmware.exists {
                do {
                    try defaultPathToFirmware.createDirectory()
                } catch {
                    debugPrint("Dir exist")
                }
            }
            return Path(defaultPathToFirmware.rawValue + "\(device.info.name)_\(device.info.serial).firmware")
        }
}

