//
//  FilesDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/21/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit
import SLExpandableTableView
import SVProgressHUD
import FileKit
import UserNotifications

extension FilesDataSource {
    enum FileLocation {
        case mixed
        case remote
        case local
        case none
    }
    
    enum FileFilter {
        case mixed
        case video
        case photo
        case none
    }
    
    struct FileSelection {
        var isSelected: Bool
    }
    
    struct FolderSelection {
        var isSelected: Bool
        var fileSelections: [FileSelection]
    }
}

class FilesDataSource: NSObject, SLExpandableTableViewDatasource, SLExpandableTableViewDelegate {
    // Types
    typealias DownloadingCompletion = (_ currentFile: FileModel, _ totalProgress: Float) -> ()
    typealias DeletingCompletion = (_ currentFile: FileModel, _ totalProgress: Float) -> ()
    typealias FileDeleteConfirmationCallback = (_ cell: UITableViewCell,
                                                _ isDownloading: Bool,
                                                _ fileLocation: FileModel.FileLocation) -> ()
    typealias DetailsCompletion = (_ currentFile: FileModel) -> ()
    typealias ShowFileCompletion = (_ file: FileModel, _ path: String) -> ()
    typealias SelectionCompletion = (_ count: Int) -> ()
    typealias WarningCompletion = () -> ()
    typealias ShareCompletion = (_ cell: UITableViewCell) -> ()
    typealias BatteryWarningCompletion = (_ percent: Float) -> ()
    typealias ConvetWillStartCompletion = (_ callback: @escaping (_ isCanceled: Bool) -> Void) -> Void
    typealias WarningConvertingInProcessCompletion = (_ file: FileModel, _ callback: @escaping (_ isCanceled: Bool) -> () ) -> ()
    // Callbacks
    var downloadingCompletion: DownloadingCompletion?
    var deletingCompletion: DeletingCompletion?
    var deleteConfirmationCallback: FileDeleteConfirmationCallback?
    var shareFileCompletion: ShareCompletion?
    var showFileCompletion: ShowFileCompletion?
    var detailsCompletion: DetailsCompletion?
    var selectionCompletion: SelectionCompletion?
    var warningCompletion: WarningCompletion?
    var convertingInProcessDeleteCompletion: WarningConvertingInProcessCompletion?
    var convertingInProcessConvertCompletion: WarningConvertingInProcessCompletion?
    var convertingBatteryLowCompletion: BatteryWarningCompletion?
    var convertWillStartCompletion: ConvetWillStartCompletion?
    
    // Variables
    weak var tableView: SLExpandableTableView!
    
    var device: VirtualDevice!
    var data: [FolderModel] = []
    var cacheData: [FolderModel] = []
    var selectionStore: [FolderSelection] = []
    var isChekable: Bool = false {
        didSet {
            deselectAll()
            tableView.allowsMultipleSelection = isChekable
            tableView.reloadDataAndResetExpansionStates(false)
        }
    }
    var filterLocation: FileLocation = .mixed {
        didSet {
            reloadData(folders: cacheData, isResetSections: false)
        }
    }
    var filterType: FileFilter = .mixed {
        didSet {
            reloadData(folders: cacheData, isResetSections: false)
        }
    }
    
    init(tableView: SLExpandableTableView, device: VirtualDevice) {
        super.init()
        
        self.tableView = tableView
        self.device = device
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 140
    }
    
    // TODO: THEREAD CHECK
    func reloadData(device: VirtualDevice, isResetSections: Bool = false) {
        self.device = device
        if SocketManager.isDeviceConnected(device) {
            if FilesManager.shared.isDownloading {
                DispatchQueue.main.async { [weak self] in
                    self?.reloadData(folders: FilesManager.shared.cacheFolders,
                                     isResetSections: isResetSections)
                }
            } else {
                SVProgressHUD.show()
                FilesManager.shared.getDeviceRemoteFolders { (folders) in
                    DispatchQueue.main.async { [weak self] in
                        self?.reloadData(folders: folders, isResetSections: isResetSections)
                        if FilesManager.shared.isCanBeResumed {
                            FilesManager.shared.resumeDownloading()
                        }
                        SVProgressHUD.dismiss()
                    }
                }
            }
        } else {
            reloadData(folders: FilesManager.shared.getLocalFoldersFor(device: device), isResetSections: isResetSections)
        }
    }
    
    fileprivate func reloadData(folders: [FolderModel], isResetSections: Bool) {
        self.cacheData = folders
        self.data = folders
        self.filterData()
        self.selectionStore = data.map({ (folder) -> FolderSelection in
            return FolderSelection(isSelected: false,
                                   fileSelections: folder.files.map({ (file) -> FileSelection in
                return FileSelection(isSelected: false)
            }))
        })
        tableView.reloadDataAndResetExpansionStates(isResetSections)
        if isResetSections {
            tableView.layoutIfNeeded()
            tableView.contentOffset = .zero
        }
        
        if isResetSections {
            let folderToOpen = 1 + (folders.first?.name == "local" ? 1 : 0)
            for index in 0...folderToOpen {
                if index < data.count {
                    if !tableView.isSectionExpanded(index) && tableView.canExpandSection(UInt(index)) {
                        tableView.expandSection(index, animated: true)
                    }
                }
            }
        }
    }
    
    func filterData() {
        self.data = self.data.compactMap({ (folder) -> FolderModel? in
            var resultFolder = folder
            
            resultFolder.files = folder.files.compactMap({ (file) -> FileModel? in
                
                switch filterType {
                case .mixed: return filterByLocation(file: file)
                case .video:
                    if file.fileType == .video || file.fileType == .converted {
                        return filterByLocation(file: file)
                    }
                case .photo:
                    if file.fileType == .image {
                        return filterByLocation(file: file)
                    }
                case .none: return nil
                }
                
                return nil
            })
            
            return resultFolder
        })
    }
    
    private func filterByLocation(file: FileModel) -> FileModel? {
        switch self.filterLocation {
        case .local:
            if file.isLocal {
                return file
            }
        case .remote:
            if file.isRemote {
                return file
            }
        case .mixed: return file
        case .none: return nil
        }
        return nil
    }
    
    // MARK: -
    func tableView(_ tableView: SLExpandableTableView!, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: SLExpandableTableView!, needsToDownloadDataForExpandableSection section: Int) -> Bool {
        return false
    }
    
    func tableView(_ tableView: SLExpandableTableView!,
                   expandingCellForSection section: Int) -> (UITableViewCell & UIExpandingTableViewCell)! {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileManagerFolderCell") as! FileManagerFolderCell
        let folder = data[section]
        cell.setup(with: folder,
                   isChekable: isChekable,
                   isChecked: selectionStore[section].isSelected,
                   section: section) { (section, isSelected) -> Bool in
                    self.selectFolder(in: section)
                    //tableView.reloadDataAndResetExpansionStates(false)
                    // TODO: Callback to out
                    return !isSelected
        }
        
        return cell
    }
    
    func tableView(_ tableView: SLExpandableTableView!, downloadDataForExpandableSection section: Int) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].files.count + 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileManagerFileCell") as! FileManagerFileCell
        let folder = data[indexPath.section]
        let file = folder.files[indexPath.row - 1]
        let hash = device.deviceString + folder.name + file.name
        
        cell.cellHash = hash
        let fileSelection = isChekable ?
            selectionStore[indexPath.section].fileSelections[indexPath.row - 1].isSelected : false
        let isDownloading = FilesManager.shared.isInDownloadingQueue(hash: hash)
        cell.setup(with: file,
                   isChekable: isChekable,
                   isChecked: fileSelection,
                   isDownloading: isDownloading,
                   isConverting: FilesManager.shared.isFileConverting(file),
                   section: indexPath.section, row: indexPath.row,
                   selectionCallback: { (section, row, isSelected) -> Bool in
                    self.selectFile(in: section, row: row)
                    // TODO: Callback to out
                    return !isSelected
        }, downloadingCallback: { section, row in
            self.deselectAll()
            self.selectFile(in: section, row: row)
            self.downloadSelected()
            UIApplication.shared.isIdleTimerDisabled = true
        }, detailsCallback: { section, row in
            self.deselectAll()
            self.selectFile(in: section, row: row)
        }, showCallback: { section, row in
            self.deselectAll()
            self.selectFile(in: section, row: row)
            if let file = self.selectedFile() {
                if let path = file.path {
                    self.showFileCompletion?(file, path)
                }
            }
        }, cancellCallback: { section, row in
            FilesManager.shared.cancelDownload(by: hash)
            self.tableView.reloadDataAndResetExpansionStates(false)
        }, shareCallback: { section, row in
            if file.fileType == .image || file.fileType == .converted {
                self.shareFile(for: section, row: row)
                return
            }
            
            self.convertWillStartCompletion?() { isCanceled in
                if !isCanceled {
                    _ = self.convert(file: file,
                                     section: indexPath.section,
                                     row: indexPath.row,
                                     progress: self.convertProgress(tableView: tableView, indexPath: indexPath))
                }
            }
        })
        
            FilesManager.shared.subscribeOnDonwloadingProgress(fileHash: hash) { (progress, downloadingHash) in
                if progress == 0 || cell.cellHash != downloadingHash {
                    cell.progressLabel.text = "FileManager.ManageDownloads.In.Queue".localized()
                    cell.progressView.progress = 0
                } else {
                    let size = cell.file.size
                    let completeSize = Int( Float(size) * progress)
                    cell.progressLabel.text = "\(completeSize.toStringUniversalMemoryUnit()) of \(size.toStringUniversalMemoryUnit())"
                    cell.progressView.progress = progress
                }
            }
        
            FilesManager.shared.subscribeOnDownloadingFinish(fileHash: hash) { (folder, file, downloadingHash, error) in
                Logging.log(message: "[FM] File finished and cell update for hash: \(downloadingHash) file: \(file.name) foldr: \(folder.name)")
                
                if error == nil {
                    if FilesManager.shared.isDownloading {
                        FilesManager.shared.update(in: &FilesManager.shared.cacheFolders,
                                                   by: folder,
                                                   and: file)
                    } else {
                        FilesManager.shared.update(in: &self.data,
                                                   by: folder,
                                                   and: file)
                    }
                }
                
                if let _ = tableView.visibleCells.firstIndex(of: cell) {//, let indexPath = tableView.indexPath(for: cell) {
                    Logging.log(message: "[FM] Cell is visible so should Reload Table")
                    DispatchQueue.main.async { [weak self] in
                        self?.tableView.reloadDataAndResetExpansionStates(false)
                    }
                }
        }
        
        // Setup cell swipe actions
        if !isChekable && !isDownloading {
            let shareButton = MGSwipeButton(title: "FileManager.Actions.Share".localized(),
                                            backgroundColor: .orange) { _ in
                                                self.shareFile(for: indexPath.section, row: indexPath.row)
                return true
            }
            
            let deleteButton = MGSwipeButton(title: "FileManager.Actions.Delete".localized(),
                                             backgroundColor: .red) { _ in
                                                if FilesManager.shared.isFileConverting(file) {
                                                    self.convertingInProcessDeleteCompletion?(file) { isCanceled in
                                                        if !isCanceled {
                                                            FilesManager.shared.cancellConverting()
                                                            self.deleteFile(for: indexPath.section, row: indexPath.row)
                                                        }
                                                    }
                                                    return true
                                                }
                                                self.deleteFile(for: indexPath.section, row: indexPath.row)
                return true
            }
            
            let infoButton = MGSwipeButton(title: "FileManager.Info.GetInfo".localized(),
                                           backgroundColor: .lightGray) { _ in
                                            self.deselectAll()
                                            self.selectFile(in: indexPath.section, row: indexPath.row)
                                            if let file = self.selectedFile() {
                                                self.detailsCompletion?(file)
                                            }
                return true
            }
            
            let convertButton = !FilesManager.shared.isFileConverting(file) ?
                MGSwipeButton(title: "FileManager.Actions.Share".localized(),
                              backgroundColor: .orange) { _ in
                                if file.fileType == .image || file.fileType == .converted {
                                    self.shareFile(for: indexPath.section, row: indexPath.row)
                                    return true
                                }
                                
                                self.convertWillStartCompletion?() { isCanceled in
                                    if !isCanceled {
                                        _ = self.convert(file: file,
                                                         section: indexPath.section,
                                                         row: indexPath.row,
                                                         progress: self.convertProgress(tableView: tableView, indexPath: indexPath))
                                    }
                                }
                                return true
                } :
                MGSwipeButton(title: "FileManager.Info.StopConvert".localized(),
                              backgroundColor: .rgba(0, 118, 255, 1)) { _ in
                                FilesManager.shared.cancellConverting()
                                self.reloadData(device: self.device)
                                return true
            }
            
            cell.rightButtons = [deleteButton]
            if file.isLocal && !file.name.contains("avi") {
                cell.rightButtons.append(shareButton)
            }
            if file.isLocal && file.name.contains("avi") {
                cell.rightButtons.append(convertButton)
            }
            cell.rightButtons.append(infoButton)
            
            cell.rightSwipeSettings.transition = .border
        } else {
            cell.rightButtons = []
        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                cell.rightSwipeSettings.offset = 10
            default: break
            }
        }
    
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        if isChekable {
            selectFile(in: indexPath.section, row: indexPath.row)
        } else {
            if tableView.visibleCells.count > 0, let cell = tableView.cellForRow(at: indexPath) as? MGSwipeTableCell {
                cell.showSwipe(.rightToLeft, animated: true)
            }
        }
    }

}

extension FilesDataSource {
    func file(by section: Int, row: Int) -> FileModel {
        return data[section].files[row - 1]
    }
    
    func selectedFilesLocation() -> FileModel.FileLocation {
        let folders: [FolderModel] = selectedFiles()
        let remoteCount = folders.flatMap { folder in
            folder.files.filter({ (file) -> Bool in
                file.location == .remote
            })
        }.count
        
        let localCount = folders.flatMap { folder in
            folder.files.filter({ (file) -> Bool in
                file.location == .local
            })
        }.count
        
        let bothCount = folders.flatMap { folder in
            folder.files.filter({ (file) -> Bool in
                file.location == .both
            })
        }.count
        
        return {
            switch (remoteCount, localCount, bothCount) {
            case let (r, l, b) where r > 0 && l == 0 && b == 0:
                return .remote
            case let (r, l, b) where l > 0 && r == 0 && b == 0:
                return .local
            default:
                return .both
            }
        }()
    }
    
    func selectedFiles() -> [FolderModel] {
        var folders: [FolderModel] = []
        for (index, folderSelections) in selectionStore.enumerated() {
            let folder = data[index]
            if folderSelections.isSelected {
                folders.append(folder)
            } else {
                var emptyFolder: FolderModel = FolderModel(from: folder)
                for (index, fileSelections) in folderSelections.fileSelections.enumerated() {
                    if fileSelections.isSelected {
                        emptyFolder.files.append(folder.files[index])
                    }
                }
                if emptyFolder.files.count > 0 {
                    folders.append(emptyFolder)
                }
            }
        }
        return folders
    }
    
    func selectedFile() -> FileModel? {
        var file: FileModel?
        for (index, folderSelections) in selectionStore.enumerated() {
            let folder = data[index]
            for (index, fileSelections) in folderSelections.fileSelections.enumerated() {
                if fileSelections.isSelected {
                    file = folder.files[index]
                }
            }
        }
        return file
    }
}

extension FilesDataSource {
    
    func shareFile(for section: Int, row: Int) {
        self.deselectAll()
        self.selectFile(in: section, row: row)
        if tableView.visibleCells.count > 0, let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) {
            self.shareFileCompletion?(cell)
        }
    }
    
    func deleteFile(for section: Int, row: Int) {
        self.deselectAll()
        let file = self.selectFile(in: section, row: row)
        if tableView.visibleCells.count > 0, let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) {
            self.deleteConfirmationCallback?(cell, FilesManager.shared.isDownloading, file.location)
        }
    }
    
    func convert(file: FileModel, section: Int, row: Int, progress: @escaping (Double) -> Void) -> Bool {
        let device = UIDevice.current
        device.isBatteryMonitoringEnabled = true
        
        if FilesManager.shared.isConverting {
            self.convertingInProcessConvertCompletion?(file) { [weak self] isCanceled in
                if !isCanceled {
                    self?.convertingBatteryLowCompletion?(device.batteryLevel * 100)
                    FilesManager.shared.cancellConverting()
                    self?.convertFile(for: section, row: row, progress: progress)
                }
            }
            return true
        } else {
            self.convertingBatteryLowCompletion?(device.batteryLevel * 100)
        }
        self.convertFile(for: section, row: row, progress: progress)
        return true
    }
    
    func convertFile(for section: Int, row: Int, progress: @escaping (Double) -> Void) {
        self.deselectAll()
        self.selectFile(in: section, row: row)
        if let file = self.selectedFile() {
            
            FilesManager.shared.startConvert(with: file, sku: device.info.sku , progress: progress) {
                if FilesManager.shared.isDownloading {
                    FilesManager.shared.updateByLocal(folders: &FilesManager.shared.cacheFolders, device: self.device)
                }
                self.reloadData(device: self.device)
                
                // After convert
                self.scheduleConvertNotification(file: file)
            }
        }
    }
    
    func scheduleConvertNotification(file: FileModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let content = UNMutableNotificationContent()
        content.title = "Notification.File.Converted.Title".localized()
//        content.subtitle = "Let's see how smart you are!"
        content.body = String(format: "Notification.File.Converted.Message".localized(), file.name)
        content.badge = 1
        content.categoryIdentifier = appDelegate.notificationIdentifier
        if let path = file.path {
            content.userInfo = ["filePath": path]
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        
        let requestIdentifier = "africaQuiz"
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            // handle error
            print("")
        })
    }
    
    func convertProgress(tableView: UITableView, indexPath: IndexPath) -> (Double) -> Void {
        return { progress in
            if tableView.visibleCells.count > 0, let cell = tableView.cellForRow(at: indexPath) as? FileManagerFileCell {
                cell.convertingProgressCallback(progress)
            }
        }
    }
    
    func downloadSelected() {
        let foldersForDownload = selectedFiles().map { (folder) -> FolderModel in
            var mutableFolder = FolderModel(from: folder)
            mutableFolder.files = folder.files.compactMap({ (file) -> FileModel? in
                return file.isLocal ? nil : file
            })
            return mutableFolder
        }
        FilesManager.shared.download(data: foldersForDownload, for: device)
        reloadData(device: self.device)
    }
    
    func deleteSelected(location: FileLocation) {
        let foldersWithFiles = selectedFiles()
        let foldersForDelete = foldersWithFiles.map { (folder) -> (folder: FolderModel, withFolder: Bool) in
            var isFolderSelected: Bool = false
            if let remoteFolderIndex = self.data.firstIndex(of: folder) {
                isFolderSelected = self.selectionStore[remoteFolderIndex].isSelected
            }
            var mutableFolder = FolderModel(from: folder)
            mutableFolder.files = folder.files.compactMap({ (file) -> FileModel? in
                if location == .remote {
                    if !file.isRemote && file.path != nil {
                        return nil
                    }
                } else if location == .local {
                    if !file.isLocal {
                        return nil
                    }
                }
                return file
            })
            return (folder: mutableFolder, withFolder: isFolderSelected)
        }
        
        if foldersForDelete.contains(where: {
            $0.folder.name == "img_0001" && $0.withFolder && $0.folder.isRemote
        }){
            warningCompletion?()
            return
        }
        
        FilesManager.shared.deleteFiles(for: foldersForDelete, device: device, location: location, progress: { (progress) in
            Logging.log(message: "Delete progress")
        }) { [weak self] error in
            Logging.log(message: "Delete finish")
            
            if let device = self?.device {
                DispatchQueue.main.async {
                    self?.reloadData(device: device)
                }
            }
        }
    }
}

extension FilesDataSource {
    func selectFolder(in section: Int) {
        let selection = !selectionStore[section].isSelected
        selectionStore[section].isSelected = selection
        for (index, _) in selectionStore[section].fileSelections.enumerated() {
            selectionStore[section].fileSelections[index].isSelected = selection
        }
        tableView.reloadDataAndResetExpansionStates(false)
    }
    
    @discardableResult
    func selectFile(in section: Int, row: Int) -> FileModel {
        let selection = !selectionStore[section].fileSelections[row - 1].isSelected
        selectionStore[section].fileSelections[row - 1].isSelected = selection
        if selectionStore[section].isSelected && !selection {
            selectionStore[section].isSelected = false
        }
        tableView.reloadDataAndResetExpansionStates(false)
        
        return data[section].files[row - 1]
    }
    
    func selectAll() {
        selectionStore = selectionStore.map({ (folderSelection) -> FolderSelection in
            return FolderSelection(isSelected: false,
                                   fileSelections: folderSelection.fileSelections.map { (fileSelection) -> FileSelection in
                                    return FileSelection(isSelected: true)
            })
        })
        tableView.reloadDataAndResetExpansionStates(false)
    }
    
    func deselectAll() {
        selectionStore = selectionStore.map({ (folderSelection) -> FolderSelection in
            return FolderSelection(isSelected: false,
                                   fileSelections: folderSelection.fileSelections.map { (fileSelection) -> FileSelection in
                                    return FileSelection(isSelected: false)
            })
        })
        tableView.reloadDataAndResetExpansionStates(false)
    }
}
