//
//  GPSAvatarView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/30/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit
import SDWebImage

class GPSAvatarView: UIView {
    private var avatarLabel: UILabel = UILabel()
    private var avatarImageView: UIImageView = UIImageView()
    var font: UIFont {
        set {
            avatarLabel.font = newValue
        }
        get {
            return avatarLabel.font
        }
    }
    var isWithLabel: Bool {
        return avatarImageView.image == nil
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    func setupView() {
        addSubview(avatarLabel)
        addSubview(avatarImageView)
        
        avatarLabel.translatesAutoresizingMaskIntoConstraints = false
        avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            avatarLabel.leftAnchor.constraint(equalTo: leftAnchor),
            avatarLabel.rightAnchor.constraint(equalTo: rightAnchor),
            avatarLabel.topAnchor.constraint(equalTo: topAnchor),
            avatarLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            avatarImageView.leftAnchor.constraint(equalTo: leftAnchor),
            avatarImageView.rightAnchor.constraint(equalTo: rightAnchor),
            avatarImageView.topAnchor.constraint(equalTo: topAnchor),
            avatarImageView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        
        // Setup avatar views
        layer.cornerRadius = frame.width / 2
        avatarImageView.layer.cornerRadius = frame.width / 2
        avatarImageView.clipsToBounds = true;
        
        avatarLabel.layer.cornerRadius = frame.width / 2
        avatarLabel.layer.masksToBounds = true
//        avatarLabel.backgroundColor = .rgba(22, 47, 66, 1)
        avatarLabel.backgroundColor = .rgba(22, 47, 66, 1)
        avatarLabel.textColor = .white
        avatarLabel.textAlignment = .center
        avatarLabel.font = UIFont(name: "Roboto-Bold", size: 37)
        avatarLabel.layer.borderWidth = 1
        
        // Setup view
        backgroundColor = .rgba(22, 47, 66, 1)
        layer.borderWidth = 1
        layer.borderColor = UIColor.rgba(118, 146, 168, 1).cgColor
    }
    
    func updateView() {
        avatarLabel.backgroundColor = .rgba(22, 47, 66, 1)
    }
    
    func update(_ text: String? = nil) {
        update(nil, text)
    }
    
    func update(url: URL? = nil) {
        update(nil, nil, url)
    }
    
    func update(url: URL? = nil, refresh: Bool) {
        update(nil, nil, url, refresh)
    }
    
    func update(_ image: UIImage? = nil,
                _ text: String? = nil,
                _ url: URL? = nil,
                _ refresh: Bool = false) {
        avatarImageView.isHidden = (image == nil) && (url == nil)
        avatarImageView.image = image
        if let avatarUrl = url {
            if !refresh {
                avatarImageView.sd_setImage(with: avatarUrl)
            } else {
                avatarImageView.sd_setImage(with: avatarUrl,
                                            placeholderImage: nil,
                                            options: .refreshCached)
            }
        }
        
        avatarLabel.isHidden = (text == nil)
        if let firstChar = text?.first {
            avatarLabel.text = "\(firstChar)"
        }
    }
}
