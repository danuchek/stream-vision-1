//
//  TextFieldWithValidation.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/31/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import UIView_Shake

class ValidationTextView: UIView, UITextFieldDelegate {
    private let kHeight: CGFloat = 40
    
    var sepparatorView: UIView!
    var textField: UITextField!
    var titleLabel: UILabel!
    var errorMessageLabel: UILabel!
    var errorPatterns: [String: String]? // Pattern, Error message
    
    @IBInspectable var rightImage: UIImage?
    @IBInspectable var rightActiveImage: UIImage?
    @IBInspectable var imageMode: CGFloat = 0.0
    @IBInspectable var isSecure: Bool = false
    @IBInspectable var length: Int = 10
    @IBInspectable var textFiledTag: Int {
        get {
            return textField.tag
        }
        set {
            textField.tag = newValue
        }
    }
    
    
    var text: String {
        get {
            return textField.text ?? ""
        }
        set {
            textField.text = newValue
        }
    }
    var title: String {
        get {
            return titleLabel.text ?? ""
        }
        set {
            titleLabel.text = newValue
        }
    }
    var titleFont: UIFont? {
        get {
            return titleLabel.font
        }
        set {
            titleLabel.font = newValue
        }
    }
    var textViewFont: UIFont? {
        get {
            return textField.font
        }
        set {
            textField.font = newValue
        }
    }
    var returnKeyType: UIReturnKeyType {
        get {
           return textField.returnKeyType
        }
        set {
            textField.returnKeyType = newValue
        }
    }
    var isErrorShown: Bool {
        return NSNumber(floatLiteral: Double(errorMessageLabel!.alpha)).boolValue
    }
    
    
    var shouldChange: ((String) -> Bool)?
    var shouldReturn: ((UITextField) -> Bool)?
    var shouldEndEditing: ((UITextField) -> Bool)?
    var didBeginEditing: ((UITextField) -> Void)?
    
    // MARK: - IBOutlets
    @IBOutlet weak var heightLayout: NSLayoutConstraint?
    
    // MARK: - ValidationTextView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        updateView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .clear
        
        textField = UITextField()
        textField.delegate = self
        addSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.trailingAnchor.constraint(equalTo: trailingAnchor),
            textField.leadingAnchor.constraint(equalTo: leadingAnchor),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
            ])
        
        titleLabel = UILabel()
        addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: textField.leadingAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: textField.topAnchor, constant: -10),
            ])
        
        errorMessageLabel = UILabel()
        addSubview(errorMessageLabel)
        errorMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            errorMessageLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 5),
            errorMessageLabel.trailingAnchor.constraint(equalTo: textField.trailingAnchor),
            errorMessageLabel.bottomAnchor.constraint(equalTo: textField.topAnchor),
            ])
        
        sepparatorView = UIView()
        addSubview(sepparatorView)
        sepparatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sepparatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            sepparatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            sepparatorView.topAnchor.constraint(equalTo: bottomAnchor),
            sepparatorView.heightAnchor.constraint(equalToConstant: 0.5)
            ])
        
        heightLayout?.constant = kHeight
        
        textField.font = UIFont(name: "Roboto-Regular", size: 13)
        textField.textColor = .white
        
        titleLabel.font = UIFont(name: "Roboto-Regular", size: 13)
        titleLabel.textColor = .rgba(42, 63, 79, 1)
        
        
        errorMessageLabel.font = UIFont(name: "Roboto-Regular", size: 13)
        errorMessageLabel.textColor = .rgba(255, 45, 71, 1)
        errorMessageLabel.alpha = 0
        errorMessageLabel.textAlignment = .right
        
        sepparatorView.backgroundColor = .rgba(200, 199, 204, 0.2)
    }
    
    func updateView() {
        textField.setRightImageMode(imageMode)
        
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(rightImage, for: .normal)
        rightButton.frameWidth(50)
        rightButton.frameHeight(50)
        textField.rightView = rightButton
        textField.autocapitalizationType = .none
        
        if isSecure {
            textField.isSecureTextEntry = isSecure
            rightButton.addTarget(self,
                                  action: #selector(secureButtonPressed(sender:)),
                                  for: .touchUpInside)
        } else {
            rightButton.addTarget(self,
                                  action: #selector(clearButtonPressed(sender:)),
                                  for: .touchUpInside)
        }
    }
    
    @objc private func secureButtonPressed(sender: UIButton) {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
        if textField.isSecureTextEntry {
            (textField.rightView as! UIButton).setImage(rightImage, for: .normal)
        } else {
            if let image = rightActiveImage {
                (textField.rightView as! UIButton).setImage(image, for: .normal)
            }
        }
        
        let temp = textField.text
        textField.text = " "
        textField.text = temp
        hideError()
    }
    
    @objc private func clearButtonPressed(sender: UIButton) {
        textField.text = ""
        hideError()
    }
    
    func showError(message: String) {
        errorMessageLabel.text = message
        self.errorMessageLabel.textColor = .rgba(255, 45, 71, 1)
        self.sepparatorView.backgroundColor = .rgba(255, 45, 71, 1)
        self.sepparatorView.alpha = 1
        
        shake(3, withDelta: 3, speed: 0.07)
        UIView.animate(withDuration: 0.7) {
            self.errorMessageLabel.alpha = 1
        }
    }
    
    func showWarning(message: String) {
        guard !isErrorShown else {
            return
        }
        
        hideError()
        errorMessageLabel.text = message
        errorMessageLabel.alpha = 1
        
        errorMessageLabel.textColor = .rgba(246, 168, 22, 1)
    }
    
    func hideError() {
        guard !errorMessageLabel.isHidden else {
            return
        }
        
        self.sepparatorView.backgroundColor = .rgba(200, 199, 204, 0.2)
        UIView.animate(withDuration: 0.7) {
            self.errorMessageLabel.alpha = 0
        }
    }
    
    func validate() -> Bool {
        hideError()
        guard let patterns = errorPatterns else {
            return true
        }
        
        var isValid = true
        for (_, object) in patterns.enumerated() {
            if let error = self.matches(for: object.key, in: self.text, error: object.value) {
                self.showError(message: error)
                isValid = false
                break
            }
        }
        return isValid
    }
    
    func validate(customPattern: [String:String]) -> Bool {
        hideError()
        var isValid = true
        for (_, object) in customPattern.enumerated() {
            if let error = self.matches(for: object.key, in: self.text, error: object.value) {
                self.showError(message: error)
                isValid = false
                break
            }
        }
        return isValid
    }
    
    private func matches(for regex: String, in text: String, error: String) -> String? {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text, range: NSRange(text.startIndex..., in: text))
            return results.map { String(text[Range($0.range, in: text)!]) }.count == 0 ? error : nil
        } catch let error {
            print("Invalid regex: \(error.localizedDescription)")
            return nil
        }
    }
    
    // MARK: - TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        hideError()
        didBeginEditing?(textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        hideError()
        let resultString: String = { text, char in
            var result: String = text
            if char == "" {
                result.removeLast()
            } else {
                result += char
            }
            return result
        }(text, string)
        
        guard let shouldChange = self.shouldChange else {
            return checkLenght(resultString)
        }
        return shouldChange(resultString) && checkLenght(resultString)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return shouldReturn?(textField) ?? false
    }
    
    func checkLenght(_ result: String) -> Bool {
        return !(result.count > length)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return shouldEndEditing?(textField) ?? true
    }
    
    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }

}
