//
//  GPSRegistrationButton.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/16/19.
//  Copyright © 2019 Ciklum. All rights reserved.
//

class GPSRegistrationButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        layer.cornerRadius = 5
        layer.borderWidth = 1
        layer.setBorderColorFromUIColor(.rgba(55,118,255,1))
    }

}
