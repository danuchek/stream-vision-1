//
//  ButtonWithActionBlock.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 2/1/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class ButtonWithActionBlock: UIButton {
    
    var isAutoSelect: Bool = true {
        didSet {
            if isAutoSelect {
                isSelectOnce = false
            }
        }
    }
    var isSelectOnce: Bool = false {
        didSet {
            if isSelectOnce {
                isAutoSelect = false
            }
        }
    }
    
    var actionBlock: ( (_ sender: ButtonWithActionBlock) ->Void )?

    func addAction(to controllEvent:UIControl.Event!, actionBlock: ((_ sender: ButtonWithActionBlock) -> Void)! ) {
        self.actionBlock = actionBlock
        addTarget(self, action: #selector(ButtonWithActionBlock.doActionBlock), for: controllEvent)
    }
    
    @objc func doActionBlock() {
        if let _ = actionBlock {
            self.isSelected = isAutoSelect ? !self.isSelected : isSelectOnce
            self.actionBlock!(self)
        }
    }

}
