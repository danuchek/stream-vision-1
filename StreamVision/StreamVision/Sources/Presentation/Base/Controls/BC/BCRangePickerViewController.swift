//
//  BCDoublePickerViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/14/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BCRangePickerViewController: BCSinglePickerViewController {
    
    override func updatePikcersFromLimitation() {
        let setting = currentSettings

        var leftValue = Double(Int(model.value))
        var rightValue = (model.value - leftValue) * 10 ^^ Double(setting.pattern.decimal)
        
        updateWithStep(leftValue: &leftValue, rightValue: &rightValue,
                       max: setting.limits.max, min: setting.limits.min,
                       decimal: setting.pattern.decimal)
        
        if rightValue > setting.limits.max {
           model.value = leftValue + (setting.limits.max * 0.1 ^^ Double(setting.pattern.decimal))
        } else if rightValue < setting.limits.min {
           model.value = leftValue + (setting.limits.min * 0.1 ^^ Double(setting.pattern.decimal))
        }

        if leftValue > rightValue {
            model.value = rightValue + (rightValue * 0.1 ^^ Double(setting.pattern.decimal))
        }
        
        updatePickerValues(animated: true)
    }
    
    override func didSelectUnitsInPicker(units: Int) {
        model.units = units
        let setting = currentSettings
        
        if var value = model.didSelectValue(model.value, units) {
            let parsedValue = "\(value)".split(separator: ".")
            let leftValue = Int("\(parsedValue.first!)")!
            let rightValue = Int("\(parsedValue.last!)")!
            
            if rightValue > Int(setting.limits.max) {
                value = Double("\(leftValue).\(Int(setting.limits.max))")!
            }
            
            if value < setting.limits.max && setting.pattern.decimal == 0 {
                model.value = round(value)
            } else {
                model.value = value
            }
        }
        updateViewBySetting()
    }
    
    private func updateWithStep(leftValue: inout Double, rightValue: inout Double, max: Double, min: Double, decimal: Int) {
        leftValue = round(leftValue / model.step) * model.step
        rightValue = round(rightValue / model.step) * model.step
        
        if rightValue > max {
            rightValue = floor(max / model.step) * model.step
        }
        
        model.value = leftValue + (rightValue * 0.1 ^^ Double(decimal))
    }
}
