//
//  BCSinglePickerViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/14/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BCValuePickerViewDataSource: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    var didSelectValue: (() -> ())?
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return 10
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    attributedTitleForRow row: Int,
                    forComponent component: Int) -> NSAttributedString? {
        
        return NSAttributedString(string: "\(row)", attributes: [.foregroundColor: UIColor.white])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        didSelectValue?()
    }
}

class BCUnitsPickerViewDataSource: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    var titlesData: [String] = []
    var didSelectUnits: ((_ units: Int) -> ())?
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return titlesData.count
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    attributedTitleForRow row: Int,
                    forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: titlesData[row], attributes: [.foregroundColor: UIColor.white])
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    viewForRow row: Int,
                    forComponent component: Int, reusing view: UIView?) -> UIView {
        let font = UIFont.systemFont(ofSize: 23.50)
        let label = UILabel(frame: CGRect(x: 0, y: 0,
                                          width: pickerView.frame.width,
                                          height: titlesData[row].height(withConstrainedWidth: pickerView.frame.width,
                                                                         font: font)))
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textColor = .white
        label.font = font
        label.text = titlesData[row]
        label.sizeToFit()
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        didSelectUnits?(row)
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        let font = UIFont.systemFont(ofSize: 23.50)
        var height: CGFloat = 0.0
        
        for title in titlesData {
            height = max(height,
                         title.height(withConstrainedWidth: pickerView.frame.width,
                                                      font: font))
        }
        return height
    }
}

class BCSinglePickerViewController: UIViewController {
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    private var stackView: UIStackView!
    
    private var pickerViewsAndDataSources: [(picker: UIPickerView, dataSource: BCValuePickerViewDataSource)] = []
    private var unitsPickerViewAndDataSourcess: (picker: UIPickerView, dataSource: BCUnitsPickerViewDataSource)?
    private var signPickerViewAndDataSourcess: (picker: UIPickerView, dataSource: BCUnitsPickerViewDataSource)?
    
    private var sepparatorLabel: UILabel = UILabel(frame: .zero)
    
    private let horisontalTopSepparatorView = UIView(frame: .zero)
    private let horisontalBottomSepparatorView = UIView(frame: .zero)
    
    var model: BCSinglePickerViewModel!
    var currentSettings: (limits: (max: Double, min: Double),
                          pattern: (integer: Int, decimal: Int), units: String) {
        return model.data[model.units]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupViewBySettings()
        updateUnits()
        localized()
        
        title = model.title
    }
    

    private func localized() {
        doneButton.title = "DevicesList.MyDevice.Done".localized()
        cancelButton.title = "General.Alert.Cancel".localized()
    }
    
    private func setupViewBySettings() {
        let setting = currentSettings
        
        if setting.pattern.integer > 0 {
            for _ in 1...(setting.pattern.integer + setting.pattern.decimal) {
                let pickerData = createValuePicker()
                pickerViewsAndDataSources.append(
                    pickerData
                )
                stackView.addArrangedSubview(pickerData.picker)
            }
            
            if setting.limits.min < 0 {
                let pickerData = createSignPicker()
                signPickerViewAndDataSourcess = pickerData
                pickerData.picker.widthAnchor.constraint(equalToConstant: 90).isActive = true
                
                stackView.insertArrangedSubview(pickerData.picker, at: 0)
            }
        }
        
        if model.isWithType {
            let pickerData = createUnitsPicker()
            unitsPickerViewAndDataSourcess = pickerData
            pickerData.picker.widthAnchor.constraint(equalToConstant: 90).isActive = true
            
            stackView.addArrangedSubview(pickerData.picker)
        }
        
        horisontalTopSepparatorView.translatesAutoresizingMaskIntoConstraints = false
        horisontalBottomSepparatorView.translatesAutoresizingMaskIntoConstraints = false
        horisontalTopSepparatorView.backgroundColor = .hex("10202E")
        horisontalBottomSepparatorView.backgroundColor = .hex("10202E")
        view.addSubview(horisontalBottomSepparatorView)
        view.addSubview(horisontalTopSepparatorView)
        
        NSLayoutConstraint.activate([
            horisontalTopSepparatorView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            horisontalTopSepparatorView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            horisontalTopSepparatorView.heightAnchor.constraint(equalToConstant: 0.5),
            horisontalTopSepparatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 17),
            horisontalBottomSepparatorView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            horisontalBottomSepparatorView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            horisontalBottomSepparatorView.heightAnchor.constraint(equalToConstant: 0.5),
            horisontalBottomSepparatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -17),
            ])
        
        updateSepparatorPosition(animate: false)
    }
    
    func updateViewBySetting() {
        let setting = currentSettings
        
        var countViewsForInsert = (setting.pattern.integer + setting.pattern.decimal) - pickerViewsAndDataSources.count
        if setting.pattern.integer == 0 {
            countViewsForInsert = 0
        }
        if countViewsForInsert != 0 {
            for _ in 1...abs(countViewsForInsert) {
                if countViewsForInsert > 0 {
                    let pickerData = self.createValuePicker()
                    self.pickerViewsAndDataSources.insert(pickerData, at: 0)
                    self.stackView.insertArrangedSubview(pickerData.picker,
                                                         at: setting.limits.min < 0 ? 1 : 0)
                } else if countViewsForInsert < 0 {
                    let picker = self.pickerViewsAndDataSources.first!.picker
                    self.stackView.removeArrangedSubview(picker)
                    self.pickerViewsAndDataSources.remove(at: 0)
                }
            }
        } else {
            return updateSepparatorPosition(animate: true)
        }
        updateSepparatorPosition(animate: false)
    }
    
    private func updateSepparatorPosition(animate: Bool) {
        let setting = currentSettings
        updatePickerValues()
        
        if setting.pattern.decimal > 0 && setting.pattern.integer > 0 {
            if !animate {
                self.stackView.insertArrangedSubview(self.sepparatorLabel, at: setting.pattern.integer)
                didSelectValueInPicker()
                return
            }
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                self.stackView.insertArrangedSubview(self.sepparatorLabel, at: setting.pattern.integer)
            }, completion: nil)
        } else {
            stackView.removeArrangedSubview(sepparatorLabel)
        }
        
        didSelectValueInPicker()
    }
    
    func updatePickerValues(animated: Bool = false) {
        let setting = currentSettings
        
        var value = String(format: "%\(setting.pattern.integer).\(setting.pattern.decimal)f", model.value)
        if setting.pattern.decimal == 0 {
            value = "\(Int(model.value))"
        }

        for pickerData in pickerViewsAndDataSources.reversed() {
            if value.last == "-" {
                value = value + "0"
            }
            
            var char = value.popLast() ?? "0"
            if char == "." {
                char = value.popLast()!
            }
            pickerData.picker.selectRow(Int(String(char))!, inComponent: 0, animated: animated)
        }
        
        if setting.limits.min < 0 {
            if value == "-" {
                signPickerViewAndDataSourcess?.picker.selectRow(0 ,inComponent: 0, animated: animated)
            } else {
                signPickerViewAndDataSourcess?.picker.selectRow(1 ,inComponent: 0, animated: animated)
            }
        }
        
    }
    
    private func updateUnits() {
        unitsPickerViewAndDataSourcess?.picker.selectRow(model.units, inComponent: 0, animated: true)
    }
    
    func updatePikcersFromLimitation() {
        let setting = currentSettings
        
        if model.value > setting.limits.max {
            model.value = setting.limits.max
        } else if model.value < setting.limits.min {
            model.value = setting.limits.min
        }
        
        updatePickerValues(animated: true)
    }
    
    private func createValuePicker() -> (picker: UIPickerView, dataSource: BCValuePickerViewDataSource) {
        let dataSource = BCValuePickerViewDataSource()
        dataSource.didSelectValue = didSelectValueInPicker
        let picker = UIPickerView(frame: .zero)
        picker.dataSource = dataSource
        picker.delegate = dataSource
        
        return (picker: picker, dataSource: dataSource)
    }
    
    private func createUnitsPicker() -> (picker: UIPickerView, dataSource: BCUnitsPickerViewDataSource) {
        let dataSource = BCUnitsPickerViewDataSource()
        dataSource.titlesData = model.data.map() { item in
            return item.units
        }
        dataSource.didSelectUnits = didSelectUnitsInPicker
        let picker = UIPickerView(frame: .zero)
        picker.dataSource = dataSource
        picker.delegate = dataSource
        
        return (picker: picker, dataSource: dataSource)
    }
    
    private func createSignPicker() -> (picker: UIPickerView, dataSource: BCUnitsPickerViewDataSource) {
        let dataSource = BCUnitsPickerViewDataSource()
        dataSource.titlesData = ["-", "+"]
        dataSource.didSelectUnits = didSelectSignInPicker
        let picker = UIPickerView(frame: .zero)
        picker.dataSource = dataSource
        picker.delegate = dataSource
        
        return (picker: picker, dataSource: dataSource)
    }
    
    private func setupView() {
        stackView = UIStackView(frame: .zero)
        stackView.axis  = .horizontal
        stackView.distribution  = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing   = 0.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        setupConstraints()
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 5),
            stackView.topAnchor.constraint(equalTo: view.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15)
        ])
        
        sepparatorLabel.text = model.sepparator
        sepparatorLabel.textColor = .white
        sepparatorLabel.font = UIFont(name: "Roboto-Regular", size: 30)
        sepparatorLabel.textAlignment = .center
        sepparatorLabel.widthAnchor.constraint(equalToConstant: 10).isActive = true
    }
    
    private func getValueFromPickers() -> Double {
        let setting = currentSettings
        
        var result = ""
        let dotPosition = { position -> Int? in
            if let _ = position  {
                return stackView.arrangedSubviews.startIndex.distance(to: position!)
            }
            return nil
        }(stackView.arrangedSubviews.firstIndex(of: sepparatorLabel))
        
        for (picker, _) in pickerViewsAndDataSources {
            let value = picker.selectedRow(inComponent: 0)
            result += "\(value)"
        }
        
        if let offset = dotPosition {
            result.insert(".", at: result.index(result.startIndex, offsetBy: offset))
        }
        
        if setting.limits.min < 0 {
            if signPickerViewAndDataSourcess?.picker.selectedRow(inComponent: 0) == 0 {
                if Double(result) == 0 {
                    return 0
                }
                return (Double(result) ?? 0) * -1
            }
        }
        
        return Double(result) ?? 0
    }
    
    private func didSelectValueInPicker() {
        model.value = getValueFromPickers()
        
        updatePikcersFromLimitation()
    }
    
    func didSelectUnitsInPicker(units: Int) {
        model.units = units
        let setting = currentSettings
        
        if let value = model.didSelectValue(model.value, units) {
            if value < setting.limits.max && setting.pattern.decimal == 0 {
                model.value = round(value)
            } else {
                model.value = value
            }
        }
        updateViewBySetting()
    }
    
    private func didSelectSignInPicker(units: Int) {
        model.value = getValueFromPickers()
        
        updatePikcersFromLimitation()
    }
    
    @IBAction func doneAction() {
        _ = model.didSelectValue(model.value, model.units)
        model.editFinished(true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction() {
        model.editFinished(false)
        dismiss(animated: true, completion: nil)
    }
}
