//
//  GuideExitButton.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/16/19.
//  Copyright © 2019 Ciklum. All rights reserved.
//

class GuideExitButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    private func setupView() {
        layer.cornerRadius = 2
        layer.borderWidth = 1
        layer.setBorderColorFromUIColor(.white)
    }
    
}
