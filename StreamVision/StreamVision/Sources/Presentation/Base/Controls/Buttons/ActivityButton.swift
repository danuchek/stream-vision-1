//
//  ActivityButton.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/27/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class ActivityButton: UIButton {
    var spinner: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        spinner = UIActivityIndicatorView(style: .white)
        spinner.isHidden = true
        
        addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])
    }
    
    func showIndicator() {
        UIView.animate(withDuration: 0.3) {
            self.spinner.isHidden = false
            self.setTitleColor(.clear, for: .normal)
            self.spinner.startAnimating()
            self.backgroundColor = .rgba(16, 41, 81, 1)
        }
    }
    
    func hideIndicator() {
        UIView.animate(withDuration: 0.3) {
            self.spinner.isHidden = true
            self.setTitleColor(.white, for: .normal)
            self.spinner.stopAnimating()
            self.backgroundColor = .rgba(56, 118, 255, 1)
        }
    }
}
