//
//  GPSNightModeButton.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/17/19.
//  Copyright © 2019 Ciklum. All rights reserved.
//

import UIKit

class GPSNightModeButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }

    private func setupView() {
        layer.cornerRadius = frame.width / 2
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowColor = UIColor.black.cgColor
        clipsToBounds = true
        setTitle(nil, for: .normal)
        setImage(#imageLiteral(resourceName: "gps_day_switch"), for: .normal)
        setImage(#imageLiteral(resourceName: "gps_night_switch.png"), for: .selected)
        imageView?.tintColor = .white
    }
}
