//
//  CustomTabBarView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/3/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class CustomTabBarView: UIStackView {
    enum TabbarStyle {
        case withNumeration
        case `default`
    }
    
    private var items: [CustomTabBarItem] = []
    private var colorForDeselectedItems: UIColor = .hex("#2A3F4F") {
        didSet {
            items.forEach() { item in
                item.deselectedColor = colorForDeselectedItems
            }
        }
    }
    private var colorForActiveItems: UIColor = .hex("#157EFB") {
        didSet {
            items.forEach() { item in
                item.selectedColor = colorForActiveItems
            }
        }
    }
    let backgroundView: UIView = UIView()
    var currentSelectedIndex: Int = 0
    var style: TabbarStyle = .default {
        didSet {
            for item in items {
                item.isNeedNumeration = style == .withNumeration
            }
        }
    }
    var fontForItems: UIFont? = nil {
        didSet {
            for item in items {
                item.font = fontForItems
            }
        }
    }
    
    func setupView() {
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(backgroundView)
        
        NSLayoutConstraint.activate([
            backgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: trailingAnchor),
            backgroundView.topAnchor.constraint(equalTo: topAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        backgroundView.backgroundColor = .hex("#0b1f2f")
        
        axis  = .horizontal
        distribution  = .fillEqually
        alignment = .fill
        spacing   = 20.0
        
        layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        isLayoutMarginsRelativeArrangement = true
    }
    
    func colorForDeselectedtems(color: UIColor) {
        colorForDeselectedItems = color
    }
    
    func colorForActiveItems(color: UIColor) {
        colorForActiveItems = color
    }
    
    func selectItemBy(_ index: Int) {
        for (itemIndex, item) in items.enumerated() {
            item.isSelected = (itemIndex == index)
            if item.isSelected {
                currentSelectedIndex = itemIndex
            }
        }
    }
    
    func turnItemBy(_ index: Int) {
        items[index].action?(index)
    }
    
    @discardableResult
    func turnNextItem() -> Bool {
        if currentSelectedIndex < items.count - 1 {
            turnItemBy(currentSelectedIndex + 1)
            return true
        }
        return false
    }
    
    func turnItem(to: Int) -> () {
        turnItemBy(to)
    }
    
    @discardableResult
    func addItem(title: String) -> CustomTabBarItem {
        return addItem(title: title, selectionAction: nil)
    }
    
    @discardableResult
    func addItem(title: String, selectionAction: ((_ index: Int) -> Bool)? ) -> CustomTabBarItem {
        let item = CustomTabBarItem(title: title, index: items.count)
        item.isNeedNumeration = (style == .withNumeration)
        item.deselectedColor = colorForDeselectedItems
        item.selectedColor =  colorForActiveItems
        if fontForItems != nil {
            item.font = fontForItems
        }
        item.action = { index in
            if let completion = selectionAction, completion(index) == true {
                self.selectItemBy(index)
            }
        }
        
        addArrangedSubview(item)
        items.append(item)
        return item
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
}
