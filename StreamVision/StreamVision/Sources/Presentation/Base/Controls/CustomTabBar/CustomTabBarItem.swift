//
//  CustomTabBarItem.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/3/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class CustomTabBarItem: UIControl {
    private let titleLabel: UILabel = UILabel()
    private let indexLabel: UILabel = UILabel()
    private let selectView: UIView = UIView()
    var selectedColor: UIColor = .hex("#157EFB") {
        didSet {
            self.isSelected = Bool(isSelected)
        }
    }
    var deselectedColor: UIColor = .hex("#2A3F4F") {
        didSet {
            self.isSelected = Bool(isSelected)
        }
    }
    var font: UIFont? = UIFont(name: "Roboto-Regular", size: 11.0) {
        didSet {
            titleLabel.font = font
        }
    }
    
    private var index: Int = 0
    var action: ((_ index: Int) -> ())?
    var isNeedNumeration: Bool = false {
        didSet {
            indexLabel.isHidden = !isNeedNumeration
            indexWidthConstraint?.constant = isNeedNumeration ? 12 : 0
            indexLeadingConstraint?.constant = isNeedNumeration ? 10 : 0
        }
    }
    var indexWidthConstraint: NSLayoutConstraint?
    var indexLeadingConstraint: NSLayoutConstraint?
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                selectView.isHidden = false
                titleLabel.textColor = selectedColor
                indexLabel.layer.borderColor = selectedColor.cgColor
                indexLabel.layer.backgroundColor = selectedColor.cgColor
            } else {
                selectView.isHidden = true
                titleLabel.textColor = deselectedColor
                indexLabel.layer.borderColor = deselectedColor.cgColor
                indexLabel.layer.backgroundColor = deselectedColor.cgColor
            }
        }
    }
    
    init(title: String, index: Int) {
        super.init(frame: .zero)
        
        let borderColor: UIColor = .hex("#2A3F4F")
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont(name: "Roboto-Regular", size: 11.0)
        titleLabel.textColor = borderColor
        titleLabel.textAlignment = .center
        titleLabel.text = title
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.numberOfLines = 2
        addSubview(titleLabel)
        
        indexLabel.translatesAutoresizingMaskIntoConstraints = false
        indexLabel.font = UIFont(name: "Roboto-Regular", size: 9.0)
        indexLabel.text = "\(index + 1)"
        indexLabel.layer.borderColor = borderColor.cgColor
        indexLabel.layer.backgroundColor = borderColor.cgColor
        indexLabel.layer.cornerRadius = 6
        indexLabel.layer.masksToBounds = true
        indexLabel.textAlignment = .center
        indexLabel.textColor = .black
        indexLabel.layer.shouldRasterize = true
        addSubview(indexLabel)
        
        selectView.translatesAutoresizingMaskIntoConstraints = false
        selectView.backgroundColor = .hex("#157EFB")
        addSubview(selectView)
        
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),

            selectView.leadingAnchor.constraint(equalTo: leadingAnchor),
            selectView.trailingAnchor.constraint(equalTo: trailingAnchor),
            selectView.bottomAnchor.constraint(equalTo: bottomAnchor),
            selectView.heightAnchor.constraint(equalToConstant: 2),
            
            indexLabel.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            indexLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
        ])
        indexWidthConstraint = indexLabel.widthAnchor.constraint(equalToConstant: 0)
        indexWidthConstraint?.isActive = true
        
        indexLeadingConstraint = indexLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10)
        indexLeadingConstraint?.isActive = true
        
        selectView.isHidden = true
        indexLabel.isHidden = true
        
        addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        self.index = index
    }
    
    @objc func touchUpInside() {
        action?(index)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
