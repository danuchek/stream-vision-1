//
//  ParameterButton.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/23/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import LLARingSpinnerView

extension ParameterButton: StoreManager {
    override func setImage(_ image: UIImage?, for state: UIControl.State) {
        if storeManager.isNightMode {
            DispatchQueue.main.async {
                super.setImage(image?.withRenderingMode(.alwaysTemplate), for: state)
            }
            return
        }
//        DispatchQueue.main.async {
            super.setImage(image, for: state)
//        }
    }
    
    override var isSelected: Bool {
        didSet {
            if storeManager.isNightMode {
                if isSelected {
                    tintColor = UIColor.colorFromHexString("721824")
                } else {
                    tintColor = storeManager.nightModeColor
                }
            }
        }
    }
}

extension ParameterButton {
    func startAnimation() {
        isUserInteractionEnabled = false
        
        reloadView = LLARingSpinnerView(frame: CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width/2, height: frame.width/2) )
        reloadView.center = center
        reloadView.lineWidth = 2
        reloadView.tintColor = storeManager.isNightMode ? storeManager.nightModeColor : .white
        
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { _ in
            self.superview?.addSubview(self.reloadView)
            self.reloadView.startAnimating()
        }
        
        perform(#selector(stopAnimating), with: nil, afterDelay: 3)
    }
    
    @objc fileprivate func stopAnimating() {
        UIView.animate(withDuration: 0.3, animations: {
            if let _ = self.reloadView {
                self.reloadView.stopAnimating()
                self.reloadView.removeFromSuperview()
                self.reloadView = nil
            }
        }) { _ in
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
                self.isUserInteractionEnabled = true
            }
        }
    }
}

extension ParameterButton {
    override func setTitle(_ title: String?, for state: UIControl.State) {
        self.title = title
        
        if let titleValue = value, let _ = title, isWithTitleValue {
            var stringValue: String
            if titleValue is NSNumber {
                let digitalValue = (titleValue as! NSNumber).floatValue
                if digitalValue.truncatingRemainder(dividingBy: 1) == 0 {
                    stringValue = "\(Int(digitalValue))"
                } else {
                    stringValue = String(format: "%.1f",digitalValue)
                }
            } else {
                stringValue = titleValue as! String
            }
            
            if isWithLocalizedValue {
                if let localizedValues = visualContentModel?.model?.limits.localizedValues {
                    if let localizedValue = Int(stringValue), localizedValue < localizedValues.count {
                        stringValue = ( localizedValues[localizedValue] ).localized()
                    }
                } else {
                    stringValue = (visualContentModel!.title + "." + stringValue).localized()
                }
            }

            super.setTitle(self.title! + " " + stringValue, for: state)
        } else {
            super.setTitle(title, for: state)
        }
    }
    
    func setTitleValue(floatValue: Float, for state: UIControl.State) {
        value = NSNumber(value: floatValue)
        
        setTitle(self.title, for: state)
    }
    
    func setTitleValue(intValue: Int, for state: UIControl.State) {
        value = NSNumber(value: intValue)
        
        setTitle(self.title, for: state)
    }
    
    func setTitleValue(stringValue: String, for state: UIControl.State) {
        value = stringValue as AnyObject?
        
        setTitle(self.title, for: state)
    }
}

class ParameterButton: StylizedButton {
    var title: String?
    var value: AnyObject?
    var isWithTitleValue: Bool = false
    var isWithLocalizedValue = false
    fileprivate var reloadView: LLARingSpinnerView!
    
    var type: ParametersConfigurator.ViewfinderParameterType? {
        didSet {
            if type == nil {
                Logging.log(message: "[ParameterButton] Type set nil", toFabric: true)
            }
        }
    }
    var visualContentModel: ParameterVisualContent? {
        didSet {
            if let action = visualContentModel?.pressAnimationAction {
                animationForAction = action
            }
            animationForAction = visualContentModel?.pressAnimationAction
        }
    }
    
    var parameterAction: ParameterVisualContent.ParameterAction?
    var isAutoExecuteActionByPress: Bool = false {
        didSet {
            if isAutoExecuteActionByPress {
                addAction(to: .touchUpInside, actionBlock: { _ in })
            }
        }
    }
    
    var animationForAction: ( (ParameterButton) -> () )?
    func doActionAnimation() {
        if let animation = animationForAction {
            animation(self)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupTitleSize()
    }
    
    required init(frame: CGRect) {
        super.init(frame: frame)

        setupTitleSize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupTitleSize()
    }
    
    fileprivate func setupTitleSize() {
        titleLabel?.minimumScaleFactor = 0.5
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
    }
    
    override func doActionBlock() {
        super.doActionBlock()
        self.doActionAnimation()
        
        if isAutoExecuteActionByPress {
            if let action = parameterAction {
                Logging.log(message: "[ParameterButton] pressed \(String(describing: type))", toFabric: true)
                action(self, nil)
            }
        }
    }
}
