//
//  InsetTextField.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/28/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

@IBDesignable
class InsetTextField: UITextField {

    @IBInspectable var inset: CGFloat = 0
    @IBInspectable var placeholderColor: UIColor = UIColor.black {
        didSet {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                            attributes: [.foregroundColor: placeholderColor])
        }
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: inset, dy: inset)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }

    
    
}
