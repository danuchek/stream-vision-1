//
//  RecButton.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 11/30/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit

class RecButton: UIControl, StoreManager {
    fileprivate var counterColor: UIColor = UIColor.white
    fileprivate var recLayer: CALayer = CALayer()
    fileprivate var circleLayer: CAShapeLayer = CAShapeLayer()
    
    fileprivate var recColor: CGColor = UIColor.red.cgColor
    fileprivate var circleColor: CGColor = UIColor.white.cgColor
    
    
    //Center of view
    fileprivate var centerView: CGPoint {
        get {
            return CGPoint(x: self.frame.width/2, y: self.frame.height/2)
        }
    }
    
    //Const
    fileprivate let size: CGFloat = 20
    var pressed: Bool = false {
        willSet(newValue) {
            if newValue != pressed {
                if newValue {
                    self.pressedState()
                } else {
                    self.unPressedState()
                }
            }
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            circleColor = UIColor(red: 1, green: 1, blue: 1, alpha: isEnabled ? 1 : 0.5).cgColor
            recColor = UIColor(red: 1, green: 0, blue: 0, alpha: isEnabled ? 1 : 0.5).cgColor
        }
    }
    
    var isVideoMode: Bool = false {
        didSet {
            if isVideoMode {
                recColor = self.isNightMode ? self.nightModeColor : UIColor.red.cgColor
            } else {
                recColor = self.isNightMode ? self.nightModeColor : UIColor.white.cgColor
            }
            
            updateColors()
        }
    }
    
    // TODO: Add appereanse in other class may by in config
    private var isNightMode: Bool {
        storeManager.isNightMode
    }
    
    // TODO: Add appereanse in other class may by in config
    private var nightModeColor: CGColor {
        storeManager.nightModeColor.cgColor
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if layer.sublayers == nil || layer.sublayers!.count < 2 {
            circleLayer = createCircleLayer()
            layer.addSublayer(circleLayer)
        
            recLayer = createRecLayer()
            layer.addSublayer(recLayer)
        }
        
        updateColors()
    }
    
    func updateColors() {
        circleLayer.strokeColor = pressed ? UIColor.red.cgColor : circleColor
        recLayer.backgroundColor = recColor
        
        if isNightMode {
            circleLayer.strokeColor = nightModeColor
            recLayer.backgroundColor = nightModeColor
        }
    }
    
    func createRecLayer() -> CALayer {
        let layer: CALayer = CALayer()
        layer.frame = CGRect(x: 0, y: 0, width: (self.frame.width/2)+size, height: (self.frame.height/2)+size)
        layer.position = centerView
        layer.backgroundColor = recColor
        layer.cornerRadius = layer.frame.width/2
        layer.masksToBounds = true
        return layer
    }
    
    func createCircleLayer() -> CAShapeLayer {
        let layer: CAShapeLayer = CAShapeLayer()
        let path = UIBezierPath(arcCenter: centerView, radius:centerView.x, startAngle: 0, endAngle: 2*3.14, clockwise: true)
        layer.path = path.cgPath
        layer.strokeColor = isNightMode ? nightModeColor : UIColor.white.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.lineWidth = 2
        return layer
    }
    
    func pressedState() {
        pressedState(nil)
    }
    
    func unPressedState() {
        unPressedState(nil)
    }
    
    func pressedState(_ completion: (() -> ())? ) {
        self.recLayer.add(self.transformAnimation(Float(self.recLayer.cornerRadius), to: 5, duration: 2.0), forKey: "cornerRadius")
    
        UIView.animate(withDuration: 0, animations:  {
            let recFrame = self.frame
            self.recLayer.frame = CGRect(x: 0, y: 0, width: recFrame.width/2, height: recFrame.height/2)
            self.recLayer.position = self.centerView
            self.recLayer.cornerRadius = 5
            
            self.recLayer.backgroundColor = self.isNightMode ? self.nightModeColor : UIColor.red.cgColor
            self.circleLayer.strokeColor = self.isNightMode ? self.nightModeColor : UIColor.red.cgColor
        }, completion: { _ in
            if let _ = completion {
                completion!()
            }
        })
    }
    
    func unPressedState(_ completion: (() -> ())? ) {
        self.recLayer.add(self.transformAnimation(Float(self.recLayer.cornerRadius), to: Float(self.recLayer.frame.width/2), duration: 2.0), forKey: "cornerRadius")
        
        UIView.animate(withDuration: 0, animations:  {
            let recFrame = self.frame
            self.recLayer.frame = CGRect(x: 0, y: 0, width: recFrame.width-self.size, height: recFrame.height-self.size)
            self.recLayer.position = self.centerView
            self.recLayer.cornerRadius = self.recLayer.frame.width/2
            
            self.recLayer.backgroundColor = self.recColor
            self.circleLayer.strokeColor = self.isNightMode ? self.nightModeColor : UIColor.white.cgColor
        }, completion: { _ in
            if let _ = completion {
                completion!()
            }
        }) 
    }
    
    fileprivate func transformAnimation(_ from: Float, to: Float, duration: Double) -> CABasicAnimation {
        let tAnimation: CABasicAnimation = CABasicAnimation(keyPath: "cornerRadius")
        tAnimation.isAdditive = true
        tAnimation.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.linear)
        
        tAnimation.fromValue = from
        tAnimation.toValue = to
        tAnimation.duration = duration;
        return tAnimation
    }

}
