//
//  MenuButton.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/6/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class MenuButton: StylizedButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setImage(UIImage(named: "navigation_menu"), for: .normal)
        setImage(UIImage(named: "navigation_menu_tap"), for: .highlighted)
    }
    
    init() {
        super.init( frame: CGRect(x: 0, y: 0, width: 20, height: 20) )
        setImage(UIImage(named: "navigation_menu"), for: .normal)
        setImage(UIImage(named: "navigation_menu_tap"), for: .highlighted)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required init(frame: CGRect) {
        super.init(frame: frame)
    }
    
}
