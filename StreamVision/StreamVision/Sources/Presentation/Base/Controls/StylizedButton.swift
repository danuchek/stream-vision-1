//
//  File.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 11/24/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit

class StylizedButton: ButtonWithActionBlock {
    var titleYOffset: CGFloat = 0
    var imageYOffset: CGFloat = 0
    private var titleHeight: CGFloat?
    
    fileprivate var extraLabel: UILabel = UILabel()
    var extraValue: Int {
        get {
            return Int(extraLabel.text!)!
        }
        set {
            if extraLabel.superview == nil {
                extraLabel.frame = CGRect(x: frame.size.width - 15, y: 0, width: 20, height: 10)
                extraLabel.font = UIFont.systemFont(ofSize: 8.0)
                extraLabel.textColor = UIColor.white
                self.addSubview(extraLabel)
            }
            extraLabel.text = (newValue > 0 ? "+" : "") + String(newValue)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setupTitleSize()
        centerContentVertically()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        centerTitleLabel()

        setupTitleSize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        centerTitleLabel()
        
        setupTitleSize()
    }

    private func centerTitleLabel() {
        self.titleLabel?.textAlignment = .center
    }
    
    fileprivate func setupTitleSize() {
        titleLabel?.minimumScaleFactor = 0.5
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.lineBreakMode = NSLineBreakMode.byClipping
    }
    
    /*
     Create instance from parameters
     */
    class func Create<T: StylizedButton>(
                      frame: CGRect,
                      title: String,
                      normalImage: UIImage,
                      selectedImage: UIImage? = nil,
                      action: ( (_ sender: ButtonWithActionBlock) ->Void )?) -> T {
        let button = T(frame: frame)
        button.setImage(normalImage, for: .normal)
        
        if let selectedImage = selectedImage {
            button.setImage(selectedImage, for: .selected)
        }
        
        button.addAction(to: .touchUpInside, actionBlock: action)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        
        button.setupTitleSize()
        
        return button
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        centerContentVertically()
    }
    
    func removeAllActions() {
        removeTarget(self, action: nil, for: .touchUpInside)
    }
    
    func centerContentVertically(padding: CGFloat = 6.0) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
            return
        }

        let totalHeight = imageViewSize.height + titleLabelSize.height + padding

        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageViewSize.height),
            left: 0.0,
            bottom: 0.0,
            right: -titleLabelSize.width
        )

        self.titleEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height),
            right: 0.0
        )

        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: 0,
            right: 0.0
        )
    }

}
