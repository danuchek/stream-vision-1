//
//  ZoomLabel.swift
//  RealStream
//
//  Created by Vladislav Chebotaryov on 12/22/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit

class ZoomLabel: UILabel {
    var zoom: Int = 0 {
        didSet {
            if zoom == 0 {
                self.text = "x1"
            }
            if zoom == 1 {
                self.text = "x2"
            }
            if zoom == 2 {
                self.text = "x4"
            }
        }
    }
}
