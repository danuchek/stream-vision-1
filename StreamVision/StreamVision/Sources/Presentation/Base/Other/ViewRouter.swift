//
//  ViewRouter.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 2/1/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit

protocol ViewRouter {
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

extension ViewRouter {
    func prepare(for segue: UIStoryboardSegue, sender: Any?) { }
}
