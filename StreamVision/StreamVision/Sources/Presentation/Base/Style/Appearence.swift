//
//  Appearence.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/8/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Foundation
import SVProgressHUD

class Appearence {
    class func setupApplicationStyle() {
        let navigationAppearance =  UINavigationBar.appearance()
        navigationAppearance.shadowImage = UIImage()
        navigationAppearance.setBackgroundImage(UIImage(), for: .default)
        navigationAppearance.tintColor = UIColor.white
        navigationAppearance.backgroundColor = UIColor.colorFromHexString("#183349")
        navigationAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
    }
}
