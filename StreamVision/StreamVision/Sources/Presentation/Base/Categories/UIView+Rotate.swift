//
//  AnimatedImageView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/28/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

extension UIImageView {
    func rotate360Degrees(_ duration: CFTimeInterval = 1.0) {
        startAnimationWithDuration(duration)
    }
    
    func startAnimationWithDuration(_ duration: CFTimeInterval) {
        layer.removeAllAnimations()
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.fromValue = 0.0
        animation.toValue = (360 * Double.pi) / 180
        animation.duration = 1.0
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        animation.repeatCount = Float.infinity
        layer.add(animation, forKey: nil)
    }
}
