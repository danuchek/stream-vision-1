//
//  UISegmentedControl+Disable.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/21/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

extension UISegmentedControl {

    func disableLeftItem() {
        self.subviews[1].tintColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.5)
        self.isUserInteractionEnabled = false
        self.selectedSegmentIndex = 1
    }
    
    func enableLeftItem() {
        enableLeftItemWithoutSwitch()
        self.selectedSegmentIndex = 0
    }

    func enableLeftItemWithoutSwitch() {
        self.subviews[1].tintColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 1)
        self.isUserInteractionEnabled = true
    }

}
