//
//  GoogleSignOutView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import TYMProgressBarView

import BPBlockActivityIndicator

class ReconnectView: UIView {
    fileprivate var view: UIView!

    // Outlets
    @IBOutlet fileprivate weak var sepparateView: UIView!
    @IBOutlet weak var cancelButton: CornerCancelButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var animatedCubeView: BPBlockActivityIndicator!
    private var action: (() -> Void)?
    
    init(center: CGPoint, cancell action: @escaping () -> Void) {
        super.init(frame: CGRect(x: 0,y: 0,width: 181, height: 181))
        xibSetup(with: frame)
        
        animatedCubeView.animate()

        self.center = center
        titleLabel.text = "Reconnecting.Alert.Title".localized()
        cancelButton.setTitle("General.Alert.Cancel".localized(), for: .normal)
        self.action = action
        cancelButton.addTarget(nil, action: #selector(cancelAction), for: .touchUpInside)
    }
    
    @objc private func cancelAction() {
        animatedCubeView.stop()
        action?()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }

}
