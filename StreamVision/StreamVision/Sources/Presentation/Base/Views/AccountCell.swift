//
//  AccountCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 10/3/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class AccountCell: UITableViewCell {

    func configure(user: LiveUserModel) {
        textLabel?.text = user.name
        detailTextLabel?.text = user.email
        textLabel?.textColor = .white
        detailTextLabel?.textColor = .rgba(44, 63, 79, 1)
        textLabel?.font = UIFont(name: "Roboto-Regular", size: 17.0)
        detailTextLabel?.font = UIFont(name: "Roboto-Regular", size: 14.0)
        
        backgroundColor = .clear
    }

}
