//
//  CustomAlertView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/27/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit

class CustomAlertView: UIView {
    fileprivate var view: UIView!

    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(with: frame)
        
        setupView()
    }
    
    func setupView() {}
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0 , y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }

}
