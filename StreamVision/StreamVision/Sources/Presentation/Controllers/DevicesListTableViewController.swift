//
//  DevicesListTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 11/20/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class DevicesListTableViewController: UITableViewController, StoreManager {
    private var devices: [VirtualDevice] = []
    var selectedDevice: VirtualDevice!
    var deviceSelectedAction: ((_ device: VirtualDevice) -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialyze variables
        devices = storeManager.devices
        
        tableView.backgroundColor = .hex("161A1E")
        tableView.register(UINib(nibName: "DeviceDetailTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "DeviceDetailTableViewCell")
        
        preferredContentSize = CGSize(width: view.frame.width / 2,
                                      height: min(CGFloat(devices.count * 58),
                                                  view.frame.height / 2.5))
        if devices.count == 1 {
            tableView.separatorStyle = .none
        }
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 140
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return devices.count
    }


    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceDetailTableViewCell") as! DeviceDetailTableViewCell

        let device = devices[indexPath.row]
        cell.configureWithDevice(device: device)


        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deviceSelectedAction?(devices[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == devices.firstIndex(of: selectedDevice)! {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else {
            cell.setSelected(false, animated: false)
        }
    }

}
