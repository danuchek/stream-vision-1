//
//  BallisticCalculatorSetBulletDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/7/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class SetBulletsDataSource: NSObject, UITextFieldDelegate, SetupProfileDataSourceProtocol {
    enum SetBullet: String {
        case catalog = "Catalog"
        case bulletCatalog = "BulletCatalog"
        case coefficient = "Coefficient"
        case profile = "Profile"
        case velocity = "Velocity"
        case gyro = "Gyro"
        case weight = "Weight"
        case length = "Length"
        case caliber = "Caliber"
        case twistType = "TwistType"
        case twistValue = "TwistValue"
    }
    enum Section: Int {
        case section1
        case section2
        case section3
        case section4
    }
    private var items : [Section: [SetBullet]] = [:]
    var tableView: UITableView!
    var isNeedValidate: Bool = false
    
    weak var headerLabel: UILabel!
    var model: SetBallisticCalculatorBaseModel!
    
    @discardableResult
    func reloadWithValidation() -> Bool {
        return reloadDataSource(validation: {
            var validation = false
            
            if self.model.bullet.isWithGyroEffect {
                validation = self.model.bullet.weight == nil ||
                             self.model.bullet.length == nil ||
                             self.model.bullet.caliber == nil ||
                             self.model.rifle.twistValue == nil ||
                             self.model.rifle.twistDirection == nil
            }
            
            if (!self.model.bullet.isWithCustomBullet && self.model.bullet.isWithAmmunition) ||
                !self.model.bullet.isWithCustomBullet && !self.model.bullet.isWithAmmunition {
                validation = validation || self.model.bullet.coefficient == nil ||
                                           self.model.rifle.muzzle == nil ||
                                           self.model.bullet.profile == nil
            }
            
            validation = validation || self.model.rifle.muzzle == nil
            
            if self.model.bullet.isWithCustomBullet && self.model.bullet.isWithAmmunition && self.model.bullet.selectedBullet == nil {
                validation = true
            }
            
            return validation
        }())
    }
    
    func reloadDataSource() {
        headerLabel.text = "BallisticCalc.SetBullet.Description".localized()
        reloadDataSource(validation: false)
    }
    
    @discardableResult
    func reloadDataSource(validation: Bool) -> Bool {
        isNeedValidate = validation
        
        if model.bullet.selectedBullet != nil || model.bullet.isWithCustomBullet {
            if let _ = model.bullet.selectedBullet {
                items = [.section1 : [.catalog, .bulletCatalog]]
                if model.bullet.isWithAmmunition {
                    items[.section2] = [.coefficient, .profile, .velocity]
                } else {
                    items[.section2] = [.coefficient, .profile, .velocity]
                }
                
                if (!model.bullet.isWithGyroEffect) {
                    items[.section3] = [.gyro]
                } else {
                    items[.section3] = [.gyro, .weight, .length, .caliber]
                    items[.section4] = [.twistValue, .twistType]
                }
            } else {
                items = [.section1 : [.catalog, .bulletCatalog]]
            }
        } else {
            items = [.section1 : [.catalog]]
            items[.section2] = [.coefficient, .profile, .velocity]
            
            if (!model.bullet.isWithGyroEffect) {
                items[.section3] = [.gyro]
            } else {
                items[.section3] = [.gyro, .weight, .length, .caliber]
                items[.section4] = [.twistValue, .twistType]
            }
        }
        
        tableView.reloadData()
        return isNeedValidate
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch items[Section(rawValue: indexPath.section)!]![indexPath.row] {
        case .catalog:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetBulletSwitchTableViewCell") as? SetPresetSwitchTableViewCell {
                
                cell.textLabel?.text = "BallisticCalc.SetBullet.BulletFromCatalog".localized()
                cell.switchView.isOn = self.model.bullet.isWithCustomBullet
                cell.switchValueChanged = { value in
                    self.model.bullet.isWithCustomBullet = value
                    if !value {
                        self.model.bullet.selectedBullet = nil
                        self.model.bullet.isWithGyroEffect = false
                    }
                    
                    self.reloadDataSource()
                }
                cell.tag = SetBullet.catalog.rawValue.hash
                
                return cell
            }
        case .bulletCatalog:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = ""
                cell.detailTextLabel?.textColor = .white
                if let bullet = model.bullet.selectedBullet {
                    details = bullet.description
                } else {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                }
                cell.setupCell(title: "BallisticCalc.SetBullet.BulletsOrAmunitions".localized(),
                               details: details)
                cell.tag = SetBullet.bulletCatalog.rawValue.hash
                
                return cell
            }
        case .coefficient:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                var details = model.bullet.coefficient?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetBulletModel.CoefficientModel.createParameter(SetBulletModel.CoefficientModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetBullet.BallisticCoefficient".localized(),
                               details: details)
                cell.tag = SetBullet.coefficient.rawValue.hash
                
                return cell
            }
        case .velocity:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                var details = model.rifle.muzzle?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetRifleModel.MuzzleModel.createParameter(SetRifleModel.MuzzleModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetRifle.MuzzleVelocity".localized(),
                               details: details)
                cell.tag = SetBullet.velocity.rawValue.hash
                
                return cell
            }
        case .weight:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                var details = model.bullet.weight?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetBulletModel.WeightModel.createParameter(SetBulletModel.WeightModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetBullet.Weight".localized(),
                               details: details)
                cell.tag = SetBullet.weight.rawValue.hash
                
                return cell
            }
        case .length:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                var details = model.bullet.length?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetBulletModel.LengthModel.createParameter(SetBulletModel.LengthModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetBullet.Length".localized(),
                               details: details)
                cell.tag = SetBullet.length.rawValue.hash
                
                return cell
            }
        case .caliber:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                var details = model.bullet.caliber?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetBulletModel.CaliberModel.createParameter(SetBulletModel.CaliberModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetBullet.Caliber".localized(),
                               details: details)
                cell.tag = SetBullet.caliber.rawValue.hash

                return cell
            }
        case .profile:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.bullet.profile?.localizedType
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetBulletModel.ProfileModel.createParameter(SetBulletModel.ProfileModel.self).localizedType
                }
                cell.setupCell(title: "BallisticCalc.SetBullet.BallisticTable".localized(),
                               details: details)
                cell.tag = SetBullet.profile.rawValue.hash
                
                return cell
            }
        case .gyro:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetBulletSwitchTableViewCell") as? SetPresetSwitchTableViewCell {
                
                cell.textLabel?.text = "BallisticCalc.SetBullet.GyroSwitch".localized()
                cell.switchView.isOn = model.bullet.isWithGyroEffect
                cell.switchValueChanged = { value in
                    self.model.bullet.isWithGyroEffect = value
                    
                    self.reloadDataSource()
                }
                cell.tag = SetBullet.gyro.rawValue.hash
                
                return cell
            }
        case .twistType:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.rifle.twistDirection?.localizedType
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetRifleModel.TwistDirectionModel.createParameter(SetRifleModel.TwistDirectionModel.self).localizedType
                }
                cell.setupCell(title: "BallisticCalc.SetRifle.TwistType.Title".localized(),
                               details: details)
                cell.tag = SetBullet.twistType.rawValue.hash
                
                
                return cell
            }
        case .twistValue:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.rifle.twistValue?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetRifleModel.TwistModel.createParameter(SetRifleModel.TwistModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetRifle.TwistNumber".localized(),
                               details: details)
                cell.tag = SetBullet.twistValue.rawValue.hash
                
                return cell
            }
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return nil
        case 1: return "BallisticCalc.SetBullet.Parameters".localized()
        case 2: return "BallisticCalc.SetBullet.GyroEffect".localized()
        case 3: return "BallisticCalc.SetRifle.TwistParameters.Title".localized()
        default: return nil
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[Section(rawValue: section)!]!.count
    }
}
