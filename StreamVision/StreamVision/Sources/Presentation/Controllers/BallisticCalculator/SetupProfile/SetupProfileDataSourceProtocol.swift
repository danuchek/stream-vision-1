//
//  SetupProfileDataSourceProtocol.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/17/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

protocol SetupProfileDataSourceProtocol: UITableViewDataSource {
    func reloadDataSource()
    @discardableResult
    func reloadWithValidation() -> Bool
    var tableView: UITableView! { get set }
    var model: SetBallisticCalculatorBaseModel! { get set }
}
