//
//  SetBulletChooseBulletViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/30/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import CSVImporter
import SVProgressHUD

class ChooseBulletFilter: NSObject, UITableViewDataSource, UITableViewDelegate {
    struct CellFilterModel {
        var type: FilterType
        var data: [String]
        var selected: Int = 0
        
        var currentValue: String {
            return data[selected]
        }
    }
    
    enum FilterType: String {
        case category = "Category"
        case manufacture = "Manufacture"
        case calibre = "Calibre"
    }
    
    let AnyKey = "BallisticCalc.SetBullet.Catalog.Filter.Any".localized()
    
    private var filterData: [SetBulletChooseBulletDataSource.Bullet] = []
    private var filterCellsData: [FilterType] = []
    private var tableView: UITableView!
    
    private var manufactureData: CellFilterModel!
    private var calibreData: CellFilterModel!
    private var categoryData: CellFilterModel!
    
    private var isBullet: Bool = false
    
    var filterWasChanged: ((_ newData: [(key: Character, value: [SetBulletChooseBulletDataSource.Bullet])]) -> ())?
    
    init(tableView: UITableView, presenterViewController: UIViewController) {
        super.init()
        self.tableView = tableView
        self.tableView.tableFooterView = UIView(frame:
            CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 1))
    }
    
    func reloadDataFor(bullet: Bool, data: [(key: Character, value: [SetBulletChooseBulletDataSource.Bullet])]) {
        isBullet = bullet
        if bullet {
            filterCellsData = [.manufacture, .calibre]
        } else {
            filterCellsData = [.category, .manufacture, .calibre]
        }
        
        var manufactureSet: Set<String> = []
        var calibreSet: Set<String> = []
        var categorySet: Set<String> = []
        data.forEach() { item in
            item.value.forEach() { bullet in
                filterData.append(bullet)
                
                manufactureSet.insert(bullet.manufacture)
                calibreSet.insert(bullet.calibreForDescripton)
                if let cat = bullet.category {
                    categorySet.insert(cat)
                }
            }
        }
        manufactureData = CellFilterModel(type: .manufacture ,data: Array(manufactureSet).sorted() {
            return $0 < $1
        }, selected: 0)
        calibreData = CellFilterModel(type: .calibre ,data: Array(calibreSet).sorted() {
            return $0 < $1
        }, selected: 0)
        categoryData = CellFilterModel(type: .category ,data: Array(categorySet).sorted() {
            return $0 < $1
        }, selected: 0)
        
        manufactureData.data.insert(AnyKey, at: 0)
        calibreData.data.insert(AnyKey, at: 0)
        categoryData.data.insert(AnyKey, at: 0)
        
        tableView.reloadData()
    }
    
    func selectFilterBy(type: FilterType, selected: Int) -> (manufacture: String,
                                                             calibre: String,
                                                             category: String)
    {
        switch type {
        case .manufacture: manufactureData.selected = selected
        case .calibre: calibreData.selected = selected
        case .category: categoryData.selected = selected
        }
        tableView.reloadData()
        return (manufacture: manufactureData.data[manufactureData.selected],
                calibre: calibreData.data[calibreData.selected],
                category: categoryData.data[categoryData.selected])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterCellsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =
            tableView.dequeueReusableCell(withIdentifier: "CatalogFilterTableViewCell") as! CatalogFilterTableViewCell
        cell.textLabel?.text = filterCellsData[indexPath.row].rawValue
        
        if !isBullet {
            switch indexPath.row {
                
            case 0: cell.model = categoryData
            cell.textLabel?.text = "BallisticCalc.SetBullet.Catalog.Filter.CategoryTitle".localized()
            cell.detailTextLabel?.text = categoryData.currentValue
                
            case 1: cell.model = manufactureData
            cell.textLabel?.text = "BallisticCalc.SetBullet.Catalog.Filter.ManufacturerTitle".localized()
            cell.detailTextLabel?.text = manufactureData.currentValue
                
            case 2: cell.model = calibreData
            cell.textLabel?.text = "BallisticCalc.SetBullet.Catalog.Filter.CalibreTitle".localized()
            cell.detailTextLabel?.text = calibreData.currentValue
                
            default: break }
        } else {
            switch indexPath.row {
            case 0: cell.model = manufactureData
            cell.textLabel?.text = "BallisticCalc.SetBullet.Catalog.Filter.ManufacturerTitle".localized()
            cell.detailTextLabel?.text = manufactureData.currentValue
                
            case 1: cell.model = calibreData
            cell.textLabel?.text = "BallisticCalc.SetBullet.Catalog.Filter.CalibreTitle".localized()
            cell.detailTextLabel?.text = calibreData.currentValue
                
            default: break }
        }
        
        return cell
    }
    
//    private func filterDataForCategory() -> [String] {
//        let selectedManufacture = manufactureData.currentValue
//        let selectedCalibre = calibreData.currentValue
//        let dataForCategory = filterData.filter() { bullet in
//            guard let _ = bullet.category else {
//                return false
//            }
//            
//            var isValid = true
//            
//            if selectedManufacture != "Any" {
//                isValid = isValid && (bullet.manufacture == selectedManufacture)
//            }
//            
//            if selectedCalibre != "Any" {
//                isValid = isValid && (bullet.calibreForDescripton == selectedCalibre)
//            }
//            
//            return isValid
//        }
//        
//        return dataForCategory.map() { bullet in
//            return bullet.category!
//        }
//    }
//    
//    private func filterDataForCalibre() -> [String] {
//        let selectedManufacture = manufactureData.currentValue
//        let selectedCategory = categoryData.currentValue
//        let dataForCategory = filterData.filter() { bullet in
//            var isValid = true
//            
//            if selectedManufacture != "Any" {
//                isValid = isValid && (bullet.manufacture == selectedManufacture)
//            }
//            
//            if let _ = bullet.category, selectedCategory != "Any" {
//                isValid = isValid && (bullet.category == selectedCategory)
//            }
//            
//            return isValid
//        }
//        
//        return dataForCategory.map() { bullet in
//            return bullet.calibreForDescripton
//        }
//    }
//    
//    private func filterDataForManufacture() -> [String] {
//        let selectedCategory = categoryData.currentValue
//        let selectedCalibre = calibreData.currentValue
//        let dataForCategory = filterData.filter() { bullet in
//            var isValid = true
//            
//            if let _ = bullet.category, selectedCategory != "Any" {
//                isValid = isValid && (bullet.category == selectedCategory)
//            }
//            
//            if selectedCalibre != "Any" {
//                isValid = isValid && (bullet.calibreForDescripton == selectedCalibre)
//            }
//            
//            return isValid
//        }
//        
//        return dataForCategory.map() { bullet in
//            return bullet.manufacture
//        }
//    }
}

class SetBulletChooseBulletDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    struct Bullet {
        var name: String
        var manufacture: String
        var calibreForDescripton: String
        var index: Int
        
        var category: String?
        var calibre: Double?
        var length: Double?
        var weight: Double?
        var bc: Double?
        var muzzle: Double?
        var profile: UnitsConvertor.Profile?
        var bulletCD: [Double]?
        
        var description: String {
            return manufacture + ", " + name + ", " + calibreForDescripton + (weight != nil ? ", \(weight!)" : "")
        }
    }
    
    private var tableView: UITableView!
    private var data: [(key: Character, value: [Bullet])] = []
    private var filtredData: [(key: Character, value: [Bullet])] = []
    
    var selectedBullet: Bullet?
    var currentSearchName: String = ""
    var manufactureFilter: String?
    var caliberFilter: String?
    var categoryFilter: String?
    
    let AnyKey = "BallisticCalc.SetBullet.Catalog.Filter.Any".localized()
    
    init(tableView: UITableView) {
        super.init()
        
        self.tableView = tableView
    }
    
    func updateData(data: [(key: Character, value: [Bullet])]) {
        self.data = data
        self.filtredData = data
        tableView.reloadData()
    }
    
    func reloadData() {
        filtredData = data
        tableView.reloadData()
    }
    
    func reloadData(newData: [(key: Character, value: [Bullet])]) {
        data = newData
        filtredData = data
        tableView.reloadData()
    }
    
    func filterBy(name: String) {
        filtredData = data
        currentSearchName = name
        
        for (index, _) in data.enumerated() {
            let values = data[index].value.filter({ (bullet) -> Bool in
                let isContainName = bullet.description.lowercased().contains(name.lowercased()) || (name == "")
                
                var isContainManufacture = true
                if let filter = manufactureFilter, filter != AnyKey {
                    isContainManufacture = (bullet.manufacture == filter)
                }
                
                var isContainCaliber = true
                if let filter = caliberFilter, filter != AnyKey {
                    isContainCaliber = (bullet.calibreForDescripton == filter)
                }
                
                var isContainCategory = true
                if let filter = categoryFilter, filter != AnyKey {
                    isContainCategory = (bullet.category == filter)
                }
                
                return (isContainManufacture && isContainCaliber && isContainCategory) && isContainName
            })
            filtredData[index].value = values
        }
        filtredData = filtredData.filter({ (filtredValues) -> Bool in
            return filtredValues.value.count > 0
        })
        
        tableView.reloadData()
    }
    
    func filterBy(manufacture: String?, caliber: String?, category: String?) {
        manufactureFilter = manufacture
        caliberFilter = caliber
        categoryFilter = category
        filterBy(name: currentSearchName)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filtredData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtredData[section].value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let cellBullet = filtredData[indexPath.section].value[indexPath.row]
        if selectedBullet?.index == cellBullet.index {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        cell.textLabel?.text = cellBullet.description
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ("\(filtredData[section].key)")
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = .hex("7692A8")
        header.backgroundColor = .hex("28343F")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedBullet = filtredData[indexPath.section].value[indexPath.row]
        tableView.reloadData()
    }
}

class SetBulletChooseBulletViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var filterTitleLabel: UILabel!
    @IBOutlet weak var customTabBarView: CustomTabBarView!
    var searchBar: UISearchBar!
    
    private var dataSource: SetBulletChooseBulletDataSource!
    private var filterDataSource: ChooseBulletFilter!
    var model: SetBallisticCalculatorBaseModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableViews()
        setupView()
        
        customTabBarView.addItem(title: "BallisticCalc.SetBullet.Catalog.Ammunitions".localized().uppercased()) { index in
            
            self.tableView.contentOffset = .zero
            self.resetSearchView()
            self.updateDataSourceWithAmmunition()
            
            return true
        }.isSelected = true
        customTabBarView.addItem(title: "BallisticCalc.SetBullet.Catalog.Bullets".localized().uppercased()) { index in
            
            self.tableView.contentOffset = .zero
            self.resetSearchView()
            self.updateDataSourceWithBullets()
            
            return true
        }
        
        if self.model.bullet.isWithAmmunition {
            self.updateDataSourceWithAmmunition()
        } else {
            updateDataSourceWithBullets()
            
            customTabBarView.selectItemBy(1)
        }
        
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "BCSetupPresetCatalog_open")
    }
    
    private func resetSearchView() {
        self.searchBar.resignFirstResponder()
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.text = ""
    }
    
    private func setupView() {
        let backgroundSearchView = UIView(frame:
            CGRect(x: 0, y: 0, width: tableView.frame.width, height: 44))
        searchBar = UISearchBar(frame: .zero)
        searchBar.searchBarStyle = .default
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        searchBar.placeholder = "BallisticCalc.SetBullet.Catalog.SearchPlaceholder".localized()
        searchBar.backgroundColor = .hex("01111B")
        backgroundSearchView.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBar.leadingAnchor.constraint(equalTo: backgroundSearchView.leadingAnchor),
            searchBar.trailingAnchor.constraint(equalTo: backgroundSearchView.trailingAnchor),
            searchBar.topAnchor.constraint(equalTo: backgroundSearchView.topAnchor),
            searchBar.bottomAnchor.constraint(equalTo: backgroundSearchView.bottomAnchor)
        ])
        tableView.tableHeaderView = backgroundSearchView
        
        searchBar.setSearchFieldBackgroundImage(UIImage(named: "bc_search_pixel_bg"), for: .normal)
        searchBar.backgroundImage = UIImage(named: "bc_search_bg")
        let searchTextFieldStyle = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        searchTextFieldStyle.textColor = .white
        
        customTabBarView.backgroundView.backgroundColor = .clear
        customTabBarView.style = .default
        customTabBarView.colorForDeselectedtems(color: .white)
        
        let leftButton = UIBarButtonItem(title: "DevicesList.MyDevice.Done".localized(),
                                         style: .plain,
                                         target: self,
                                         action: #selector(doneAction))
        navigationItem.rightBarButtonItem = leftButton
    }
    
    private func setupTableViews() {
        filterDataSource = ChooseBulletFilter(tableView: filterTableView, presenterViewController: self)
        filterTableView.dataSource = filterDataSource
        filterTableView.delegate = filterDataSource
        filterDataSource.filterWasChanged = { newData in
            self.dataSource.reloadData(newData: newData)
        }
        
        dataSource = SetBulletChooseBulletDataSource(tableView: tableView)
        dataSource.selectedBullet = model.bullet.selectedBullet
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.backgroundView = UIView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let model = (sender as! CatalogFilterTableViewCell).model!
        
        let navigationViewController = segue.destination as! UINavigationController
        let filterViewController = navigationViewController.viewControllers[0] as! CatalogFilterViewController
        filterViewController.data = model.data
        filterViewController.selected = model.selected
        filterViewController.filterType = model.type
        filterViewController.editFinished = { cancelled, selected, filter in
            if !cancelled {
                let dataForFilter = self.filterDataSource.selectFilterBy(type: filter, selected: selected)
                self.dataSource.filterBy(manufacture: dataForFilter.manufacture,
                                         caliber: dataForFilter.calibre,
                                         category: dataForFilter.category)
            }
        }
    }
    
    func localizeView() {
        filterTitleLabel.text = "BallisticCalc.SetBullet.Catalog.FiltersTitle".localized().uppercased()
        title = "BallisticCalc.SetBullet.Catalog.Title".localized()
    }
    
    @IBAction func doneAction() {
        if let _ = dataSource.selectedBullet {
            let bullet = dataSource.selectedBullet
            model.bullet.selectedBullet = bullet
            
            if let calibre = bullet?.calibre {
                model.bullet.caliber =
                SetBulletModel.CaliberModel(value: (value: calibre, unit: .inch))
            } else {
                model.bullet.caliber = nil
            }
            if let bc = bullet?.bc {
                model.bullet.coefficient =
                    SetBulletModel.CoefficientModel(value: (value: bc, unit: .raw))
            } else {
                model.bullet.coefficient = nil
            }
            if let length = bullet?.length {
                model.bullet.length =
                    SetBulletModel.LengthModel(value: (value: length, unit: .inch))
            } else {
                model.bullet.length = nil
            }
            if let muzzle = bullet?.muzzle {
                model.rifle.muzzle =
                    SetRifleModel.MuzzleModel(value: (value: muzzle, unit: .ms))
            } else {
                model.rifle.muzzle = nil
            }
            if let weight = bullet?.weight {
                model.bullet.weight =
                    SetBulletModel.WeightModel(value: (value: weight, unit: .grain))
            } else {
                model.bullet.weight = nil
            }
            if let profile = bullet?.profile {
                model.bullet.profile =
                    SetBulletModel.ProfileModel(value: (value: 0, unit: profile))
            } else {
                model.bullet.profile = nil
            }
            if let bulletCD = bullet?.bulletCD {
                model.bullet.bulletCD = bulletCD
            } else {
                model.bullet.bulletCD = []
            }
            
            model.bullet.isWithAmmunition = customTabBarView.currentSelectedIndex == 0
            
            navigationController?.popViewController(animated: true)
        }
    }
    
    func updateDataSourceWithBullets(dbSource : [String : UnitsConvertor.Profile] = ["PULI_full_bore" : .g7, "PULI_rim" : .g1]) {
        var result = [(key: Character, value: [SetBulletChooseBulletDataSource.Bullet])]()
        let group = DispatchGroup()
        
        DispatchQueue.global(qos: .userInitiated).async {
            for (name, profile) in dbSource {
                let filePath = Bundle.main.path(forResource: name, ofType: "csv")!
                let importer = CSVImporter<[String]>(path: filePath)
                
                group.enter()
                importer.startImportingRecords { $0 }.onFinish { importedRecords in
                    var tempData: [Character: [SetBulletChooseBulletDataSource.Bullet]] = [:]
                    for (index , record) in importedRecords.enumerated() {
                        if index == 0 {
                            continue
                        }
                        
                        let bullet = SetBulletChooseBulletDataSource.Bullet(name: record[1],
                                                                            manufacture: record[0],
                                                                            calibreForDescripton: record[2],
                                                                            index: index,
                                                                            category: nil,
                                                                            calibre: Double(record[3]),
                                                                            length: Double(record[4]),
                                                                            weight: Double(record[6]),
                                                                            bc: Double(record[5]),
                                                                            muzzle: nil,
                                                                            profile: profile,
                                                                            bulletCD: nil)
                        
                        let latter = bullet.manufacture.first!
                        var currentData = tempData[latter] ?? []
                        currentData.append(bullet)
                        tempData[latter] = currentData
                    }
                    let data = tempData.sorted(by: { (lhs, rhs) -> Bool in
                        return "\(lhs.key)" < "\(rhs.key)"
                    })
                    result.append(contentsOf: data)
                    group.leave()
                }
            }
            group.wait()
            
            DispatchQueue.main.async {
                var mixedResult: [Character: [SetBulletChooseBulletDataSource.Bullet]] = [:]
                result.forEach() {
                    mixedResult[$0.key] = $0.value.sorted() { lhs, rhs in
                        return lhs.manufacture < rhs.manufacture
                    }
                }
                
                let finalResult = mixedResult.sorted(by: { (lhs, rhs) -> Bool in
                    return "\(lhs.key)" < "\(rhs.key)"
                })
                self.updateData(isBullet: true, data: finalResult)
            }
        }
    }
    
    func updateDataSourceWithAmmunition() {
        let filePath = Bundle.main.path(forResource: "ammunition", ofType: "csv")!
        let importer = CSVImporter<[String]>(path: filePath)
        
        SVProgressHUD.show()
        importer.startImportingRecords { $0 }.onFinish { importedRecords in
            var tempData: [Character: [SetBulletChooseBulletDataSource.Bullet]] = [:]
            for (index , record) in importedRecords.enumerated() {
                if index == 0 || record[1] == "" {
                    continue
                }
                
                let bullet = SetBulletChooseBulletDataSource.Bullet(name: record[2],
                                                                    manufacture: record[1],
                                                                    calibreForDescripton: record[3],
                                                                    index: index,
                                                                    category: record[11],
                                                                    calibre: Double(record[4]),
                                                                    length: nil,
                                                                    weight: Double(record[9]),
                                                                    bc: Double(record[7]),
                                                                    muzzle: Double(record[6]),
                                                                    profile: .g1,
                                                                    bulletCD: nil)
                let latter = bullet.manufacture.first!
                var currentData = tempData[latter] ?? []
                currentData.append(bullet)
                tempData[latter] = currentData
            }
            let data = tempData.sorted(by: { (lhs, rhs) -> Bool in
                return "\(lhs.key)" < "\(rhs.key)"
            })
            self.updateData(isBullet: false, data: data)
            SVProgressHUD.dismiss()
        }
    }
    
    private func updateData(isBullet: Bool, data: [(key: Character, value: [SetBulletChooseBulletDataSource.Bullet])]) {
        self.dataSource.updateData(data: data)
        self.filterDataSource.reloadDataFor(bullet: false, data: data)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dataSource.filterBy(name: searchBar.text!)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        dataSource.filterBy(name: searchBar.text!)
    }

}
