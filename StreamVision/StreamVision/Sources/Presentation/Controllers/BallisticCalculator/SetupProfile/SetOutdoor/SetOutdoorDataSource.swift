//
//  BallisticCalculatorSetBulletDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/7/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class SetOutdoorDataSource: NSObject, SetupProfileDataSourceProtocol {
    enum SetOutdoor: String {
        case temperature = "Temperature"
        case pressure = "Pressure"
        case humidity = "Humidity"
    }
    enum Section: Int {
        case section1
    }
    private var items: [Section: [SetOutdoor] ] = [.section1 : [.temperature, .pressure, .humidity]]
    var tableView: UITableView!
    
    weak var headerLabel: UILabel!
    var model: SetBallisticCalculatorBaseModel!
    var isNeedValidate: Bool = false
    
    @discardableResult
    func reloadWithValidation() -> Bool {
        return reloadDataSource(validation: {
            return false
        }())
    }
    
    func reloadDataSource() {
        headerLabel.text = "BallisticCalc.SetOutdoor.Description".localized()
        reloadDataSource(validation: false)
    }
    
    @discardableResult
    func reloadDataSource(validation: Bool) -> Bool {
        isNeedValidate = validation
        
        tableView.reloadData()
        return isNeedValidate
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch items[Section(rawValue: indexPath.section)!]![indexPath.row] {
        case .temperature:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.outdoor.temperature?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = .gray
                    details = SetOutdoorModel.TemperatureModel.createParameter(SetOutdoorModel.TemperatureModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetOutdoor.Temperature".localized(),
                               details: details)
                cell.tag = SetOutdoor.temperature.rawValue.hash
                
                return cell
            }
        case .pressure:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.outdoor.pressure?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = .gray
                    details = SetOutdoorModel.PressureModel.createParameter(SetOutdoorModel.PressureModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetOutdoor.Pressure".localized(),
                               details: details)
                cell.tag = SetOutdoor.pressure.rawValue.hash
                
                return cell
            }
        case .humidity:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.outdoor.humidity?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = .gray
                    details = SetOutdoorModel.HumidityModel.createParameter(SetOutdoorModel.HumidityModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetOutdoor.Humidity".localized(),
                               details: details)
                cell.tag = SetOutdoor.humidity.rawValue.hash

                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[Section(rawValue: section)!]!.count
    }
}
