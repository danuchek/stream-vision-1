//
//  BallisticCalculatorSetBulletDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/7/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class SetRifleDataSource: NSObject, SetupProfileDataSourceProtocol {
    enum SetRifle: String {
        case sights = "Sights"
        case zero = "Zero"
        case clickValue = "ClickValue"
    }
    enum Section: Int {
        case section1 = 0
    }
    private var items: [Section: [SetRifle] ] = [.section1 : [.sights, .zero]]
    var tableView: UITableView!
    
    weak var headerLabel: UILabel!
    var model: SetBallisticCalculatorBaseModel!
    var isNeedValidate: Bool = false
    
    @discardableResult
    func reloadWithValidation() -> Bool {
        return reloadDataSource(validation: {
            return self.model.rifle.sights == nil ||
                   self.model.rifle.zero == nil ||
                   self.model.rifle.clickValue == nil
        }())
    }
    
    func reloadDataSource() {
        headerLabel.text = "BallisticCalc.SetRifle.Description".localized()
        reloadDataSource(validation: false)
    }
    
    @discardableResult
    func reloadDataSource(validation: Bool) -> Bool {
        isNeedValidate = validation
        
        items = [.section1 : [.sights, .zero, .clickValue]]
        
        tableView.reloadData()
        return isNeedValidate
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch items[Section(rawValue: indexPath.section)!]![indexPath.row] {
        case .sights:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.rifle.sights?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetRifleModel.SightsModel.createParameter(SetRifleModel.SightsModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetRifle.SightsHeight".localized(),
                               details: details)
                cell.tag = SetRifle.sights.rawValue.hash
                
                return cell
            }
        case .zero:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.rifle.zero?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetRifleModel.ZeroModel.createParameter(SetRifleModel.ZeroModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetRifle.ZeroRange".localized(),
                               details: details)
                cell.tag = SetRifle.zero.rawValue.hash

                return cell
            }
        case .clickValue:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.rifle.clickValue?.localizedType
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = isNeedValidate ? .hex("#FF2D47") : .gray
                    details = SetRifleModel.ClickValueModel.createParameter(SetRifleModel.ClickValueModel.self).localizedType
                }
                cell.setupCell(title: "BallisticCalc.SetRifle.ClickValue".localized(),
                               details: details)
                cell.tag = SetRifle.clickValue.rawValue.hash
                
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "BallisticCalc.SetRifle.ParametersTitle".localized()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[Section(rawValue: section)!]!.count
    }
}
