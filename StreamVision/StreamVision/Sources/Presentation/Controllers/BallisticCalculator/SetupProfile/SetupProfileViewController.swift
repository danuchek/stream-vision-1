//
//  SetBulletsViewController.swift
//  StreamVision
//
//  Created by Oleksii Shvachenko on 05.02.17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import RealmSwift
import LSStatusBarHUD

extension SetupProfileViewController {
    func selectedCellFromSetBullet(identifier: Int) {
        let model = dataSource.model!
        
        switch (identifier) {
        // Section 1
        case (SetBulletsDataSource.SetBullet.bulletCatalog.rawValue.hash):
            performSegue(withIdentifier: "to choose bullet", sender: model)
            
        // Section 2
        case (SetBulletsDataSource.SetBullet.coefficient.rawValue.hash):
            processSelectionCellWith(model: model.bullet.coefficient,
                                     title: "BallisticCalc.SetBullet.BallisticCoefficient".localized(),
                                     isWithType: false)
            { canceled, newModel in
                if !canceled {
                    model.bullet.coefficient = newModel
                    self.dataSource.reloadDataSource()
                }
            }
            
        case (SetBulletsDataSource.SetBullet.profile.rawValue.hash):
            processSelectionCellWith(model: model.bullet.profile,
                                     title: "BallisticCalc.SetBullet.BallisticTable".localized())
            { canceled, newModel in
                if !canceled {
                    model.bullet.profile = newModel
                    self.dataSource.reloadDataSource()
                }
            }
            
        case (SetBulletsDataSource.SetBullet.velocity.rawValue.hash):
            processSelectionCellWith(model: model.rifle.muzzle,
                                     title: "BallisticCalc.SetRifle.MuzzleVelocity".localized())
            { canceled, newModel in
                if !canceled {
                    model.rifle.muzzle = newModel
                    self.dataSource.reloadDataSource()
                    
                    if let _ = model.rifle.zero?.value.value {
                    model.rifle.zero!.limits = self.updateZeroLimit(muzzle: model.rifle.muzzle!,
                                                                    limits:  model.rifle.zero!.limits)
                    let selectedZeroLimit = model.rifle.zero!.selectedLimit.limits.max
                        if model.rifle.zero!.value.value > selectedZeroLimit {
                            model.rifle.zero!.value.value = selectedZeroLimit
                        }
                    }
                }
            }
            
        // Section 3
        case (SetBulletsDataSource.SetBullet.weight.rawValue.hash):
            processSelectionCellWith(model: model.bullet.weight,
                                     title: "BallisticCalc.SetBullet.Weight".localized())
            { canceled, newModel in
                if !canceled {
                    model.bullet.weight = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetBulletsDataSource.SetBullet.length.rawValue.hash):
            processSelectionCellWith(model: model.bullet.length,
                                     title: "BallisticCalc.SetBullet.Length".localized())
            { canceled, newModel in
                if !canceled {
                    model.bullet.length = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetBulletsDataSource.SetBullet.caliber.rawValue.hash):
            processSelectionCellWith(model: model.bullet.caliber,
                                     title: "BallisticCalc.SetBullet.Caliber".localized())
            { canceled, newModel in
                if !canceled {
                    model.bullet.caliber = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetBulletsDataSource.SetBullet.twistType.rawValue.hash):
            processSelectionCellWith(model: model.rifle.twistDirection,
                                     title: "BallisticCalc.SetRifle.TwistType.Title".localized())
            { canceled, newModel in
                if !canceled {
                    model.rifle.twistDirection = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetBulletsDataSource.SetBullet.twistValue.rawValue.hash):
            processSelectionCellWith(model: model.rifle.twistValue,
                                     title: "BallisticCalc.SetRifle.TwistNumber".localized(),
                                     isWithType: false)
            { canceled, newModel in
                if !canceled {
                    model.rifle.twistValue = newModel
                    self.dataSource.reloadDataSource()
                }
            }
            
        default: break
        }
    }
    
    private func updateZeroLimit(muzzle: SetRifleModel.MuzzleModel,
                                     limits: [(limits: (max: Double, min: Double),
        pattern: (integer: Int, decimal: Int),
        units: UnitsConvertor.Distance)]) ->
        ([(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsConvertor.Distance)])
    {
        return
            { value, units, limits in
                let distance = (value: value, units: units)
                return limits.map {
                    return (limits: (max: UnitsConvertor.convertUnitsBy(value: distance, destUnits: $0.units).0, min: $0.limits.min),
                            pattern: $0.pattern, units: $0.units)
                }
            }(muzzle.value.value * 1.0 / 2.0, { units -> UnitsConvertor.Distance in
                switch units {
                case .ms: return .m
                case .fps: return .yd
                default: return .m
                }
            }(muzzle.selectedLimit.units), limits)
    }
    
    func selectedCellFromSetRifle(identifier: Int) {
        let model = dataSource.model!
        
        switch (identifier) {
        case (SetRifleDataSource.SetRifle.sights.rawValue.hash):
            processSelectionCellWith(model: model.rifle.sights,
                                     title: "BallisticCalc.SetRifle.SightsHeight".localized())
            { canceled, newModel in
                if !canceled {
                    model.rifle.sights = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetRifleDataSource.SetRifle.zero.rawValue.hash):
            var rifleModel = model.rifle.zero
            if rifleModel == nil {
                rifleModel = SetRifleModel.ZeroModel.createParameter(SetRifleModel.ZeroModel.self)
            }
            rifleModel!.limits = self.updateZeroLimit(muzzle: model.rifle.muzzle!,
                                                      limits: rifleModel!.limits)
            processSelectionCellWith(model: rifleModel,
                                     title: "BallisticCalc.SetRifle.ZeroRange".localized())
            { canceled, newModel in
                if !canceled {
                    model.rifle.zero = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetRifleDataSource.SetRifle.clickValue.rawValue.hash):
            processSelectionCellWith(model: model.rifle.clickValue,
                                     title: "BallisticCalc.SetRifle.ClickValue".localized())
            { canceled, newModel in
                if !canceled {
                    model.rifle.clickValue = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        
        default: break
        }
    }
    
    func selectedCellFromSetOutdoor(identifier: Int) {
        let model = dataSource.model!
        
        switch (identifier) {
        case (SetOutdoorDataSource.SetOutdoor.temperature.rawValue.hash):
            processSelectionCellWith(model: model.outdoor.temperature, title: "BallisticCalc.SetOutdoor.Temperature".localized())
            { canceled, newModel in
                if !canceled {
                    model.outdoor.temperature = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetOutdoorDataSource.SetOutdoor.pressure.rawValue.hash):
            processSelectionCellWith(model: model.outdoor.pressure,
                                     title: "BallisticCalc.SetOutdoor.Pressure".localized())
            { canceled, newModel in
                if !canceled {
                    model.outdoor.pressure = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (SetOutdoorDataSource.SetOutdoor.humidity.rawValue.hash):
            processSelectionCellWith(model: model.outdoor.humidity,
                                     title: "BallisticCalc.SetOutdoor.Humidity".localized())
            { canceled, newModel in
                if !canceled {
                    model.outdoor.humidity = newModel
                    self.dataSource.reloadDataSource()
                }
            }
            
        default: break
        }
    }
}

class SetupProfileViewController: UIViewController, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var customTabbarView: CustomTabBarView!
    
    fileprivate var dataSource: SetupProfileDataSourceProtocol!
    fileprivate var bulletsDataSource = SetBulletsDataSource()
    fileprivate var rifleDataSource = SetRifleDataSource()
    fileprivate var outdoorDataSource = SetOutdoorDataSource()
    
    var screenCompleted = 0
    @IBOutlet weak var headerLabel: UILabel!
    var model: SetBallisticCalculatorBaseModel!
    var preset: BallisticCalcPreset?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "BallisticCalc.DefaultTitle".localized()
        
        if model == nil {
            model = SetBallisticCalculatorBaseModel()
            model.bullet.isWithCustomBullet = true
            navigationItem.rightBarButtonItem =
                UIBarButtonItem(title: "BallisticCalc.Wizard.Next".localized(),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(nextScreen))
        } else {
            screenCompleted = 1
            navigationItem.rightBarButtonItem =
                UIBarButtonItem(title: "BallisticCalc.Wizard.Save".localized(),
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(saveAction))
        }
        
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 25;
        
        bulletsDataSource.tableView = tableView
        bulletsDataSource.headerLabel = headerLabel
        bulletsDataSource.model = model
        
        rifleDataSource.tableView = tableView
        rifleDataSource.headerLabel = headerLabel
        rifleDataSource.model = model
        
        outdoorDataSource.tableView = tableView
        outdoorDataSource.headerLabel = headerLabel
        outdoorDataSource.model = model
        
        dataSource = bulletsDataSource
        
        tableView.delegate = self
        tableView.dataSource = dataSource
        
        customTabbarView.addItem(title:
        "BallisticCalc.Wizard.SetBullet".localized().uppercased()) { index in
            self.dataSource = self.bulletsDataSource
            self.tableView.dataSource = self.dataSource
            self.dataSource.reloadDataSource()
            
            return true
        }.isSelected = true
        
        customTabbarView.addItem(title:
        "BallisticCalc.Wizard.SetRifle".localized().uppercased()) { index in
            if !self.dataSource.reloadWithValidation() {
                self.dataSource = self.rifleDataSource
                self.tableView.dataSource = self.dataSource
                self.dataSource.reloadDataSource()
                self.screenCompleted = 1
                
                return true
            } else {
                self.showNotification()
            }
            
            return false
        }
        
        customTabbarView.addItem(title:
        "BallisticCalc.Wizard.SetOutdoor".localized().uppercased()) { index in
            if !self.dataSource.reloadWithValidation() {
                if self.screenCompleted != 1 || self.model.rifle.sights == nil ||
                                                self.model.rifle.zero == nil ||
                                                self.model.rifle.clickValue == nil {
                    return false
                }
                self.dataSource = self.outdoorDataSource
                self.tableView.dataSource = self.dataSource
                self.dataSource.reloadDataSource()
                
                return true
            } else {
                self.showNotification()
            }
            
            return false
        }
        
        customTabbarView.style = .withNumeration
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        googleReport(action: "BCSetupPreset_open")
        dataSource.reloadDataSource()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let cell = tableView.cellForRow(at: indexPath)!
        switch dataSource {
        case is SetBulletsDataSource:
            selectedCellFromSetBullet(identifier: cell.tag)
        case is SetRifleDataSource:
            selectedCellFromSetRifle(identifier: cell.tag)
        case is SetOutdoorDataSource:
            selectedCellFromSetOutdoor(identifier: cell.tag)
        default:
            break
        }
    }
    
    fileprivate func showNotification() {
        let text = LSStatusBarHUD.createAttributedText("BallisticCalc.SetProfile.ValidationError".localized(), color: .white, font: UIFont(name: "Roboto-Regular", size: 14.0))
        LSStatusBarHUD.showMessage(text, backgroundColor: .hex("#FF2D47"))
    }
    
    @objc fileprivate func nextScreen() {
        if customTabbarView.currentSelectedIndex == 2 {
            if !dataSource.reloadWithValidation() {
                performSegue(withIdentifier: "to save preset", sender: model)
            }
        }
        
        customTabbarView.turnNextItem()
    }
    
    @objc fileprivate func saveAction() {
        view.endEditing(true)
        
        guard !dataSource.reloadWithValidation() else {
            showNotification()
            return
        }
        
        let realm = try! Realm()
        let updatedPreset = BallisticCalcPreset.by(model!)
        updatedPreset.name = preset!.name
        updatedPreset.date = preset!.date
        try! realm.write {
            realm.add(updatedPreset, update: .all)
        }
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func processSelectionCellWith<T: BallisticCalcParameterProtocol, D>(model: T?,
                                  title: String = "",
                                  isWithType: Bool = true,
                                  completion: @escaping (_ canceled: Bool,
                                                         _ model: T) -> ()) -> () where D.RawValue == Int, T.UnitsType == D {
        var resultModel = model ?? T.createParameter(T.self)
        var pickerData: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: String)] = []
        let localizedUnits = UnitsConvertor.allLocalizedUnitsBy(resultModel.value.unit as! UnitsProtocol)
        for (i, limits) in resultModel.limits.enumerated() {
            pickerData.append( (limits: limits.limits, pattern: limits.pattern, units: localizedUnits[i]) )
        }
        let pickerModel =
            BCSinglePickerViewModel(value: resultModel.value.value,
                                    title: title,
                                    units: resultModel.value.unit.rawValue,
                                    isWithType: isWithType,
                                    data: pickerData,
                                    didSelectValue: { (value, rawUnits) in
                                        let result =
                                            self.processValueFromPicker(value: value,
                                                                        rawUnits: rawUnits,
                                                                        model: &resultModel)
                                        return result?.0
            },
                                    editFinished: { canceled in
                                        completion( !canceled, resultModel)
            })
        performSegue(withIdentifier: "BallisticCalcPickerViewController", sender: pickerModel)
    }
    
    func processValueFromPicker<T: BallisticCalcParameterProtocol, D>(value: Double, rawUnits: Int, model: inout T) -> (Double, D)? where D.RawValue == Int, T.UnitsType == D {
        let type = Swift.type(of: model.value.unit)
        var convertedValue: (Double, D)? = nil
        let units = type.init(rawValue: rawUnits)!
        if model.value.unit != units {
            model.value.value = value
            convertedValue = UnitsConvertor.convertUnitsBy(value: model.value, destUnits: units)
            model.value = convertedValue!
        } else {
            model.value.value = value
            convertedValue = (value, units)
        }
        
        return convertedValue
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "BallisticCalcPickerViewController":
            let pickerNavigationViewController = segue.destination as! UINavigationController
            let pickerViewController = pickerNavigationViewController.viewControllers[0] as! BCSinglePickerViewController
            pickerViewController.model = sender as? BCSinglePickerViewModel
        case "to save preset":
            let savePresetViewController = segue.destination as! SavePresetViewController
            savePresetViewController.model = sender as? SetBallisticCalculatorBaseModel
        case "to choose bullet":
            let chooseBulletViewController = segue.destination as! SetBulletChooseBulletViewController
            chooseBulletViewController.model = sender as? SetBallisticCalculatorBaseModel
            
        default:
            break
        }
        
        Logging.log(message: "[BC] Sender:\(String(describing: sender)) Source:\(segue.source)", toFile: true, toFabric: true)
    }
}
