//
//  FilterViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/17/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class CatalogFilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    var data: [String] = []
    var selected: Int = 0
    var filterType: ChooseBulletFilter.FilterType!
    var editFinished: ((_ cancelled: Bool,
                        _ selected: Int,
                        _ filterType: ChooseBulletFilter.FilterType) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doneButton.title = "DevicesList.MyDevice.Done".localized()
        cancelButton.title = "General.Alert.Cancel".localized()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell")!
        cell.textLabel?.text = data[indexPath.row]
        cell.accessoryType = indexPath.row == selected ? .checkmark : .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected = indexPath.row
        tableView.reloadData()
    }
    
    @IBAction func cancellAction() {
        editFinished?(true, 0, filterType)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction() {
        editFinished?(false, selected, filterType)
        self.dismiss(animated: true, completion: nil)
    }
}
