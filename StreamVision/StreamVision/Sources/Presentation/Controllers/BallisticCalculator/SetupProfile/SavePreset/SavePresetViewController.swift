//
//  SavePresetViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/24/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import RealmSwift

class SavePresetViewController: UIViewController, UITextFieldDelegate {
    // Private
    var presetName: String?
    var model: SetBallisticCalculatorBaseModel!
    
    @IBOutlet weak var overviewTableView: UITableView!
    @IBOutlet weak var editTextField: UITextField!
    
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var overViewLabel: UILabel!
    @IBOutlet weak var inputTitleLabel: UILabel!
    

    
    @IBOutlet weak var customTabbarView: CustomTabBarView!
    
    var dataSource: OverviewDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        localizeView()
        
        customTabbarView.addItem(title: "BallisticCalc.Wizard.SetBullet".localized().uppercased()) { index in
            self.popBack(index: index)
            return true
        }
        
        customTabbarView.addItem(title: "BallisticCalc.Wizard.SetRifle".localized().uppercased()) { index in
            self.popBack(index: index)
            return true
        }
        
        customTabbarView.addItem(title: "BallisticCalc.Wizard.SetOutdoor".localized().uppercased()) { index in
            self.popBack(index: index)
            return true
        }
        customTabbarView.style = .withNumeration
        
        editTextField.delegate = self
        dataSource = OverviewDataSource(tableView: overviewTableView)
        overviewTableView.dataSource = dataSource
        dataSource.reloadTableView(item: model)

        navigationItem.rightBarButtonItem =
            UIBarButtonItem(title:"BallisticCalc.Wizard.Save".localized(),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(popToRoot))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "BCSavePreset_open")
    }
    
    private func localizeView() {
        subTitleLabel.text = "BallisticCalc.EnterName.Description".localized()
        overViewLabel.text = "BallisticCalc.EnterName.Overview".localized().uppercased()
        inputTitleLabel.text = "BallisticCalc.EnterName.InputTitle".localized().uppercased()
        title = "BallisticCalc.Wizard.SavePreset.Title".localized()
    }
    
    func popBack(index: Int) {
        let viewControllers = self.navigationController!.viewControllers
        let setupProfileViewController = viewControllers[viewControllers.count - 2] as! SetupProfileViewController
        setupProfileViewController.customTabbarView.turnItem(to: index)
        self.navigationController!.popViewController(animated: true)

    }

    @objc func popToRoot() {
        editTextField.resignFirstResponder()
        
        guard let _ = presetName, presetName != "" else {
            let alert = UIAlertController(title: "BallisticCalc.Wizard.Error.NameShouldNotBeEmpty".localized(), message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        let realm = try! Realm()
        let presets = realm.objects(BallisticCalcPreset.self).filter("name == '\(presetName!)'")
        if presets.count > 0 {
            let alert = UIAlertController(title: "BallisticCalc.Wizard.Error.NameIsAlreadyInUse".localized(), message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        let preset = BallisticCalcPreset.by(model)
        preset.name = presetName!
        preset.date = Date()
        
        try! realm.write {
            realm.add(preset)
        }
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        presetName = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount) {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 25
    }

}
