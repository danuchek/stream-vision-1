//
//  BallisticCalculatorNavigationVC.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/24/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import RealmSwift

class BallisticCalcNavigationVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let realm = try! Realm()
        if realm.objects(BallisticCalcPreset.self).count == 0 {
            performSegue(withIdentifier: "to wizard", sender: nil)
        }
    }

}
