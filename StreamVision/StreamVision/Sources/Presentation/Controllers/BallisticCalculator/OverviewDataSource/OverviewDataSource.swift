//
//  OverviewDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/29/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class OverviewDataSource: NSObject, UITableViewDataSource {
    var tableView: UITableView!
    var parameters: [(title: String, iconName: String)] = []
    
    init(tableView: UITableView) {
        self.tableView = tableView
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 60.0
    }
    
    func reloadTableView() {
        parameters = []
        tableView.reloadData()
    }
    
    func reloadTableView(item: SetBallisticCalculatorBaseModel) {
        parameters = []
        
        parameters.append(
            (createBulletStringBy(item.bullet), "bc_bullet")
        )
        parameters.append(
            (createRifleStringBy(item.rifle), "bc_rifle")
        )
        parameters.append(
            (createTemperatureStringBy(item.outdoor), "bc_outdoor")
        )
        
        tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parameters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewTableViewCell") as! OverviewTableViewCell
        cell.textLabel?.text = parameters[indexPath.row].title
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 12.0)
        cell.textLabel?.textColor = .rgba(255, 255, 255, 0.7)
        cell.imageView?.image = UIImage(named: parameters[indexPath.row].iconName)
        return cell
    }
}

extension OverviewDataSource {
    func createBulletStringBy(_ bulletModel: SetBulletModel) -> String {
        var resultString = ""
        
        if let selectedBullet = bulletModel.selectedBullet {
            resultString += selectedBullet.manufacture + " " + selectedBullet.name + "\n"
        }
        
        if let coefficient = bulletModel.coefficient, (bulletModel.isWithAmmunition || bulletModel.isWithCustomBullet) {
            resultString += "BallisticCalc.SetBullet.BallisticCoefficient".localized() + ", \(coefficient.value.value)"
        }
        
        if let profile = bulletModel.profile?.localizedType {
            resultString += "\n" + "BallisticCalc.SetBullet.BallisticTable".localized() + ", " + profile
        }
        
        if let caliber = bulletModel.caliber?.localizedValue, bulletModel.isWithGyroEffect {
            resultString += "\n" + "BallisticCalc.SetBullet.Caliber".localized() + ", " + caliber
        }
        
        if let weigth = bulletModel.weight?.localizedValue, bulletModel.isWithGyroEffect {
            resultString += "\n" + "BallisticCalc.SetBullet.Weight".localized() + ", " + weigth
        }
        
        if let length = bulletModel.length?.localizedValue, bulletModel.isWithGyroEffect {
            resultString += "\n" + "BallisticCalc.SetBullet.Length".localized() + ", " + length
        }
        
        return resultString
    }
    
    func createRifleStringBy(_ model: SetRifleModel) -> String {
        return "BallisticCalc.SetRifle.MuzzleVelocity".localized() + ", " + model.muzzle!.localizedValue + "\n"
        + "BallisticCalc.SetRifle.SightsHeight".localized() + ", " + model.sights!.localizedValue + "\n"
        + "BallisticCalc.SetRifle.ZeroRange".localized() + ", " + model.zero!.localizedValue + "\n"
        + "BallisticCalc.SetRifle.ClickValue".localized() + ", " + model.clickValue!.localizedType
    }
    
    func createTemperatureStringBy(_ model: SetOutdoorModel) -> String {
        var result = ""
        let temperature = model.temperature?.localizedValue ??
            SetOutdoorModel.TemperatureModel.createParameter(
                SetOutdoorModel.TemperatureModel.self).localizedValue
        result += "BallisticCalc.SetOutdoor.Temperature".localized() + ", " + temperature + "\n"
        
        let presure = model.pressure?.localizedValue ??
                SetOutdoorModel.PressureModel.createParameter(
                    SetOutdoorModel.PressureModel.self).localizedValue
        result += "BallisticCalc.SetOutdoor.Pressure".localized() + ", " + presure + "\n"
        
        let humidity = model.humidity?.localizedValue ??
            SetOutdoorModel.HumidityModel.createParameter(
                SetOutdoorModel.HumidityModel.self).localizedValue
        result += "BallisticCalc.SetOutdoor.Humidity".localized() + ", " + humidity
        
        return result
    }
}
