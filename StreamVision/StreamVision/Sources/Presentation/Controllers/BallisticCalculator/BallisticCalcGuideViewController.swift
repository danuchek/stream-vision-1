//
//  BallisticCalculatorInitialViewController.swift
//  StreamVision
//
//  Created by Oleksii Shvachenko on 05.02.17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BallisticCalcGuideViewController: UIViewController {
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var setCalculatorButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let revealController = self.revealViewController() {
            view.addGestureRecognizer(revealController.panGestureRecognizer())
        }
        
        let menu = MenuButton()
        menu.addTarget(self, action: #selector(openSideMenu), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)
        
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "BCWizard_open")
        localizeView()
    }
    
    fileprivate func localizeView() {
        navigationItem.title = "Main.BallisticCalculator".localized()
        
        subtitleLabel.text = "BallisticCalc.FirstEntrance.Title".localized()
        descriptionLabel.text = "BallisticCalc.FirstEntrance.Description".localized()
        setCalculatorButton.title = "BallisticCalc.FirstEntrance.SetCalculator".localized()
    }

    // MARK: Navigation
    @objc fileprivate func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }
    
}
