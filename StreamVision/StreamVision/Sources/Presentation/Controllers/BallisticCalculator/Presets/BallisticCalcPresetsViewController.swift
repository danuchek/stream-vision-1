//
//  PresetsViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/28/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import RealmSwift

private extension MGSwipeButton {
    func setupTitle() {
        titleLabel?.minimumScaleFactor = 0.5
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.lineBreakMode = NSLineBreakMode.byCharWrapping
        titleLabel?.numberOfLines = 1
    }
}

class BallisticCalcPresetsViewController: UIViewController {
    @IBOutlet weak var presetsTableView: UITableView!
    @IBOutlet weak var overviewTableView: UITableView!
    
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var overViewLabel: UILabel!
    
    fileprivate var dataSource: OverviewDataSource!
    fileprivate var presetIndexPath: IndexPath!
    
    var presets: Results<BallisticCalcPreset>!
    var notificationToken: NotificationToken?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = OverviewDataSource(tableView: overviewTableView)
        overviewTableView.dataSource = dataSource
        
        if let revealController = self.revealViewController() {
            view.addGestureRecognizer(revealController.panGestureRecognizer())
        }
        
        let realm = try! Realm()

        // Set realm notification block
        notificationToken = realm.observe { [unowned self] note, realm in
            self.presetsTableView.reloadData()
            self.dataSource.reloadTableView()
        }
        presets = realm.objects(BallisticCalcPreset.self).sorted(byKeyPath: "date")
        presetsTableView.reloadData()
        
        navigationItem.rightBarButtonItem =
            UIBarButtonItem(title:"BallisticCalc.List.StartCalc".localized(),
                            style: .plain,
                            target: self,
                            action: #selector(startCalc))
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        let menu = MenuButton()
        menu.addTarget(self, action: #selector(openSideMenu), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)
        
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "BCPresets_open")
    }
    
    private func localizeView() {
        subTitleLabel.text = "BallisticCalc.List.Description".localized()
        overViewLabel.text = "BallisticCalc.EnterName.Overview".localized().uppercased()
        title = "BallisticCalc.List.Title".localized()
    }
    
    @objc fileprivate func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }
    
    @objc fileprivate func newPreset() {
        performSegue(withIdentifier: "to setup preset", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "to setup preset" {
            if let model = (sender as? BallisticCalcPreset) {
                let setupPresetVC = segue.destination as! SetupProfileViewController
                setupPresetVC.model = model.setBallisticCalcBaseModel()
                setupPresetVC.preset = model
            }
        } else if segue.identifier == "to calculate" {
            if let model = (sender as? BallisticCalcPreset) {
                let ballisticCalc = segue.destination as! BallisticCalcViewController
                ballisticCalc.model = model.setBallisticCalcBaseModel()
                ballisticCalc.presetName = model.name
            }
        }
    }
    
    @objc fileprivate func startCalc() {
        performSegue(withIdentifier: "to calculate", sender: self.presetBy(presetIndexPath))
    }
}


// MARK: Data Source
extension BallisticCalcPresetsViewController: UITableViewDataSource, MGSwipeTableCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presets.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != presets?.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewPresetTableViewCell") as!  NewPresetTableViewCell
            cell.newPresetButton.addTarget(self, action: #selector(newPreset), for: .touchUpInside)
            return cell
        }
        
        let item = presetBy(indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "PresetTableViewCell") as! PresetTableViewCell
        cell.row = indexPath.row
        cell.textLabel?.text = item.name
        cell.detailTextLabel?.text = item.date.fullDateString()
        
        let deleteButton = MGSwipeButton(title: "BallisticCalc.List.Menu.Delete".localized(), backgroundColor: .red) { _ in
            
            let realm = try! Realm()
            try! realm.write {
                realm.delete(item)
            }
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            return true }
        let renameButton = MGSwipeButton(title: "BallisticCalc.List.Menu.Rename".localized(), backgroundColor: .lightGray) { _ in
            
            let alert = UIAlertController(title: "BallisticCalc.List.Menu.Rename".localized(),
                                          message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default) { alertAction in
                                            if let presetName = alert.textFields?[0].text, presetName != "" {
                                                
                                                let realm = try! Realm()
                                                let presets = realm.objects(BallisticCalcPreset.self).filter("name == '\(presetName)'")
                                                if presets.count > 0 {
                                                    let alert = UIAlertController(title: "BallisticCalc.Wizard.Error.NameIsAlreadyInUse".localized(), message: "", preferredStyle: .alert)
                                                    alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil))
                                                    self.present(alert, animated: true, completion: nil)
                                                    return
                                                }
                                                
                                                let preset = self.presetBy(indexPath)
                                                let presetBaseModel = preset.setBallisticCalcBaseModel()
                                                let newPreset = BallisticCalcPreset.by(presetBaseModel)
                                                newPreset.name = presetName
                                                newPreset.date = preset.date
                                                
                                                try! realm.write {
                                                    realm.delete(preset)
                                                }
                                                
                                                try! realm.write {
                                                    realm.add(newPreset)
                                                }
                                                
                                            }
            })
            alert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                          style: .cancel) { _ in
            })
            alert.addTextField() { textField in
                let preset = self.presetBy(self.presetIndexPath)
                textField.text = preset.name
            }
            self.present(alert, animated: true)
            
            return true }
        let editButton = MGSwipeButton(title: "BallisticCalc.List.Menu.Edit".localized(), backgroundColor: .orange) { _ in
            
            self.performSegue(withIdentifier: "to setup preset", sender: self.presetBy(indexPath))
            
            return true }
        
        cell.rightSwipeSettings.offset = view.safeAreaInsets.right
        let width = tableView.contentSize.width / 3
        let offset = cell.rightSwipeSettings.offset / 3
        renameButton.buttonWidth = width - offset
        deleteButton.buttonWidth = width - offset
        editButton.buttonWidth = width - offset
        
        renameButton.setupTitle()
        deleteButton.setupTitle()
        editButton.setupTitle()
        
        cell.rightButtons = [deleteButton, renameButton, editButton]
        cell.delegate = self
        return cell
    }
    
    func swipeTableCellWillBeginSwiping(_ cell: MGSwipeTableCell) {
        let presetCell = cell as! PresetTableViewCell
        presetIndexPath = IndexPath(row: presetCell.row, section: 0)
    }
}

// MARK: Delegate
extension BallisticCalcPresetsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row != presets?.count else {
            dataSource.reloadTableView()
            tableView.deselectRow(at: indexPath, animated: false)
            navigationItem.rightBarButtonItem?.isEnabled = false
            //newPreset()
            return
        }
        
        let preset = presetBy(indexPath)
        presetIndexPath = indexPath
        dataSource.reloadTableView(item: preset.setBallisticCalcBaseModel())
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func presetBy(_ indexPath: IndexPath) -> BallisticCalcPreset {
        return presets[indexPath.row]
    }
}
