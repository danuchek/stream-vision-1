//
//  OverviewTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/29/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class OverviewTableViewCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textLabel?.font = UIFont(name: "Roboto-Regular", size: 13.0)
        textLabel?.textColor = .white
        textLabel?.numberOfLines = 0
    }
}
