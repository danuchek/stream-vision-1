//
//  SetBulletSwitchTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/7/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class SetPresetSwitchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var switchView: UISwitch!
    var switchValueChanged: ( (_ value: Bool) -> () )?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        textLabel?.textColor = .white
        textLabel?.font = UIFont(name: "RobotoCondensed-Regular", size: 14.0)
        backgroundColor = .clear
        
        let sepparatorView = UIView()
        sepparatorView.translatesAutoresizingMaskIntoConstraints = false
        sepparatorView.backgroundColor = .gray
        addSubview(sepparatorView)
        
        NSLayoutConstraint.activate([
        sepparatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
        sepparatorView.leadingAnchor.constraint(equalTo: textLabel!.leadingAnchor),
        sepparatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
        sepparatorView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
    
    @IBAction func switchValueChanged(sender: UISwitch) {
        switchValueChanged?(sender.isOn)
    }

}
