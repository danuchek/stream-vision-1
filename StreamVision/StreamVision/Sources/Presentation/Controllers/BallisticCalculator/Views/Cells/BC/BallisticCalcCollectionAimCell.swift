//
//  BallisticCalcCollectionAimCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/14/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BallisticCalcCollectionAimCell: UICollectionViewCell {
    @IBOutlet weak var xOffsetContraint: NSLayoutConstraint!
    @IBOutlet weak var yOffsetContraint: NSLayoutConstraint!
    var displayOffset: CGFloat = 4.5
    
    func biasBy(elevation: Double, windage: Double) {
        var elevationPixelsOffset = elevation
        var windagePixelsOffset = windage
        
        if windage > 7 {
            windagePixelsOffset = 7
        }
        if windage < -7 {
            windagePixelsOffset = -7
        }
        if elevation > 7 {
            elevationPixelsOffset = 7
        }
        if elevation < -7 {
            elevationPixelsOffset = -7
        }
        
        xOffsetContraint.constant = displayOffset - CGFloat(elevationPixelsOffset * 10)
        yOffsetContraint.constant = -CGFloat(windagePixelsOffset * 10)
    }
}
