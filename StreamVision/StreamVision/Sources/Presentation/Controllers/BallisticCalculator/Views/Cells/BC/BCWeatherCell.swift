//
//  BCWeatherCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/5/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BCWeatherCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
}
