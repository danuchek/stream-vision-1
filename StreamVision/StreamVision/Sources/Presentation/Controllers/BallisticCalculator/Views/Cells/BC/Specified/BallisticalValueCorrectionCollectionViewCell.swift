//
//  BallisticalValueCorrectionCollectionViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/18/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BallisticalValueCorrectionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var correctionView: BallisticCalcValuesCorrectionView!
    var dropAction: (() -> ())? {
        didSet {
            correctionView.dropCompletion = dropAction
        }
    }
    var driftAction: (() -> ())? {
        didSet {
            correctionView.driftCompletion = driftAction
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        correctionView.layer.cornerRadius = 5
        correctionView.clipsToBounds = true
    }
}
