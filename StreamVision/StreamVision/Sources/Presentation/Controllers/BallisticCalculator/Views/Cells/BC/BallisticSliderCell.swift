//
//  BallisticSliderCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/10/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import AORangeSlider

class BallisticSliderCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    @IBOutlet weak var slider: AORangeSlider!
    var rangeUpdated: ((_ min: Double,_ max: Double) -> ())?
    var changeValueByPicker: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        slider.changeValueContinuously = false
        let trackImage = AORangeSlider.getImage(color: .hex("1E75FE"), size: CGSize(width: 1, height: 5))
        slider.trackImage = trackImage
//
//        slider.tintColorBetweenHandles =
//        slider.handleColor = .hex("1E75FE")
//        slider.tintColor = .lightGray
//        slider.minLabelColour = .white
//        slider.maxLabelColour = .white
//
//        slider.handleDiameter = 20
//        slider.selectedHandleDiameterMultiplier = 1
//        slider.lineHeight = 2
//        slider.labelPadding = 8
//        slider.step = 1
//        slider.enableStep = true
//
//        slider.delegate = self
        slider.addTarget(self, action: #selector(valueChange(sender:)), for: .valueChanged)
    }
    
    @IBAction func changeValueAction() {
        changeValueByPicker?()
    }
    
    @objc func valueChange(sender: AORangeSlider) {
        rangeUpdated?(sender.lowValue, sender.highValue)
    }
    
//    func rangeSlider(_ sender: TTRangeSlider!,
//                     didChangeSelectedMinimumValue selectedMinimum: Float,
//                     andMaximumValue selectedMaximum: Float) {
//    }
//
//    func didEndTouches(in sender: TTRangeSlider!) {
//        rangeUpdated?(Double(sender.selectedMinimum), Double(sender.selectedMaximum))
//    }
    
}
