//
//  SetBulletBasicTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/7/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class SetupProfileBaseTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        let sepparatorView = UIView()
        sepparatorView.translatesAutoresizingMaskIntoConstraints = false
        sepparatorView.backgroundColor = .gray
        addSubview(sepparatorView)
        
        NSLayoutConstraint.activate([
        sepparatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
        sepparatorView.leadingAnchor.constraint(equalTo: textLabel!.leadingAnchor),
        sepparatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
        sepparatorView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
    
    func setupCell(title: String?, details: String?) {
        textLabel?.text = title
        detailTextLabel?.text = details
    }

}
