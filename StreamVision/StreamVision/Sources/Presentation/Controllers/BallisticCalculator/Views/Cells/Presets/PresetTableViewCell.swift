//
//  PresetTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/29/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class PresetTableViewCell: MGSwipeTableCell {
    var row: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let sepparatorView = UIView(frame: CGRect(x: 0, y: frame.height, width: frame.width, height: 0.5))
        sepparatorView.backgroundColor = .rgba(200, 199, 204, 1)
        sepparatorView.alpha = 0.2
        addSubview(sepparatorView)
        
        sepparatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sepparatorView.bottomAnchor.constraint(equalTo: bottomAnchor),
            sepparatorView.leadingAnchor.constraint(equalTo: textLabel!.leadingAnchor),
            sepparatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            sepparatorView.heightAnchor.constraint(equalToConstant: 0.5)
        ])
    }
}
