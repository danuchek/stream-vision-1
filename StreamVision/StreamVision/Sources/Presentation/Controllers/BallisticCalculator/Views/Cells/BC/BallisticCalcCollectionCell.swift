//
//  BallisticCalcCollectionCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/11/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BallisticCalcCollectionCell: UICollectionViewCell {
    @IBOutlet private weak var arrowImageView: UIImageView!
    @IBOutlet private weak var crossImageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var textConstraint: NSLayoutConstraint!
    
    var isClicable: Bool = false {
        didSet {
            arrowImageView.isHidden = !isClicable
            if isClicable {
                textConstraint.constant = 0
            } else {
                textConstraint.constant = -11
            }
        }
    }
    var isValid: Bool = true {
        didSet {
            crossImageView.isHidden = isValid
            if !isValid {
                textLabel.text = ""
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        arrowImageView.isHidden = !isClicable
        if isClicable {
            textConstraint.constant = 0
        } else {
            textConstraint.constant = -11
        }
    }

//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        textLabel.preferredMaxLayoutWidth = layoutAttributes.size.width - contentView.layoutMargins.left - contentView.layoutMargins.left
//        layoutAttributes.bounds.size.height = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
//        return layoutAttributes
//    }
//
    func prefferedHeight() -> CGFloat {
        return systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
    }
}
