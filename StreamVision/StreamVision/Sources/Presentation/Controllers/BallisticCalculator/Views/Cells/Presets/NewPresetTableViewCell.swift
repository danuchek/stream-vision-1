//
//  NewPresetTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/30/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class NewPresetTableViewCell: UITableViewCell {
    @IBOutlet weak var newPresetButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        newPresetButton.setTitle("BallisticCalc.List.AddNew".localized(), for: .normal)
    }
}
