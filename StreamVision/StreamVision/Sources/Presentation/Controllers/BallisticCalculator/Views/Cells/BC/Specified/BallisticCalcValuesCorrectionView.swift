//
//  BallisticCalcValuesCorrectionView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/18/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BallisticCalcValuesCorrectionView: UIView {
    @IBOutlet weak var dropTextLabel: UILabel!
    @IBOutlet weak var dropValueLabel: UILabel!

    @IBOutlet weak var driftTextLabel: UILabel!
    @IBOutlet weak var driftValueLabel: UILabel!
    
    var dropCompletion: (() -> ())?
    var driftCompletion: (() -> ())?
    
    private let generateDots: (Int) -> String = {
        var result = ""
        for x in 0...$0 {
            result += "."
        }
        return result
    }
    
    override func awakeFromNib() {
        dropTextLabel.text = "BallisticCalc.Calculator.Aim.DropTitle".localized() + generateDots(100)
        driftTextLabel.text = "BallisticCalc.Calculator.Aim.DriftTitle".localized() + generateDots(100)
    }
    
    @IBAction func dropAction() {
        dropCompletion?()
    }
    
    @IBAction func driftAction() {
        driftCompletion?()
    }
}
