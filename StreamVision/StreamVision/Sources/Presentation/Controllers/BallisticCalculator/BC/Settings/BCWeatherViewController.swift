//
//  BCWeatherViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/4/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

extension BCWeatherViewController {
    func selectedCellFromSetOutdoor(identifier: Int) {
        let model = dataSource.model!
        
        switch (identifier) {
        case (BCWeatherDataSource.SetOutdoor.temperature.rawValue.hash):
            processSelectionCellWith(value: model.outdoor.temperature?.value.value ??
                SetOutdoorModel.TemperatureModel.defaultValue,
                                     model: model.outdoor.actualTemperature ?? model.outdoor.temperature,
                                     title: "BallisticCalc.SetOutdoor.Temperature".localized())
            { canceled, newModel in
                if !canceled {
                    model.outdoor.actualTemperature = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (BCWeatherDataSource.SetOutdoor.pressure.rawValue.hash):
            processSelectionCellWith(value: model.outdoor.pressure?.value.value ??
                SetOutdoorModel.PressureModel.defaultValue,
                                     model: model.outdoor.actualPressure ?? model.outdoor.pressure,
                                     title: "BallisticCalc.SetOutdoor.Pressure".localized())
            { canceled, newModel in
                if !canceled {
                    model.outdoor.actualPressure = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (BCWeatherDataSource.SetOutdoor.humidity.rawValue.hash):
            processSelectionCellWith(value: model.outdoor.humidity?.value.value ??
                SetOutdoorModel.HumidityModel.defaultValue,
                                     model: model.outdoor.actualHumidity,
                                     title: "BallisticCalc.SetOutdoor.Humidity".localized())
            { canceled, newModel in
                if !canceled {
                    model.outdoor.actualHumidity = newModel
                    self.dataSource.reloadDataSource()
                }
            }
        case (BCWeatherDataSource.SetOutdoor.default.rawValue.hash):
            model.outdoor.actualHumidity = nil
            model.outdoor.actualPressure = nil
            model.outdoor.actualTemperature = nil
            dataSource.reloadDataSource()
            
        default: break
        }
    }

}

class BCWeatherViewController: UIViewController, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    var model: SetBallisticCalculatorBaseModel!
    var dataSource: BCWeatherDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = BCWeatherDataSource()
        dataSource.model = model
        tableView.dataSource = dataSource
        tableView.delegate = self
        headerLabel.text = "BallisticCalc.Calculator.SetupWeather.Header".localized()
        title = "BallisticCalc.Calculator.SetupWeather.Title".localized()
        dataSource.tableView = tableView
        dataSource.reloadDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "BCCalculationWeatherScreen_open")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "BallisticCalcPickerViewController":
            let pickerNavigationViewController = segue.destination as! UINavigationController
            let pickerViewController = pickerNavigationViewController.viewControllers[0] as! BCSinglePickerViewController
            pickerViewController.model = sender as? BCSinglePickerViewModel
            
            Logging.log(message: "[BC] Sender:\(String(describing: sender)) Destenation: \(pickerViewController) Source:\(segue.source)", toFile: true, toFabric: true)
        default: break
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        selectedCellFromSetOutdoor(identifier: cell.tag)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func processSelectionCellWith<T: BallisticCalcParameterProtocol, D>(value: Double,
                                  model: T?,
                                  title: String = "",
                                  isWithType: Bool = true,
                                  completion: @escaping (_ canceled: Bool,
        _ model: T) -> ()) -> () where D.RawValue == Int, T.UnitsType == D {
        var resultModel = model ?? T.createParameter(value, T.self)
        var pickerData: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: String)] = []
        let localizedUnits = UnitsConvertor.allLocalizedUnitsBy(resultModel.value.unit as! UnitsProtocol)
        for (i, limits) in resultModel.limits.enumerated() {
            pickerData.append( (limits: limits.limits, pattern: limits.pattern, units: localizedUnits[i]) )
        }
        let pickerModel =
            BCSinglePickerViewModel(value: resultModel.value.value,
                                    title: title,
                                    units: resultModel.value.unit.rawValue,
                                    isWithType: isWithType,
                                    data: pickerData,
                                    didSelectValue: { (value, rawUnits) in
                                        let result =
                                            self.processValueFromPicker(value: value,
                                                                        rawUnits: rawUnits,
                                                                        model: &resultModel)
                                        return result?.0
            },
                                    editFinished: { canceled in
                                        completion( !canceled, resultModel)
            })
        performSegue(withIdentifier: "BallisticCalcPickerViewController", sender: pickerModel)
    }
    
    func processValueFromPicker<T: BallisticCalcParameterProtocol, D>(value: Double, rawUnits: Int, model: inout T) -> (Double, D)? where D.RawValue == Int, T.UnitsType == D {
        let type = Swift.type(of: model.value.unit)
        var convertedValue: (Double, D)? = nil
        let units = type.init(rawValue: rawUnits)!
        if model.value.unit != units {
            model.value.value = value
            convertedValue = UnitsConvertor.convertUnitsBy(value: model.value, destUnits: units)
            model.value = convertedValue!
        } else {
            model.value.value = value
            convertedValue = (value, units)
        }
        
        return convertedValue
    }
}


class BCWeatherDataSource: NSObject, UITableViewDataSource {
    enum SetOutdoor: String {
        case temperature = "Temperature"
        case pressure = "Pressure"
        case humidity = "Humidity"
        case `default` = "Default"
    }
    enum Section: Int {
        case section1
    }
    private var items: [Section: [SetOutdoor] ] = [.section1 : [.temperature, .pressure, .humidity]]
    weak var tableView: UITableView!
    
    weak var headerLabel: UILabel!
    var model: SetBallisticCalculatorBaseModel!
    
    func reloadDataSource() {
        if (model.outdoor.actualTemperature != nil) ||
           (model.outdoor.actualHumidity != nil) ||
           (model.outdoor.actualPressure != nil) {
            items = [.section1 : [.temperature, .pressure, .humidity, .default]]
        } else {
            items = [.section1 : [.temperature, .pressure, .humidity]]
        }
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch items[Section(rawValue: indexPath.section)!]![indexPath.row] {
        case .temperature:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.outdoor.actualTemperature?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = .gray
                    details = model.outdoor.temperature?.localizedValue ??
                        SetOutdoorModel.TemperatureModel.createParameter(SetOutdoorModel.TemperatureModel.defaultValue,
                                                                         SetOutdoorModel.TemperatureModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetOutdoor.Temperature".localized(),
                               details: details)
                cell.tag = SetOutdoor.temperature.rawValue.hash
                
                return cell
            }
        case .pressure:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.outdoor.actualPressure?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = .gray
                    details = model.outdoor.pressure?.localizedValue ??  SetOutdoorModel.PressureModel.createParameter(SetOutdoorModel.PressureModel.defaultValue,
                                                                      SetOutdoorModel.PressureModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetOutdoor.Pressure".localized(),
                               details: details)
                cell.tag = SetOutdoor.pressure.rawValue.hash
                
                return cell
            }
        case .humidity:
            if let cell =
                tableView.dequeueReusableCell(withIdentifier: "SetupProfileBaseTableViewCell") as? SetupProfileBaseTableViewCell {
                
                var details = model.outdoor.actualHumidity?.localizedValue
                cell.detailTextLabel?.textColor = .white
                if details == nil {
                    cell.detailTextLabel?.textColor = .gray
                    details = model.outdoor.humidity?.localizedValue ??
                        SetOutdoorModel.HumidityModel.createParameter(SetOutdoorModel.HumidityModel.defaultValue,
                                                                            SetOutdoorModel.HumidityModel.self).localizedValue
                }
                cell.setupCell(title: "BallisticCalc.SetOutdoor.Humidity".localized(),
                               details: details)
                cell.tag = SetOutdoor.humidity.rawValue.hash
                
                return cell
            }
        default: break
        }
        let cell = UITableViewCell()
        cell.textLabel?.text = "BallisticCalc.Calculator.SetupWeather.UseDefault".localized()
        cell.textLabel?.textColor = .hex("167EFB")
        cell.backgroundColor = .hex("01111B")
        cell.tag = SetOutdoor.default.rawValue.hash
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[Section(rawValue: section)!]!.count
    }
}
