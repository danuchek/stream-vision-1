//
//  BallisticCalcViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/10/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

class BallisticCalcViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calculatorCollectionView: UICollectionView!
    @IBOutlet weak var customTabbarView: CustomTabBarView!
    
    var presetName: String!
    var model: SetBallisticCalculatorBaseModel!
    
    fileprivate var calculationDataSource: BCCalculationCollectionDataSource!
    fileprivate var aimDataSource: BCAimColletionDataSource!
    fileprivate var tableViewDataSource: BallisticCalcTableDataSource!
    fileprivate var specifiedViewDataSource: BallisticCalcTableDataSource!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "BallisticCalcHeaderView", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "BallisticCalcHeaderView")
        tableView.estimatedRowHeight = 100
        
        tableViewDataSource = BallisticCalcTableDataSource(tableView: tableView,
                                                           muzzle: model.rifle.muzzle!,
                                                           zero: model.rifle.zero!)
        tableViewDataSource.dataWasChanged = self.dataWasChanged
        
        specifiedViewDataSource = BallisticCalcTableDataSource(tableView: tableView,
                                                               muzzle: model.rifle.muzzle!,
                                                               zero: model.rifle.zero!)
        tableView.dataSource = specifiedViewDataSource
        tableView.delegate = specifiedViewDataSource
        specifiedViewDataSource.dataWasChanged = self.dataWasChanged
        
        calculationDataSource = BCCalculationCollectionDataSource(calculatorCollectionView)
        aimDataSource = BCAimColletionDataSource(calculatorCollectionView, presenter: self)
        
        calculatorCollectionView.delegate = aimDataSource
        calculatorCollectionView.dataSource = aimDataSource
        calculationDataSource.presenterViewController = self
        
        tableView.register(UINib(nibName: "BallisticSliderCell", bundle: nil), forCellReuseIdentifier: "BallisticSliderCell")
        tableView.register(UINib(nibName: "BCWeatherCell", bundle: nil), forCellReuseIdentifier: "BCWeatherCell")
        tableViewDataSource.presenterViewController = self
        specifiedViewDataSource.presenterViewController = self
        
        title = presetName
        
        customTabbarView.addItem(
        title: "BallisticCalc.Calculator.Specified".localized().uppercased()) { [weak self] index in

            self?.calculatorCollectionView.delegate = self?.aimDataSource
            self?.calculatorCollectionView.dataSource = self?.aimDataSource
            self?.tableView.delegate = self?.specifiedViewDataSource
            self?.tableView.dataSource = self?.specifiedViewDataSource
            self?.specifiedViewDataSource.reloadWithRange(false)
            self?.calculatorCollectionView.reloadData()
            
            return true
            }.isSelected = true
        customTabbarView.addItem(
        title: "BallisticCalc.Calculator.Table".localized().uppercased()) { [weak self] index in
            self?.calculatorCollectionView.delegate = self?.calculationDataSource
            self?.calculatorCollectionView.dataSource = self?.calculationDataSource
            self?.tableView.delegate = self?.tableViewDataSource
            self?.tableView.dataSource = self?.tableViewDataSource
            self?.tableViewDataSource.reloadWithRange(true)
            self?.calculatorCollectionView.reloadData()
            self?.calculateWithRange()
            
            return true
        }
        customTabbarView.backgroundView.backgroundColor = .hex("#0B1F2F")
        customTabbarView.style = .default
        customTabbarView.colorForDeselectedtems(color: .hex("#2A3F4F"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

            
        googleReport(action: "BCCalculationScreen_open")
        dataWasChanged()
    }
    
    func processSelectionCellWith<T: BallisticCalcParameterProtocol, D>(model: T?,
                                  title: String = "",
                                  step: Double = 1.0,
                                  isWithType: Bool = true,
                                  isRange: Bool = false,
                                  completion: @escaping (_ canceled: Bool,
        _ model: T) -> ()) -> () where D.RawValue == Int, T.UnitsType == D {
        var resultModel = model ?? T.createParameter(T.self)
        var pickerData: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: String)] = []
        let localizedUnits = UnitsConvertor.allLocalizedUnitsBy(resultModel.value.unit as! UnitsProtocol)
        for (i, limits) in resultModel.limits.enumerated() {
            pickerData.append( (limits: limits.limits, pattern: limits.pattern, units: localizedUnits[i]) )
        }
        let pickerModel =
            BCSinglePickerViewModel(value: resultModel.value.value,
                                    title: title,
                                    sepparator: isRange ? "-" : ".",
                                    step: step,
                                    units: resultModel.value.unit.rawValue,
                                    isWithType: isWithType,
                                    data: pickerData,
                                    didSelectValue: { (value, rawUnits) in
                                        var result: (Double, D)?
                                        if !isRange {
                                            result = self.processValueFromPicker(value: value,
                                                                                 rawUnits: rawUnits,
                                                                                 model: &resultModel)
                                        } else {
                                            result = self.processRangeFromPicker(value: value,
                                                                                 rawUnits: rawUnits,
                                                                                 model: &resultModel)
                                        }
                                        return result?.0
            },
                                    editFinished: { canceled in
                                        completion( !canceled, resultModel)
            })
        performSegue(withIdentifier: !isRange ?
            "BallisticCalcPickerViewController" : "BCRangePickerViewController", sender: pickerModel)
    }
    
    func processValueFromPicker<T: BallisticCalcParameterProtocol, D>(value: Double,
                                rawUnits: Int, model: inout T) -> (Double, D)? where D.RawValue == Int, T.UnitsType == D {
        let type = Swift.type(of: model.value.unit)
        var convertedValue: (Double, D)? = nil
        let units = type.init(rawValue: rawUnits)!
        if model.value.unit != units {
            model.value.value = value
            convertedValue = UnitsConvertor.convertUnitsBy(value: model.value, destUnits: units)
            model.value = convertedValue!
        } else {
            model.value.value = value
            convertedValue = (value, units)
        }
        
        return convertedValue
    }
    
    func processRangeFromPicker<T: BallisticCalcParameterProtocol, D>(value: Double,
                                rawUnits: Int, model: inout T) -> (Double, D)? where D.RawValue == Int, T.UnitsType == D {
        let type = Swift.type(of: model.value.unit)
        var leftConvertedValue: (Double, D)? = nil
        var rightConvertedValue: (Double, D)? = nil
        var convertedRange: (Double, D)? = nil
        let units = type.init(rawValue: rawUnits)!
        
        let limit = model.selectedLimit
        let parsedValue = String(format: "%\(limit.pattern.integer).\(limit.pattern.decimal)f", value).components(separatedBy: ".")
        let range = (max: Double(parsedValue[0])!, min: Double(parsedValue[1])!)
        
        if model.value.unit != units {
            leftConvertedValue = UnitsConvertor.convertUnitsBy(value: (range.max, model.value.unit), destUnits: units)
            rightConvertedValue = UnitsConvertor.convertUnitsBy(value: (range.min, model.value.unit), destUnits: units)
            let letft = Int(round(leftConvertedValue!.0))
            let right = Int(round(rightConvertedValue!.0))
            convertedRange = (Double("\(letft).\(right)")!, units)
            model.value = convertedRange!
        } else {
            model.value.value = value
            convertedRange = (value, units)
        }
        
        return convertedRange
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "BallisticCalcPickerViewController":
            let pickerNavigationViewController = segue.destination as! UINavigationController
            let pickerViewController = pickerNavigationViewController.viewControllers[0] as! BCSinglePickerViewController
            pickerViewController.model = sender as? BCSinglePickerViewModel
        case "BCRangePickerViewController":
            let pickerNavigationViewController = segue.destination as! UINavigationController
            let pickerViewController = pickerNavigationViewController.viewControllers[0] as! BCRangePickerViewController
            pickerViewController.model = sender as? BCSinglePickerViewModel
        case "BCWeatherViewController":
            let weatherViewController = segue.destination as! BCWeatherViewController
            weatherViewController.model = model
        default: break
        }
        
        Logging.log(message: "[BC] Sender:\(String(describing: sender)) Source:\(segue.source)", toFile: true, toFabric: true)
    }
    
    func dataWasChanged() {
        if customTabbarView.currentSelectedIndex == 0 {
            calculateWithDistance()
        } else {
            calculateWithRange()
        }
    }
}

extension BallisticCalcViewController: RateManager {
    
    fileprivate func calculateWithRange() {
        let bullet = bulletForCalculation(model.bullet)
        let minRange = tableViewDataSource.distanceRangeModel.parameter.range.value.min
        let maxRange = tableViewDataSource.distanceRangeModel.parameter.range.value.max
        let rows = (maxRange - minRange) / tableViewDataSource.stepModel.parameter.value.value
        
        let outdoor = outdoorForCalculation(model.outdoor)
        let actualOutdoor = actualOutdoorForCalculation(model.outdoor)
        
        var data = BallisticCalcWraper.calculateDistances(
            byRowsCount: Int32(rows),
            min: UnitsConvertor.distance(
                (minRange, tableViewDataSource.distanceRangeModel.parameter.value.unit), .m).0,
            max: UnitsConvertor.distance(
                (maxRange, tableViewDataSource.distanceRangeModel.parameter.value.unit), .m).0,
            bullet: bullet,
            windSpeed: UnitsConvertor.speed(tableViewDataSource.windSpeedModel.parameter.value, .ms).0,
            windAngle: tableViewDataSource.windDirectionModel.parameter.value.value * Double.pi / 180,
            muzzle:  UnitsConvertor.speed(model.rifle.muzzle!.value, .ms).0,
            sights: UnitsConvertor.measure(model.rifle.sights!.value, .m).0,
            temperature: UnitsConvertor.temperature(outdoor.t, .k).0,
            presure: UnitsConvertor.pressure(outdoor.p, .pA).0,
            humidity: outdoor.h.value / 100.0,
            actualTemperature: UnitsConvertor.temperature(actualOutdoor.t, .k).0,
            actualPresure: UnitsConvertor.pressure(actualOutdoor.p, .pA).0,
            actualHumidity: actualOutdoor.h.value / 100.0,
            alpha: tableViewDataSource.angleModel.parameter.value.value * Double.pi / 180,
            zeroRange: UnitsConvertor.distance(model.rifle.zero!.value, .m).0,
            twist: (model.rifle.twistValue?.value.value) ?? 0,
            rightTwist: Int32(model.rifle.twistDirection?.value.unit.rawValue ?? 0),
            useGyro: Int32(model.bullet.isWithGyroEffect ? 1 : 0))
        
        data = data.map() { calculations in
            let convertedResult = convertDropAndDrift(distance: UnitsConvertor.distance((calculations.distance, .m), .m),
                                                      calculations: calculations,
                                                      dropUnits: calculationDataSource.dropParameter.value.unit,
                                                      driftUnits: calculationDataSource.driftParameter.value.unit)
            calculations.elevation = convertedResult.elevation
            calculations.windage = convertedResult.windage
            
            switch tableViewDataSource.distanceRangeModel.parameter.value.unit {
            case .m: break
            case .yd: calculations.distance =
                UnitsConvertor.distance((calculations.distance, .m), .yd).0
            }
            
            return calculations
        }
        rateManager.bcCalculation()
        calculationDataSource.reloadWith(data: data, distanceModel: tableViewDataSource.distanceRangeModel.parameter)
    }
    
    fileprivate func calculateWithDistance() {
        let bullet = bulletForCalculation(model.bullet)
        let outdoor = outdoorForCalculation(model.outdoor)
        let actualOutdoor = actualOutdoorForCalculation(model.outdoor)
        let distance = UnitsConvertor.distance(specifiedViewDataSource.distanceModel.parameter.value, .m)
        
        let result = BallisticCalcWraper.calculate(
            byDistance: distance.0,
            bullet: bullet,
            windSpeed: UnitsConvertor.speed(specifiedViewDataSource.windSpeedModel.parameter.value, .ms).0,
            windAngle: specifiedViewDataSource.windDirectionModel.parameter.value.value * Double.pi / 180,
            muzzle:  UnitsConvertor.speed(model.rifle.muzzle!.value, .ms).0,
            sights: UnitsConvertor.measure(model.rifle.sights!.value, .m).0,
            temperature: UnitsConvertor.temperature(outdoor.t, .k).0,
            presure: UnitsConvertor.pressure(outdoor.p, .pA).0,
            humidity: outdoor.h.value / 100.0,
            actualTemperature: UnitsConvertor.temperature(actualOutdoor.t, .k).0,
            actualPresure: UnitsConvertor.pressure(actualOutdoor.p, .pA).0,
            actualHumidity: actualOutdoor.h.value / 100.0,
            alpha: specifiedViewDataSource.angleModel.parameter.value.value * Double.pi / 180,
            zeroRange: UnitsConvertor.distance(model.rifle.zero!.value, .m).0,
            twist: (model.rifle.twistValue?.value.value) ?? 0,
            rightTwist: Int32(model.rifle.twistDirection?.value.unit.rawValue ?? 0),
            useGyro: Int32(model.bullet.isWithGyroEffect ? 1 : 0))
        aimDataSource.dropInMil = result.elevation * 3200 / Double.pi
        aimDataSource.driftInMil = result.windage * 3200 / Double.pi

        let convertedResult = convertDropAndDrift(distance: distance,
                                                  calculations: result,
                                                  dropUnits: aimDataSource.dropParameter.value.unit,
                                                  driftUnits: aimDataSource.driftParameter.value.unit)
        aimDataSource.dropParameter.value.value = convertedResult.elevation
        aimDataSource.driftParameter.value.value = convertedResult.windage
        
        rateManager.bcCalculation()
        calculatorCollectionView.reloadData()
    }
    
    fileprivate func convertDropAndDrift(distance: (Double, UnitsConvertor.Distance),
                                         calculations: CalculatedDistance,
                                         dropUnits: UnitsConvertor.Correction,
                                         driftUnits: UnitsConvertor.Correction) -> (elevation: Double,
                                                                                    windage: Double) {
        var result = (elevation: calculations.elevation, windage: calculations.windage)
        
        switch dropUnits {
        case .moa: result.elevation = result.elevation * 10800 / Double.pi
        case .clicks:
            result.elevation = { click, elevation in
                switch click {
                case .halfMoa: return (elevation * 10800 / Double.pi) * 2.0
                case .oneQuarterMoa: return (elevation * 10800 / Double.pi) * 4.0
                case .oneEighthMoa: return (elevation * 10800 / Double.pi) * 8.0
                case .oneTenthMil: return (elevation * 3200 / Double.pi) * 10.0
                case .twoTenthMil: return (elevation * 3200 / Double.pi) * 5.0
                }
            }(model.rifle.clickValue!.value.unit, result.elevation)
            
        case .mil: result.elevation = result.elevation * 3200 / Double.pi
        case .cm: result.elevation = (result.elevation * 10800 / Double.pi) * ( UnitsConvertor.distance(distance, .m).0 / 100) * 2.9089
        case .inches: result.elevation = (result.elevation * 10800 / Double.pi) * ( UnitsConvertor.distance(distance, .yd).0 / 100) * 1.047
        }
        
        switch driftUnits {
        case .moa: result.windage = result.windage * 10800 / Double.pi
        case .clicks:
            result.windage = { click, windage in
                switch click {
                case .halfMoa: return (windage * 10800 / Double.pi) * 2.0
                case .oneQuarterMoa: return (windage * 10800 / Double.pi) * 4.0
                case .oneEighthMoa: return (windage * 10800 / Double.pi) * 8.0
                case .oneTenthMil: return (windage * 3200 / Double.pi) * 10.0
                case .twoTenthMil: return (windage * 3200 / Double.pi) * 5.0
                }
            }(model.rifle.clickValue!.value.unit, result.windage)
            
        case .mil: result.windage = result.windage * 3200 / Double.pi
        case .cm: result.windage = (result.windage * 10800 / Double.pi) * ( UnitsConvertor.distance(distance, .m).0 / 100) * 2.9089
        case .inches: result.windage = (result.windage * 10800 / Double.pi) * ( UnitsConvertor.distance(distance, .yd).0 / 100) * 1.047
        }
        
        return result
    }
    
    fileprivate func outdoorForCalculation(_ outdoor: SetOutdoorModel) ->
        (t: (value: Double, unit: UnitsConvertor.Temperature),
         p: (value: Double, unit: UnitsConvertor.Pressure),
         h: (value: Double, unit: UnitsConvertor.Humidity)) {
            let temperature = model.outdoor.temperature?.value ?? (288.15, .k)
            let presure = model.outdoor.pressure?.value ?? (100000.0, .pA)
            let humidity = model.outdoor.humidity?.value ?? (78, .percent)
            return (t: temperature, p: presure, h: humidity)
    }
    
    fileprivate func actualOutdoorForCalculation(_ outdoor: SetOutdoorModel) ->
        (t: (value: Double, unit: UnitsConvertor.Temperature),
         p: (value: Double, unit: UnitsConvertor.Pressure),
         h: (value: Double, unit: UnitsConvertor.Humidity)) {
            let defaultWeather = outdoorForCalculation(outdoor)
            
            let temperature = model.outdoor.actualTemperature?.value ?? defaultWeather.t
            let presure = model.outdoor.actualPressure?.value ?? defaultWeather.p
            let humidity = model.outdoor.actualHumidity?.value ?? defaultWeather.h
            return (t: temperature, p: presure, h: humidity)
    }
    
    fileprivate func bulletForCalculation(_ bullet: SetBulletModel) -> UnsafeMutablePointer<Bullet> {
        let length = bullet.length?.value ?? (0.0, .inch)
        let caliber = bullet.caliber?.value ?? (0.0, .inch)
        let weight = bullet.weight?.value ?? (0.0, .grain)
        var profile: BallisticTable!
        if let unit = bullet.profile?.value.unit {
            profile = BallisticTable.tableBy(unit)
        } else {
            profile = BallisticTable.bullet(cd: bullet.bulletCD)
        }
        
        var updatedCD: [Double] = []
        for cd in profile.cd {
            updatedCD.append(cd / bullet.coefficient!.value.value)
        }
        
        let resultBullet =
        makeBullet(Int32(profile.v.count),
                   &profile.v, &updatedCD,
                   Int32(profile.v.count), Int32(updatedCD.count),
                   UnitsConvertor.measure(length, .inch).0,
                   UnitsConvertor.measure(caliber, .inch).0,
                   UnitsConvertor.weight(weight, .grain).0,
                   profile.m,
                   profile.sRef)
        return resultBullet!
    }
}
