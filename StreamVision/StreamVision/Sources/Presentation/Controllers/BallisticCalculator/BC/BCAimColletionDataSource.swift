//
//  AimDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/14/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BCAimColletionDataSource: NSObject,
                                UICollectionViewDelegateFlowLayout,
                                UICollectionViewDataSource {
    struct BallisticCalcDropParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Correction
        static var defaultValue: Double = 1
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .moa),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .clicks),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .mil),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .cm),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .inches)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct BallisticCalcDriftParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Correction
        static var defaultValue: Double = 1
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .moa),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .clicks),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .mil),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .cm),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 2), units: .inches)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    var dropParameter: BallisticCalcDropParameter!
    var driftParameter: BallisticCalcDriftParameter!
    var dropInMil: Double!
    var driftInMil: Double!
    
    private var collectionView: UICollectionView!
    weak var presenterViewController: BallisticCalcViewController!
    
    init(_ collectionView: UICollectionView, presenter: BallisticCalcViewController) {
        self.collectionView = collectionView
        
        presenterViewController = presenter
        
        dropParameter = BallisticCalcDropParameter(value: (value: 0.0, unit: .moa))
        driftParameter = BallisticCalcDriftParameter(value: (value: 0.0, unit: .moa))
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BallisticCalcCollectionAimCell",
                                                      for: indexPath) as! BallisticCalcCollectionAimCell
        cell.biasBy(elevation: driftInMil, windage: dropInMil)
        return cell
            
        case 1:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BallisticalValueCorrectionCollectionViewCell",
                                                      for: indexPath) as! BallisticalValueCorrectionCollectionViewCell
        cell.dropAction = {
            self.presenterViewController.processSelectionCellWith(model: self.dropParameter,
                                                                  title: "BallisticCalc.Calculator.Aim.DropTitle".localized(),
                                                                  completion: { (cancelled, newModel) in
                                                                    if !cancelled {
                                                                        self.dropParameter = newModel
                                                                        self.presenterViewController.dataWasChanged()
                                                                    }
            })
        }
        var rawValue = dropParameter.value.value
        var prefix = rawValue == 0 ? "" : (rawValue > 0 ? "D " : "U ")
        var localizedValue = "\(dropParameter.absValue) \(dropParameter.localizedType)"
        cell.correctionView.dropValueLabel.text = prefix + localizedValue
            
        cell.driftAction = {
            self.presenterViewController.processSelectionCellWith(model: self.driftParameter,
                                                                  title: "BallisticCalc.Calculator.Aim.DriftTitle".localized(),
                                                                  completion: { (cancelled, newModel) in
                                                                    if !cancelled {
                                                                        self.driftParameter = newModel
                                                                        self.presenterViewController.dataWasChanged()
                                                                    }
            })
        }
        rawValue = driftParameter.value.value
        prefix = rawValue == 0 ? "" : (rawValue < 0 ? "L " : "R ")
        localizedValue = "\(driftParameter.absValue) \(driftParameter.localizedType)"
        cell.correctionView.driftValueLabel.text = prefix + localizedValue
        
        return cell
        
        default: break
        }
        
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: -9, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: 200, height: 200)
        }
        return CGSize(width: collectionView.frame.width, height: 100)
    }
}
