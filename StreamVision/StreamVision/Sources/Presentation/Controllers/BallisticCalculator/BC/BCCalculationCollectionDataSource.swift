//
//  BallisticCalcCollectionDelegate.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/11/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class BCCalculationCollectionDataSource: NSObject,
                                         UICollectionViewDelegateFlowLayout,
                                         UICollectionViewDataSource {
    private static let COLUMNS_CONST = 3
    private var collectionView: UICollectionView!
    
    private var columnTitles: [String]
    private var data: [CalculatedDistance] = []
    private var distanceUnits: UnitsConvertor.Distance!
    weak var presenterViewController: BallisticCalcViewController!
    
    var dropParameter: BCAimColletionDataSource.BallisticCalcDropParameter!
    var driftParameter: BCAimColletionDataSource.BallisticCalcDriftParameter!
    
    init(_ collectionView: UICollectionView) {
        self.collectionView = collectionView
        //(collectionView.collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        //(collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInsetReference = .fromLayoutMargins
        
        self.collectionView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 40, right: 0)
        columnTitles =
        ["BallisticCalc.Calculator.Distance".localized() + ", M",
         "BallisticCalc.Calculator.Elevation".localized() + ", " + "BallisticCalc.Calculator.VisualizationSettings.MOA".localized(),
        "BallisticCalc.Calculator.Windage".localized() + ", " + "BallisticCalc.Calculator.VisualizationSettings.MOA".localized()]
        
        dropParameter = BCAimColletionDataSource.BallisticCalcDropParameter(value: (value: 0.0, unit: .moa))
        driftParameter = BCAimColletionDataSource.BallisticCalcDriftParameter(value: (value: 0.0, unit: .moa))
    }
    
    func reloadWith(data: [CalculatedDistance], distanceModel: BallisticCalcTableDataSource.BallisticCalcDistanceRangeParameter) {
        self.data = data
        columnTitles[0] = "BallisticCalc.Calculator.Distance".localized() + ", " + distanceModel.localizedType
        columnTitles[1] = "BallisticCalc.Calculator.Elevation".localized() + ", " + dropParameter.localizedType
        columnTitles[2] = "BallisticCalc.Calculator.Windage".localized() + ", " + driftParameter.localizedType
        distanceUnits = distanceModel.value.unit
        collectionView.reloadData()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return columnTitles.count
        }
        return data.count * BCCalculationCollectionDataSource.COLUMNS_CONST
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BallisticCalcCollectionCell",
                                                      for: indexPath) as! BallisticCalcCollectionCell
        let path = UIBezierPath(roundedRect:cell.bounds,
                                byRoundingCorners:[.topRight, .topLeft],
                                cornerRadii: CGSize(width: 0, height:  0))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        cell.layer.mask = maskLayer
        
        let itemIndex = Float(indexPath.row + 1)
        let line = itemIndex.truncatingRemainder(dividingBy: 3) == 0 ? 3 :
            ( (itemIndex + 1).truncatingRemainder(dividingBy: 3) == 0 ? 2 : 1 )
        
        switch indexPath.section {
        case 0:
            cell.textLabel.text = columnTitles[indexPath.row]
            cell.textLabel.numberOfLines = 0
            cell.isValid = true
            cell.isClicable = true
            cell.backgroundColor = .hex("162F42")
            switch indexPath.row {
            case 0:
                let path = UIBezierPath(roundedRect:cell.bounds,
                                        byRoundingCorners:[.topLeft],
                                        cornerRadii: CGSize(width: 5, height:  5))
                let maskLayer = CAShapeLayer()
                maskLayer.path = path.cgPath
                cell.isClicable = false
                cell.layer.mask = maskLayer
            case 2:
                let path = UIBezierPath(roundedRect:cell.bounds,
                                        byRoundingCorners:[.topRight],
                                        cornerRadii: CGSize(width: 5, height:  5))
                let maskLayer = CAShapeLayer()
                maskLayer.path = path.cgPath
                cell.layer.mask = maskLayer
                
            default: break
            }
        case 1:
            let dataForLine = data[(Int(itemIndex) - line)/3]
            cell.backgroundColor = .hex("0B1F2F")
            cell.isClicable = false
            switch line {
            case 1:
                cell.textLabel.text = "\(Int(dataForLine.distance))"
                cell.isValid = true
            case 2:
                let elevation = dataForLine.elevation
                let prefix = elevation == 0 ? "" : (elevation > 0 ? "D " : "U ")
                dropParameter.value.value = abs(elevation)
                cell.textLabel.text = prefix + dropParameter.rawValue
                cell.isValid = dataForLine.isValid
            case 3:
                let windage = dataForLine.windage
                let prefix = windage == 0 ? "" : (windage < 0 ? "L " : "R ")
                driftParameter.value.value = abs(windage)
                cell.textLabel.text = prefix + driftParameter.rawValue
                cell.isValid = dataForLine.isValid
            default: break
            }
            
        default:
            return cell
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 1:
                self.presenterViewController.processSelectionCellWith(model: self.dropParameter,
                                                                      title: "BallisticCalc.Calculator.Aim.DropTitle".localized(),
                                                                      completion: { (cancelled, newModel) in
                                                                        if !cancelled {
                                                                            self.dropParameter = newModel
                                                                            self.presenterViewController.dataWasChanged()
                                                                        }
                })
                
            case 2:
                self.presenterViewController.processSelectionCellWith(model: self.driftParameter,
                                                                      title: "BallisticCalc.Calculator.Aim.DriftTitle".localized(),
                                                                      completion: { (cancelled, newModel) in
                                                                        if !cancelled {
                                                                            self.driftParameter = newModel
                                                                            self.presenterViewController.dataWasChanged()
                                                                        }
                })
            default: break
            }
        default: break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: -9, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width/3)-7.5
        
        guard indexPath.section != 0 else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BallisticCalcCollectionCell",
            for: indexPath) as! BallisticCalcCollectionCell
            var height: CGFloat = 30.0
            for title in columnTitles {
                height = max(height,
                    title.height(withConstrainedWidth:cell.textLabel.frame.width,
                                 font: cell.textLabel.font))
            }
            return CGSize(width: width,
                          height: height)
        }
        return CGSize(width: width, height: 30)
    }

}
