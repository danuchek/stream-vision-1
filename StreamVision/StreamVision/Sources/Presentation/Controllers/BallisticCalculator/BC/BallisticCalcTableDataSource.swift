//
//  BallisticCalcDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/11/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit


precedencegroup PowerPrecedence { higherThan: MultiplicationPrecedence }
infix operator ^^ : PowerPrecedence
func ^^ (radix: Double, power: Double) -> Double {
    return pow(radix, power)
}
func ^^ (radix: Int, power: Int) -> Int {
    return Int(pow(Double(radix), Double(power)))
}

class BallisticCalcTableDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    struct BallisticCalcDistanceRangeParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Distance
        static var defaultValue: Double = 100.0100
        
        var value: (value: Double, unit: UnitsType)
        var range: (value: (max: Double, min: Double), unit: UnitsType) {
            let limit = selectedLimit
            return (value: (max: round((value.value - (Double(Int(value.value)))) * (10.0 ^^ Double(limit.pattern.decimal))),
                            min: Double(Int(value.value))), unit: value.unit)
        }
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 1000, min: 0), pattern: (integer: 4, decimal: 4), units: .m),
             (limits: (max: 1000, min: 0), pattern: (integer: 4, decimal: 4), units: .yd)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct BallisticCalcDistanceParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Distance
        static var defaultValue: Double = 100
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 1, min: 0), pattern: (integer: 4, decimal: 0), units: .m),
             (limits: (max: 1, min: 0), pattern: (integer: 4, decimal: 0), units: .yd)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct BallisticCalcStepsParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.RawUnits
        static var defaultValue: Double = 50
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 200, min: 1), pattern: (integer: 3, decimal: 0), units: .raw)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct BallisticCalcAngleParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Angle
        static var defaultValue: Double = 0
        
        var value: (value: Double, unit: UnitsConvertor.Angle)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 90, min: -90), pattern: (integer: 2, decimal: 0), units: .degree)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct BallisticCalcWindSpeedParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Speed
        static var defaultValue: Double = 0
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 50, min: 0), pattern: (integer: 2, decimal: 0), units: .ms),
             (limits: (max: 164, min: 0), pattern: (integer: 3, decimal: 0), units: .fps)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct BallisticCalcWindDirectionParameter: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Angle
        static var defaultValue: Double = 0
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 360, min: 0), pattern: (integer: 3, decimal: 0), units: .degree)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    class CellSliderRangeModel<T : BallisticCalcRangeParameterProtocol>: NSObject {
        var title: String
        var parameter: T
        var action: () -> ()
        var isWithType: Bool = true
        
        init(title: String, parameter: T, action: @escaping () -> () ) {
            self.title = title
            self.parameter = parameter
            self.action = action
        }
    }
    
    class CellSliderModel<T : BallisticCalcParameterProtocol>: NSObject {
        var title: String
        var parameter: T
        var action: () -> ()
        var isWithType: Bool = true
        
        init(title: String, parameter: T, action: @escaping () -> () ) {
            self.title = title
            self.parameter = parameter
            self.action = action
        }
    }
    
    enum TableData: String {
        case distance = "Distance"
        case rangeDistance = "RangeDistance"
        case steps = "Steps"
        case angle = "Angle"
        case speed = "WindSpeed"
        case direction = "WindDirection"
        case weather = "Weather"
    }
    var dataWasChanged: (() -> ())?
    
    var distanceRangeModel: CellSliderModel<BallisticCalcDistanceRangeParameter>!
    var distanceModel: CellSliderModel<BallisticCalcDistanceParameter>!
    var stepModel: CellSliderModel<BallisticCalcStepsParameter>!
    var angleModel: CellSliderModel<BallisticCalcAngleParameter>!
    var windSpeedModel: CellSliderModel<BallisticCalcWindSpeedParameter>!
    var windDirectionModel: CellSliderModel<BallisticCalcWindDirectionParameter>!
    
    private var data: [TableData] = []
    var tableView: UITableView!
    weak var presenterViewController: BallisticCalcViewController!
    
    init(tableView: UITableView, muzzle: SetRifleModel.MuzzleModel, zero: SetRifleModel.ZeroModel) {
        super.init()
        
        self.tableView = tableView
        resetParameters()
        reloadWithRange(false)
        
        updateDistanceLimitiations(muzzle, zero: zero)
    }
    
    private func updateDistanceLimitiations(_ muzzle: SetRifleModel.MuzzleModel, zero: SetRifleModel.ZeroModel) {
        self.distanceModel.parameter.limits = updateDistanceLimit(muzzle: muzzle, limits: distanceModel.parameter.limits)
        self.distanceRangeModel.parameter.limits = updateDistanceLimit(muzzle: muzzle, limits: distanceRangeModel.parameter.limits)

        distanceModel.parameter.value.value = 100.0
        distanceRangeModel.parameter.value.value = distanceValueBySteps(min: 0, max: zero.value.value * 5, model: self.stepModel.parameter)
    }
    
    private func updateDistanceLimit(muzzle: SetRifleModel.MuzzleModel,
                                     limits: [(limits: (max: Double, min: Double),
                                               pattern: (integer: Int, decimal: Int),
                                               units: UnitsConvertor.Distance)]) ->
        ([(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsConvertor.Distance)])
    {
        return
            { value, units, limits in
                let distance = (value: value, units: units)
                return limits.map {
                    let maxForPattern = Double(10 ^^ $0.pattern.integer)
                    var maxRange = UnitsConvertor.convertUnitsBy(value: distance, destUnits: $0.units).0
                    if maxRange >= maxForPattern {
                        maxRange = maxForPattern - 1
                    }
                    return (limits: (max: maxRange, min: $0.limits.min),
                            pattern: $0.pattern, units: $0.units)
                }
            }(muzzle.value.value * 2, { units -> UnitsConvertor.Distance in
                switch units {
                case .ms: return .m
                case .fps: return .yd
                default: return .m
                }
            }(muzzle.selectedLimit.units), limits)
    }
    
    func resetParameters() {
        stepModel = CellSliderModel(title: "BallisticCalc.Calculator.Steps".localized().uppercased(),
                                    parameter: BallisticCalcStepsParameter.createParameter(BallisticCalcStepsParameter.self), action: {
                                        self.presenterViewController.processSelectionCellWith(model: self.stepModel.parameter,
                                                                                              title: "BallisticCalc.Calculator.Steps".localized(),
                                                                                              isWithType: false,
                                                                                              completion: { (cancelled, newModel) in
                                                                                                if !cancelled {
                                                                                                    self.stepModel.parameter = newModel
                                                                                                    self.distanceRangeModel.parameter.value.value =
                                                                                                    self.distanceValueBySteps(min:
                                                                                                    self.distanceRangeModel.parameter.range.value.min,
                                                                                                                              max:
                                                                                                    self.distanceRangeModel.parameter.range.value.max,
                                                                                                                              model: newModel)
                                                                                                    self.tableView.reloadData()
                                                                                                    self.dataWasChanged?()
                                                                                                }
                                        })
        })
        
        distanceModel = CellSliderModel(title: "BallisticCalc.Calculator.Distance".localized().uppercased(),
                                        parameter: BallisticCalcDistanceParameter.createParameter(BallisticCalcDistanceParameter.self), action: {
                                            self.presenterViewController.processSelectionCellWith(model: self.distanceModel.parameter,
                                                                                                  title: "BallisticCalc.Calculator.Distance".localized(),
                                                                                                  completion: { (cancelled, newModel) in
                                                                                                    if !cancelled {
                                                                                                        self.distanceModel.parameter = newModel
                                                                                                        self.tableView.reloadData()
                                                                                                        self.dataWasChanged?()
                                                                                                    }
                                            })
        })
        
        distanceRangeModel =
            CellSliderModel(title: "BallisticCalc.Calculator.Distance".localized().uppercased(),
                            parameter: BallisticCalcDistanceRangeParameter.createParameter(BallisticCalcDistanceRangeParameter.self), action: {
                                self.presenterViewController.processSelectionCellWith(model: self.distanceRangeModel.parameter,
                                                                                      title: "BallisticCalc.Calculator.Distance".localized(),
                                                                                      step: self.stepModel.parameter.value.value,
                                                                                      isRange: true,
                                                                                      completion: { (cancelled, newModel) in
                                                                                        if !cancelled {
                                                                                            self.distanceRangeModel.parameter = newModel
                                                                                            self.tableView.reloadData()
                                                                                            self.dataWasChanged?()
                                                                                        }
                                })
            })
        
        angleModel = CellSliderModel(title: "BallisticCalc.Calculator.Angle".localized().uppercased(),
                                     parameter: BallisticCalcAngleParameter.createParameter(BallisticCalcAngleParameter.self), action: {
                                        self.presenterViewController.processSelectionCellWith(model: self.angleModel.parameter,
                                                                                              title: "BallisticCalc.Calculator.Angle".localized(),
                                                                                              completion: { (cancelled, newModel) in
                                                                                                if !cancelled {
                                                                                                    self.angleModel.parameter = newModel
                                                                                                    self.tableView.reloadData()
                                                                                                    self.dataWasChanged?()
                                                                                                }
                                        })
        })
        
        windSpeedModel = CellSliderModel(title: "BallisticCalc.Calculator.WindSpeed".localized().uppercased(),
                                         parameter: BallisticCalcWindSpeedParameter.createParameter(BallisticCalcWindSpeedParameter.self), action: {
                                            self.presenterViewController.processSelectionCellWith(model: self.windSpeedModel.parameter,
                                                                                                  title: "BallisticCalc.Calculator.WindSpeed".localized(),
                                                                                                  completion: { (cancelled, newModel) in
                                                                                                    if !cancelled {
                                                                                                        self.windSpeedModel.parameter = newModel
                                                                                                        self.tableView.reloadData()
                                                                                                        self.dataWasChanged?()
                                                                                                    }
                                            })
        })
        
        windDirectionModel = CellSliderModel(title: "BallisticCalc.Calculator.WindAngle".localized().uppercased(),
                                             parameter: BallisticCalcWindDirectionParameter.createParameter(BallisticCalcWindDirectionParameter.self), action: {
                                                self.presenterViewController.processSelectionCellWith(model: self.windDirectionModel.parameter,
                                                                                                      title: "BallisticCalc.Calculator.WindAngle".localized(),
                                                                                                      completion: { (cancelled, newModel) in
                                                                                                        if !cancelled {
                                                                                                            self.windDirectionModel.parameter = newModel
                                                                                                            self.tableView.reloadData()
                                                                                                            self.dataWasChanged?()
                                                                                                        }
                                                })
        })
    }
    
    func reloadWithRange(_ isRange: Bool) {
        if isRange {
            data = [.rangeDistance, .steps, .angle, .speed, .direction, .weather]
        } else {
            data = [.distance, .angle, .speed, .direction, .weather]
        }
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableData = data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "BallisticSliderCell") as! BallisticSliderCell
        if tableData == .weather {
            let weatherCell = tableView.dequeueReusableCell(withIdentifier: "BCWeatherCell") as! BCWeatherCell
            weatherCell.titleLabel.text = "BallisticCalc.Calculator.SetupWeather.Title".localized()
            weatherCell.subtitleLabel.text = "BallisticCalc.Calculator.SetupWeather.Subtitle".localized()
            return weatherCell
        }
        
        configureCell(cell: cell, tableData: tableData)
        return cell
    }
    
    private func configureCell(cell: BallisticSliderCell, tableData: TableData) {
        switch tableData {
        case .rangeDistance:
            processCellWithRangeModel(rawModel: distanceRangeModel,
                                      step: stepModel.parameter.value.value, cell: cell)
            cell.iconImageView.image = UIImage(named: "bc_distance")
        case .distance:
            processCellWithModel(model: distanceModel, isInteger: true, cell: cell)
            cell.iconImageView.image = UIImage(named: "bc_distance")
            
        case .steps: processCellWithModel(model: stepModel, isInteger: true, cell: cell)
            cell.iconImageView.image = UIImage(named: "bc_steps")
            
        case .angle: processCellWithModel(model: angleModel, cell: cell)
            cell.iconImageView.image = UIImage(named: "bc_angle")
            
        case .speed: processCellWithModel(model: windSpeedModel, cell: cell)
            cell.iconImageView.image = UIImage(named: "bc_speed")
            
        case .direction: processCellWithModel(model: windDirectionModel, cell: cell)
            cell.iconImageView.image = UIImage(named: "bc_windDirection")
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let tableData = data[indexPath.row]
        if tableData == .weather {
            presenterViewController.performSegue(withIdentifier: "BCWeatherViewController", sender: nil)
            return indexPath
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func processCellWithRangeModel<B: CellSliderModel<BallisticCalcDistanceRangeParameter>>(
        rawModel: B, step: Double, cell: BallisticSliderCell) {
        let model = rawModel as CellSliderModel<BallisticCalcDistanceRangeParameter>
        let limitsData = model.parameter.selectedLimit
        let parameter = model.parameter

        cell.slider.maximumValue = Double(floor(limitsData.limits.max / stepModel.parameter.value.value) * stepModel.parameter.value.value)
        cell.slider.minimumValue = Double(ceil(limitsData.limits.min / stepModel.parameter.value.value) * stepModel.parameter.value.value)
        cell.slider.setValue(low: parameter.range.value.min, high: parameter.range.value.max, animated: false)
        DispatchQueue.main.async {
            cell.slider.setValue(low: parameter.range.value.min, high: parameter.range.value.max, animated: false)
        }
        cell.slider.isLowHandleHidden = false
        cell.slider.stepValue = step
        cell.titleLabel.text = model.title
        let type = model.isWithType ? model.parameter.localizedType : ""
        cell.valueLabel.text = "\(Int(parameter.range.value.min)) - \(Int(parameter.range.value.max)) " + type

        cell.rangeUpdated = { min, max in
            model.parameter.value.value =
                self.distanceValueBySteps(min: min,
                                          max: max,
                                          model: self.stepModel.parameter)
            let type = model.isWithType ? model.parameter.localizedType : ""
            cell.valueLabel.text = "\(Int(min)) - \(Int(max)) " + type
            self.dataWasChanged?()
        }
        cell.changeValueByPicker = model.action
    }
    
    private func processCellWithModel<T: BallisticCalcParameterProtocol>(model: CellSliderModel<T>,
                                      isInteger: Bool = false,
                                      cell: BallisticSliderCell) {
        let limitsData = model.parameter.selectedLimit
        cell.slider.maximumValue = limitsData.limits.max
        cell.slider.minimumValue = limitsData.limits.min
        cell.slider.setValue(low: model.parameter.value.value, high: model.parameter.value.value, animated: false)
//        cell.slider.lowValue =  model.parameter.value.value
        cell.slider.isLowHandleHidden = true
//        cell.slider.disableRange = true
//        cell.slider.hideLabels = true
        
        cell.titleLabel.text = model.title
        let type = model.isWithType ? model.parameter.localizedType : ""
        let value = isInteger ? "\(Int(model.parameter.value.value)) " : "\(String(format: "%.2f", model.parameter.value.value)) "
        cell.valueLabel.text = value + type
        
        cell.rangeUpdated = { min, max in
            model.parameter.value.value = max
            let type = model.isWithType ? model.parameter.localizedType : ""
            let value = isInteger ? "\(Int(max)) " : "\(String(format: "%.2f", max)) "
            cell.valueLabel.text = value + type
            
            if Swift.type(of: model.parameter) == Swift.type(of: self.stepModel.parameter) {
                self.distanceRangeModel.parameter.value.value =
                    self.distanceValueBySteps(min: self.distanceRangeModel.parameter.range.value.min,
                                              max: self.distanceRangeModel.parameter.range.value.max,
                                              model: model.parameter as! BallisticCalcStepsParameter)
            }
            
            self.dataWasChanged?()
            self.tableView.reloadData()
        }
        cell.changeValueByPicker = model.action
    }
    
    private func distanceValueBySteps(min: Double, max: Double, model: BallisticCalcStepsParameter) -> Double {
        var minRange = round(min / model.value.value) * model.value.value
        var maxRange = round(max / model.value.value) * model.value.value
        
        let limit = self.distanceRangeModel.parameter.selectedLimit
        if maxRange > limit.limits.max {
            maxRange = floor(limit.limits.max / model.value.value) * model.value.value
        }
        if minRange < limit.limits.min {
            minRange = ceil(limit.limits.min / model.value.value) * model.value.value
        }
        let maxForPattern = Double(10 ^^ limit.pattern.integer)
        if maxRange >= maxForPattern {
            maxRange = maxForPattern - 1
        }

        return minRange + (0.1 ^^ Double(limit.pattern.integer)) * maxRange
    }
}
