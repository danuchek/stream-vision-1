//
//  DeviceConnect.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/16/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import SVProgressHUD
import STZPopupView
import Localize_Swift

extension DeviceDetailsViewController: StoreManager, RateManager {}

class DeviceDetailsViewController: UIViewController,
                                   DeviceManager,
                                   SocketConnectionSubsriber,
                                   ViewControllerWithDeviceProtocol,
                                   UIPopoverPresentationControllerDelegate {
    // Outlets
    @IBOutlet fileprivate weak var deviceImageView: UIImageView!
    @IBOutlet fileprivate weak var deviceLogoImageView: UIImageView!
    @IBOutlet fileprivate weak var addNewDeviceButton: UIButton!
    @IBOutlet fileprivate weak var dropDevicesListButton: UIButton!
    
    @IBOutlet fileprivate weak var statusStackView: UIStackView!
    @IBOutlet fileprivate weak var functionButtonsStackView: UIStackView!
    
    private enum ViewState {
        case demo
        case unnown
        case connected
        case disconnected
        case downloadFirmware
        case installFirmware
        case checkFirmware
        case guest
    }
    
    private var viewsForState: (_ states: [ViewState]) -> [[UIView]] {
        return { [weak self] states in
            guard let deviceModel = self?.deviceModel else {
                return []
            }
            
            let labelFont = UIFont(name: "Roboto-Regular", size: 14.0)
            
            let connectionLabel = UILabel()
            connectionLabel.font = labelFont
            connectionLabel.textColor = .white
            
            let serialLabel = UILabel()
            serialLabel.font = labelFont
            serialLabel.textColor = .white
            let serial = deviceModel.info.serial
            serialLabel.text = "\( "DeviceDetails.SerialNumber".localized() ) \(serial)"
            
            let hardwareLabel = UILabel()
            hardwareLabel.font = labelFont
            hardwareLabel.textColor = .white
            if let hw = self?.deviceModel.info.hw {
                hardwareLabel.text = "HW: \(hw)"
            }
            
            let checkForUpdatesLabel = UILabel()
            checkForUpdatesLabel.font = labelFont
            checkForUpdatesLabel.textColor = .white
            checkForUpdatesLabel.numberOfLines = 0
            checkForUpdatesLabel.adjustsFontSizeToFitWidth = true
            checkForUpdatesLabel.minimumScaleFactor = 0.5
            checkForUpdatesLabel.lineBreakMode = .byClipping
            
            let hasFirmware = deviceModel.hasDownloadedFirmware
            let actualFirmwareVersion: FirmwareVersion = deviceModel.firmware.version
            let firmwareVersion = deviceModel.info.version
            
            checkForUpdatesLabel.text = "\("DeviceDetails.Software".localized() ) \(firmwareVersion.versionString)"
            if (deviceModel.firmware.lastCheckForUpdates != nil) && !hasFirmware && firmwareVersion < actualFirmwareVersion {
                checkForUpdatesLabel.text = "\("DeviceDetails.Software".localized()) \(actualFirmwareVersion.versionString) \("DeviceDetails.Available".localized())"
            } else if !hasFirmware {
                let deviceConnectionDateString = deviceModel.connectionDate.deviceConnectionStyledDateString()
                checkForUpdatesLabel.text = "\("DeviceDetails.Software".localized()) \(firmwareVersion.versionString) (\("DeviceDetails.LastCheck".localized()) \(deviceConnectionDateString))"
            }
            
            let informForUploadingLabel = UILabel()
            informForUploadingLabel.font = labelFont
            informForUploadingLabel.textColor = .white
            informForUploadingLabel.numberOfLines = 0
            informForUploadingLabel.textColor = .gray
            informForUploadingLabel.text = "DeviceDetails.PleaseConnectDevice".localized()
            
            let deviceDescriptionButton = UIButton()
            deviceDescriptionButton.titleLabel?.font = labelFont
            deviceDescriptionButton.contentHorizontalAlignment = .left
            deviceDescriptionButton.setTitleColor(.hex("0075F2"), for: .normal)
            deviceDescriptionButton.setTitle("DeviceDetails.DeviceDescription".localized(), for: .normal)
            deviceDescriptionButton.addTarget(self, action: #selector(self?.showDeviceDescription), for: .touchUpInside)
            
            let settingButton = UIButton()
            settingButton.translatesAutoresizingMaskIntoConstraints = false
            settingButton.titleLabel?.font = labelFont
            settingButton.layer.cornerRadius = 5
            settingButton.layer.borderColor = UIColor.white.cgColor
            settingButton.layer.borderWidth = 1
            settingButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            settingButton.setTitle("DeviceDetails.Settings".localized(), for: .normal)
            settingButton.addTarget(self, action: #selector(self?.goToSettings), for: .touchUpInside)
            
            let updateButton = UIButton()
            updateButton.translatesAutoresizingMaskIntoConstraints = false
            updateButton.titleLabel?.font = labelFont
            updateButton.layer.cornerRadius = 5
            updateButton.layer.borderColor = UIColor.white.cgColor
            updateButton.layer.borderWidth = 1
            updateButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            updateButton.setTitle("DeviceDetails.FetchDevices".localized(), for: .normal)
            updateButton.addTarget(self, action: #selector(self?.downloadDevice), for: .touchUpInside)
            
            let disconnectButton = UIButton()
            disconnectButton.translatesAutoresizingMaskIntoConstraints = false
            disconnectButton.titleLabel?.font = labelFont
            disconnectButton.layer.cornerRadius = 5
            disconnectButton.layer.borderColor = UIColor.white.cgColor
            disconnectButton.layer.borderWidth = 1
            disconnectButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            disconnectButton.setTitle("DeviceDetails.Disconnect".localized(), for: .normal)
            disconnectButton.addTarget(self, action: #selector(self?.disconnectDemo), for: .touchUpInside)
            
            let checkForUpdatesButton = UIButton()
            checkForUpdatesButton.titleLabel?.font = labelFont
            checkForUpdatesButton.layer.cornerRadius = 5
            checkForUpdatesButton.layer.borderColor = UIColor.white.cgColor
            checkForUpdatesButton.layer.borderWidth = 1
            checkForUpdatesButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            checkForUpdatesButton.setTitle("DeviceDetails.CheckForUpdate".localized(), for: .normal)
            checkForUpdatesButton.addTarget(self, action: #selector(self?.checkUpdatesForDevice), for: .touchUpInside)
            
            let installButton = UIButton()
            installButton.titleLabel?.font = labelFont
            installButton.layer.cornerRadius = 5
            installButton.backgroundColor = .hex("1E75FE")
            installButton.layer.borderWidth = 1
            installButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            installButton.setTitle("DeviceDetails.InstallNewSoftware".localized(), for: .normal)
            installButton.addTarget(self, action: #selector(self?.installFirmwareForDevice), for: .touchUpInside)
            
            let downloadButton = UIButton()
            downloadButton.titleLabel?.font = labelFont
            downloadButton.layer.cornerRadius = 5
            downloadButton.backgroundColor = .hex("1E75FE")
            downloadButton.layer.borderWidth = 1
            downloadButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            downloadButton.setTitle("DeviceDetails.DownloadNewSoft".localized(), for: .normal)
            downloadButton.addTarget(self, action: #selector(self?.downloadFirmwareForDevice), for: .touchUpInside)

            var infoViews: [UIView] = [connectionLabel, serialLabel, hardwareLabel, checkForUpdatesLabel]
            var buttons: [UIView] = []
            
            for state in states {
                switch state {
                case .checkFirmware:
                    buttons.append(checkForUpdatesButton)
                case .installFirmware:
                    if !states.contains(.guest) && !states.contains(.disconnected) {
                        buttons.append(installButton)
                    }
                case .downloadFirmware:
                    buttons.append(downloadButton)
                case .connected:
                    if states.contains(.guest) {
                        connectionLabel.text = "DeviceDetails.GuestMode".localized()
                    } else {
                        connectionLabel.text = "DeviceDetails.ConnectedNow".localized()
                    }
                    
                    if !states.contains(.demo) {
                        buttons.append(settingButton)
                    }
                case .demo:
                    buttons.append(disconnectButton)
                case .unnown:
                    if states.contains(.guest) {
                        connectionLabel.text = "DeviceDetails.GuestMode".localized()
                    } else {
                        connectionLabel.text = "DeviceDetails.ConnectedNow".localized()
                    }
                    
                    if !states.contains(.demo) {
                        buttons.append(updateButton)
                    }
                    
                case .disconnected:
                    if let lastConnectionDate = self?.deviceModel.disconnectionDate.deviceConnectionStyledDateString() {
                        connectionLabel.text = "\("DeviceDetails.LastConnection".localized()) " + "\(lastConnectionDate)"
                    }
                    
                    if !states.contains(.demo) {
                        buttons.append(settingButton)
                    }
                    if states.contains(.installFirmware) {
                        infoViews.append(informForUploadingLabel)
                    }
                case .guest: break
                }
            }
            
            infoViews.append(deviceDescriptionButton)
            
            return [infoViews, buttons]
        }
    }
    
    
    // Variables
    var deviceModel: VirtualDevice!
    fileprivate var firmwareUpdateService = FirmwareUpdateService()
    fileprivate var popupView: FirmwareUpdateView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize veriables
        deviceModel = storeManager.devices.first

        if let revealController = self.revealViewController() {
            view.addGestureRecognizer(revealController.panGestureRecognizer())
            let rearViewController = revealController.rearViewController!
            (rearViewController as! SideTableViewController).selectedScreen = .devices
        }
        setupView()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "MyDevices_open")
        
        // Subcribe
        SocketManager.subscribe(self, notificationsType: [.DeviceAccessUpdated, .DeviceConnected])
        checkFirmware()
        updateView()
        localizeView()
        
        SocketManager.send(.getDeviceInfo)
    }
    
    // MARK: Navigation
    @objc fileprivate func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        SocketManager.unsubscribe(self)
    }
    
    @IBAction func showDevicesList(sender: UIButton) {
        let popController = DevicesListTableViewController()
        popController.selectedDevice = deviceModel
        popController.deviceSelectedAction = { device in
            self.deviceModel = device
            self.updateView()
        }
        
        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: sender.bounds.width, height: sender.bounds.height)
        
        popController.popoverPresentationController?.backgroundColor = .hex("161A1E")
        
        // present the popover
        self.present(popController, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController,
                                   traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }

    fileprivate func setupView() {
        let menu = MenuButton()
        menu.addTarget(self, action: #selector(openSideMenu), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)
        
        
        functionButtonsStackView.axis  = .vertical
        functionButtonsStackView.distribution = .equalSpacing
        functionButtonsStackView.alignment = .top
        functionButtonsStackView.spacing = 20.0
    }
    
    fileprivate func checkFirmware() {
        if deviceModel.info.version > deviceModel.firmware.version {
            _ = try? FilesManager.shared.pathToFirmwareFromDevice(deviceModel).deleteFile()
        }
    }
    
    // MARK: Update view
    fileprivate func updateView() {
        guard let deviceModel = deviceModel else {
            return
        }
        
        var views: [[UIView]]!
        
        for view in statusStackView.arrangedSubviews {
            view.removeFromSuperview()
        }
        for view in functionButtonsStackView.arrangedSubviews {
            view.removeFromSuperview()
        }
        
        if deviceModel.info.sku == "00000" {
            views = viewsForState([.demo, .connected])
        } else if deviceModel.info.sku == "0" {
            views = viewsForState([.unnown, .connected])
        } else if SocketManager.isDeviceConnected(self.deviceModel),
            SocketManager.isGuestMode, !deviceModel.hasNewestFirmware,
            !deviceModel.hasDownloadedFirmware {
            views = viewsForState([.checkFirmware ,.guest, .connected])
        } else if SocketManager.isDeviceConnected(deviceModel),
            SocketManager.isGuestMode, deviceModel.hasNewestFirmware {
            views = viewsForState([.downloadFirmware ,.guest, .connected])
        } else if SocketManager.isDeviceConnected(deviceModel),
            SocketManager.isGuestMode, deviceModel.hasDownloadedFirmware {
            views = viewsForState([.installFirmware ,.guest, .connected])
        } else if SocketManager.isDeviceConnected(deviceModel), SocketManager.isGuestMode {
            views = viewsForState([.guest, .connected])
        } else if SocketManager.isDeviceConnected(deviceModel), deviceModel.hasDownloadedFirmware {
            views = viewsForState([.installFirmware, .connected])
        } else if !SocketManager.isDeviceConnected(deviceModel), deviceModel.hasDownloadedFirmware {
            views = viewsForState([.checkFirmware ,.installFirmware, .disconnected])
        } else if SocketManager.isDeviceConnected(deviceModel), deviceModel.hasNewestFirmware {
            views = viewsForState([.downloadFirmware, .connected])
        } else if !SocketManager.isDeviceConnected(deviceModel), deviceModel.hasNewestFirmware {
            views = viewsForState([.downloadFirmware, .disconnected])
        } else if !deviceModel.hasDownloadedFirmware, !deviceModel.hasNewestFirmware {
            views = viewsForState([.checkFirmware, SocketManager.isConnected ? .connected : .disconnected])
        }
        
        for view in views[0] {
            statusStackView.addArrangedSubview(view)
        }
        for view in views[1] {
            functionButtonsStackView.addArrangedSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false

            NSLayoutConstraint.activate([
                view.heightAnchor.constraint(equalToConstant: 40),
                view.centerXAnchor.constraint(equalTo: functionButtonsStackView.centerXAnchor)
            ])
        }
        
        let hackView = UIView()
        hackView.backgroundColor = .clear
        functionButtonsStackView.addArrangedSubview(hackView)
        
        deviceImageView.image = deviceModel.info.image
        deviceLogoImageView.image = deviceModel.info.brand.image()
        dropDevicesListButton.setTitle(deviceModel.info.name, for: .normal)
    }
    
    fileprivate func localizeView() {
        addNewDeviceButton.setTitle("DevicesList.MyDevice.AddNewDevice".localized(), for: .normal)
        navigationItem.rightBarButtonItem?.title = "DeviceDetails.AllDevices".localized()
        
        title = "DeviceDetails.Title".localized()
    }
    
    // MARK: Notification
    internal func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        DispatchQueue.main.async { [weak self] in
           self?.updateView()
        }
    }
    
    fileprivate func createFirmwareUpdateView() -> FirmwareUpdateView {
        let popupView = FirmwareUpdateView(frame: CGRect(x: 0,y: 0,width: 314,height: 204))
        popupView.center = view.center
        popupView.cancelButton.addTarget(self, action: #selector(cancelDownloadFirmware), for: .touchUpInside)
        return popupView
    }
    
    // MARK: Actions
    @IBAction func showDeviceDescription() {
        let alertController =
            UIAlertController(title: "DeviceDetails.Description.Alert.Title".localized(),
                              message: deviceModel.info.specification.current,
                              preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "General.Alert.Ok".localized() ,
                                     style: .default,
                                     handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func checkUpdatesForDevice() {
        checkUpdatesWith { hasNewestFirmware in
            if !hasNewestFirmware {
                self.present(self.alertFirmwareNewest(), animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func checkUpdatesWith(completion: ((Bool) -> ())?) {
        SVProgressHUD.show()
        
        ConnectionService.reachability { [weak self] (status) in
            if let deviceModel = self?.deviceModel, status == .reachable {
                self?.firmwareUpdateService.checkForUpdatesForDevice(deviceModel) { device in
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                    }
                    completion?(device.hasNewestFirmware)
                    
                    self?.updateView()
                }
            } else {
                SVProgressHUD.dismiss()
                if let alert = self?.alertInternetConnection() {
                    self?.present(alert, animated: true, completion: nil)
                }
            }
        }
    }

    fileprivate func alertFirmwareNewest() -> UIAlertController {
        let confirmationAlert = UIAlertController(title: "DeviceDetails.CheckForUpdate.Alert.Title".localized() , message: nil, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized() , style: .default, handler: nil))
        return confirmationAlert
    }
    
    fileprivate func alertFirmwareNoNewUpdates() -> UIAlertController {
        let confirmationAlert = UIAlertController(title: "DeviceDetails.NoNewUpdates".localized() , message: nil, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized() , style: .default, handler: nil))
        return confirmationAlert
    }
    
    fileprivate func alertInternetConnection() -> UIAlertController {
        let confirmationAlert = UIAlertController(title: "DeviceDetails.CheckingForUpdate.NoConnection".localized() , message: nil, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized() , style: .default, handler: nil))
        return confirmationAlert
    }
    
    fileprivate func alertCorruptedFirmware() -> UIAlertController {
        let confirmationAlert = UIAlertController(title: "DevicesList.MyDevice.Alert.CrcError.Message".localized() , message: nil, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized() , style: .default, handler: nil))
        return confirmationAlert
    }
    
    fileprivate func alertCancelDownload() -> UIAlertController {
        let confirmationAlert = UIAlertController(title: "DeviceDetails.CancelAlert.Message".localized() , message: nil, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized() , style: .default, handler: nil))
        return confirmationAlert
    }

    @IBAction func downloadFirmwareForDevice() {
        checkUpdatesWith { [weak self] hasNewestFirmware in
            if let alert = self?.alertFirmwareNoNewUpdates(), !hasNewestFirmware {
                self?.present(alert, animated: true, completion: nil)
                return
            }
            
            let confirmationAlert = UIAlertController(title: "DeviceDetails.DownloadUpdate.Alert.Title".localized() ,
                                                      message: self?.deviceModel.firmware.firmwareDescription,
                                                      preferredStyle: .alert)
            confirmationAlert.addAction(UIAlertAction(title: "DeviceDetails.DownloadAction.Alert.Download".localized() , style: .default, handler: { (action) in
                self?.downloadFirmware()
            }))
            confirmationAlert.addAction(UIAlertAction(title: "DeviceDetails.DownloadUpdate.Alert.Later".localized() , style: .cancel, handler: nil))
            self?.present(confirmationAlert, animated: true, completion: nil)
        }
    }
    
    fileprivate func downloadFirmware() {
        guard let version = deviceModel?.info.version.versionString,
              let bytes = deviceModel?.firmware.sizeInBytes.toStringUniversalMemoryUnit(),
              let name = deviceModel?.info.name, let deviceModel = deviceModel else {
                return
        }
        
        popupView = createFirmwareUpdateView()
        popupView?.titleLabel.text = "DeviceDetails.DownloadNewSoft".localized()
        popupView?.descriptionLabel.text =  "\("DeviceDetails.Software".localized()) \(version) (\(bytes)) \("DeviceDetails.DownloadingAlert.NewFeatures.Union".localized()) \(name). \("DeviceDetails.DownloadingAlert.NewFeatures".localized())"

        let config = STZPopupViewConfig()
        config.dismissTouchBackground = false
        presentPopupView(popupView!, config: config)

        firmwareUpdateService.downloadFirmwareForDevice(deviceModel, progressBlock: { [weak self] percents in
            if let weakSelf = self {
                weakSelf.popupView!.updateProgressInPercents(percents)
            }
        }) { [weak self] error in
            if let _ = error {
                if error!.localizedDescription.contains("cancelled") {
                    self?.present(self?.alertCancelDownload() ?? UIViewController(), animated: true, completion: nil)
                } else {
                    self?.present(self?.alertCorruptedFirmware() ?? UIViewController(), animated: true, completion: nil)
                }

            }
            self?.updateView()
            self?.dismissPopupView()
        }
    }
    
    @objc fileprivate func cancelDownloadFirmware() {
        firmwareUpdateService.cancelCurrentDownloadFirmware()
    }
    
    @IBAction func installFirmwareForDevice() {
        // Check if battery ok?
        
        SocketManager.send(.getParams).response { [weak self] model, error in
            Logging.log(message: "[SocketConnect] Device params was received", toFabric: true)
            
            guard let parametersData = model as? Data,
                  let device = SocketManager.connectedDevice, error == nil else {
                    return
            }
            
            _ = device.parameters.update(parametersData)
            
            if let extDc = device.parameters.extDc, extDc.value == 1 {
                self?.installFirmware()
            } else if let battery = device.parameters.battery, battery.value <= 50.0 {
                self?.checkBattery()
            } else {
                self?.installFirmware()
            }
        }
    }
    
    private func checkBattery() {
        let confirmationAlert = UIAlertController(title: String(format: "DeviceDetails.UpdateLowBattery".localized(), "50%") , message: nil, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized() ,
                                                  style: .default,
                                                  handler: nil))
        
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    private func installFirmware() {
        let confirmationAlert = UIAlertController(title: "DeviceDetails.InstallSoftware.Confirmation.Title".localized() , message: nil, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "DeviceDetails.InstallSoftware.InstallNow".localized() , style: .default, handler: { (action) in
            self.installFirmwareToDevice()
        }))
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized() , style: .cancel, handler: nil))
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    @IBAction func disconnectDemo() {
        SocketManager.connectionMode = .normal
        navigateToMain(animated: true)
    }
    
    fileprivate func installFirmwareToDevice() {
        UIApplication.shared.isIdleTimerDisabled = true
        
        popupView = createFirmwareUpdateView()
        popupView?.hideCancelButton()
        popupView?.titleLabel.text = "DeviceDetails.InstallSoftware.Title.Prefix".localized()
        popupView?.descriptionLabel.text = "DeviceDetails.InstallSoftware.Message".localized()
        
        let config = STZPopupViewConfig()
        config.dismissTouchBackground = false
        presentPopupView(popupView!, config: config)
        
        firmwareUpdateService.uploadFirmwareToDevice({ [weak self] totalIterations in
            if let weakSelf = self {
                weakSelf.popupView?.setupProgress(totalIterations)
            }
            }, progressBlock: { [weak self] iteration in
                if let weakSelf = self {
                    weakSelf.popupView?.updateProgress(iteration)
                }
        }) { [weak self] result in
                self?.updateView()
                switch result {
                case .receivedUpdate:
                    SocketManager.disconnect()
                    self?.firmwareUpdatedAlert(title: "DeviceDetails.SendSuccessAlert.Title".localized(),
                                               message: "DeviceDetails.SendSuccessAlert.Message".localized())
                    self?.rateManager.firmwareUpdated()
                case .finishUpdate:
                    SocketManager.disconnect()
                    self?.firmwareUpdatedAlert(title: "DeviceDetails.UploadSuccessAlert.Title".localized(),
                                              message: "DeviceDetails.UploadSuccessAlert.Message".localized())
                    self?.rateManager.firmwareUpdated()
                case .errorUpdate(_):
                    if self == nil {
                        break
                    }
                    self?.firmwareUpdatedAlert(title: "DeviceDetails.UploadFailedAlert.Title".localized(),
                                               message: "DeviceDetails.UploadFailedAlert.Message".localized(),
                                               success: false
                    )
                }
                self?.dismissPopupView()
        }
    }
    //DeviceDetails.SendSuccessAlert.Message
    
    fileprivate func firmwareUpdatedAlert(title: String, message: String, success: Bool = true) {
        let confirmationAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: { (action) in
            if success {
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
        }))
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    fileprivate func informIfFirmwareUpdated(_ updated: Bool) {
        UIApplication.shared.isIdleTimerDisabled = false
        
        var message = "DeviceDetails.UploadSuccessAlert.Message".localized()
        var title = "DeviceDetails.UploadSuccessAlert.Title".localized()
        if !updated {
            message = "DeviceDetails.UploadFailedAlert.Message".localized()
            title = "DeviceDetails.UploadFailedAlert.Title".localized()
        }
        
        let confirmationAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: { (action) in
            if updated {
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
        }))
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    @IBAction func goToSettings() {
        if SocketManager.isGuestMode {
            let confirmationAlert = UIAlertController(title: "DeviceDetails.Settings.CantEnterDueToGuestMode".localized(), message: nil, preferredStyle: .alert)
            confirmationAlert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil))
            present(confirmationAlert, animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: "to device settings", sender: self)
        }
    }
    
    @IBAction func downloadDevice() {
        SVProgressHUD.show()
        
        ConnectionService.reachability { [weak self] (status) in
            if status == .reachable {
                self?.deviceManager.fetch {
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        let currentDeviceSku = self?.deviceModel.info.invalidSku == "" ? self?.deviceModel.info.sku : self?.deviceModel.info.invalidSku
                        self?.deviceManager.reload()
                        self?.deviceModel = self?.storeManager.devices.filter({ $0.info.sku == currentDeviceSku || $0.info.invalidSku == currentDeviceSku }).last
                        self?.updateView()
                    }
                }
            } else {
                SVProgressHUD.dismiss()
                if let self = self {
                    self.present(self.alertInternetConnection(), animated: true, completion: nil)
                }
            }
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "to device settings") {
            let deviceSettings = segue.destination as! DeviceSettingsViewController
            deviceSettings.selectedDevice = deviceModel
        }
    }
}
