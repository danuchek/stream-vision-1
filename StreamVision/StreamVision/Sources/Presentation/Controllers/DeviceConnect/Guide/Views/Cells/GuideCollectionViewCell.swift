//
//  GuideCollectionViewCell.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 12/7/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit

class GuideCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var decriptionImageView: UIImageView!
    @IBOutlet weak var viewfinderButton: UIButton!
    @IBOutlet weak var exitButton: UIButton!

    @IBOutlet weak var step1Label: UILabel!
    @IBOutlet weak var step2Label: UILabel!
    @IBOutlet weak var step3Label: UILabel!
    @IBOutlet weak var step4Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        exitButton.setTitle("Guide.Exit.Button".localized(), for: .normal)
        viewfinderButton.setTitle("Guide.Viewfinder.Button".localized(), for: .normal)

        step1Label.text = "Guide.item1.description".localized()
        step1Label.tag = 0

        step2Label.text = "Guide.item2.description".localized()
        step2Label.tag = 1

        step3Label.text = "Guide.item3.description".localized()
        step3Label.tag = 2

        step4Label.text = "Guide.item4.description".localized()
        step4Label.tag = 3
        
        
        self.decriptionImageView.translatesAutoresizingMaskIntoConstraints = false
        self.step1Label.translatesAutoresizingMaskIntoConstraints = false
        self.step2Label.translatesAutoresizingMaskIntoConstraints = false
        self.step3Label.translatesAutoresizingMaskIntoConstraints = false
        self.step4Label.translatesAutoresizingMaskIntoConstraints = false
        self.exitButton.translatesAutoresizingMaskIntoConstraints = false
        self.viewfinderButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.decriptionImageView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 10),
            self.decriptionImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            self.decriptionImageView.widthAnchor.constraint(equalTo: self.decriptionImageView.heightAnchor,
                                                            multiplier: 1,
                                                            constant: 0),
            self.decriptionImageView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            
            self.step1Label.leadingAnchor.constraint(equalTo: self.decriptionImageView.trailingAnchor, constant: 20),
            self.step1Label.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -18),
            self.step1Label.topAnchor.constraint(equalTo: self.decriptionImageView.topAnchor),
            
            self.step2Label.leadingAnchor.constraint(equalTo: self.step1Label.leadingAnchor),
            self.step2Label.trailingAnchor.constraint(equalTo: self.step1Label.trailingAnchor),
            self.step2Label.topAnchor.constraint(equalTo: self.step1Label.bottomAnchor, constant: 10),
            
            self.step3Label.leadingAnchor.constraint(equalTo: self.step1Label.leadingAnchor),
            self.step3Label.trailingAnchor.constraint(equalTo: self.step1Label.trailingAnchor),
            self.step3Label.topAnchor.constraint(equalTo: self.step2Label.bottomAnchor, constant: 10),
            
            self.step4Label.leadingAnchor.constraint(equalTo: self.step1Label.leadingAnchor),
            self.step4Label.trailingAnchor.constraint(equalTo: self.step1Label.trailingAnchor),
            self.step4Label.topAnchor.constraint(equalTo: self.step3Label.bottomAnchor, constant: 10),
            
            self.exitButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            self.exitButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.4),
            self.exitButton.centerXAnchor.constraint(equalTo: step1Label.centerXAnchor),
            self.exitButton.heightAnchor.constraint(equalToConstant: 30),
            
            self.viewfinderButton.bottomAnchor.constraint(equalTo: self.exitButton.topAnchor, constant: -10),
            self.viewfinderButton.leadingAnchor.constraint(equalTo: self.exitButton.leadingAnchor),
            self.viewfinderButton.trailingAnchor.constraint(equalTo: self.exitButton.trailingAnchor),
            self.viewfinderButton.heightAnchor.constraint(equalToConstant: 30),
        ])
    }
    
    func setupWithItem(_ item: GuideItem) {
        decriptionImageView.image = item.descriptionImage
        exitButton.isHidden = !item.needExitButton
        viewfinderButton.isHidden = !item.needViewfinderButton

        for label in [step1Label, step2Label , step3Label, step4Label] {
            if label?.tag == item.selectionTag {
                label?.font = UIFont(name: "Roboto-Bold", size: 20.0)
                label?.textColor = .white
            } else {
                label?.font = UIFont(name: "Roboto-Regular", size: 17.0)
                label?.textColor = .lightText
            }
        }
    }
}
