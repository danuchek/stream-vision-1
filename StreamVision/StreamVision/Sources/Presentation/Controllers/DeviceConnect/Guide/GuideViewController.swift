//
//  Guide.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 12/7/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit
import SWRevealViewController

class GuideViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, SocketConnectionSubsriber {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var menuButton: StylizedButton!
    var items: Array<GuideItem>!

    override func viewDidLoad() {
        super.viewDidLoad()

        localizeView()

        items = [
            GuideItem.item(with: UIImage(named: "guide_first_img")!),
            GuideItem.item(with: UIImage(named: "guide_second_img")!),
            GuideItem.item(with: UIImage(named: "guide_third_img")!),
            GuideItem.item(with: UIImage(named: "guide_fourth_img")!)
        ]
        items.last?.needExitButton = true

        pageControll.numberOfPages = items.count

        let trans: CGAffineTransform  = CGAffineTransform(rotationAngle: 3.14/2);
        self.pageControll.transform = trans;
        self.edgesForExtendedLayout = []
        collectionView.isPagingEnabled = true
        pageControll.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            pageControll.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 5),
            pageControll.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 38)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        googleReport(action: "ConnectionGuide_open")
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        SocketManager.subscribe(self, notificationsType: [.DeviceConnected, .DeviceDisconnected])
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        SocketManager.unsubscribe(self)
    }

    // MARK: Socket connection notification
    internal func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.reloadData()
        }
    }

    fileprivate func localizeView() {
        title = "Guide.Title".localized()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, targetContentOffsetForProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        
        return collectionView.collectionViewLayout.targetContentOffset(forProposedContentOffset: proposedContentOffset)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let guidCollectionViewCell: GuideCollectionViewCell =
        collectionView.dequeueReusableCell(withReuseIdentifier: "GuideCollectionViewCell", for: indexPath) as! GuideCollectionViewCell

        let index = (indexPath as NSIndexPath).item
        let item = items[index]
        item.selectionTag = index
        item.needViewfinderButton = SocketManager.isConnected && (items.count-1) == index
        
        guidCollectionViewCell.setupWithItem(item)
        return guidCollectionViewCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let curentOffset = Int(collectionView.contentOffset.y * 0.1) * 10
        let curentWidth = Int(collectionView.frame.height * 0.1) * 10
        let page = round(CGFloat(curentOffset / curentWidth))
        pageControll.currentPage = Int(page)
    }

    // MARK: Actions
    @IBAction func goToMain() {
        let mainViewController: UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeRootViewController(mainViewController, withAnimation: true)
    }

    @IBAction func goToViewfinder() {
        if isNavigateFromMain() {
            (navigationController?.viewControllers.first as! MainViewController).performSegue(withIdentifier: "Viewfinder", sender: nil)
            _ = navigationController?.popViewController(animated: false)
        } else {
            let storyboard = UIStoryboard(name: "Viewfinder", bundle: nil)
            revealViewController().frontViewController = storyboard.instantiateInitialViewController()
        }
    }
}

class GuideItem {
    var descriptionImage: UIImage?
    var descriptionString: String?
    var needExitButton: Bool = false
    var needViewfinderButton: Bool = false
    var selected: Bool = false
    var selectionTag: Int = 0

    class func item(with descriptionImage: UIImage) -> GuideItem {
        let item: GuideItem = GuideItem()
        item.descriptionImage = descriptionImage
        return item
    }
}
