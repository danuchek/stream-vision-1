//
//  DeviceDetailTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/19/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

class DeviceDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var deviceStatusLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        deviceImageView.backgroundColor = UIColor.white
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        deviceImageView.backgroundColor = UIColor.white
    }
    
    func configureWithDevice(device: VirtualDevice) {
        deviceNameLabel.text = device.info.name
        deviceImageView.image = device.info.image
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .hex("228dff")
        selectedBackgroundView = backgroundView
        
        if SocketManager.isDeviceConnected(device) {
            deviceStatusLabel?.text = "DevicesList.MyDevice.Item.Connected".localized()
            deviceStatusLabel?.textColor = .hex("6ECE1B")
        } else {
            deviceStatusLabel?.text = "DevicesList.MyDevice.Item.LastConnectionPrefix".localized() +
            device.connectionDate.deviceConnectionStyledDateString()
            deviceStatusLabel?.textColor = .rgba(255, 255, 255, 0.5)
        }
    }
}
