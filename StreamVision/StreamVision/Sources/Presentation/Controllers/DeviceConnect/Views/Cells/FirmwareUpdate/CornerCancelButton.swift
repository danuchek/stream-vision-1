//
//  CornerCancelButton.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class CornerCancelButton: UIButton {
    var corners: UIRectCorner = []

    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(roundedRect:self.bounds, byRoundingCorners:self.corners, cornerRadii: CGSize(width: 10, height: 10))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }

    func updateWithCornerRadius(_ corners : UIRectCorner) {
        self.corners = corners
    }


}
