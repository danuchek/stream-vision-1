//
//  GoogleSignOutView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import TYMProgressBarView

class FirmwareUpdateView: UIView {
    fileprivate var view: UIView!

    // Outlets
    @IBOutlet fileprivate weak var progressBar: TYMProgressBarView!
    @IBOutlet fileprivate weak var percentLabel: UILabel!
    @IBOutlet fileprivate weak var sepparateView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet fileprivate weak var sepparatorLayout: NSLayoutConstraint!

    // Variables
    var totalIterations: Int = 0


    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)

        xibSetup(with: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)

        setupView()
    }

    fileprivate func setupView() {
        progressBar.barBorderWidth = 0
        progressBar.barFillColor = UIColor.colorFromHexString("80B753")
        progressBar.barBackgroundColor = UIColor.white
        progressBar.barInnerPadding = 0
        
    }

    func hideCancelButton() {
        sepparateView.isHidden = true
        cancelButton.isHidden = true
        sepparatorLayout.constant = 0
        frameHeight(184)
    }

    func setupProgress(_ totalIterations: Int) {
        self.totalIterations = totalIterations
    }

    func updateProgress(_ iteration: Int) {
        guard let _ = percentLabel, let _ = progressBar else {
            return
        }
        
        let progress = (CGFloat(iteration*100)) / CGFloat(totalIterations) / 100
        percentLabel.text = String(format: "%.0f ", progress*100) + "%"
        progressBar.progress = progress
    }

    func updateProgressInPercents(_ percents: Float) {
        percentLabel.text = String(format: "%.0f ", percents*100) + "%"
        progressBar.progress = CGFloat(percents)
    }

}
