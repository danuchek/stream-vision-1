//
//  DateTimeTableViewController.swift
//  StreamVision
//
//  Created by Oleksii Shvachenko on 26.10.16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class DeviceSettingListSelectionTableViewController: UITableViewController {

    struct ViewModel {
        typealias SelectionAction = (Int) -> Void

        let title: String
        let dataArray: [String]
        let selectionAction: SelectionAction
    }
    var selected: Int!
    var viewModel: ViewModel! {
        didSet {
            render()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    private func render() {
        title = viewModel.title
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selection row", for: indexPath)
        cell.textLabel?.text = viewModel.dataArray[indexPath.row].localized()
        if selected == indexPath.row {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectionAction(indexPath.row)
    }
}
