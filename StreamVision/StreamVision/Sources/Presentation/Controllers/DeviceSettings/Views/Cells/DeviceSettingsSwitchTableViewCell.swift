//
//  DeviceSettingsSwitchTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 11/18/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class DeviceSettingsSwitchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var switchComponent: UISwitch!
    @IBOutlet weak var titleLabel: UILabel!
    
    typealias SelectionAction = (UITableViewCell) -> Void
    
    struct ViewModel {
        let title: NSAttributedString
        let selectionAction: SelectionAction?
    }
    
    var viewModel: ViewModel! {
        didSet {
            render()
        }
    }
    
    private func render() {
        titleLabel.attributedText = viewModel.title
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        switchComponent.addTarget(self, action: #selector(didValueChange), for: .valueChanged)
    }
    
    @objc fileprivate func didValueChange() {
        guard let selectectionAction = viewModel.selectionAction else {
            return
        }
        selectectionAction(self)
    }

}
