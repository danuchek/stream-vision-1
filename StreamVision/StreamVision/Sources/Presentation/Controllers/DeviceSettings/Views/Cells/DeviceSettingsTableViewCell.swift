//
//  DeviceSettingsTableViewCell.swift
//  StreamVision
//
//  Created by Oleksii Shvachenko on 07.11.16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class DeviceSettingsTableViewCell: UITableViewCell {

    typealias SelectionAction = (UITableViewCell) -> Void

    struct ViewModel {
        let title: NSAttributedString
        let subtitle: String?
        let selectionAction: SelectionAction?
    }

    var viewModel: ViewModel! {
        didSet {
            render()
        }
    }

    private func render() {
        textLabel?.attributedText = viewModel.title
        detailTextLabel?.text = viewModel.subtitle
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapOnCell))
        contentView.addGestureRecognizer(gestureRecognizer)
    }

    @objc fileprivate func didTapOnCell() {
        guard let selectectionAction = viewModel.selectionAction else {
            return
        }
        selectectionAction(self)
    }
}
