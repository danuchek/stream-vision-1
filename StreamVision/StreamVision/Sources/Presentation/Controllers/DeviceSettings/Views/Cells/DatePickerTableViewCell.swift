//
//  DatePickerTableViewCell.swift
//  StreamVision
//
//  Created by Oleksii Shvachenko on 25.10.16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class DatePickerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
