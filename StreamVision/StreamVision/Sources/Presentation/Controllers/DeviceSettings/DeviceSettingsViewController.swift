//
//  DeviceSettingsViewController.swift
//  StreamVision
//
//  Created by Oleksii Shvachenko on 19.10.16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

class DeviceSettingsViewController: UIViewController, SocketConnectionSubsriber, StoreManager {
    enum Sections: Int {
        case section0 = 0
        case section1 = 1
        case section2 = 2
        case section3 = 3
        case section4 = 4
    }
    
    enum Identifiers: String {
        case timeAndDate = "time and date"
        case setTime = "set time"
        case setDate = "set date"
        case memoryFormatting = "memory formatting"
        case deleteDevice = "delete device"
        case language = "language"
        case units = "units"
        case setTimePicker = "set time picker"
        case setDatePicker = "set date picker"
        case goafGun = "goaf gun"
        case timeFormat = "time format"
        case calibration = "calibration"
        case autoOff = "auto display off"
        case profile = "profile"
    }
    
    var selectedDevice: VirtualDevice!
    let connectedDevice = SocketManager.connectedDevice
    fileprivate var selectedTime: MigrationService.DeviceTime = .Manual
    fileprivate var selectedLanguage: MigrationService.DeviceLanguage = .English
    fileprivate var selectedUnits: MigrationService.DeviceUnits = .Metrs
    fileprivate var selectedAutoOff: MigrationService.DeviceAutoOff = .off
    fileprivate var selectedShutterMode: MigrationService.ShutterMode = .Manual
    fileprivate var selectedProfileMode: MigrationService.Profile = .A
    fileprivate var selectedTimeFormat: Bool = false
    fileprivate var selectedGoafGun: Bool = false
    
    fileprivate var isDeviceConnected: Bool = false {
        didSet {
            dataSourceItemsDictionary = getDataSourceItems()
            tableView.reloadData()
        }
    }
    fileprivate let notificationCenter: NotificationCenter = NotificationCenter.default
    fileprivate let timeFormatter: DateFormatter = DateFormatter()
    fileprivate let dateFormatter: DateFormatter = DateFormatter()
    fileprivate let dateTimeFormatter: DateFormatter = DateFormatter()
    
    typealias DataSourceItems = [Sections : Array<Identifiers> ]
    var dataSourceItemsDictionary: DataSourceItems = [:]
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeView()
        
        view.backgroundColor = tableView.backgroundColor
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
        timeFormatter.dateFormat = "HH:mm"
        dateFormatter.dateFormat = "dd.MM.YYYY"
        
        dateTimeFormatter.locale = Locale(identifier: "en_GB")
        dateTimeFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        
        dataSourceItemsDictionary = getDataSourceItems()
        fetchDeviceParameters()
    }
    
    private func localizeView() {
        title = "Settings.Title".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SocketManager.subscribe(self, notificationsType: [.DeviceConnected, .DeviceDisconnected, .DeviceAccessUpdated, .DeviceParamsUpdated])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        SocketManager.unsubscribe(self)
    }

    fileprivate func performFormatAction() {
        SVProgressHUD.show()
        SocketManager.send(.fomat).response { _, error in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                let alertController = UIAlertController(title: error == nil ?
                    "Settings.MemoryFormatting.Alert.Success".localized() :
                    "Settings.MemoryFormatting.Alert.Failure".localized(), message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                                        style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func performDeleteDeviceAction() {
        storeManager.deleteDevice(selectedDevice)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.resetToMain()
    }
    
    fileprivate func getDataSourceItems() -> DataSourceItems {
        if SocketManager.isDeviceConnected(selectedDevice) {
            var sections: [Sections: [Identifiers]] = [:]
            let deviceParameters = connectedDevice?.parameters
            
            var section0: [Identifiers] = []
            if let _ = deviceParameters?.language {
                section0.append(.language)
            }
            if let _ = deviceParameters?.units {
                section0.append(.units)
            }
            if let _ = deviceParameters?.dateTime {
                section0.append(.timeAndDate)
                section0.append(.setTime)
                section0.append(.setDate)
            }
            if let _ = deviceParameters?.timeFormat {
                section0.append(.timeFormat)
            }
            
            var section1: [Identifiers] = []
            if let _ = deviceParameters?.autoOff {
                section1.append(.autoOff)
            }
            if let _ = deviceParameters?.blockage {
                section1.append(.goafGun)
            }
            if let _ = deviceParameters?.preset {
                section1.append(.profile)
            }
            
            var section2: [Identifiers] = []
            if let _ = deviceParameters?.calibration {
                section2.append(.calibration)
            }
            
            var section3: [Identifiers] = []
            if let _ = deviceParameters?.format {
                section3.append(.memoryFormatting)
            }
            
            let section4: [Identifiers] = [.deleteDevice]
            
            // TODO: May be use filter instead?
            let mappedSections = [section0, section1, section2, section3, section4].compactMap () {
                return $0.count > 0 ? $0 : nil
            }
            for (index, value) in mappedSections.enumerated() {
                sections[Sections(rawValue: index)!] = value
            }
            return sections
        }
        
        return [ .section0 : [.deleteDevice] ]
    }
    
    fileprivate func fetchDeviceParameters() {
        if SocketManager.isConnected {
            SVProgressHUD.show()
            SocketManager.send(.getParams).response { [weak self] model, error in
                SVProgressHUD.dismiss(withDelay: 1)
                guard error == nil else {
                    return
                }
                let model = self?.connectedDevice!.parameters
                self?.updateParamsBy(model)
            }
        } else {
            isDeviceConnected = false
        }
    }
    
    private func updateParamsBy(_ model: VirtualParameters?) {
        if let language = model?.language?.value {
            switch Int(language) {
            case 0:
                selectedLanguage = .English
            case 1:
                selectedLanguage = .German
            case 2:
                selectedLanguage = .Spanish
            case 3:
                selectedLanguage = .French
            case 4:
                selectedLanguage = .Russian
            default:
                break
            }
        }
        if let units = model?.units?.value {
            switch Int(units) {
            case 0:
                selectedUnits = .Metrs
            case 1:
                selectedUnits = .Yards
            default: break
            }
        }
        if let shutterModel = model?.calibration?.value {
            switch Int(shutterModel) {
            case 0:
                selectedShutterMode = .Manual
            case 1:
                selectedShutterMode = .Hybrid
            case 2:
                selectedShutterMode = .Auto
            default: break
            }
        }
        if let autoOff = model?.autoOff?.value {
            switch Int(autoOff) {
            case 0: selectedAutoOff = .min1
            case 1: selectedAutoOff = .min3
            case 2: selectedAutoOff = .min5
            case 3: selectedAutoOff = .off
            default: break
            }
        }
        if let profile = model?.preset?.value {
            switch Int(profile) {
            case 0: selectedProfileMode = .A
            case 1: selectedProfileMode = .B
            case 2: selectedProfileMode = .C
            default: break
            }
        }
        if let timeFormat = model?.timeFormat?.value {
            selectedTimeFormat = !NSNumber(value: timeFormat).boolValue
        }
        if let blockage = model?.blockage?.value {
            selectedGoafGun = NSNumber(value: blockage).boolValue
        }
        
        isDeviceConnected = true
    }

    // MARK: Notification
    internal func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        DispatchQueue.main.async { [weak self] in
            switch notification {
            case .DeviceConnected:
                self?.updateConnectionStatus()
            case .DeviceDisconnected:
                self?.updateConnectionStatus()
            case .DeviceAccessUpdated:
                _ = self?.navigationController?.popViewController(animated: true)
            case .DeviceParamsUpdated:
                let model = self?.connectedDevice?.parameters
                self?.updateParamsBy(model)
                self?.tableView.reloadData()
            default: break
            }
        }
    }

    // MARK: Update view

    fileprivate func updateDeviceTime(time: Date) {
        let dateTime = Date(timeIntervalSince1970: TimeInterval(connectedDevice!.parameters.dateTime!.value))
        let calendar = Calendar.current
        
        let timeComponents = calendar.dateComponents([.hour, .minute], from: time)
        var dateComponents = calendar.dateComponents([.year, .month, .day], from: dateTime)
        dateComponents.hour = timeComponents.hour
        dateComponents.minute = timeComponents.minute
        let newDate = calendar.date(from: dateComponents)!
        connectedDevice?.parameters.dateTime?.value = Float(newDate.timeIntervalSince1970)
        SocketManager.send(.setDateTime, params: ["newValue": dateTimeFormatter.string(from: newDate)])
    }
    
    fileprivate func updateDeviceDate(date: Date) {
        let dateTime = Date(timeIntervalSince1970: TimeInterval(connectedDevice!.parameters.dateTime!.value))
        let calendar = Calendar.current
        
        var timeComponents = calendar.dateComponents([.hour, .minute], from: dateTime)
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: date)
        timeComponents.year = dateComponents.year
        timeComponents.month = dateComponents.month
        timeComponents.day = dateComponents.day
        let newDate = calendar.date(from: timeComponents)!
        connectedDevice?.parameters.dateTime?.value = Float(newDate.timeIntervalSince1970)
        SocketManager.send(.setDateTime, params: ["newValue": dateTimeFormatter.string(from: newDate)])
    }
    
    fileprivate func setDeviceLanguage(language: MigrationService.DeviceLanguage) {
        let value: Int
        switch language {
        case .English:
            value = 0
        case .German:
            value = 1
        case .Spanish:
            value = 2
        case .French:
            value = 3
        case .Russian:
            value = 4
        }
        connectedDevice?.parameters.language?.value = Float(value)
        _ = SocketManager.send(.setLanguage, params: ["newValue": "\(value)"])
    }

    fileprivate func setDeviceUnits(units: MigrationService.DeviceUnits) {
        let value: Int
        switch units {
        case .Metrs:
            value = 0
        case .Yards:
            value = 1
        }
        connectedDevice?.parameters.units?.value = Float(value)
        _ = SocketManager.send(.setUnits, params: ["newValue": "\(value)"])
    }
    
    fileprivate func setDeviceProfile(units: MigrationService.Profile) {
        var profileValue: Int
        switch units {
        case .A:
            profileValue = 0
        case .B:
            profileValue = 1
        case .C:
            profileValue = 2
        case .D:
            profileValue = 3
        case .E:
            profileValue = 4
        }
        connectedDevice?.parameters.preset?.value = Float(profileValue)
        SocketManager.send(.setProfile, params: ["newValue": "\(profileValue)"])
    }

    fileprivate func setDeviceAutoOff(autoOff: MigrationService.DeviceAutoOff) {
        let value: Int
        switch autoOff {
        case .min1: value = 0
        case .min3: value = 1
        case .min5: value = 2
        case .off : value = 3
        }
        connectedDevice?.parameters.autoOff?.value = Float(value)
        SocketManager.send(.setAutoOff, params: ["newValue": "\(value)"])
    }

    fileprivate func setDeviceCalibration(mode: MigrationService.ShutterMode) {
        let value: Int
        switch mode {
        case .Manual:
            value = 0
        case .Hybrid:
            value = 1
        case .Auto:
            value = 2
        }
        connectedDevice?.parameters.calibration?.value = Float(value)
        SocketManager.send(.setModeShutter, params: ["newValue": "\(value)"])
    }

    fileprivate func setPhoneDateToDevice() {
        let date = Date()
        connectedDevice?.parameters.dateTime?.value = Float(date.timeIntervalSince1970)
        SocketManager.send(.setDateTime, params: ["newValue": dateTimeFormatter.string(from: date)])
    }
    
    fileprivate func setGoafGun(value: Bool) {
        connectedDevice?.parameters.blockage?.value = NSNumber(value: value).floatValue
        SocketManager.send(.enableGunAngle, params: ["newValue": "\(NSNumber(value: value).intValue)" ])
    }
    
    fileprivate func setTimeFormat(value: Bool) {
        connectedDevice?.parameters.timeFormat?.value = NSNumber(value: value).floatValue
        SocketManager.send(.setTimeFormat, params: ["newValue": "\(NSNumber(value: !value).intValue)" ])
    }
    
    fileprivate func updateConnectionStatus() {
        DispatchQueue.main.async { [weak self] in
            self!.isDeviceConnected = SocketManager.isDeviceConnected(self!.selectedDevice)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "to select time & date mode":
                let dataArray = MigrationService.DeviceTime.ordered
                setupSettingsListViewController(destenationVC: segue.destination,
                                                title: "Settings.DateTime".localized(),
                                                dataArray: dataArray,
                                                selected: selectedTime.rawValue,
                                                selectionAction: { [unowned self] index in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                    if (index == 0) {
                                                        self.didSelectManualTime()
                                                    } else {
                                                        self.didSelectIphoneTime()
                                                    }
                })
            case "to select calibration":
                let dataArray = connectedDevice?.parameters.calibration?.limits.localizedValues.map { $0 } ?? []
                setupSettingsListViewController(destenationVC: segue.destination,
                                                title: "Settings.ShutterMode".localized(),
                                                dataArray: dataArray,
                                                selected: selectedShutterMode.rawValue,
                                                selectionAction: { [unowned self] index in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                    self.didSelectCalibration(mode:
                                                        MigrationService.ShutterMode(rawValue: dataArray[index])!)
                })
            case "to select language":
                let dataArray = connectedDevice?.parameters.language?.limits.localizedValues.map { $0 } ?? []
                setupSettingsListViewController(destenationVC: segue.destination,
                                                title: "Settings.Language".localized(),
                                                dataArray: dataArray,
                                                selected: selectedLanguage.rawValue,
                                                selectionAction: { [unowned self] index in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                    self.didSelectLanguage(language:
                                                        MigrationService.DeviceLanguage(rawValue: dataArray[index])!)
                })
            case "to select units":
                let dataArray = connectedDevice?.parameters.units?.limits.localizedValues.map { $0 } ?? []
                setupSettingsListViewController(destenationVC: segue.destination,
                                                title:"Settings.Units".localized(),
                                                dataArray: dataArray,
                                                selected: selectedUnits.rawValue,
                                                selectionAction: { [unowned self] index in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                    self.didSelectUnits(mode:
                                                        MigrationService.DeviceUnits(rawValue: dataArray[index])!)
                })
            case "to profile":
                let dataArray = connectedDevice?.parameters.preset?.limits.localizedValues.map { $0 } ?? []
                setupSettingsListViewController(destenationVC: segue.destination,
                                                title:"Viewfinder.Parameters.Preset".localized(),
                                                dataArray: dataArray,
                                                selected: selectedProfileMode.rawValue,
                                                selectionAction: { [unowned self] index in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                    self.didSelectProfile(mode:
                                                        MigrationService.Profile(rawValue: dataArray[index])!)
                })
            case "to select auto off":
                let dataArray = connectedDevice?.parameters.autoOff?.limits.localizedValues.map { $0 } ?? []
                setupSettingsListViewController(destenationVC: segue.destination,
                                                title:"Settings.AutoPowerOff".localized(),
                                                dataArray: dataArray,
                                                selected: selectedAutoOff.rawValue,
                                                selectionAction: { [unowned self] index in
                                                    _ = self.navigationController?.popViewController(animated: true)
                                                    self.didSelectAutoOff(mode:
                                                        MigrationService.DeviceAutoOff(rawValue: dataArray[index])!)
                })

            default: break
            }
        }
    }

    fileprivate func setupSettingsListViewController(destenationVC: UIViewController,
                                                     title: String,
                                                     dataArray: [String],
                                                     selected: String,
                                                     selectionAction: @escaping DeviceSettingListSelectionTableViewController.ViewModel.SelectionAction ) {
        guard let controller = destenationVC as? DeviceSettingListSelectionTableViewController else {
            return
        }
        controller.selected = dataArray.firstIndex(of: selected)
        controller.viewModel = DeviceSettingListSelectionTableViewController.ViewModel(title: title,
                                                                                       dataArray: dataArray,
                                                                                       selectionAction: selectionAction)
    }
}

// MARK: - List Selection
extension DeviceSettingsViewController {
    fileprivate func didSelectIphoneTime() {
        selectedTime = .iPhone
        setPhoneDateToDevice()
        tableView.reloadData()
    }
    
    fileprivate func didSelectManualTime() {
        selectedTime = .Manual
        tableView.reloadData()
    }
    
    fileprivate func didSelectCalibration(mode: MigrationService.ShutterMode) {
        selectedShutterMode = mode
        setDeviceCalibration(mode: mode)
        tableView.reloadData()
    }
    
    fileprivate func didSelectLanguage(language: MigrationService.DeviceLanguage) {
        selectedLanguage = language
        setDeviceLanguage(language: language)
        tableView.reloadData()
    }
    
    fileprivate func didSelectUnits(mode: MigrationService.DeviceUnits) {
        selectedUnits = mode
        setDeviceUnits(units: mode)
        tableView.reloadData()
    }
    fileprivate func didSelectProfile(mode: MigrationService.Profile) {
        selectedProfileMode = mode
        setDeviceProfile(units: mode)
        tableView.reloadData()
    }
    
    fileprivate func didSelectAutoOff(mode: MigrationService.DeviceAutoOff) {
        selectedAutoOff = mode
        setDeviceAutoOff(autoOff: mode)
        tableView.reloadData()
    }
}

// MARK: - table view datasource
extension DeviceSettingsViewController: UITableViewDataSource {
    private struct AssociatedKeys {
        static var formatAction = "formatAction"
    }
    var formatAction: UIAlertAction? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.formatAction) as? UIAlertAction
        }
        set(value) {
            objc_setAssociatedObject(self, &AssociatedKeys.formatAction, value,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceItemsDictionary[ Sections(rawValue: section)! ]!.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceItemsDictionary.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let items = dataSourceItemsDictionary[ Sections(rawValue: indexPath.section)! ]!
        
        switch items[indexPath.row] {
            
        case .language:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.language.rawValue, for: indexPath) as! DeviceSettingsTableViewCell
            cell.viewModel = DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.Language".localized(),
                                                     attributes: nil),
                           subtitle: selectedLanguage.rawValue.localized(),
                           selectionAction: { [unowned self] cell in
                            self.performSegue(withIdentifier: "to select language", sender: cell)
                })
            return cell
            
        case .units:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.units.rawValue, for: indexPath) as! DeviceSettingsTableViewCell
            cell.viewModel = DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.Units".localized(),
                                                     attributes: nil),
                           subtitle: selectedUnits.rawValue.localized(),
                           selectionAction: { [unowned self] cell in
                            self.performSegue(withIdentifier: "to select units", sender: cell)
                })
            return cell
            
        case .profile:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.profile.rawValue, for: indexPath) as! DeviceSettingsTableViewCell
            cell.viewModel = DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Viewfinder.Parameters.Preset".localized(),
                                                     attributes: nil),
                           subtitle: selectedProfileMode.rawValue.localized(),
                           selectionAction: { [unowned self] cell in
                            self.performSegue(withIdentifier: "to profile", sender: cell)
                })
            return cell
            
        case .timeAndDate:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.timeAndDate.rawValue, for: indexPath) as! DeviceSettingsTableViewCell
            cell.viewModel = DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.DateTime".localized(),
                                                     attributes: nil),
                           subtitle: selectedTime.rawValue.localized(),
                           selectionAction: { [unowned self] cell in
                            self.performSegue(withIdentifier: "to select time & date mode", sender: cell)
                })
            return cell
            
        case .setTime:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.setTime.rawValue, for: indexPath) as! DeviceSettingsTableViewCell
            cell.viewModel = DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.DateTime.TimeSet".localized(),
                                                     attributes: nil),
                           subtitle: timeFormatter.string(from: connectedDevice!.parameters.dateTime!.dateValue),
                           selectionAction: { [unowned self] cell in
                            let indexPath = self.tableView.indexPath(for: cell)!
                            let itemsSection = Sections(rawValue: indexPath.section)!
                            
                            if let indexTimePicker = self.dataSourceItemsDictionary[itemsSection]?.firstIndex(of: .setTimePicker) {
                                self.dataSourceItemsDictionary[itemsSection]?.remove(at: indexTimePicker)
                                
                                self.removePickerCellAtIndexPath(indexPath: indexPath)
                            } else {
                                let index = self.dataSourceItemsDictionary[itemsSection]?.firstIndex() {
                                    $0 == .setTime
                                }
                                self.dataSourceItemsDictionary[itemsSection]?.insert(.setTimePicker, at: index! + 1)
                                
                                self.insertPickerCellAtIndexPath(indexPath: indexPath)
                            }
                })
            return cell
            
        case .setTimePicker:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.setTimePicker.rawValue, for: indexPath) as! DatePickerTableViewCell
            cell.datePicker.date = connectedDevice!.parameters.dateTime!.dateValue
            cell.datePicker.addTarget(self, action: #selector(didChangeTime(_:)), for: .valueChanged)
            return cell
            
        case .setDate:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.setDate.rawValue, for: indexPath) as! DeviceSettingsTableViewCell
            cell.viewModel = DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.DateTime.DateSet".localized(),
                                                     attributes: nil),
                           subtitle: dateFormatter.string(from: connectedDevice!.parameters.dateTime!.dateValue),
                           selectionAction: { [unowned self] cell in
                            let indexPath = self.tableView.indexPath(for: cell)!
                            let itemsSection = Sections(rawValue: indexPath.section)!
                            
                            if let indexTimePicker = self.dataSourceItemsDictionary[itemsSection]?.firstIndex(of: .setDatePicker) {
                                self.dataSourceItemsDictionary[itemsSection]?.remove(at: indexTimePicker)
                                
                                self.removePickerCellAtIndexPath(indexPath: indexPath)
                            } else {
                                let index = self.dataSourceItemsDictionary[itemsSection]?.firstIndex() {
                                    $0 == .setDate
                                }
                                self.dataSourceItemsDictionary[itemsSection]?.insert(.setDatePicker, at: index! + 1)
                                
                                self.insertPickerCellAtIndexPath(indexPath: indexPath)
                            }
                })
            return cell
            
        case .setDatePicker:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.setDatePicker.rawValue, for: indexPath) as! DatePickerTableViewCell
            cell.datePicker.date = connectedDevice!.parameters.dateTime!.dateValue
            cell.datePicker.addTarget(self, action: #selector(didChangeDate(_:)), for: .valueChanged)
            return cell
            
        case .autoOff:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.autoOff.rawValue, for: indexPath) as! DeviceSettingsTableViewCell
            cell.viewModel = DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.AutoPowerOff".localized(),
                                                     attributes: nil),
                           subtitle: selectedAutoOff.rawValue.localized(),
                           selectionAction: { [unowned self] cell in
                            self.performSegue(withIdentifier: "to select auto off", sender: cell)
                })
            return cell
            
        case .memoryFormatting:
            return dequeFormatCell(for: indexPath)
            
        case .goafGun:
            let cell: DeviceSettingsSwitchTableViewCell = tableView.dequeueReusableCell(withIdentifier: Identifiers.goafGun.rawValue,
                                                                                        for: indexPath) as! DeviceSettingsSwitchTableViewCell
            cell.selectionStyle = .none
            cell.switchComponent.isOn = selectedGoafGun
            cell.viewModel =
                DeviceSettingsSwitchTableViewCell
                    .ViewModel(title: NSAttributedString(string: "Settings.Blockage".localized(),
                                                         attributes: nil),
                               selectionAction: { [unowned self] cell in
                                let cell = cell as! DeviceSettingsSwitchTableViewCell
                                self.setGoafGun( value: cell.switchComponent.isOn )
                    })
            return cell;
            
        case .timeFormat:
            let cell: DeviceSettingsSwitchTableViewCell = tableView.dequeueReusableCell(withIdentifier: Identifiers.timeFormat.rawValue,
                                                                                        for: indexPath) as! DeviceSettingsSwitchTableViewCell
            cell.selectionStyle = .none
            cell.switchComponent.isOn = selectedTimeFormat
            cell.viewModel =
                DeviceSettingsSwitchTableViewCell
                    .ViewModel(title: NSAttributedString(string: "Settings.DateTime.TimeFormat.UseFormat24".localized(),
                                                         attributes: nil),
                               selectionAction: { [unowned self] cell in
                                let cell = cell as! DeviceSettingsSwitchTableViewCell
                                self.setTimeFormat( value: cell.switchComponent.isOn )
                    })
            return cell;
            
        case .deleteDevice:
            return dequeDeleteDeviceCell(for: indexPath)
            
        case .calibration:
            return dequeCalibrationCell(for: indexPath)
        }
    }
    
    fileprivate func insertPickerCellAtIndexPath(indexPath: IndexPath) {
        self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        let indexPathToChange = IndexPath(row: indexPath.row + 1, section: indexPath.section)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [indexPathToChange], with: .fade)
        self.tableView.endUpdates()
    }
    
    fileprivate func removePickerCellAtIndexPath(indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        let indexPathToChange = IndexPath(row: indexPath.row + 1, section: indexPath.section)
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPathToChange], with: .fade)
        self.tableView.endUpdates()
    }
    
    private func dequeDeleteDeviceCell(for indexPath: IndexPath) -> UITableViewCell {
        let cell: DeviceSettingsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "delete device",
                                                                              for: indexPath) as! DeviceSettingsTableViewCell
        cell.viewModel =
            DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.DeleteDevice".localized(),
                                                     attributes: nil),
                           subtitle: nil,
                           selectionAction: { [unowned self] cell in
                            let indexPath = self.tableView.indexPath(for: cell)
                            self.tableView.deselectRow(at: indexPath!, animated: true)
                            if (SocketManager.isDeviceConnected(self.selectedDevice)) {
                                self.showConnectedWarningAlertController()
                            } else {
                                self.showDeleteAlertController()
                            }
                })
        return cell;
    }

    private func dequeFormatCell(for indexPath: IndexPath) -> UITableViewCell {
        let cell: DeviceSettingsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "memory formatting",
                                                                              for: indexPath) as! DeviceSettingsTableViewCell
        cell.viewModel =
            DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.MemoryFormatting".localized(),
                                                     attributes: nil),
                           subtitle: nil,
                           selectionAction: { [unowned self] cell in
                            let indexPath = self.tableView.indexPath(for: cell)
                            self.tableView.deselectRow(at: indexPath!, animated: true)
                            self.showFormatAlertController()
                })
        return cell;
    }

    private func dequeCalibrationCell(for indexPath: IndexPath) -> UITableViewCell {
        let cell: DeviceSettingsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "calibration",
                                                                              for: indexPath) as! DeviceSettingsTableViewCell
        cell.viewModel =
            DeviceSettingsTableViewCell
                .ViewModel(title: NSAttributedString(string: "Settings.ShutterMode".localized(),
                                                     attributes: nil),
                           subtitle: self.selectedShutterMode.rawValue.localized(),
                           selectionAction: { [unowned self] cell in
                            self.performSegue(withIdentifier: "to select calibration", sender: cell)
                })
        return cell;
    }
    
    private func showConnectedWarningAlertController() {
        let alertController = UIAlertController(title: nil,
                                                message: "Settings.DeleteDevice.NeedDisconnection".localized(),
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(), style: .default, handler: nil))
        present(alertController, animated: true)
    }
    
    private func showDeleteAlertController() {
        let alertController = UIAlertController(title: "Settings.DeleteDevice.ConfirmationDialog.Title".localized(),
                                                message: "Settings.DeleteDevice.ConfirmationDialog.Message".localized(),
                                                preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Settings.DeleteDevice.ConfirmationDialog.Delete".localized(),
                                         style: .destructive,
                                         handler: { [weak self] _ in self?.performDeleteDeviceAction() })
        alertController.addAction(deleteAction)
        alertController.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(), style: .default, handler: nil))
        present(alertController, animated: true)
    }
    
    private func showFormatAlertController() {
        let alertController = UIAlertController(title: "Settings.MemoryFormatting.Dialog.Title".localized(),
                                                message: "Settings.MemoryFormatting.Dialog.Message".localized(),
                                                preferredStyle: .alert)
        formatAction = UIAlertAction(title: "Settings.MemoryFormatting.Dialog.Format".localized(),
                                     style: .destructive,
                                     handler: { [weak self] _ in self?.performFormatAction() })
        formatAction!.isEnabled = false
        alertController.addTextField { [weak self] textField in
            textField.placeholder = "Settings.MemoryFormatting.Dialog.ProtectionMessage".localized()
            textField.autocapitalizationType = .allCharacters
            textField.addTarget(self, action: #selector(self!.didUpdateFormatText(_:)), for: .editingChanged)
        }
        alertController.addAction(formatAction!)
        alertController.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(), style: .default, handler: nil))
        present(alertController, animated: true)
    }
    
    @objc fileprivate func didUpdateFormatText(_ textField: UITextField) {
        formatAction?.isEnabled = (textField.text == "FORMAT")
    }
    
    @objc fileprivate func didChangeTime(_ picker: UIDatePicker) {
        let date = picker.date
        let row = dataSourceItemsDictionary[.section0]?.firstIndex(of: .setTimePicker)
        
        if let timeCell = tableView.cellForRow(at: IndexPath(row: row! - 1, section: 0)) {
            timeCell.detailTextLabel?.text = timeFormatter.string(from: date)
            updateDeviceTime(time: date)
        }
    }
    
    @objc fileprivate func didChangeDate(_ picker: UIDatePicker) {
        let date = picker.date
        let row = dataSourceItemsDictionary[.section0]?.firstIndex(of: .setDatePicker)
        
        if let dateCell = tableView.cellForRow(at: IndexPath(row: row! - 1, section: 0)) {
            dateCell.detailTextLabel?.text = dateFormatter.string(from: date)
            updateDeviceDate(date: date)
        }
    }
}

extension DeviceSettingsViewController: UITableViewDelegate {
}
