//
//  ApiGroupAdmin.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/19/19.
//  Copyright © 2019 Ciklum. All rights reserved.
//

struct ApiBaseUser: InitializableWithData, InitializableWithJson {
    var id: Int
    var username: String
    var email: String?
    
    init(data: Data?) throws {
        // Here you can parse the JSON or XML using the build in APIs or your favorite libraries
        guard let data = data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data),
            let json = jsonObject as? [String: Any] else {
                throw NSError.createPraseError()
        }
        
        try self.init(json: json)
    }
    
    init(json: [String : Any]) throws {
        guard let id = json["id"] as? Int,
              let username = json["username"] as? String
            else {
                throw NSError.createPraseError()
        }
        self.id = id
        self.username = username
        self.email = json["email"] as? String
    }
}

extension ApiBaseUser {
    var admin: GPSGroupAdmin {
        return GPSGroupAdmin(id: id,
                             username: username,
                             email: email)
    }
}
