//
//  FileManagerFileCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/10/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import RPCircularProgress

class FileManagerFileCell: MGSwipeTableCell {
    // Types
    typealias SelectionCallback = ((_ section: Int, _ row: Int, _ selected: Bool) -> Bool)?
    typealias ActionCallback = ((_ section: Int, _ row: Int) -> Void)?
    typealias ProgressCallback = ((_ progress: Float) -> Void)?
    
    // Outlets
    @IBOutlet weak var fileTypeImage: UIImageView!
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var fileInfoLabel: UILabel!
    @IBOutlet weak var fileActionButton: UIButton!
    @IBOutlet weak var fileDownloadButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var convertLabel: UILabel!
    @IBOutlet weak var infinityProgressView: RPCircularProgress!
    

    // Variables
    var isChecked: Bool = false {
        didSet {
            selectButton.isSelected = isChecked
        }
    }
    var isChekable: Bool = false
    var isDownloading = false
    weak var file: FileModel!
    var section: Int!
    var row: Int!
    var cellHash: String!

    var selectionCallback: SelectionCallback = nil
    var downloadingCallback: ActionCallback = nil
    var detailsCallback: ActionCallback = nil
    var shareCallback: ActionCallback = nil
    var showCallback: ActionCallback = nil
    var cancelCallback: ActionCallback = nil
    var progressCallback: ProgressCallback = nil
    
    var convertingProgressCallback: ((Double) -> Void)!

    fileprivate var fileType: FileType = .video

    override func awakeFromNib() {
        super.awakeFromNib()
        localizeView()
        
        fileActionButton.titleLabel?.adjustsFontSizeToFitWidth = true
        fileActionButton.titleLabel?.minimumScaleFactor = 0.5
        fileActionButton.titleLabel?.lineBreakMode = .byClipping
        
        fileDownloadButton.titleLabel?.adjustsFontSizeToFitWidth = true
        fileDownloadButton.titleLabel?.minimumScaleFactor = 0.5
        fileDownloadButton.titleLabel?.lineBreakMode = .byClipping
        
        cancelButton.titleLabel?.lineBreakMode = .byClipping
        cancelButton.titleLabel?.minimumScaleFactor = 0.5
        cancelButton.titleLabel?.lineBreakMode = .byClipping
        
        fileDownloadButton.layer.cornerRadius = 2
        fileDownloadButton.layer.borderWidth = 1
        fileActionButton.layer.cornerRadius = 2
        fileDownloadButton.layer.borderColor = UIColor.white.cgColor
        
        progressCallback = { progress in
            self.progressView.progress = progress
        }
        
        convertingProgressCallback = { progress in
//            self.infinityProgressView.updateProgress(CGFloat(progress))
        }
    }
    
    private func localizeView() {
        fileDownloadButton.setTitle("FileManager.Actions.Download".localized(), for: .normal)
    }

    func updateView(with type: FileType,
                    isLocal: Bool,
                    isDownloading: Bool,
                    isConverting: Bool) {
        shareButton.isHidden = !isLocal || isConverting || isChekable || isDownloading
        
        guard !isDownloading else {
            fileTypeImage.image = UIImage(named: "filemanager_downloading")
            return
        }
        fileType = type

        switch (type, isLocal) {
        case (.video, true):
            fileTypeImage.image = UIImage(named: "filemanager_icon_on_device_video")
        case (.image, true):
            fileTypeImage.image = UIImage(named: "filemanager_icon_on_device_photo")
        case (.video, false):
            fileTypeImage.image = UIImage(named: "filemanager_icon_download_video")
        case (.image, false):
            fileTypeImage.image = UIImage(named: "filemanager_icon_download_photo")
        case (.converted, _):
            fileTypeImage.image = UIImage(named: "filemanager_mp4")
        }
    }

    fileprivate func setupCheckableView(_ isChekable: Bool) {
        fileActionButton.isHidden = isChekable
        fileDownloadButton.isHidden = isChekable
        selectButton.isHidden = !isChekable
    }

    // MARK: - Actions
    
    @IBAction func checkFolderAction() {
        isChecked = selectionCallback?(section, row, isChecked) ?? false
    }

    @IBAction func showFileAction() {
        showCallback?(section, row)
    }

    @IBAction func downloadFileAction() {
        downloadingCallback?(section, row)
    }

    @IBAction func shareAction() {
        shareCallback?(section, row)
    }
    
    @IBAction func cancelAction() {
        cancelCallback?(section, row)
    }

}

extension FileManagerFileCell {
    func setup(with file: FileModel,
               isChekable: Bool,
               isChecked: Bool,
               isDownloading: Bool,
               isConverting: Bool,
               section: Int,
               row: Int,
               selectionCallback: SelectionCallback,
               downloadingCallback: ActionCallback,
               detailsCallback: ActionCallback,
               showCallback: ActionCallback,
               cancellCallback: ActionCallback,
               shareCallback: ActionCallback) {
        self.fileNameLabel.text = file.name
        let itemDate = file.date
        let itemSize = file.size
        self.fileInfoLabel.text = itemDate.shortMonthString() + " " + itemDate.dayString() + " | " + itemSize.toStringUniversalMemoryUnit()
        self.fileActionButton.setTitle((file.fileType == .video || file.fileType == .converted ? "FileManager.Item.ShowVideo" : "FileManager.Item.ShowPhoto").localized(), for: .normal)
        self.cancelButton.setTitle("FileManager.Item.Cancel".localized(), for: .normal)
        self.progressLabel.text = "FileManager.ManageDownloads.In.Queue".localized()
        
        self.isChecked = isChecked
        self.isChekable = isChekable
        self.section = section
        self.row = row
        
        self.selectionCallback = selectionCallback
        self.downloadingCallback = downloadingCallback
        self.detailsCallback = detailsCallback
        self.showCallback = showCallback
        self.cancelCallback = cancellCallback
        self.shareCallback = shareCallback
        
        self.selectButton.isHidden = !isChekable
        self.fileActionButton.isHidden = isDownloading || isChekable || !file.isLocal
        self.fileDownloadButton.isHidden = isDownloading || isChekable || file.isLocal
        self.progressView.isHidden = !isDownloading
        self.cancelButton.isHidden = !isDownloading
        self.progressLabel.isHidden = !isDownloading
        self.progressView.progress = 0
        self.infinityProgressView.enableIndeterminate()
        
        self.infinityProgressView.isHidden = !isConverting
        self.convertLabel.isHidden = !isConverting
        
        self.file = file
    
        updateView(with: file.fileType,
                   isLocal: file.isLocal,
                   isDownloading: isDownloading,
                   isConverting: isConverting)
    }
    
}
