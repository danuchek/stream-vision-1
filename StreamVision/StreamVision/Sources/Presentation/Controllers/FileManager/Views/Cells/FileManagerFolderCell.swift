//
//  FileManagerFolderCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/10/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import SLExpandableTableView

@objc class FileManagerFolderCell: MGSwipeTableCell, UIExpandingTableViewCell {
    // Types
    typealias SelectionCallback = (_ section: Int, _ selected: Bool) -> Bool
    
    // Outlets
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var selectButtonConstraint: NSLayoutConstraint!

    // Variables
    var isLoading: Bool = false
    var expansionStyle: UIExpansionStyle = UIExpansionStyleCollapsed
    
    var isChecked: Bool = false {
        didSet {
            selectButton.isSelected = isChecked
        }
    }
    var isChekable: Bool = false {
        didSet {
            self.selectButtonConstraint.constant = isChekable ? 0 : -60
            self.setNeedsUpdateConstraints()
            self.layoutIfNeeded()
        }
    }
    var selectionCallback: SelectionCallback?
    var section: Int!

    // Outlets
    @IBOutlet fileprivate weak var rowImageView: UIImageView!
    @IBOutlet weak var folderNameLabel: UILabel!
    @IBOutlet weak var folderCountLabel: UILabel!

    func setExpansionStyle(_ style: UIExpansionStyle, animated: Bool) {
        if style.rawValue == 0 {
            rowImageView.image = UIImage(named: "filemanager_row_left")
        } else {
            rowImageView.image = UIImage(named: "filemanager_row_up")
        }
    }

    @IBAction func checkFolderAction() {
        isChecked = selectionCallback?(section, isChecked) ?? false
    }

}

extension FileManagerFolderCell {
    func setup(with folder: FolderModel,
               isChekable: Bool,
               isChecked: Bool,
               section: Int , callback: @escaping SelectionCallback) {
        folderCountLabel.text = folder.name.getStringNumbers() ?? folder.name
        let itemDate = folder.date
        let filesCount = folder.files.count
        if filesCount == 1 {
            let fileDate = folder.files.first!.date
            folderNameLabel.text = fileDate.monthString() + " " + fileDate.dayString()
        } else if filesCount > 1 {
            let firstDate = folder.files.first!.date
            let lastDate = folder.files.last!.date
            folderNameLabel.text = firstDate.monthString() + " " + firstDate.dayString() + " - " + lastDate.monthString() + " " + lastDate.dayString()
        } else {
            folderNameLabel.text = itemDate.monthString() + " " + itemDate.dayString()
        }
        folderNameLabel.text = "\(folderNameLabel.text!) \(String(format: "FileManager.Item.FilesCount.Many".localized(), folder.files.count))"
        
        self.isChecked = isChecked
        self.isChekable = isChekable
        self.selectButton.isHidden = !isChekable
        self.section = section
        self.selectionCallback = callback
    }
}
