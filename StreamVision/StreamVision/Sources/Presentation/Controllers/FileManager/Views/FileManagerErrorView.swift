//
//  GoogleSignOutView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import TYMProgressBarView

class FileManagerErrorView: UIView {
    fileprivate var view: UIView!

    // Outlets
    @IBOutlet fileprivate weak var sepparateView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet fileprivate weak var sepparatorLayout: NSLayoutConstraint!

    // Variables
    var shown: Bool = false

    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)

        xibSetup(with: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }

}
