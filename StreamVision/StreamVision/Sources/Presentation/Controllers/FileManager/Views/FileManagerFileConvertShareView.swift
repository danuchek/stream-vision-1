//
//  FileManagerFileConvertShareAlertView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/27/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit

class FileManagerFileConvertShareView: CustomAlertView {
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var okButton: ButtonWithActionBlock!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var note1Label: UILabel!
    @IBOutlet var note2Label: UILabel!
    
    override func setupView() {
        localizeView()
    }
    
    fileprivate func localizeView() {
        okButton.setTitle("FileManager.ConvertAlert.ConvertAndShare.Button".localized(), for: .normal)
        cancelButton.setTitle("General.Alert.Cancel".localized(), for: .normal)
        
        titleLabel.text = "FileManager.ConvertAlert.ConvertAndShare.Title".localized()
        note1Label.text = "FileManager.ConvertAlert.ConvertAndShare.Leave.Warning".localized()
        note2Label.text = "FileManager.ConvertAlert.ConvertAndShare.Power.Warning".localized()
        
        let style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        style.headIndent = 52
        let attributedString = NSAttributedString(string: "FileManager.ConvertAlert.ConvertAndShare.Message".localized(), attributes: [.paragraphStyle: style])
        
        subtitleLabel.attributedText = attributedString
        
        okButton.titleLabel?.adjustsFontSizeToFitWidth = true
        okButton.titleLabel?.minimumScaleFactor = 0.5
        okButton.titleLabel?.lineBreakMode = .byClipping
    }
}
