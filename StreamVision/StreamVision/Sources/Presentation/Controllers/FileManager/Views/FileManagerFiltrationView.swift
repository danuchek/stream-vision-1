//
//  FileManagerFiltrationView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/31/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class FileManagerFiltrationView: UIView {
    fileprivate var view: UIView!
    
    @IBOutlet fileprivate var photosSwitch: UISwitch!
    @IBOutlet fileprivate var videosSwitch: UISwitch!
    @IBOutlet fileprivate var phoneSwitch: UISwitch!
    @IBOutlet fileprivate var deviceSwitch: UISwitch!
    @IBOutlet fileprivate var photosLabel: UILabel!
    @IBOutlet fileprivate var videosLabel: UILabel!
    @IBOutlet fileprivate var onPhoneLabel: UILabel!
    @IBOutlet fileprivate var onDeviceLabel: UILabel!
    @IBOutlet fileprivate var sectionFileTypeLabel: UILabel!
    @IBOutlet fileprivate var sectionFileLocationLabel: UILabel!
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var okButton: ButtonWithActionBlock!
    
    var filtration: (FilesDataSource.FileLocation, FilesDataSource.FileFilter) {
        get {
            var fileLocation: FilesDataSource.FileLocation = .mixed
            var filtrationExtensions : FilesDataSource.FileFilter = .mixed

            if phoneSwitch.isOn {
                fileLocation = .local
            }
            if deviceSwitch.isOn {
                fileLocation = .remote
            }
            if phoneSwitch.isOn && deviceSwitch.isOn {
                fileLocation = .mixed
            }

            if photosSwitch.isOn {
                filtrationExtensions = .photo
            }
            if videosSwitch.isOn {
                filtrationExtensions = .video
            }
            if photosSwitch.isOn && videosSwitch.isOn {
                filtrationExtensions = .mixed
            }

            return (fileLocation, filtrationExtensions)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(with: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0 , y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        
        okButton.isAutoSelect = false
        localizeView()
    }
    
    func updateView(filtration: (FilesDataSource.FileLocation, FilesDataSource.FileFilter) ) {

        switch filtration.0 {
        case .remote:
            phoneSwitch.isOn  = false
            deviceSwitch.isOn = true
        case .local:
            phoneSwitch.isOn  = true
            deviceSwitch.isOn = false
        case .mixed:
            phoneSwitch.isOn  = true
            deviceSwitch.isOn = true
        case .none:
            phoneSwitch.isOn  = false
            deviceSwitch.isOn = false
        }

        switch filtration.1 {
        case .photo:
            photosSwitch.isOn  = true
            videosSwitch.isOn = false
        case .video:
            photosSwitch.isOn  = false
            videosSwitch.isOn = true
        case .mixed:
            photosSwitch.isOn  = true
            videosSwitch.isOn = true
        case .none:
            photosSwitch.isOn  = false
            videosSwitch.isOn = false
        }

    }
    
    fileprivate func localizeView() {
        videosLabel.text = "FileManager.Filter.Video".localized()
        photosLabel.text = "FileManager.Filter.Image".localized()
        onPhoneLabel.text = "FileManager.Filter.OnPhone".localized()
        onDeviceLabel.text = "FileManager.Filter.OnDevice".localized()
        sectionFileTypeLabel.text = "FileManager.Filter.FileLocation.Title".localized().uppercased()
        sectionFileLocationLabel.text = "FileManager.Filter.FilePlace.Title".localized().uppercased()
        
        okButton.setTitle("General.Alert.Ok".localized(), for: .normal)
        cancelButton.setTitle("General.Alert.Cancel".localized(), for: .normal)
    }

}
