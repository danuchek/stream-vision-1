//
//  GoogleSignOutView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import TYMProgressBarView

class FileManagerFileDetailsView: UIView {
    fileprivate var view: UIView!

    // Outlets
    @IBOutlet fileprivate weak var sepparateView: UIView!
    @IBOutlet weak var fileTypeImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!

    @IBOutlet fileprivate weak var shareButtonConstaint: NSLayoutConstraint!

    // Variables
    var shown: Bool = false

    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(with: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0 , y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    fileprivate func localizeView() {
        
    }

    func updateViewFromFile(_ file: FileModel) {
        titleLabel.text = file.name
        let spaceChar = " "
        dateLabel.text = "FileManager.Info.Created".localized() + spaceChar + file.date.fullDateString()
        sizeLabel.text = "FileManager.Info.Size".localized() + spaceChar + file.size.toStringUniversalMemoryUnit()
        let fileType = file.fileType
        fileTypeImageView.image = (fileType == .video) ? UIImage(named: "filemanager_video-1") : UIImage(named: "filemanager_photo")
    }

}
