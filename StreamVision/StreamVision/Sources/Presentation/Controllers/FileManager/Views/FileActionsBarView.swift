//
//  FileActionsBarView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/27/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class FileActionsBarView: UIView {
    @IBOutlet weak var deleteMultiplyButton: UIButton!
    @IBOutlet weak var downloadMultiplyButton: UIButton!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var deselectAllButton: UIButton!
    @IBOutlet weak var downloadMultiplyConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        deleteMultiplyButton.setTitle("FileManager.Actions.DeleteMultiple".localized(), for: .normal)
        downloadMultiplyButton.setTitle("FileManager.Actions.DownloadMultiple".localized(), for: .normal)
        selectAllButton.setTitle("FileManager.EditBar.SelectAll".localized(), for: .normal)
        deselectAllButton.setTitle("FileManager.EditBar.DeselectAll".localized(), for: .normal)
        
        deleteMultiplyButton.titleLabel?.adjustsFontSizeToFitWidth = true
        deleteMultiplyButton.titleLabel?.minimumScaleFactor = 0.5
        deleteMultiplyButton.titleLabel?.lineBreakMode = .byClipping
        
        selectAllButton.titleLabel?.adjustsFontSizeToFitWidth = true
        selectAllButton.titleLabel?.minimumScaleFactor = 0.5
        selectAllButton.titleLabel?.lineBreakMode = .byClipping
        
        deselectAllButton.titleLabel?.adjustsFontSizeToFitWidth = true
        deselectAllButton.titleLabel?.minimumScaleFactor = 0.5
        deselectAllButton.titleLabel?.lineBreakMode = .byClipping
    }

    func setupLocalFileActions(_ local : Bool) {
        if local {
            downloadMultiplyButton.isHidden = true
            downloadMultiplyConstraint.constant = 0
        } else {
            downloadMultiplyButton.isHidden = false
            downloadMultiplyConstraint.constant = 165
        }
    }
    
    func updateWithSelections(_ count: Int) {
        let filesSelected = true//count > 0
        downloadMultiplyButton.isEnabled = filesSelected
        deleteMultiplyButton.isEnabled = filesSelected
        var color = UIColor.gray
        if filesSelected {
            color = UIColor.white
        }
        deleteMultiplyButton.layer.setBorderColorFromUIColor(color)
        deleteMultiplyButton.setTitleColor(color, for: .normal)
        downloadMultiplyButton.layer.setBorderColorFromUIColor(color)
        downloadMultiplyButton.setTitleColor(color, for: .normal)
    }
}
