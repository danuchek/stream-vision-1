//
//  DeviceFilesViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/8/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import SLExpandableTableView
import SVProgressHUD
import STZPopupView
import FileKit
import UserNotifications

extension DeviceFilesViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert, .badge, .sound])
    }
    
    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        switch response.actionIdentifier {
        case appDelegate.laterActionIdentifier: break
        case appDelegate.shareActionIdentifier:
            if let path = response.notification.request.content.userInfo["filePath"] as? String {
                share(path: path)
            }
        default:
            break
        }
        completionHandler()
    }
}

extension DeviceFilesViewController: StoreManager {}
class DeviceFilesViewController: UIViewController,
                                 SocketConnectionSubsriber,
                                 ViewControllerWithDeviceProtocol,
                                 UIPopoverPresentationControllerDelegate {

    // Outlets
    @IBOutlet weak var tableView: SLExpandableTableView!
    @IBOutlet weak var editTableToolsViewConstarint: NSLayoutConstraint!
    @IBOutlet weak var editTableToolsView: FileActionsBarView!
    @IBOutlet weak var filtrationButtonItem: UIBarButtonItem!
    @IBOutlet weak var editFilesButtonItem: UIBarButtonItem!
    @IBOutlet fileprivate weak var dropDevicesListButton: UIButton!

    var deviceModel: VirtualDevice!
    var tableViewDataSource: FilesDataSource!
    
    // TODO: Remove hack and rewrite on safeInsets
    // Constants
    // 21 inset for iPhone X
    let insetConstant: CGFloat = 44 + 21

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize variables
        if SocketManager.isConnected {
            deviceModel = SocketManager.connectedDevice
        } else {
            deviceModel = storeManager.devices.first
        }
        
        guard let _ = deviceModel else {
            navigateToMain(animated: false)
            return
        }
        
        if let revealController = self.revealViewController() {
            let rearViewController = revealController.rearViewController!
            (rearViewController as! SideTableViewController).selectedScreen = .files
        }
        setupView()
        
        self.dropDevicesListButton.setTitle(self.deviceModel.info.name, for: .normal)
        navigationController?.setNavigationBarHidden(false, animated: true)

        let menu = MenuButton()
        menu.addTarget(self, action: #selector(openSideMenu), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)

        localizeView()
        UNUserNotificationCenter.current().delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        googleReport(action: "FilesManager_open")
        SocketManager.subscribe(self, notificationsType: [.DeviceConnected,
                                                             .DeviceDisconnected,
                                                             .DeviceAccessUpdated,
                                                             .InvalidDevice])
        UIApplication.shared.isIdleTimerDisabled = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SocketManager.unsubscribe(self)
    }

    // MARK: Navigation
    @objc fileprivate func openSideMenu() {
        if FilesManager.shared.isDownloading || FilesManager.shared.isConverting {
            let alert = UIAlertController(title: "FileManager.LeaveAlert.Title".localized(),
                                          message: "FileManager.LeaveAlert.Text".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default) { action in
                                            if let revealController = self.revealViewController() {
                                                revealController.revealToggle(animated: true)
                                            }
            })
            alert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                          style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            if let revealController = self.revealViewController() {
                revealController.revealToggle(animated: true)
            }
        }
    }
    
    fileprivate func localizeView() {
        editFilesButtonItem.title = "DevicesList.MyDevice.Edit".localized()
    }

    fileprivate func setupView() {
        if let device = deviceModel {
            tableViewDataSource = FilesDataSource(tableView: self.tableView, device: device)
            tableView.dataSource = tableViewDataSource
            tableView.delegate = tableViewDataSource
        }
        
        tableViewDataSource?.deleteConfirmationCallback = { [weak self] cell, isDownloading, location in
            if isDownloading {
                let alert = UIAlertController(title: nil,
                                              message: "FileManager.DeleteFilesAlert.Text".localized(),
                                              preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                              style: .default,
                                              handler: nil))
                self?.present(alert, animated: true, completion: nil)
            } else {
                self?.deleteSelectedFilesAction(view: cell, location: location)
            }
        }

        tableViewDataSource?.shareFileCompletion = { [weak self] cell in
            self?.shareSelectedFile(cell: cell)
        }
        
        tableViewDataSource?.detailsCompletion = { [weak self] file in
            if let detailsPopup = self?.createFileDetailsPopupView() {
                detailsPopup.updateViewFromFile(file)
                self?.presentPopupView(detailsPopup)
            }
        }
        
        tableViewDataSource?.showFileCompletion = { [weak self] file, path in
            if FilesManager.shared.isFileConverting(file) {
                let alert = UIAlertController(title: "FileManager.Info.Convert".localized(),
                                              message: String(format: "FileManager.ConvertAlert.ConvertingNow".localized(), file.name),
                                              preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                              style: .default,
                                              handler: nil))
                self?.present(alert, animated: true, completion: nil)
                return
            }
            
            SVProgressHUD.show()
            let player = PlayerViewController()
            self?.present(player, animated: true) {
                SVProgressHUD.dismiss()
                player.playVideoFromPath(path)
            }
            self?.tableViewDataSource?.deselectAll()
        }

        tableViewDataSource?.warningCompletion = { [weak self] in
            let alert = UIAlertController(title: "FileManager.DeleteAlert.Title".localized(),
                                          message: "FileManager.CantDeleteLastFolder".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default,
                                          handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        
        tableViewDataSource?.convertingInProcessConvertCompletion = { [weak self] file, callback in
            let alert = UIAlertController(title: "FileManager.Info.Convert".localized(),
                                          message: String(format: "FileManager.ConvertAlert.ConvertAlreadyStarted".localized(),
                                                          file.name),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default) { action in
                                            callback(false)
            })
            alert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                          style: .default,
                                          handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        
        tableViewDataSource?.convertWillStartCompletion = { [weak self] callback in
            if let warningPopup = self?.createConvertWarningPopup( callback: {
                callback(false)
            }) {
                let config = STZPopupViewConfig()
                config.cornerRadius = 12
                warningPopup.clipsToBounds = true
                self?.presentPopupView(warningPopup, config: config)
            }
        }
        
        tableViewDataSource?.convertingBatteryLowCompletion = { [weak self] percents in
            guard percents <= 30 else {
                return
            }
            
            let alert = UIAlertController(title: String(format: "FileManager.ConvertAlert.LowBattery.Title".localized(), Int(percents)),
                                          message: "FileManager.ConvertAlert.LowBattery.Text".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default) { action in
            })
            self?.present(alert, animated: true, completion: nil)
        }
        
        tableViewDataSource?.convertingInProcessDeleteCompletion = { [weak self] file, callback in
            let alert = UIAlertController(title: "FileManager.Info.Convert".localized(),
                                          message: String(format: "FileManager.Alert.DeleteConverting.Text".localized(), file.name),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default) { action in
                                            callback(false)
            })
            alert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                          style: .default,
                                          handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        
        if let device = deviceModel {
            tableViewDataSource?.reloadData(device: device, isResetSections: true)
        }
    }

    // MARK: Notification
    internal func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        switch notification {
        case .DeviceConnected:
            if deviceModel == SocketManager.connectedDevice, let device = deviceModel {
                tableViewDataSource?.reloadData(device: device, isResetSections: true)
            }
        case .DeviceAccessUpdated: break
        case .InvalidDevice: break
        default: break
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController,
                                   traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    // MARK: Popup
    
    private func actionsPanelView(isShouldBeShown: Bool) {
        let isChekable = isShouldBeShown
        tableViewDataSource?.isChekable = isChekable
        
        if isChekable {
            editTableToolsView.updateWithSelections(0)
        }
        
        editFilesButtonItem.title = isChekable ? "DevicesList.MyDevice.Done".localized() : "DevicesList.MyDevice.Edit".localized()
        
        let insetNumber = isChekable ? 0 : -self.insetConstant
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.3, animations: {
            self.editTableToolsViewConstarint.constant = insetNumber
            self.view.layoutIfNeeded()
        })
    }
    
    fileprivate func shareSelectedFile(cell: UITableViewCell) {
        if let selectedFile = tableViewDataSource?.selectedFile() {
            if let path = selectedFile.path {
                let attachments = [Path(path).url]

                let sharingActivity = UIActivityViewController(activityItems: attachments,
                                                               applicationActivities: nil)
                
                if let popoverController = sharingActivity.popoverPresentationController {
                    popoverController.sourceView = cell
                    popoverController.sourceRect = CGRect(x: 0, y: 0,
                                                          width: cell.bounds.width,
                                                          height: cell.bounds.height)
                }
                
                present(sharingActivity, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func share(path: String) {
        let attachments = [Path(path).url]
        
        let sharingActivity = UIActivityViewController(activityItems: attachments, applicationActivities: nil)
        present(sharingActivity, animated: true, completion: nil)
    }

    
    fileprivate func deleteSelectedFiles(location: FilesDataSource.FileLocation) {
        tableViewDataSource?.deleteSelected(location: location)
        if let dataSource = tableViewDataSource, dataSource.isChekable {
            actionsPanelView(isShouldBeShown: false)
        }
    }
    
    fileprivate func createFiltrationPopupView() -> FileManagerFiltrationView {
        guard let dataSource = tableViewDataSource else {
            return FileManagerFiltrationView()
        }
        
        let popupView = FileManagerFiltrationView(frame: CGRect(x: 0,y: 0,width: 375,height: 324))
        popupView.updateView(filtration: (dataSource.filterLocation, dataSource.filterType))
        popupView.cancelButton.addTarget(self,
                                         action: #selector(dismissConfiramtionDialog),
                                         for: .touchUpInside)
        popupView.okButton.addAction(to: .touchUpInside) { (button) in
            dataSource.filterType = popupView.filtration.1
            dataSource.filterLocation = popupView.filtration.0
            self.dismissConfiramtionDialog()
        }
        return popupView
    }
    
    fileprivate func createFileDetailsPopupView() -> FileManagerFileDetailsView {
        let popupView = FileManagerFileDetailsView(frame: CGRect(x: 0, y: 0, width: 314, height: 180))
        return popupView
    }
    
    fileprivate func createWarningPopup() -> FileManagerErrorView {
        let popupView = FileManagerErrorView(frame: CGRect(x: 0,y: 0,width: 314,height: 204))
        popupView.center = view.center
        popupView.titleLabel.text = "FileManager.DeleteAlert.Title".localized()
        popupView.descriptionLabel.text = "FileManager.CantDeleteLastFolder".localized()
        popupView.cancelButton.setTitle("General.Alert.Ok".localized(), for: .normal)
        popupView.cancelButton.addTarget(self, action: #selector(dismissConfiramtionDialog), for: .touchUpInside)
        return popupView
    }
    
    fileprivate func createConvertWarningPopup(callback: @escaping () -> Void ) -> FileManagerFileConvertShareView {
        let popupView = FileManagerFileConvertShareView(frame: CGRect(x: 0,y: 0,
                                                                      width: 350,
                                                                      height: 250))
        popupView.center = view.center
        popupView.okButton.addAction(to: .touchUpInside) { [weak self] (sender) in
            self?.dismissConfiramtionDialog()
            callback()
        }
        popupView.cancelButton.addTarget(self,
                                         action: #selector(dismissConfiramtionDialog),
                                         for: .touchUpInside)
        return popupView
    }
    
    @objc fileprivate func dismissConfiramtionDialog() {
        self.dismissPopupView()
    }
    
    // MARK: Actions
    
    @IBAction func deleteSelectedFilesAction(sender: UIButton) {
        if let dataSource = tableViewDataSource {
            deleteSelectedFilesAction(view: sender,
                                      location: dataSource.selectedFilesLocation())
        }
    }
    
    private func deleteSelectedFilesAction(view: UIView, location: FileModel.FileLocation) {
        
        let controller = UIAlertController(title: "FileManager.Actions.Delete.Alert.Message".localized(),
                                           message: nil,
                                           preferredStyle: .actionSheet)
        if location == .local || location == .both {
            controller.addAction(UIAlertAction(title: "FileManager.DeletionConfirmation.OnPhone".localized(),
                                               style: .default,
                                               handler: { _ in
                                                self.deleteSelectedFiles(location: .local)
            }))
        }
        if location == .remote || location == .both {
            controller.addAction(UIAlertAction(title: "FileManager.DeletionConfirmation.OnDevice".localized(),
                                               style: .default,
                                               handler: { _ in
                                                self.deleteSelectedFiles(location: .remote)
            }))
        }
        if location == .both {
            controller.addAction(UIAlertAction(title: "Both",
                                               style: .default,
                                               handler: { _ in
                                                self.deleteSelectedFiles(location: .mixed)
            }))
        }
        controller.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(), style: .cancel, handler: nil))
        
        if let popoverController = controller.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = CGRect(x: 0, y: 0,
                                                  width: view.bounds.width,
                                                  height: view.bounds.height)
        }
        
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func filtrationAction() {
        let popupView = createFiltrationPopupView()
        let config = STZPopupViewConfig()
        config.cornerRadius = 12
        popupView.clipsToBounds = true
        presentPopupView(popupView, config: config)
    }
    
    @IBAction func downloadSelectedFiles() {
        tableViewDataSource?.downloadSelected()
        if let dataSource = tableViewDataSource, dataSource.isChekable {
            actionsPanelView(isShouldBeShown: false)
        }
    }
    
    @IBAction func selectAllContent(_ sender: UIButton) {
        tableViewDataSource?.selectAll()
    }
    
    @IBAction func deselectAllContent(_ sender: UIButton) {
        tableViewDataSource?.deselectAll()
    }
    
    @IBAction func setTableEditable(_ sender: UIBarButtonItem) {
        guard !FilesManager.shared.isDownloading && !FilesManager.shared.isConverting else {
            let alert = UIAlertController(title: nil,
                                          message: FilesManager.shared.isDownloading ? "FileManager.EditFilesAlert.Download.Text".localized() : "FileManager.EditFilesAlert.Convert.Text".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default,
                                          handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if let dataSource = tableViewDataSource {
            actionsPanelView(isShouldBeShown: !dataSource.isChekable)
        }
    }
    
    @IBAction func showDevicesList(sender: UIButton) {
        let popController = DevicesListTableViewController()
        popController.selectedDevice = deviceModel
        popController.deviceSelectedAction = { device in
            self.deviceModel = device
            self.dropDevicesListButton.setTitle(self.deviceModel.info.name, for: .normal)
            self.tableViewDataSource.reloadData(device: device, isResetSections: true)
        }
        
        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0,
                                                                         width: sender.bounds.width,
                                                                         height: sender.bounds.height)
        
        popController.popoverPresentationController?.backgroundColor = .hex("161A1E")
        
        // present the popover
        self.present(popController, animated: true, completion: nil)
    }
    
    // MARK: Deinit
    
    deinit {
        FilesManager.shared.cancellConverting()
        FilesManager.shared.stopDownloading()
        SocketManager.cancellDownload()
        UIApplication.shared.isIdleTimerDisabled = false
    }

}
