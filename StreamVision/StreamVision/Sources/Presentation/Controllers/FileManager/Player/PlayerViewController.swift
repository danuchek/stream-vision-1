//
//  PlayerViewController.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 12/17/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit
import TYMProgressBarView
import SVProgressHUD

class PlayerViewController: UIViewController, VLCMediaPlayerDelegate {
    // Types
    enum PlayerContent {
        case video
        case image
    }
    
    @IBOutlet weak var progressView: TYMProgressBarView!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var videoControllButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var streamImageView: UIImageView!
    var streamService: StreamService!
    var playerContent: PlayerContent!
    var videoHandler: StreamHandler!
    
    var dismissCompletion: (() -> ())?

    override func loadView() {
        Bundle.main.loadNibNamed("PlayerViewController", owner: self, options: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        disableInterface(true)

        SVProgressHUD.show()

        progressView.barBorderWidth = 1
        progressView.barFillColor = UIColor(red: 34/255, green: 141/255, blue: 255/255, alpha: 1)
        progressView.barBorderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.30)
        progressView.barBackgroundColor = UIColor.clear
        
        streamImageView.contentMode = .scaleAspectFit
        
        videoHandler = StreamHandler(motionDetectionView: streamImageView, withMotion: false)
    }

    func playVideoFromPath(_ path: String) {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }

        playerContent = .video
        guard !path.contains("jpg") else {
            playerContent = .image
            
            disableInterface(true)
            streamImageView.image = UIImage(contentsOfFile: path)
//            let photoViewer = UIImageView(frame: streamView.frame)
//            photoViewer.contentMode = .scaleAspectFit
//            photoViewer.image = UIImage(contentsOfFile: path)
//
//            streamView.addSubview(photoViewer)

            return
        }
        
        streamService = StreamService(mode: .player(path: path, videoHandler))
        streamService.start()
        disableInterface(false)
    }

    func disableInterface(_ enable: Bool) {
        endTimeLabel.isHidden = enable
        startTimeLabel.isHidden = enable
        progressView.isHidden = enable
        videoControllButton.isHidden = enable
    }

    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        let player: VLCMediaPlayer = aNotification.object! as! VLCMediaPlayer
        switch player.state {
        case .stopped:
            self.videoControllButton.isSelected = false
            endTimeLabel.text = totalTimeWith(player, timeOffset: 0).stringValue

        default: break
        }
    }

    func mediaPlayerTimeChanged(_ aNotification: Notification!) {
        let player: VLCMediaPlayer = aNotification.object! as! VLCMediaPlayer
        let time: VLCTime = player.time
        startTimeLabel.text = time.stringValue

        endTimeLabel.text = totalTimeWith(player, timeOffset: 1000).stringValue
        progressView.progress = CGFloat(player.position)
    }

    func totalTimeWith(_ player: VLCMediaPlayer, timeOffset: Int) -> VLCTime {
        let time: VLCTime = player.time
        let remainingTime = player.remainingTime?.value
        if let _ = remainingTime {
            let totalTime = Int(ceil(Float(Int(time.value.floatValue/1000))*1000)) +
                Int(ceil(Float(Int(-remainingTime!.floatValue)/1000))*1000) + timeOffset
            return VLCTime(number: totalTime as NSNumber?)
        }
        return VLCTime(number: 0)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        UIView.animate(withDuration: 1, animations: { [unowned self] in
            self.videoControllButton.alpha = 0
        }) 
    }

    @IBAction func didTapOnVieo() {
        guard playerContent == .video else {
            return
        }
        
        UIView.animate(withDuration: 0, animations: { [unowned self] in
            self.videoControllButton.alpha = 1
            }, completion: { finished in
                self.videoControllButton.isSelected = !self.videoControllButton.isSelected
                if !self.videoControllButton.isSelected {
                    self.streamService.pause()
                } else {
                    self.streamService.start()
                }

                UIView.animate(withDuration: 1, animations: { [unowned self] in
                    self.videoControllButton.alpha = 0
                }) 
        }) 
    }

    @IBAction func closePlayer(_ sender: UIButton) {
        if let _ = streamService {
            streamService.stop()
        }

        dismiss(animated: true, completion: dismissCompletion)
    }

}
