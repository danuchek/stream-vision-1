//
//  SaveImageActivity.swift
//  RealStream
//
//  Created by Vladislav Chebotaryov on 2/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import SVProgressHUD

class SaveImageActivity: UIActivity {
    fileprivate var paths: [String]?

    required init(paths: [String]?) {
        if let _paths = paths {
            self.paths = _paths
        }

        super.init()
    }

    override var activityTitle : String? {
        return "SaveImageActivity.Title".localized()
    }

    override var activityType : UIActivity.ActivityType {
        
        return UIActivity.ActivityType(rawValue: "streamvision.SaveToGallery.App")
    }

    override var activityImage : UIImage? {
        return UIImage(named: "save_image")
    }

    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        return true
    }

    override var activityViewController : UIViewController? {
        return nil
    }

    override func perform() {
        let imageForSave = UIImage(contentsOfFile: paths!.first!)

        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        imageForSave!.saveToGallery() { error in
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
        }
        activityDidFinish(true)
    }
}
