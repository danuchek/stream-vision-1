//
//  VideoEncoder.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/27/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import AVFoundation
import CoreGraphics
import CoreAudio
import FileKit

extension VideoEncodingOperation {
    func pixelBufferFrom(_ image: UIImage, width: Int, height: Int) -> CVPixelBuffer? {
        guard let cgImage = image.cgImage else {
            return nil
        }
        
        return pixelBufferFrom(cgImage: cgImage, width: width, height: height)
    }
    
    func pixelBufferFrom(cgImage: CGImage, width: Int, height: Int) -> CVPixelBuffer? {
        var keyCallBack: CFDictionaryKeyCallBacks = .init()
        var valueCallBacks: CFDictionaryValueCallBacks = .init()
        
        let options = CFDictionaryCreateMutable(kCFAllocatorDefault,
                                                2,
                                                &keyCallBack,
                                                &valueCallBacks);
        
        var сGImageCompatibilityPropertiesKey = kCVPixelBufferCGImageCompatibilityKey
        var сGBitmapContextCompatibilityPropertiesKey = kCVPixelFormatCGBitmapContextCompatibility
        
        withUnsafePointer(to: &сGImageCompatibilityPropertiesKey) { unsafePointer in
            var value = true
            withUnsafePointer(to: &value) { valuePointer in
                CFDictionarySetValue(options, unsafePointer, valuePointer)
            }
        }
        withUnsafePointer(to: &сGBitmapContextCompatibilityPropertiesKey) { unsafePointer in
            var value = true
            withUnsafePointer(to: &value) { valuePointer in
                CFDictionarySetValue(options, unsafePointer, valuePointer)
            }
        }
        
        var pxBuffer: CVPixelBuffer? = nil
        
        let status = CVPixelBufferCreate(kCFAllocatorDefault, width, height, kCVPixelFormatType_32ARGB, options, &pxBuffer)
        
        guard status == kCVReturnSuccess, let pixelBuffer = pxBuffer else {
          return nil
        }
        
        let flags = CVPixelBufferLockFlags(rawValue: 0)
        guard kCVReturnSuccess == CVPixelBufferLockBaseAddress(pixelBuffer, flags) else {
          return nil
        }
        defer { CVPixelBufferUnlockBaseAddress(pixelBuffer, flags) }
        
        
        let pxData = CVPixelBufferGetBaseAddress(pixelBuffer)
        
        let rgb = CGColorSpaceCreateDeviceRGB()
        
        let context = CGContext(data: pxData,
                                width: width,
                                height: height,
                                bitsPerComponent: 8,
                                bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer),
                                space: rgb,
                                bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)
        
        context?.concatenate(CGAffineTransform(rotationAngle: 0))
        context?.draw(cgImage, in: CGRect(x: 0, y: 0, width: resolution.height, height: resolution.width))
        
        return pxBuffer
    }
}

extension AudioEncodingOperation {
    func dataToSampleBuffer(data: Data, channels: Int, rate: Int, samples: Int) -> CMSampleBuffer? {
        guard let audioFormat = AVAudioFormat(commonFormat: AVAudioCommonFormat.pcmFormatInt16,
                                              sampleRate: Double(rate),
                                              channels: AVAudioChannelCount(channels),
                                              interleaved: false) else { return nil }
        
        guard let audioBuffer = AVAudioPCMBuffer(pcmFormat: audioFormat,
                                                 frameCapacity: AVAudioFrameCount(samples)) else { return nil }
        
        audioBuffer.frameLength = audioBuffer.frameCapacity
        
        let array = data.withUnsafeBytes {
            [Int16]($0.bindMemory(to: Int16.self)).map(Int16.init(littleEndian:))
        }
        
        for c in 0..<channels {
            let offset = c * samples
            for s in 0..<samples {
                audioBuffer.int16ChannelData![c][s] = array[s + offset]
            }
        }
        
        return createAudioSampleBufferFrom(pcmBuffer: audioBuffer, samples: samples)
    }
    
    func createAudioSampleBufferFrom(pcmBuffer: AVAudioPCMBuffer, samples: Int) -> CMSampleBuffer? {
        let asbd: AudioStreamBasicDescription = pcmBuffer.format.streamDescription.pointee
        var sampleBuffer: CMSampleBuffer?
        let format: CMFormatDescription? = pcmBuffer.format.formatDescription
        
        let duration = CMTimeMake(value: 1, timescale: Int32(asbd.mSampleRate))
        var timing = CMSampleTimingInfo(duration: duration,
                                        presentationTimeStamp: CMTime.zero,
                                        decodeTimeStamp: CMTime.invalid)
        
        let bufferCreationError = CMSampleBufferCreate(allocator: kCFAllocatorDefault,
                                                       dataBuffer: nil, dataReady: false, makeDataReadyCallback: nil, refcon: nil, formatDescription: format,
                                                       sampleCount: CMItemCount(pcmBuffer.frameLength),
                                                       sampleTimingEntryCount: 1, sampleTimingArray: &timing, sampleSizeEntryCount: 0, sampleSizeArray: nil, sampleBufferOut: &sampleBuffer);
        if (bufferCreationError != noErr) {
            return nil
        }
        let setSampleError = CMSampleBufferSetDataBufferFromAudioBufferList(sampleBuffer!,
                                                                            blockBufferAllocator: kCFAllocatorDefault,
                                                                            blockBufferMemoryAllocator: kCFAllocatorDefault, flags: 0,
                                                                            bufferList: pcmBuffer.audioBufferList)
        if (setSampleError != noErr) {
            return nil
        }
        
        return sampleBuffer
    }
}

class AudioEncodingOperation: ConcurrentOperation {
    private let assetWriter: AVAssetWriterInput
    private var data: Data?
    private let pts: Int64
    private let samples: Int
    private let rate: Int
    private let channels: Int
    private let isAudioSessionStarted: Bool
    private var isWriting: Bool = false
    
    required init(with data: Data,
                  isRedyToWrite: Bool,
                  isAudioSessionStarted: Bool,
                  pts: Int64,
                  samples: Int,
                  rate: Int,
                  channels: Int,
                  assetWriter: AVAssetWriterInput) {
        self.assetWriter = assetWriter
        self.data = data
        self.pts = pts
        self.samples = samples
        self.rate = rate
        self.channels = channels
        self.isAudioSessionStarted = isAudioSessionStarted
    }
    
    override func main() {
        Logging.log(message: "Audio operation main", toFile: false)
        writeData()
    }
    
    func writeData() {
        while !assetWriter.isReadyForMoreMediaData && !isAudioSessionStarted {
            Logging.log(message: "Audio assetwriter busy", toFile: false)
        }
        
        if let data = self.data, let buffer = self.dataToSampleBuffer(data: data,
                                                channels: channels,
                                                rate: rate,
                                                samples: samples) {
            self.assetWriter.append(buffer)
            Logging.log(message: "Append audio - OK", toFile: false)
        }
        data = nil
        finish()
    }
}

class VideoEncodingOperation: ConcurrentOperation {
    typealias Resolution = (width: Int, height: Int)

    private var adaptor: AVAssetWriterInputPixelBufferAdaptor!
    private var assetWriter: AVAssetWriterInput
    private var frame: File<Data>?
    private let pts: Int64
    private let isVideoSessionStarted: Bool
    fileprivate let resolution: Resolution
    private var baseBuffer: CVPixelBuffer?
    
    private var isWriting: Bool = false

    required init(with frame: Data,
                  pts: Int64,
                  resolution: Resolution,
                  assetWriter: AVAssetWriterInput,
                  baseBuffer: inout CVPixelBuffer?,
                  isVideoSessionStarted: Bool,
                  adaptor: AVAssetWriterInputPixelBufferAdaptor) {
        self.assetWriter = assetWriter
        self.adaptor = adaptor
        self.pts = pts
        self.resolution = resolution
        self.isVideoSessionStarted = isVideoSessionStarted
        self.baseBuffer = baseBuffer
        
        self.frame = File<Data>(path: Path.userTemporary + "/\(pts).video")
        try? self.frame?.create()
        try? self.frame?.write(frame)
    }
    
    override func main() {
        Logging.log(message: "Video operation main", toFile: false)
        writeData()
    }
    
    func writeData() {
        while !assetWriter.isReadyForMoreMediaData && !isVideoSessionStarted {
            Logging.log(message: "Video assetwriter busy", toFile: false)
        }

        Logging.log(message: "Operation PTS: \(pts) START", toFile: false)
        
        let presentation = CMTimeMake(value: self.pts, timescale: 1000)
        if let path = self.frame?.path.rawValue, let image = UIImage(contentsOfFile: path) {
            baseBuffer = self.pixelBufferFrom(image, width: self.resolution.height,
                                              height: self.resolution.width)
            if let pixelBuffer = baseBuffer, self.adaptor.append(pixelBuffer, withPresentationTime: presentation) {
                Logging.log(message: "Append video - OK", toFile: false)
            } else {
                Logging.log(message: "Append video - FAIL", toFile: false)
            }
        }
        
        baseBuffer = nil
        try? frame?.delete()
        self.finish()
    }
    
    deinit {
        Logging.log(message: "Operation deinit", toFile: false)
    }
}

class VideoEncodingQueue: OperationQueue {
    
    override init() {
        super.init()
        self.maxConcurrentOperationCount = 1
        self.underlyingQueue = .global(qos: .userInteractive)
        self.qualityOfService = .userInteractive
    }
}

class AudioEncodingQueue: OperationQueue {
    
    override init() {
        super.init()
        self.maxConcurrentOperationCount = 1
    }
    
    public var runningOperations: [AudioEncodingOperation] {
        return operations.filter {$0.isExecuting && !$0.isFinished && !$0.isCancelled}.compactMap({ operation in
            operation as? AudioEncodingOperation
        })
    }
    
    public var runningOperation: AudioEncodingOperation? {
        return runningOperations.first
    }
    
    public var encodingOperations: [AudioEncodingOperation] {
        return operations.compactMap({ $0 as? AudioEncodingOperation })
    }
}

class VideoEncoder {
    // File System:
    private var videoHandler: FileHandle!
    private var audioHandler: FileHandle!
    private var videoFrameSize: [Int] = []
    private var audioFrameSize: [Int] = []
    private var videoFinished: Bool = false
    private var audioFinished: Bool = false
    
    private let assetWriter: AVAssetWriter!
    private let videoWriterInput: AVAssetWriterInput!
    private let audioWriterInput: AVAssetWriterInput!
    private var group: AVAssetWriterInputGroup!
    
    private let adaptor: AVAssetWriterInputPixelBufferAdaptor!
    private let mediaQueue = DispatchQueue(label: "MediaQueue")
    
    private var resolution: (width: Int, height: Int)
    private var videoTime: Double = 0.0
    private var fps: Int = 0
    
    private var oneFramePts: Int64 = 50
    private var totalVideoFramePTS: Int64 = 0
    private var totalAudioFramePTS: Int64 = 0
    
    private var progress: ((Double) -> Void)?
    private var endCompletion: ((_ error: Error?) -> Void)?
    
    private var isVideoSessionStarted = false
    private var isAudioSessionStarted = false
    private var isSessionEnd = false
    private var isRedyToWrite = false
    private var isNoAudio = true
    private var isFinished = false
    
    private let videoEncodingQueue: VideoEncodingQueue = VideoEncodingQueue()
    private let audioEncodingQueue: AudioEncodingQueue = AudioEncodingQueue()
    
    var videoWriterObservation: NSKeyValueObservation!
    var audioWriterObservation: NSKeyValueObservation!
    
    var baseBuffer: CVPixelBuffer?
    
    
    init(path: URL, resolution: (width: Int, height: Int),
         isStream: Bool = true, totalTime: Int = 0, fps: Int = 24, progress: ((Double) -> Void)? = nil) {
        Logging.log(message: "[VideoEncoder] Initilize with path:\(path) resolution:\(resolution)", toFabric: true)
        
        // TODO: Compress video if need
        let videoCompressionProps: Dictionary<String, Any> = [
            AVVideoAverageBitRateKey : 1100000,
            AVVideoMaxKeyFrameIntervalKey : fps,
            AVVideoProfileLevelKey : AVVideoProfileLevelH264HighAutoLevel
        ]
 
        
        _ = try? FileManager.default.removeItem(at: path)
        assetWriter = try? AVAssetWriter(outputURL: path, fileType: AVFileType.mov)
        let settings: [String : Any] =
            [AVVideoCodecKey : AVVideoCodecType.h264,
            AVVideoWidthKey : NSNumber(value: resolution.width),
            AVVideoHeightKey : NSNumber(value: resolution.height),
            AVVideoCompressionPropertiesKey : videoCompressionProps]
        videoWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: settings)
        adaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoWriterInput,
                                                       sourcePixelBufferAttributes: nil)
        
        var channelLayout = AudioChannelLayout()
        memset(&channelLayout, 0, MemoryLayout<AudioChannelLayout>.size)
        channelLayout.mChannelLayoutTag = kAudioChannelLayoutTag_Stereo

        let audioSettingsPCM: [String: Any] = [
            AVFormatIDKey: NSNumber(value: kAudioFormatLinearPCM),
            AVSampleRateKey: NSNumber(value: 48000),
            AVLinearPCMBitDepthKey: NSNumber(value: 16),
            AVLinearPCMIsBigEndianKey: NSNumber(booleanLiteral: false),
            AVLinearPCMIsNonInterleaved: NSNumber(booleanLiteral: false),
            AVLinearPCMIsFloatKey: NSNumber(booleanLiteral: false),
            AVChannelLayoutKey: NSData(bytes: &channelLayout, length: MemoryLayout<AudioChannelLayout>.size)
        ]
        
        audioWriterInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: audioSettingsPCM)
        audioWriterInput.expectsMediaDataInRealTime = true
        
        self.resolution = resolution
        self.progress = progress
        self.fps = fps
        
        if let _ = assetWriter {
            videoWriterInput.expectsMediaDataInRealTime = true

            if assetWriter.canAdd(audioWriterInput) {
                assetWriter.add(audioWriterInput)
            } else {
                Logging.log(message: "[VideoEncoder] Audio error input")
            }

            if assetWriter.canAdd(videoWriterInput) {
                assetWriter.add(videoWriterInput)
            } else {
                Logging.log(message: "[VideoEncoder] Video error input")
            }
        }
        
        Logging.log(message: "[VideoEncoder] Initialize start writing", toFabric: true)
        assetWriter.startWriting()
        
        startSession()
    }
    
    private func startSession() {
        
        // Start session at Zero Time
        assetWriter.startSession(atSourceTime: CMTime.zero)
        isVideoSessionStarted = true
        
//        CVPixelBufferRef buffer;
        if let pixelBuffer = adaptor.pixelBufferPool {
            CVPixelBufferPoolCreatePixelBuffer(nil, pixelBuffer, &baseBuffer)
        }
    }
    
    private func checkIfSessionCanBeFinished() -> Bool {
        if !isFinished, !isVideoSessionStarted,
            audioEncodingQueue.operationCount == 0, videoEncodingQueue.operationCount == 0 {
            isFinished = true
            
            videoWriterInput.markAsFinished()
            audioWriterInput.markAsFinished()
            
            let presentation = CMTimeMake(value: totalVideoFramePTS, timescale: 1000)
            assetWriter.endSession(atSourceTime: presentation)
            
            assetWriter.finishWriting { [weak self] in
                DispatchQueue.main.async {
                    self?.endCompletion?(nil)
                }
            }
            return true
        }
        return false
    }
    
    func encodeFrame(frame: Data, pts: Int) {
        guard isVideoSessionStarted else {
            return
        }
        
        let videoEncodingOperation = VideoEncodingOperation(with: frame,
                                                            pts: totalVideoFramePTS,
                                                            resolution: resolution,
                                                            assetWriter: videoWriterInput,
                                                            baseBuffer: &baseBuffer,
                                                            isVideoSessionStarted: isVideoSessionStarted,
                                                            adaptor: adaptor)
        self.videoEncodingQueue.addOperation(videoEncodingOperation)
        totalVideoFramePTS += oneFramePts
    }
    
    func encodeAudio(data: Data, channels: Int, rate: Int, samples: Int, bps: Int, pts: Int) {
        guard isVideoSessionStarted else {
            return
        }
        
        let audioEncodingOperation = AudioEncodingOperation(with: data,
                                                            isRedyToWrite: audioWriterInput.isReadyForMoreMediaData,
                                                            isAudioSessionStarted: isVideoSessionStarted,
                                                            pts: totalAudioFramePTS,
                                                            samples: samples,
                                                            rate: rate,
                                                            channels: channels,
                                                            assetWriter: audioWriterInput)
        
        self.audioEncodingQueue.addOperation(audioEncodingOperation)
        totalAudioFramePTS += oneFramePts
    }
    
    func endSession(comletion: ((_ error: Error?) -> Void)?) {
        Logging.log(message: "[VideoEncoder] Video encoder end session", toFabric: true)
        isVideoSessionStarted = false
        endCompletion = comletion
        
        if !checkIfSessionCanBeFinished() {
            
            let checkIfSessionCanBeFinishedBlock: () -> Void = { [weak self] in
                DispatchQueue.main.async {
                    _ = self?.checkIfSessionCanBeFinished()
                }
            }
            
            if let operation = videoEncodingQueue.operations.last {
                let checkOperation = BlockOperation(block: checkIfSessionCanBeFinishedBlock)
                checkOperation.addDependency(operation)
                videoEncodingQueue.addOperation(checkOperation)
            }
            
            if let operation = audioEncodingQueue.operations.last {
                let checkOperation = BlockOperation(block: checkIfSessionCanBeFinishedBlock)
                checkOperation.addDependency(operation)
                audioEncodingQueue.addOperation(checkOperation)
            }
        }
        
    }
    
    func cancellSession() {
        Logging.log(message: "[VideoEncoder] assetWriter cancel session", toFabric: true)
        Logging.log(message: "[VideoEncoder] videoEncodingQueue count \(videoEncodingQueue.operationCount)",
                   toFabric: true)
        Logging.log(message: "[VideoEncoder] audioEncodingQueue count \(audioEncodingQueue.operationCount)",
                   toFabric: true)
        
        videoEncodingQueue.cancelAllOperations()
        audioEncodingQueue.cancelAllOperations()
        
        assetWriter.cancelWriting()
        isVideoSessionStarted = false
        isAudioSessionStarted = false
    }

}
