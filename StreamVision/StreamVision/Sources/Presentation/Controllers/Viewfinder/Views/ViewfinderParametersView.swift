//
//  ViewfinderParametersView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/2/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class ViewfinderParametersView: UIView {
    enum MoveDirection {
        case right
        case left
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    // Variables
    var customView: UIView!
    var _displayed = false
    var displayed: Bool {
        get {
            return _displayed
        }
        set {
            if _displayed != newValue {
                if newValue == false {
                    hide()
                } else {
                    show()
                }
            }
            _displayed = newValue
        }
    }
    
    private(set) var isAppear: Bool = true
    
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark)) as UIVisualEffectView
    var blurred: Bool = false {
        didSet {
            if blurred == true {
                addBlurToView()
            }
        }
    }
    override var frame: CGRect {
        didSet {
            blurView.frame = bounds
        }
    }
    var animationOffset: CGFloat = 0
    
    var direction: MoveDirection = .left

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    fileprivate func setupView() {
        let className = NSStringFromClass(type(of: self)).components(separatedBy: ".").last!
        customView = Bundle.main.loadNibNamed(className, owner: self, options: nil)?.first as? UIView
        customView.frame = self.bounds
        if frame.isEmpty {
            self.bounds = customView.bounds
        }
        self.addSubview(customView)
    }
    
    func addBlurToView() {
        blurView.removeFromSuperview()
        blurView.frame = bounds
        insertSubview(blurView, at: 0)
        customView.backgroundColor = UIColor.clear
    }
    
    func appear() {
        isAppear = true
        
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func disappear() {
        isAppear = false
        
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        }
    }
    
    func show() {
        guard displayed == false else {
            return
        }
        
        self.isHidden = false
        _displayed = true
        UIView.animate(withDuration: 0.3, animations: {
            var width = self.frame.width
            if self.direction == .left {
                width *= -1
            }
            self.frameX(self.frame.origin.x + width)
        }) 
    }
    
    func hide() {
        hide(){}
    }
    
    func hide(_ completion: @escaping () -> ()) {
        guard displayed == true else {
            completion()
            return
        }
        
        _displayed = false
        UIView.animate(withDuration: 0.3, animations: {
            var width = self.frame.width
            if self.direction == .right {
                width *= -1
            }
            self.frameX((self.frame.origin.x - self.animationOffset) + width)
        }, completion: { _ in
            self.isHidden = true
            completion()
        }) 
    }

}
