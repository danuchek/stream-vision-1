//
//  SecondLevelParametersView.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 12/2/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit

class CustomSliderView: UISlider {
    
    override func maximumValueImageRect(forBounds bounds: CGRect) -> CGRect {
        var newRect = super.maximumValueImageRect(forBounds: bounds)
        newRect.origin.x += 20
        return newRect
    }
}

protocol ParametersScrollViewDelegate {
    func valueDidChanged(_ value: Int)
}

class ParametersScrollView: ViewfinderParametersView, StoreManager {
    // Outlets
    @IBOutlet private weak var parameterSlider: CustomSliderView!
    @IBOutlet private weak var sepparatorView: UIView!
    @IBOutlet private weak var switchView: UISwitch!
    @IBOutlet private weak var switchLabel: UILabel!
    @IBOutlet private weak var sliderWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var sliderCenterYConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var maxLabel: UILabel!
    @IBOutlet private weak var minLabel: UILabel!
    
    private var delimetesCount = 1
    private var delimetesLabelsArray: [UILabel] = []
    
    fileprivate let notificationCenter: NotificationCenter = NotificationCenter.default
    var model: Parameter! {
        didSet {
            updateScroll()
        }
    }
    var pipModel: PiP? {
        didSet {
            if let _ = pipModel {
                sliderWidthConstraint.constant = -15
                sliderCenterYConstraint.constant = -10
                sepparatorView.isHidden = false
                switchView.isHidden = false
                switchLabel.isHidden = false
            } else {
                sliderWidthConstraint.constant = 15
                sliderCenterYConstraint.constant = 15
                sepparatorView.isHidden = true
                switchView.isHidden = true
                switchLabel.isHidden = true
            }
            
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    
    var isPipActive: Bool {
        get {
            return (pipModel != nil) && switchView.isOn
        }
    }
    private var isNightMode: Bool {
        get {
            return storeManager.isNightMode
        }
    }
    var isWithTitle = true {
        didSet {
            titleLabel.isHidden = !isWithTitle
        }
    }
    var isWithIcons = false {
        didSet {
            maxLabel.isHidden = !isWithIcons
            minLabel.isHidden = !isWithIcons
        }
    }
    var icons: (top: UIImage, bottom: UIImage)?
    var isWithValueLabels = true

    var changeCallBack: (() -> ())?
    private var previosValue: Float = 0.0
    private var minValue: Float = 0.0
    private var maxValue: Float = 0.0
    private var colorForDelimitersLabel: UIColor = .white

    @objc func updateScroll() {
        guard let _ = model, displayed else {
            return
        }
        
        var value = model.value
        if let _ = pipModel {
            switchView.isOn = NSNumber(value: pipModel!.value).boolValue
        }
        
        if model is Zoom {
            maxValue = (model.limits.max / model.limits.min)
            minValue = 1
            var oldMin = model.limits.min
            if isPipActive {
                minValue *= 2
                oldMin *= 2
            }
            
            let oldMax = model.limits.max
//            let oldMin = model.min
            let newMax = maxValue
            let newMin = minValue
            let oldValue = value
            
            let oldRange = (oldMax - oldMin)
            let newRange = (newMax - newMin)
            value = (((oldValue - oldMin) * newRange) / oldRange) + newMin
        } else {
            maxValue = model.limits.max
            minValue = model.limits.min
        }
        
        parameterSlider.maximumValue = maxValue
        parameterSlider.minimumValue = minValue
        parameterSlider.setValue(value, animated: true)
        
        maxValue = model.limits.max
        minValue = model.limits.min

        if isPipActive {
            minValue *= 2
        }
        
        maxLabel.text = "Viewfinder.MotionPanel.Sound.Vibration.High".localized()
        minLabel.text = "Viewfinder.MotionPanel.Sound.Vibration.Low".localized()
        updateSliderImages()
        
        if isWithValueLabels {
            addDelimiters()
        }
    }
    
    private func updateSliderImages() {
        if isWithIcons {
            parameterSlider.maximumValueImage = icons?.top
            parameterSlider.minimumValueImage = icons?.bottom
            
            if isNightMode {
                parameterSlider.setMaximumValueImageTint(color: storeManager.nightModeColor)
                parameterSlider.setMinimumValueImageTint(color: storeManager.nightModeColor)
            }
        } else {
            parameterSlider.maximumValueImage = nil
            parameterSlider.minimumValueImage = nil
        }
        
        maxLabel.frameWidth(frame.width)
        maxLabel.textAlignment = .center
        maxLabel.frameY(parameterSlider.frame.origin.y)
        maxLabel.frameX(0)
        if !isNightMode {
            maxLabel.textColor = .rgba(118, 146, 168, 1)
        }
        
        minLabel.frameWidth(frame.width)
        minLabel.textAlignment = .center
        minLabel.frameY(parameterSlider.frame.height + parameterSlider.frame.origin.y)
        minLabel.frameX(0)
        if !isNightMode {
            minLabel.textColor = .rgba(118, 146, 168, 1)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(updateScroll),
                                       name: NSNotification.Name(rawValue: "ViewParamsUpdated"),
                                       object: nil)
        switchLabel.text = "Viewfinder.Parameters.Zoom.PipMode".localized()
        
        let transform: CGAffineTransform  = CGAffineTransform(rotationAngle: -3.14/2)
        self.parameterSlider.transform = transform
        
        if isNightMode {
            parameterSlider.maximumTrackTintColor = storeManager.nightModeColor
            parameterSlider.minimumTrackTintColor = storeManager.nightModeColor
            parameterSlider.thumbTintColor = storeManager.nightModeColor
            sepparatorView.backgroundColor = storeManager.nightModeColor
            colorForDelimitersLabel = storeManager.nightModeColor
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func addDelimiters() {
        var yOffset: CGFloat = 21.5
        var heightOffset: CGFloat = 10
        if model is Zoom {
            yOffset = 0.0
            heightOffset = -30.0
            
            delimetesCount = 2//(model as! ZoomParameter).delimiters + 1
        } else {
            delimetesCount = 0
        }
        
        let offset = parameterSlider.frame.origin.y - yOffset
        let sliderHeight = parameterSlider.frame.height + heightOffset
        let multiplierCofficient = sliderHeight / CGFloat(maxValue - minValue)
        
        delimetesLabelsArray.forEach() {
            $0.removeFromSuperview()
        }
        delimetesLabelsArray = []
        for x in 1...delimetesCount + 1 {
            let label = createDelimeterLabel(offset: offset,
                                             sliderHeight: sliderHeight,
                                             multiplierCofficient: multiplierCofficient,
                                             x: x)
            addSubview(label)
            delimetesLabelsArray.append(label)
        }
        
        // Add max delimeter
        let label = createDelimeterLabel(offset: offset,
                                         sliderHeight: sliderHeight,
                                         multiplierCofficient: multiplierCofficient,
                                         x: Int(maxValue / (minValue > 0 ? minValue : 1)) )
        addSubview(label)
        delimetesLabelsArray.append(label)
    }
    
    //TODO: Delimitrs
    private func createDelimeterLabel(offset: CGFloat, sliderHeight: CGFloat, multiplierCofficient: CGFloat, x: Int) -> UILabel {
        var minValue: CGFloat = CGFloat(self.minValue)
        if !(model is Zoom), Float(x) == model.limits.max {
            minValue = 1
        }
        
        let currentY = multiplierCofficient * ( minValue * CGFloat(x) - minValue)
        let label = UILabel(frame: CGRect(x: parameterSlider.frame.origin.x + 32,
                                          y: ( (offset) + sliderHeight) - currentY,
                                          width: 40,
                                          height: 20))
        label.textColor = colorForDelimitersLabel
        var extraSymbol = ""
        if model is Zoom {
            extraSymbol = "x"
        }
        
        let textValue = minValue * CGFloat(x)
        let sepparetedValue = Int(String(format:"\(extraSymbol)%3.1f", textValue).components(separatedBy: ".")[1])! > 0 ? String(format:"\(extraSymbol)%3.1f", textValue) : "\(extraSymbol)\(Int(textValue))"
        label.text = sepparetedValue
        label.font = UIFont(name: "Roboto-Regular", size: 12.0)
        
        return label
    }
    
    @IBAction func scrollEndEditing(_ sender: UISlider) {
        var value: Float = sender.value
        if model.limits.step.truncatingRemainder(dividingBy: 1) == 0 {
            if model is Zoom {
                value = round(value / model.limits.step) * model.limits.step
            } else {
                value = round(value)
            }
        } else {
            if model.limits.step == 0.1 || model.limits.step == 0.001 {
                Logging.log(message: "Zoom change step = \(model.limits.step), change from(\(previosValue)), to(\(value))")
            } else {
                value = round(value / model.limits.min) * model.limits.min
            }
        }
        
        sender.setValue(value, animated: true)
        model.update(value: value) { success in
            if !success {
                sender.setValue(self.previosValue, animated: true)
            }

            self.previosValue = value
            self.changeCallBack?()
        }
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        let numberValue = NSNumber(value: sender.isOn)
        pipModel?.update(value: numberValue.floatValue) { success in
            if !success {
                sender.isOn = numberValue.boolValue
            }
            
            self.updateScroll()
        }
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }

}
