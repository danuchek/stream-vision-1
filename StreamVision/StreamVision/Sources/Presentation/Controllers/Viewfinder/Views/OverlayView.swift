//
//  OverlayView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/25/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class OverlayView: UIView, StoreManager {
    @IBOutlet var batteryInfoButton: UIButton!
    @IBOutlet var recordInfoButton: UIButton!
    
    @IBOutlet var batteryInfoButtonLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var recordInfoButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var liveViewTrailingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        recordInfoButton.isHidden = true
        let image = UIImage(named: "stream_fill_circle")?.withRenderingMode(.alwaysTemplate)
        recordInfoButton.setImage(image, for: .normal)
        recordInfoButton.tintColor = UIColor.red
        
        var batteryImage = UIImage(named: "vf_battery")
        if storeManager.isNightMode {
            batteryImage = batteryImage?.withRenderingMode(.alwaysTemplate)
        }
        batteryInfoButton.setImage(batteryImage, for: .normal)
    }
    
    func showOnlyRecord(_ show: Bool) {
        batteryInfoButton.isHidden = show
        recordInfoButton.isHidden = !show
        isHidden = !show
    }
    
    func updateForX(orientation: UIInterfaceOrientation) {
        switch orientation {
        case .landscapeRight:
            batteryInfoButtonLeadingConstraint.constant = 50
            recordInfoButtonTrailingConstraint.constant = 30
            if let _ = liveViewTrailingConstraint {
                liveViewTrailingConstraint.constant = -8
            }
        case .landscapeLeft:
            batteryInfoButtonLeadingConstraint.constant = 16
            recordInfoButtonTrailingConstraint.constant = 50
            if let _ = liveViewTrailingConstraint {
                liveViewTrailingConstraint.constant = -50
            }
        default: break
        }
        
        
    }
}
