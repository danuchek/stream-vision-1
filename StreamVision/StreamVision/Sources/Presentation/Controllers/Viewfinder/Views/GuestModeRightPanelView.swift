//
//  GuestModeRightPanelView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 2/7/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class GuestModeRightPanelView: UIView, StoreManager {
    fileprivate var view: UIView!
    
    @IBOutlet private weak var guestModeTitleLabel: UILabel!
    
    var isNightMode: Bool {
        get {
            return storeManager.isNightMode
        }
    }
    
    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(with: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0 , y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        
        localizeView()
        
        if isNightMode {
            view.backgroundColor = .black
        } else {
            view.backgroundColor = UIColor.colorFromHexString("011321")
        }
    }
    
    fileprivate func localizeView() {
        guestModeTitleLabel.text = "Viewfinder.GuestMode.Line1".localized() + " " + "Viewfinder.GuestMode.Line2".localized()
    }

}
