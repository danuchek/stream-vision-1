//
//  ViewfinderGenericRightPanel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/20/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class ViewfinderRightPanel: ViewfinderParametersView, StoreManager {
    enum RightPanelButtonsPosition: Int {
        case Top = 0
        case Middle = 1
        case Bottom = 2
    }
    
    @IBOutlet var recButton: RecButton!
    @IBOutlet var buttons: [ParameterButton]!
    @IBOutlet var parametersButton: ParameterButton!
    
    // TODO: Remove this hack
    @IBOutlet var titleLabelForRecord: UILabel!
    
    var isNightMode: Bool {
        get {
            return storeManager.isNightMode
        }
    }
    
    var isGuestAppereance: Bool = false {
        didSet {
            buttons.forEach() {
                $0.isHidden = isGuestAppereance
            }
            recButton.isHidden = isGuestAppereance
            parametersButton.isHidden = isGuestAppereance
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if isNightMode {
            customView.backgroundColor = .black
        } else {
            customView.backgroundColor = UIColor.colorFromHexString("011321")
        }
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        parametersButton?.type = .ParametersCommon
        parametersButton?.setTitle("Viewfinder.Parameters".localized(), for: .normal)
        parametersButton?.isAutoExecuteActionByPress = true
        parametersButton?.setImage(UIImage(named: "vf_parameters"), for: .normal)
        parametersButton?.setImage(UIImage(named: "vf_parameters_sel"), for: .selected)
        parametersButton?.isAutoSelect = false
    }
    
    func deselectAll() {
        buttons.forEach() {
            $0.isSelected = false
        }
    }
    
    func addButton(position: RightPanelButtonsPosition,
                   visualContent: ParameterVisualContent) {
        
        let button = buttons[position.rawValue]
        button.type = visualContent.type
        button.isWithTitleValue = visualContent.isWithTitleValue
        button.isWithLocalizedValue = visualContent.isWithLocalizedValue
        button.isAutoSelect = visualContent.autoSelectAfterPress
        button.visualContentModel = visualContent
        
        if button.isWithTitleValue {
            let value = visualContent.model!.value
            button.setTitleValue(floatValue: value, for: .normal)
        }
        
        setupButton(button: button,
                    title: visualContent.title,
                    normalImage: visualContent.normalImage,
                    selectedImage: visualContent.selectedImage,
                    action: visualContent.action!)
        
        button.doActionAnimation()
    }
    
    private func setupButton(button: ParameterButton,
                             title: String,
                             normalImage: UIImage?,
                             selectedImage: UIImage?,
                             action: @escaping ParameterVisualContent.ParameterAction) {
        button.setTitle(title.localized(), for: .normal)
        button.setImage(normalImage, for: .normal)
        if let img = selectedImage {
            button.setImage(img, for: .selected)
        }
        button.isHidden = false
        button.parameterAction = action
        button.isAutoExecuteActionByPress = true
    }
    
    func updateButtonsValue() {
        buttons.forEach() { button in
            button.doActionAnimation()
            
            if button.isWithTitleValue, let value = button.visualContentModel?.model?.value {
                button.setTitleValue(floatValue: value, for: .normal)
            }
        }
    }
    
}
