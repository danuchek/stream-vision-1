//
//  ParameterCollectionViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/5/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class ParameterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var parameterButton: ParameterButton!
}
