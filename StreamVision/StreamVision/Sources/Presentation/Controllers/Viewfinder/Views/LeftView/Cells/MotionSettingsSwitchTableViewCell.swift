//
//  MotionSettingsSwitchTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/12/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit

class MotionSettingsSwitchTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchView: UISwitch!
    
    var valueDidChange: ((_ value: Bool) -> ())?
    
    @IBAction func valueDidChange(switchView: UISwitch) {
        valueDidChange?(switchView.isOn)
    }
}
