//
//  MotionSettingsSoundTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/12/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit
import MediaPlayer

extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume;
        }
    }
    
    var volumeSlider:UISlider {
        self.showsRouteButton = false
        self.showsVolumeSlider = false
        self.isHidden = true
        var slider = UISlider()
        for subview in self.subviews {
            if subview is UISlider {
                slider = subview as! UISlider
                slider.isContinuous = false
                //(subview as! UISlider).value = AVAudioSession.sharedInstance().outputVolume
                return slider
            }
        }
        return slider
    }
}

class MotionSettingsSliderTableViewCell: UITableViewCell {
    @IBOutlet weak var sliderView: UISlider!
    var valueDidChange: ((_ value: Float) -> ())?
    
    @IBAction func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began: break
            case .moved: break
            case .ended:
                MPVolumeView.setVolume( slider.value )
                //MPVolumeView().volumeSlider.sendActions(for: .touchUpInside)
                valueDidChange?(slider.value)
            default: break
            }
        }
    }
}
