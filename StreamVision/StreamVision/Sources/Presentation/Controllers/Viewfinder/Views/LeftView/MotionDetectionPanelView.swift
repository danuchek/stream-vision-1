//
//  MotionDetectionPanelView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/10/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit

class MotionDetectionPanelViewAnimation: UIView, StoreManager {
    enum MoveDirection {
        case right
        case left
    }
    
    var _displayed = false
    var displayed: Bool {
        get {
            return _displayed
        }
        set {
            if _displayed != newValue {
                if newValue == false {
                    hide()
                } else {
                    show()
                }
            }
            _displayed = newValue
        }
    }
    var isNightMode: Bool {
        get {
            return storeManager.isNightMode
        }
    }
    
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark)) as UIVisualEffectView
    var blurred: Bool = false {
        didSet {
            if blurred == true {
                addBlurToView()
            }
        }
    }
    override var frame: CGRect {
        didSet {
            blurView.frame = bounds
        }
    }
    var animationOffset: CGFloat = 0.0
    
    
    func addBlurToView() {
        blurView.removeFromSuperview()
        blurView.frame = bounds
        insertSubview(blurView, at: 0)
        backgroundColor = UIColor.clear
    }
    
    private(set) var isAppear: Bool = true
    var direction: MoveDirection = .right
    func appear() {
        isAppear = true
        
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func disappear() {
        isAppear = false
        
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        }
    }
    
    func show() {
        guard displayed == false else {
            return
        }
        
        _displayed = true
        isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            var width = self.frame.width
            if self.direction == .left {
                width *= -1
            }
            self.frameX((self.frame.origin.x + self.animationOffset) + width)
        })
    }
    
    func hide() {
        hide(){}
    }
    
    func hide(_ completion: @escaping () -> ()) {
        guard displayed == true else {
            completion()
            return
        }
        
        _displayed = false
        UIView.animate(withDuration: 0.3, animations: {
            var width = self.frame.width * 1.2
            if self.direction == .right {
                width *= -1.2
            }
            self.frameX((self.frame.origin.x + self.animationOffset) + width)
        }, completion: { _ in
            self.isHidden = true
            completion()
        })
    }
}

class MotionDetectionPanelView: MotionDetectionPanelViewAnimation {
    var accuracyAction: ParameterVisualContent.ParameterAction?
    var settingsAction: ParameterVisualContent.ParameterAction?
    
    let accuracyButton = ParameterButton()
    let settingsButton = ParameterButton()
    
    func deselectAll() {
        accuracyButton.isSelected = false
        settingsButton.isSelected = false
    }
    
    func setupView() {
        if isNightMode {
            backgroundColor = .black
        } else {
            backgroundColor = .rgba(22, 47, 66, 1)
        }
        
        let motionActiveButton = ParameterButton()
        if var motionContent = ParametersConfigurator.visualContentBy(type: .motionActivate) {
            motionContent.model = SocketManager.connectedDevice?.parameters.motionActive

            motionActiveButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11)
            motionActiveButton.setTitle(motionContent.title.localized(), for: .normal)
            motionActiveButton.setImage(motionContent.normalImage, for: .normal)
            motionActiveButton.setImage(motionContent.selectedImage, for: .selected)
            motionActiveButton.isAutoSelect = true
            motionActiveButton.isAutoExecuteActionByPress = true
            motionActiveButton.visualContentModel = motionContent
            motionActiveButton.animationForAction = motionContent.pressAnimationAction
            motionActiveButton.doActionAnimation()
        }
        if let isSelected = SocketManager.connectedDevice?.parameters.motionActive.value {
            motionActiveButton.isSelected = !NSNumber(value: isSelected).boolValue
        }
        motionActiveButton.parameterAction = { button, value in
            SocketManager.connectedDevice?.parameters.motionActive.value =
                NSNumber(value: !button.isSelected).floatValue
            button.doActionAnimation()
            NotificationCenter.default.post(name: NSNotification.Name ("PanelWasScrolled"),
                                            object: nil)
        }
        
        let sepparatorLabel = UILabel()
        if !isNightMode {
            sepparatorLabel.textColor = .rgba(118, 146, 168, 1)
        } else {
            sepparatorLabel.textColor = storeManager.nightModeColor
        }
        sepparatorLabel.font = UIFont(name: "Roboto-Regular", size: 11)
        sepparatorLabel.numberOfLines = 0
        sepparatorLabel.textAlignment = .center
        sepparatorLabel.text = "Viewfinder.MotionPanel.Additional.Settings".localized()
        
        let motionAccuracyContent = ParametersConfigurator.visualContentBy(type: .motionAccuracy)
        accuracyButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11)
        accuracyButton.setTitle(motionAccuracyContent?.title.localized(), for: .normal)
        accuracyButton.setImage(motionAccuracyContent?.normalImage, for: .normal)
        accuracyButton.setImage(motionAccuracyContent?.selectedImage, for: .selected)
        accuracyButton.isAutoSelect = true
        accuracyButton.isAutoExecuteActionByPress = true
        accuracyButton.parameterAction = { button, value in
            self.settingsButton.isSelected = false
            self.accuracyAction?(button, value)
        }
        
        let motionSettingsContent = ParametersConfigurator.visualContentBy(type: .motionSettings)
        settingsButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11)
        settingsButton.setTitle(motionSettingsContent?.title.localized(), for: .normal)
        settingsButton.setImage(motionSettingsContent?.normalImage, for: .normal)
        settingsButton.setImage(motionSettingsContent?.selectedImage, for: .selected)
        settingsButton.isAutoSelect = true
        settingsButton.isAutoExecuteActionByPress = true
        settingsButton.parameterAction = { button, value in
            self.accuracyButton.isSelected = false
            self.settingsAction?(button, value)
        }
        
        motionActiveButton.translatesAutoresizingMaskIntoConstraints = false
        accuracyButton.translatesAutoresizingMaskIntoConstraints = false
        sepparatorLabel.translatesAutoresizingMaskIntoConstraints = false
        settingsButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(motionActiveButton)
        addSubview(accuracyButton)
        addSubview(sepparatorLabel)
        addSubview(settingsButton)
        
        settingsButton.titleLabel?.lineBreakMode = .byWordWrapping
        settingsButton.titleLabel?.numberOfLines = 0
        settingsButton.titleLabel?.textAlignment = .center
        
        accuracyButton.titleLabel?.lineBreakMode = .byWordWrapping
        accuracyButton.titleLabel?.numberOfLines = 0
        accuracyButton.titleLabel?.textAlignment = .center
        
        motionActiveButton.titleLabel?.lineBreakMode = .byWordWrapping
        motionActiveButton.titleLabel?.numberOfLines = 0
        motionActiveButton.titleLabel?.textAlignment = .center
        
        
        NSLayoutConstraint.activate([
            motionActiveButton.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            motionActiveButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            motionActiveButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
            motionActiveButton.heightAnchor.constraint(equalToConstant: 50),
            
            settingsButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),
            settingsButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            settingsButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            settingsButton.heightAnchor.constraint(equalToConstant: 50),
            
            accuracyButton.bottomAnchor.constraint(equalTo: settingsButton.topAnchor, constant: -12),
            accuracyButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            accuracyButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            accuracyButton.heightAnchor.constraint(equalToConstant: 50),
            
            sepparatorLabel.bottomAnchor.constraint(equalTo: accuracyButton.topAnchor, constant: -5),
            sepparatorLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            sepparatorLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            sepparatorLabel.heightAnchor.constraint(equalToConstant: 50),
            
            ])
    }
}
