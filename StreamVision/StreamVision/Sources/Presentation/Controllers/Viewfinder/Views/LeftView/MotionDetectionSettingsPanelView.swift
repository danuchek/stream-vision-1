//
//  MotionDetectionSettingsPanelView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/12/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import RealmSwift
import MediaPlayer

extension MotionDetectionSettingsPanelView {
    func startObservingVolumeChanges() {
        audioSession.addObserver(self, forKeyPath: Observation.VolumeKey, options: [.initial, .new], context: &Observation.Context)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &Observation.Context {
            if keyPath == Observation.VolumeKey, let _ = (change?[NSKeyValueChangeKey.newKey] as? NSNumber)?.floatValue {
                motionTableView.reloadData()
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    func stopObservingVolumeChanges() {
        audioSession.removeObserver(self, forKeyPath: Observation.VolumeKey, context: &Observation.Context)
    }
}

class MotionDetectionSettingsPanelView: MotionDetectionPanelViewAnimation,
                                        UITableViewDelegate,
                                        UITableViewDataSource {
    let motionTableView: UITableView = UITableView()
    
    let audioSession = AVAudioSession.sharedInstance()
    fileprivate struct Observation {
        static let VolumeKey = "outputVolume"
        static var Context = 0
        
    }
    
    func setupView() {
        motionTableView.dataSource = self
        motionTableView.delegate = self
        motionTableView.allowsSelection = false
        
        let nib = UINib(nibName: "MotionSettingsSwitchTableViewCell", bundle: nil)
        let nibSlider = UINib(nibName: "MotionSettingsSliderTableViewCell", bundle: nil)
        motionTableView.register(nib, forCellReuseIdentifier: "MotionSettingsSwitchTableViewCell")
        motionTableView.register(nibSlider, forCellReuseIdentifier: "MotionSettingsSliderTableViewCell")
        
        motionTableView.tableFooterView = UIView(frame: .zero)
        motionTableView.backgroundColor = .white
        blurred = true
        addSubview(motionTableView)
        motionTableView.translatesAutoresizingMaskIntoConstraints = false
        motionTableView.backgroundColor = .clear
        if isNightMode {
            motionTableView.separatorColor = storeManager.nightModeColor
        } else {
            motionTableView.separatorColor = .rgba(200, 199, 204, 1)
        }
        
        NSLayoutConstraint.activate([
            motionTableView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            motionTableView.rightAnchor.constraint(equalTo: rightAnchor, constant: -19),
            motionTableView.topAnchor.constraint(equalTo: topAnchor, constant: 34),
            motionTableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
        
        do {
            try audioSession.setActive(true)
            startObservingVolumeChanges()
        } catch {
            print("Failed to activate audio session")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if let isSoundIndication =
//            SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.isSoundIndication, isSoundIndication {
//            return 3
//        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if let isSoundIndication =
//            SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.isSoundIndication, isSoundIndication {
//            switch indexPath.row {
//            case 0: return configureSettingsSoundActiveCell(tableView:tableView, indexPath: indexPath)
//            case 1: return configureSettingsSliderSoundCell(tableView:tableView, indexPath: indexPath)
//            case 2: return configureSettingsVibrationActiveCell(tableView:tableView, indexPath: indexPath)
//                
//            default:
//                return UITableViewCell()
//            }
//        } else {
//            switch indexPath.row {
//            case 0: return configureSettingsSoundActiveCell(tableView:tableView, indexPath: indexPath)
//            case 1: return configureSettingsVibrationActiveCell(tableView:tableView, indexPath: indexPath)
//                
//            default:
//                return UITableViewCell()
//            }
//        }
        return UITableViewCell()
    }
    
    private func configureSettingsSoundActiveCell(tableView: UITableView,
                                                  indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MotionSettingsSwitchTableViewCell",
                                                 for: indexPath) as! MotionSettingsSwitchTableViewCell
        cell.titleLabel.text = "Viewfinder.MotionPanel.Sounds".localized()
//        if let isSoundIndication = SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.isSoundIndication {
//            cell.switchView.isOn = isSoundIndication
//        }
//        cell.valueDidChange = { value in
//            NotificationCenter.default.post(name: NSNotification.Name ("PanelWasScrolled"),
//                                            object: nil)
//            let realm = try? Realm()
//            try? realm?.write {
//                SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.isSoundIndication = value
//                tableView.reloadData()
//            }
//        }
//        cell.backgroundColor = .clear
        return cell
    }
    
    private func configureSettingsSliderSoundCell(tableView: UITableView,
                                                  indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MotionSettingsSliderTableViewCell",
                                                 for: indexPath) as! MotionSettingsSliderTableViewCell
        cell.backgroundColor = .clear
//        if let _ = SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.soundVolume {
//            cell.sliderView.value = audioSession.outputVolume
//        }
        cell.valueDidChange = { value in
            NotificationCenter.default.post(name: NSNotification.Name ("PanelWasScrolled"),
                                            object: nil)
            let realm = try? Realm()
            try? realm?.write {
//                SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.soundVolume = value
            }
        }
        if isNightMode {
            cell.sliderView.maximumTrackTintColor = storeManager.nightModeColor
            cell.sliderView.minimumTrackTintColor = storeManager.nightModeColor
            cell.sliderView.thumbTintColor = storeManager.nightModeColor
            cell.sliderView.setMaximumValueImageTint(color: storeManager.nightModeColor)
            cell.sliderView.setMinimumValueImageTint(color: storeManager.nightModeColor)
        }
        
        return cell
    }
    
    private func configureSettingsVibrationActiveCell(tableView: UITableView,
                                                      indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MotionSettingsSwitchTableViewCell",
                                                 for: indexPath) as! MotionSettingsSwitchTableViewCell
        cell.titleLabel.text = "Viewfinder.MotionPanel.Vibration".localized()
        cell.backgroundColor = .clear
//        if let isVibroIndication = SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.isVibroIndication {
//            cell.switchView.isOn = isVibroIndication
//        }
        cell.valueDidChange = { value in
            NotificationCenter.default.post(name: NSNotification.Name ("PanelWasScrolled"),
                                            object: nil)
            _ = try? Realm()
//            try? realm?.write {
//                SocketManager.connectedDevice?.config.parameters.motionActive.motionModel.isVibroIndication = value
//                tableView.reloadData()
//            }
        }
        return cell
    }
}
