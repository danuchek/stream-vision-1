//
//  ViewfinderParametersPanelView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/2/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import LNCollectionViewPagedLayout

class ViewfinderEnumView: ViewfinderParametersView, LNCollectionViewDelegatePagedLayout, StoreManager {
    @IBOutlet private weak var parametersCollectionView: UICollectionView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet private weak var sepparatorView: UIView!
    @IBOutlet weak var recordToPhoneSwitch: UISwitch!
    
    @IBOutlet private weak var nextButton: UIButton!
    @IBOutlet private weak var previosButton: UIButton!
    @IBOutlet private weak var switchView: UIView!
    @IBOutlet private weak var recordToPhoneLabel: UILabel!
    @IBOutlet private weak var bottomArrowConstraint: NSLayoutConstraint!
    @IBOutlet private weak var topArrowConstraint: NSLayoutConstraint!

    fileprivate let dataSource = ParametersCollectionViewDataSource()
    fileprivate var maxVisibleItems = 0
    
    fileprivate let defaultNotificationCenter = NotificationCenter.default

    var withSecondLineToLabels: Bool = false {
        didSet {
            let layout = parametersCollectionView.collectionViewLayout as! LNCollectionViewPagedLayout
            if withSecondLineToLabels {
                layout.itemSize = CGSize(width: 80, height: 70)
                layout.minimumRowSpacing = -2;
            } else {
                layout.itemSize = CGSize(width: 80, height: 50)
                layout.minimumRowSpacing = 5;
            }
        }
    }
    var withTitle: Bool = false {
        didSet {
            titleLabel.isHidden = !withTitle
            closeButton.isHidden = withTitle
        }
    }
    var withSwitch: Bool = false {
        didSet {
            switchView.isHidden = !withSwitch
        }
    }
    private var isNightMode: Bool {
        get {
            return storeManager.isNightMode
        }
    }
    
    func setupView() {
        let cellNib = UINib(nibName: "ParameterCollectionViewCell", bundle: nil)
        parametersCollectionView.register(cellNib, forCellWithReuseIdentifier: "ParameterCollectionViewCell")
        
        parametersCollectionView.dataSource = dataSource
        parametersCollectionView.delegate = self
        parametersCollectionView.isPagingEnabled = true
        
        let layout = LNCollectionViewPagedLayout()
        layout.itemSize = CGSize(width: 80, height: 50)
        layout.pageContentInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10);
        
        layout.startAllSectionsOnNewPage = true;
        layout.minimumRowSpacing = 5;
        layout.scrollDirection = .vertical;
        layout.footerSize = .zero
        parametersCollectionView.collectionViewLayout = layout
        
        if isNightMode {
            customView.backgroundColor = .black
            sepparatorView.backgroundColor = storeManager.nightModeColor
            closeButton.setImage(UIImage(named: "panel_close")?.withRenderingMode(.alwaysTemplate), for: .normal)
            nextButton.setImage(UIImage(named: "panel_down")?.withRenderingMode(.alwaysTemplate), for: .normal)
            previosButton.setImage(UIImage(named: "panel_up")?.withRenderingMode(.alwaysTemplate), for: .normal)
        } else {
            customView.backgroundColor = UIColor.colorFromHexString("011321")
        }
        
        localizeView()
    }
    
    func localizeView() {
        recordToPhoneLabel.text = "Viewfinder.RecordToPhone".localized()
        closeButton.setTitle("Viewfinder.Parameters.Close".localized(), for: .normal)
    }
    
    func reload() {
        reload(dataSource.data)
    }
    
    func reloadWithoutOffset() {
        DispatchQueue.main.async {
            self.parametersCollectionView.reloadData()
        }
    }
    
    func reload(_ data: [ParameterVisualContent]) {
        dataSource.data = data
        parametersCollectionView.reloadData()
        
        parametersCollectionView.contentOffset = CGPoint(x: 0, y: 0)
        layoutIfNeeded()
        
        nextButton.isHidden = parametersCollectionView.contentSize.height <= parametersCollectionView.frame.height
        maxVisibleItems = parametersCollectionView.indexPathsForVisibleItems.count
    }
    
    // MARK: Navigation
    
    @IBAction func switchValueChanged(sender: UISwitch) {
        storeManager.isRecordToPhone = sender.isOn
        NotificationCenter.default.post(name: NSNotification.Name(ViewfinderPanelsConfigurator.RecordToPhone), object: nil)
    }
    
    @IBAction func nextButtonAction() {
        let visibleItems = parametersCollectionView.indexPathsForVisibleItems
        guard visibleItems.count > 0 else {
            return
        }
        let index: IndexPath = IndexPath(item: 0, section: 0)
        let sortedItems = visibleItems.sorted() {
            $0.item < $1.item
        }
        let indexForItem =
            (sortedItems.last!.item + 1) > dataSource.data.count - 1 ? sortedItems.last!.item : (sortedItems.last!.item + 1)

        let nextItem = IndexPath(item: indexForItem, section: index.section)
        parametersCollectionView.scrollToItem(at: nextItem, at: .top, animated: true)
        
        defaultNotificationCenter.post(name: Notification.Name( "PanelWasScrolled" ), object: self)
    }

    @IBAction func prevButtonAction() {
        let index: NSIndexPath = NSIndexPath(item: 0, section: 0)
        let visibleItems = parametersCollectionView.indexPathsForVisibleItems
        
        let sortedItems = visibleItems.sorted() {
            $0.item < $1.item
        }
        let indexForItem = (sortedItems.first!.item - maxVisibleItems) >= 0 ? sortedItems.first!.item - maxVisibleItems : sortedItems.first!.item

        let nextItem = IndexPath(item: indexForItem, section: index.section)
        parametersCollectionView.scrollToItem(at: nextItem, at: .top, animated: true)
        
        defaultNotificationCenter.post(name: Notification.Name( "PanelWasScrolled" ),
                                       object: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.3) {
            if self.parametersCollectionView.contentOffset.y < self.parametersCollectionView.frame.height {
                self.previosButton.alpha = 0
            } else {
                self.previosButton.alpha = 1
            }
            
            if (self.parametersCollectionView.contentOffset.y - 50) * 2 >= (self.parametersCollectionView.contentSize.height - 20) {
                self.nextButton.alpha = 0
            } else {
                self.nextButton.alpha = 1
            }
        }
        
        defaultNotificationCenter.post(name: Notification.Name( "PanelWasScrolled" ),
                                       object: self)
    }
    
}
