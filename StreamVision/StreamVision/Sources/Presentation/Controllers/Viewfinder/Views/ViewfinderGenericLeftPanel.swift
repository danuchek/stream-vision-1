//
//  ViewfinderGenericLeftPanel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/19/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class ViewfinderGenericLeftPanel: UIView, StoreManager {
    
    var isMenuButton = true
    var isOptionsButton = false {
        didSet {
           optionsButton.isHidden = !isOptionsButton
        }
    }
    var menuButton: ParameterButton!
    var optionsButton: ParameterButton!
    private var bottomButtons: [ParameterButton] = []
    private var topButtons: [ParameterButton] = []
    
    var isGuestAppereance: Bool = false {
        didSet {
            bottomButtons.forEach() {
                $0.isHidden = isGuestAppereance
            }
        }
    }
    
    var isNightMode: Bool {
        get {
            return storeManager.isNightMode
        }
    }
    
    // TODO: Night mode story
    var isNightmodeAppereance: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if isNightMode {
            self.backgroundColor = .black
        } else {
            self.backgroundColor = UIColor.colorFromHexString("011321")
        }
        
        self.setupViewComponents()
    }
    
    private func setupViewComponents() {
        
        // Menu button setup
        if isMenuButton {
            menuButton = ParameterButton()
            menuButton.setTitle("Navigation.Menu".localized(), for: .normal)
            menuButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11.0)
            
            var imageForNormal = UIImage(named: "navigation_menu")
            var imageForSelected = UIImage(named: "navigation_menu_tap")
            if isNightMode {
                imageForNormal = imageForNormal?.withRenderingMode(.alwaysTemplate)
                imageForSelected = imageForSelected?.withRenderingMode(.alwaysTemplate)
            }
            
            menuButton.setImage(imageForNormal, for: .normal)
            menuButton.setImage(imageForSelected, for: .highlighted)

            addSubview(menuButton)
            
            menuButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                menuButton.heightAnchor.constraint(equalToConstant: 55),
                menuButton.widthAnchor.constraint(equalToConstant: 50),
                menuButton.topAnchor.constraint(equalTo: topAnchor, constant: 10),
                menuButton.centerXAnchor.constraint(equalTo: centerXAnchor)
            ])
        }
        
        topButtons.append(menuButton)
//        setupOptionsButton()
    }
    
    func addButtonToTop(visualContent: ParameterVisualContent) {
        let button = ParameterButton()
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11.0)
        
        button.type = visualContent.type
        button.isWithTitleValue = visualContent.isWithTitleValue
        button.isWithLocalizedValue = visualContent.isWithLocalizedValue
        button.isAutoSelect = visualContent.autoSelectAfterPress
        button.visualContentModel = visualContent
        
        if button.isWithTitleValue {
            let value = visualContent.model!.value
            button.setTitleValue(floatValue: value, for: .normal)
        }
        
        setupToTopButton(button: button,
                         title: visualContent.title,
                         normalImage: visualContent.normalImage,
                         selectedImage: visualContent.selectedImage,
                         action: visualContent.action!)
        
        button.doActionAnimation()
    }
    
    func addButton(visualContent: ParameterVisualContent) {
        let button = ParameterButton()
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11.0)
        
        button.type = visualContent.type
        button.isWithTitleValue = visualContent.isWithTitleValue
        button.isWithLocalizedValue = visualContent.isWithLocalizedValue
        button.isAutoSelect = visualContent.autoSelectAfterPress
        button.visualContentModel = visualContent
        
        if button.isWithTitleValue {
            let value = visualContent.model!.value
            button.setTitleValue(floatValue: value, for: .normal)
        }
    
        setupButton(button: button,
                    title: visualContent.title,
                    normalImage: visualContent.normalImage,
                    selectedImage: visualContent.selectedImage,
                    action: visualContent.action!)
        
        button.doActionAnimation()
    }
    
    private func setupToTopButton(button: ParameterButton,
                                  title: String,
                                  normalImage: UIImage?,
                                  selectedImage: UIImage?,
                                  action: @escaping ParameterVisualContent.ParameterAction) {
        button.setTitle(title.localized(), for: .normal)
        button.setImage(normalImage, for: .normal)
        if let img = selectedImage {
            button.setImage(img, for: .selected)
        }
        button.isHidden = false
        button.parameterAction = action
        button.isAutoExecuteActionByPress = true
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        
        addButtonToTopView(button: button)
    }
    
    private func setupButton(button: ParameterButton,
                             title: String,
                             normalImage: UIImage?,
                             selectedImage: UIImage?,
                             action: @escaping ParameterVisualContent.ParameterAction) {
        button.setTitle(title.localized(), for: .normal)
        button.setImage(normalImage, for: .normal)
        if let img = selectedImage {
            button.setImage(img, for: .selected)
        }
        button.isHidden = false
        button.parameterAction = action
        button.isAutoExecuteActionByPress = true
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        
        addButtonToView(button: button)
    }
    
    func addButtonToTopView(button: ParameterButton) {
        addSubview(button)
        
        setupConstraintForTopButton(button: button)
        topButtons.append(button)
    }
    
    func addButtonToView(button: ParameterButton) {
        addSubview(button)
        
        guard bottomButtons.count > 0 else {
            setupConstraintForFirstButton(button: button)
            
            bottomButtons.append(button)
            return
        }
        
        setupConstraintForButton(button: button)
        bottomButtons.append(button)
    }
    
    func updateButtonsValue() {
        topButtons.forEach() { button in
            button.doActionAnimation()
            
            if button.isWithTitleValue {
                let value = button.visualContentModel!.model!.value
                button.setTitleValue(floatValue: value, for: .normal)
            }
        }
        
        bottomButtons.forEach() { button in
            button.doActionAnimation()
            
            if button.isWithTitleValue {
                let value = button.visualContentModel!.model!.value
                button.setTitleValue(floatValue: value, for: .normal)
            }
        }
    }
    
    private func setupConstraintForTopButton(button: ParameterButton) {
        button.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
//            button.heightAnchor.constraint(lessThanOrEqualToConstant: 65),
            button.heightAnchor.constraint(equalToConstant: 55),
            button.widthAnchor.constraint(equalTo: widthAnchor),
            button.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        if let bottomAnchor = topButtons.last?.bottomAnchor {
            button.topAnchor.constraint(equalTo: bottomAnchor, constant: 10).isActive = true
        }
    }

    private func setupConstraintForFirstButton(button: ParameterButton) {
        button.translatesAutoresizingMaskIntoConstraints = false
        
//        let height = button.title!.height(withConstrainedWidth: frame.width,
//                                          font: button.titleLabel!.font)
        
        NSLayoutConstraint.activate([
//            button.heightAnchor.constraint(lessThanOrEqualToConstant: 65),
            button.heightAnchor.constraint(equalToConstant: 55),
            button.widthAnchor.constraint(equalTo: widthAnchor),
            button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            button.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
    private func setupConstraintForButton(button: StylizedButton) {
        button.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 55),
            button.widthAnchor.constraint(equalTo: widthAnchor),
            button.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        if let topAnchor = bottomButtons.last?.topAnchor {
            button.bottomAnchor.constraint(equalTo: topAnchor, constant: -10).isActive = true
        }
    }
    
    private func setupOptionsButton() {
        optionsButton = ParameterButton()
        optionsButton.setTitle("Restreaming.Preview.Options.Title".localized(), for: .normal)
        optionsButton.isHidden = !isOptionsButton
        optionsButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 11.0)
        
        var imageForNormal = UIImage(named: "rs_options")
        var imageForSelected = UIImage(named: "rs_options_sel")
        if isNightMode {
            imageForNormal = imageForNormal?.withRenderingMode(.alwaysTemplate)
            imageForSelected = imageForSelected?.withRenderingMode(.alwaysTemplate)
        }
        
        optionsButton.setImage(imageForNormal, for: .normal)
        optionsButton.setImage(imageForSelected, for: .highlighted)
        
        addSubview(optionsButton)
        
        optionsButton.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: optionsButton!,
                                                      attribute: .centerX,
                                                      relatedBy: .equal,
                                                      toItem: self,
                                                      attribute: .centerX,
                                                      multiplier: 1,
                                                      constant: 0)
        let topConstraint = NSLayoutConstraint(item: optionsButton!,
                                               attribute: .top,
                                               relatedBy: .greaterThanOrEqual,
                                               toItem: menuButton,
                                               attribute: .bottom,
                                               multiplier: 1,
                                               constant: 2)

        NSLayoutConstraint.activate([
//            optionsButton.heightAnchor.constraint(lessThanOrEqualToConstant: 65),
            optionsButton.heightAnchor.constraint(equalToConstant: 65),
            optionsButton.widthAnchor.constraint(equalToConstant: 50),
            horizontalConstraint, topConstraint
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
