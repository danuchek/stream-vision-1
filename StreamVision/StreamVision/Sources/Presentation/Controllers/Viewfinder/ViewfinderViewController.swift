//
//  StreamViewController.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 11/25/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD
import FileKit
import AVFoundation

extension ViewfinderViewController: StoreManager, RateManager {}

class ViewfinderViewController: UIViewController, SocketConnectionSubsriber {
    @IBOutlet weak var viewConfigurator: ViewfinderPanelsConfigurator!
    fileprivate let fileManager = FilesManager.shared
    
    var streamService: StreamService!
    var streamHandler: StreamHandler!
    
    @IBOutlet weak var motionDetectionView: UIImageView!
    @IBOutlet weak var streamWidthLayout: NSLayoutConstraint!
    @IBOutlet weak var streamHeightLayout: NSLayoutConstraint!
    @IBOutlet weak var streamVerticalLayout: NSLayoutConstraint!
    @IBOutlet weak var streamHorisontalLayout: NSLayoutConstraint!

    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set callbacks
        viewConfigurator.recordCallback = { [weak self] in
            self?.recordCallback(active: $0)
        }
        viewConfigurator.snapshotCallback = { [weak self] in
            self?.shapshotCallback()
        }
        
        // Setup left menu panel
        if let revealController = self.revealViewController() {
            viewConfigurator.revealViewController = revealController
        }
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        viewConfigurator.setNeedsLayout()
        viewConfigurator.layoutIfNeeded()
        viewConfigurator.updateConstraints()
        viewConfigurator.configureViews()
        
        setNeedsUpdateOfHomeIndicatorAutoHidden()
    }

    func setupStream() {
        streamHandler = StreamHandler(motionDetectionView: motionDetectionView)
        streamHandler.didFirstFrameObtained = { resolution in
            let viewWidth = self.view.frame.width -
                (self.viewConfigurator.leftPanelView.frame.width + self.viewConfigurator.rightPanelView.frame.width)
            let viewHeight = self.view.frame.height
            let k1 = CGFloat(resolution.w) / CGFloat(resolution.h)
            if (viewWidth / k1 <= viewHeight) {
                self.streamHeightLayout.constant = viewWidth / k1
                self.streamWidthLayout.constant = viewWidth
            } else {
                self.streamHeightLayout.constant = viewHeight
                self.streamWidthLayout.constant = k1 * viewHeight
            }
            self.streamHorisontalLayout.constant = (self.viewConfigurator.leftPanelView.frame.width + (viewWidth / 2)) - self.view.center.x
        }

        if SocketManager.isConnection(.demo) {
            streamService = StreamService(mode: .camera(layer: self.view.layer))
        } else {
//            TODO: Disable for a time
//            let angle = storeManager.isInvertedVideoStream ? Double.pi : 0.0
//            motionDetectionView.layer.transform = CATransform3DMakeRotation(CGFloat(angle), 0, 0.0, 1.0);
            let isH264 = SocketManager.connectedDevice?.parameters.stream?.isH264
            streamService = StreamService(mode: .viewfinder(streamHandler, isH264: isH264 ?? false))
        }
        streamService.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutSubviews()

        setupStream()
        viewConfigurator.goToGuestMode()

        if storeManager.isNightMode {
            updateWithNightMode()
        }


        googleReport(action: "Viewfinder_open")
        SocketManager.subscribe(self, notificationsType: [.DeviceAccessUpdated, .DeviceParamsUpdated])

        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                NotificationCenter.default.addObserver(self,
                                                       selector: #selector(orientationWillChange),
                                                       name: UIApplication.willChangeStatusBarOrientationNotification,
                                                       object: nil)
                let orientation = UIApplication.shared.statusBarOrientation
                viewConfigurator.updateViewForX(orientation: orientation)
            default: break
            }
        }
        subscribeToNotifications()

        if #available(iOS 11.0, *) {
        } else {
            guard SocketManager.connectionMode != .demo else {
                return
            }
            let alert = UIAlertController(title: "Stream.UsageLimitation.Alert.Title".localized(),
                                          message: "Stream.UsageLimitation.Alert.Message".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default,
                                          handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

        rateManager.enterOnTrackingVC()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if let revealController = self.revealViewController() {
            view.removeGestureRecognizer(revealController.panGestureRecognizer())
        }
        self.streamService?.stop()

        fileManager.endEncoding()
        SocketManager.unsubscribe(self)

        // Deactivate apearance
        if storeManager.isNightMode {
            updateWithoutNightMode()
        }
        SVProgressHUD.dismiss()
        viewConfigurator.motionSettingsView?.stopObservingVolumeChanges()
        NotificationCenter.default.removeObserver(self)
        if let _ = viewConfigurator {
            NotificationCenter.default.removeObserver(viewConfigurator!)
        }
        if let _ = streamService.cameraService {
            NotificationCenter.default.removeObserver(streamService.cameraService!)
        }

        rateManager.leftFromTrackingVC()
    }
    
    // MARK: Setup view
    fileprivate func subscribeToNotifications() {
        let defaultCenter = NotificationCenter.default;
        defaultCenter.addObserver(self, selector: #selector(applicationDidEnterBackgroundNotification),
                                  name: UIApplication.didEnterBackgroundNotification, object: nil)
        defaultCenter.addObserver(self, selector: #selector(applicationWillEnterForeground),
                                  name: UIApplication.willEnterForegroundNotification, object: nil)
        defaultCenter.addObserver(self, selector: #selector(appMovedToBackground),
                                  name: UIApplication.willResignActiveNotification, object: nil)
        if let _ = viewConfigurator {
            defaultCenter.addObserver(viewConfigurator!,
                                      selector: #selector(ViewfinderPanelsConfigurator.recordToPhoneUpdate),
                                      name: Notification.Name(ViewfinderPanelsConfigurator.RecordToPhone),
                                      object: nil)
        }
    }
    
    
    @objc fileprivate func applicationWillEnterForeground() {
        if SocketManager.isConnected, !SocketManager.isConnection(.demo) {
            setupStream()
        }
    }
    
    @objc fileprivate func applicationDidEnterBackgroundNotification() {
        streamService.stop()
    }
    
    @objc func orientationWillChange(notification: Notification) {
        if let statusBarOrientation = notification.userInfo?[UIApplication.statusBarOrientationUserInfoKey] as? Int,
            let orientation = UIDeviceOrientation(rawValue: statusBarOrientation) {
            viewConfigurator.updateViewForX(orientation: orientation)
        }
    }
    
    @objc func appMovedToBackground() {
        fileManager.endEncoding()
        if viewConfigurator.isRecordToPhone {
            viewConfigurator.rightPanelView.recButton.pressed = false
            viewConfigurator.isRecord = false
        }
        SVProgressHUD.dismiss()
    }
    
    // TODO: Update
    private func updateWithNightMode() {
        let buttonStyle = UIButton.appearance(whenContainedInInstancesOf:[ViewfinderPanelsConfigurator.self])
        buttonStyle.setTitleColor(storeManager.nightModeColor, for: .normal)
        buttonStyle.setTitleColor(storeManager.nightModeColor, for: .disabled)
        buttonStyle.tintColor = storeManager.nightModeColor

        let labelStyle = UILabel.appearance(whenContainedInInstancesOf:[ViewfinderPanelsConfigurator.self])
        labelStyle.textColor = storeManager.nightModeColor

        let switchStyle = UISwitch.appearance(whenContainedInInstancesOf:[ViewfinderPanelsConfigurator.self])
        switchStyle.thumbTintColor = storeManager.nightModeColor
        switchStyle.onTintColor = UIColor.colorFromHexString("721824")
        switchStyle.tintColor = storeManager.nightModeColor

        let sliderStyle = UISlider.appearance(whenContainedInInstancesOf: [ViewfinderPanelsConfigurator.self])
        sliderStyle.tintColor = .blue

        if let sideController: SideTableViewController  = self.revealViewController()?.rearViewController as? SideTableViewController {
            sideController.isNightMode = true
        }
    }
    
    // TODO: Update
    private func updateWithoutNightMode() {
        let buttonStyle = UIButton.appearance(whenContainedInInstancesOf:[ViewfinderPanelsConfigurator.self])
        buttonStyle.setTitleColor(nil, for: .normal)
        buttonStyle.setTitleColor(nil, for: .disabled)
        buttonStyle.tintColor = nil
        let labelStyle = UILabel.appearance(whenContainedInInstancesOf:[ViewfinderPanelsConfigurator.self])
        labelStyle.textColor = .white
        let switchStyle = UISwitch.appearance(whenContainedInInstancesOf:[ViewfinderPanelsConfigurator.self])
        switchStyle.thumbTintColor = nil
        switchStyle.onTintColor = nil
        switchStyle.tintColor = nil
        let sliderStyle = UISlider.appearance(whenContainedInInstancesOf: [ViewfinderPanelsConfigurator.self])
        sliderStyle.tintColor = nil

        if let sideController: SideTableViewController = revealViewController()?.rearViewController as? SideTableViewController {
            sideController.isNightMode = false
        }
    }
    
    @IBAction func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }

    // MARK: Notification
    internal func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        DispatchQueue.main.async { [weak self] in
            switch notification {
            case .DeviceAccessUpdated:
                self?.viewConfigurator.goToGuestMode()

            case .DeviceParamsUpdated:
                self?.viewConfigurator.paramsUpdated()

            case .DeviceDisconnected:
                self?.rateManager.negativeDisconnected()

            default: break
            }
        }
    }
    
    deinit {
        Logging.log(message: "[Viewfinder] - Deinit")
    }
}

extension ViewfinderViewController {
    func recordCallback(active: Bool) {
        guard !SocketManager.isConnection(.demo) &&
        (streamHandler.resolution.h != 0 || streamHandler.resolution.w != 0) else {
            return
        }
        
        if active {
            fileManager.startEncodingWithSize(streamHandler.resolution)
        } else {
            fileManager.endEncoding()
        }
    }
    
    func shapshotCallback() {
        guard !SocketManager.isConnection(.demo) else {
            return
        }
        
        fileManager.saveSnapshot(streamHandler.snapshot)
    }
}
