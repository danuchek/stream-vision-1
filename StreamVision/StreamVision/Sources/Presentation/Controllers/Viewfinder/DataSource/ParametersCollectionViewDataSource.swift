//
//  CollectionViewParametersDataSource.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/5/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class ParametersCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    var data: [ParameterVisualContent] = []
    var isNeedSelection: Bool = true
    
    @objc func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count;
    }
    
    @objc func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParameterCollectionViewCell", for: indexPath) as! ParameterCollectionViewCell
        let itemIndex = (indexPath as NSIndexPath).row
        let dataModel = data[itemIndex]
        
        if let type = dataModel.sender?.type, type == .ParametersCommon {
            if let selectionIndex = dataModel.sender?.visualContentModel?.model?.value {
                cell.parameterButton.isSelected = ( Int(selectionIndex) == itemIndex )
            }
        }
        
        if let sender = dataModel.sender, sender.type == .ParametersCommon {
            let model = sender.visualContentModel!.model!
            
            cell.parameterButton.isSelected = ( Int(model.value) == itemIndex )
        } else {
            if let model = dataModel.model {
                cell.parameterButton.isSelected = ( Int(model.value) == itemIndex ) && (dataModel.type != .motionAccuracy)
            }
        }
        
        cell.parameterButton.isEnabled = true
        
        cell.parameterButton.titleLabel?.textAlignment = .center
        cell.parameterButton.isWithTitleValue = dataModel.isWithTitleValue
        cell.parameterButton.isWithLocalizedValue = dataModel.isWithLocalizedValue
        
        cell.parameterButton.setTitle(dataModel.title.localized(), for: .normal)
        cell.parameterButton.setImage(dataModel.normalImage, for: .normal)
        cell.parameterButton.setImage(dataModel.selectedImage, for: .selected)
        
        cell.parameterButton.type = dataModel.type
        cell.parameterButton.visualContentModel = dataModel
        cell.parameterButton.isAutoSelect = dataModel.autoSelectAfterPress
        cell.parameterButton.parameterAction = dataModel.action
        cell.parameterButton.doActionAnimation()
        cell.parameterButton.removeAllActions()
        
        if dataModel.isWithTitleValue {
            let value = dataModel.model!.value
            if dataModel.type == .DistancesCommon {
                let model = (dataModel.model as! Distance)
                let _value = model.selectedDistance
                
                if _value == -1 || value == -1 {
                    cell.parameterButton.setTitleValue(stringValue: "None", for: .normal)
                } else {
                    cell.parameterButton.setTitleValue(intValue: _value, for: .normal)
                }
            } else {
                cell.parameterButton.setTitleValue(floatValue: value, for: .normal)
            }
        }
        
        let parameterAction = cell.parameterButton.parameterAction
        cell.parameterButton.addAction(to: .touchUpInside) { (sender) in
            
            if let action = parameterAction {
                action(sender as! ParameterButton, Float(itemIndex))
            }
        }
        
        return cell
    }
}
