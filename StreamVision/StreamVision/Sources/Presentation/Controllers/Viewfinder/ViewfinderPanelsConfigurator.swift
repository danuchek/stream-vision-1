//
//  ViewfinderViewConfigurator.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/2/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import SWRevealViewController
import RealmSwift

extension ViewfinderPanelsConfigurator {
    fileprivate func selectorBy(type: ParametersConfigurator.ViewfinderParameterType) -> ParameterVisualContent.ParameterAction {
        switch type {
            
            // Common
        case .CaptureModeCommon:
            return showEnumView
        case .ZoomCommon:
            return showScrollView
        case .ParametersCommon:
            return showEnumView
        case .DisplayOffCommon:
            return displayOffAction
        case .ColorsPaletteCommon:
            return showEnumView
        case .ReticleColorCommon:
            return showEnumView
        case .RecognitionCommon:
            return showEnumView
        case .DistancesCommon:
            return showEnumView
        case .ReticleTypeCommon:
            return showEnumView
        case .ShutterCommon:
            return shutterAction
        case .IRScrollable:
            return showScrollView
        
            // Parameters
        case .BrightnessCommon:
            return showScrollView
        case .ContrastCommon:
            return showScrollView
        case .ReticleBrightnessCommon:
            return showScrollView

            
        default:
            return showEnumView
        }
    }
}

class ViewfinderPanelsConfigurator: UIView, UIGestureRecognizerDelegate, StoreManager {
    
    // Variables
    fileprivate var motionPanelView: MotionDetectionPanelView?
    fileprivate var parameterEnumView: ViewfinderEnumView!
    fileprivate var parameterScrollView: ParametersScrollView!
    fileprivate var guestModeRightPanelView: GuestModeRightPanelView?
    fileprivate var leftParametersScrollView: ParametersScrollView!
    var leftParametersEnumView: ViewfinderEnumView!
    var commonParametersView: ViewfinderEnumView!
    var motionSettingsView: MotionDetectionSettingsPanelView?
    
    @IBOutlet var overlayView: OverlayView!
    @IBOutlet var warningLabel: UILabel!
    
    // Constants
    static let RecordToPhone: String = "RecordToPhone"
    
    // Parameters data
    var visualConstraints: VirtualParameters.Constraints!
    let constWidthForPanel: CGFloat = 119.0
    
    fileprivate let notificationCenter: NotificationCenter = NotificationCenter.default
    
    var recordCallback: ((_ active : Bool) -> ())? = nil
    var snapshotCallback: (() -> ())? = nil

    var timer: Timer?
    var isRecord: Bool = false
    var isRecordToPhone: Bool {
        return storeManager.isRecordToPhone
    }
    
    var timeoutForHidding: Int = 5
    fileprivate var recordTime = 0.0 {
        didSet {
            overlayView.recordInfoButton.setTitle(formatRecordTime(), for: .normal)
        }
    }

    weak var revealViewController: SWRevealViewController?
    
    var leftPanelView: ViewfinderGenericLeftPanel!
    var rightPanelView: ViewfinderRightPanel!
    
    func setupLeftPanel() {
        Logging.log(message: "[Viewfinder] Left panel created")
        
        if let _ = leftPanelView {
            leftPanelView.removeFromSuperview()
        }
        
        leftPanelView = ViewfinderGenericLeftPanel(frame: CGRect(x: 0, y: 0, width: 67, height: frame.height))
        addSubview(leftPanelView)
        
        leftPanelView.menuButton.addAction(to: .touchUpInside) { [weak self] (button) in
            self?.openSideMenu()
        }
        
        if let _parameters = visualConstraints["left"]?["level0"] {
            for (_, item) in _parameters.enumerated() {
                guard let parameter = item else {
                    continue
                }

                if var visualContent = ParametersConfigurator.visualContentBy(parameter: parameter) {
                    visualContent.action = { [weak self] sender, newValue in
                        self?.selectorBy(type: visualContent.type)(sender, newValue)
                    }
                    visualContent.model = parameter

                    leftPanelView.addButton(visualContent: visualContent)
                }
            }
        }
    }
    
    func extraFunctionsInLeftPanel() {
        Logging.log(message: "[Viewfinder] Extra functions called")
        
        if var motionVisualContent = ParametersConfigurator.visualContentBy(type: .Motion) {
            motionVisualContent.action = { [weak self] in
                self?.showMotionEnumView(sender: $0, newValue: $1)
            }
            leftPanelView.addButtonToTop(visualContent: motionVisualContent)
        }
    }
    
    func setupRightPanel() {
        Logging.log(message: "[Viewfinder] Setup right panel")
        
        if let _ = rightPanelView {
            rightPanelView.removeFromSuperview()
        }
        let frameWidth = frame.width
        
        rightPanelView = ViewfinderRightPanel(frame: CGRect(x:frameWidth - constWidthForPanel,
                                                                   y:0,
                                                                   width:constWidthForPanel,
                                                                   height:frame.height))
        insertSubview(rightPanelView, belowSubview: commonParametersView)
        
        rightPanelView.recButton.addTarget(self,
                                           action: #selector(beginCapture(_:)),
                                           for: .touchUpInside)
        
        let parametersButton = rightPanelView.parametersButton
        parametersButton?.parameterAction = selectorBy(type: .ParametersCommon)
        parametersButton?.visualContentModel?.model = SocketManager.connectedDevice?.parameters.visual
        
        if let _parameters = visualConstraints["right"]?["level0"] {
            for (index, item) in _parameters.enumerated() {
                guard let parameter = item else {
                    return
                }
                
                // TODO: Hack for scrollable ir
                if let max = item?.limits.max, var visualContent = ParametersConfigurator.visualContentBy(type: .IRScrollable), item is IR, max > 4 {
                    visualContent.action = selectorBy(type: visualContent.type)
                    visualContent.model = parameter
                    
                    rightPanelView.addButton(position: ViewfinderRightPanel.RightPanelButtonsPosition(rawValue: index)!,
                                             visualContent: visualContent)
                } else if var visualContent = ParametersConfigurator.visualContentBy(parameter: parameter) {
                    visualContent.action = selectorBy(type: visualContent.type)
                    visualContent.model = parameter

                    rightPanelView.addButton(position: ViewfinderRightPanel.RightPanelButtonsPosition(rawValue: index)!,
                                             visualContent: visualContent)
                }
            }
        }
    }
    
    @objc func timerTick(sender: Timer) {
        DispatchQueue.main.async {
            if (self.isRecord && SocketManager.connectedDevice?.parameters.overlay != nil) ||
                (self.isRecord && self.storeManager.isRecordToPhone) {

                self.recordTime += 1

                if let overlay = SocketManager.connectedDevice?.parameters.overlay, overlay.value == 1 {
                    self.overlayView.recordInfoButton.isHidden = false
                } else {
                    if self.storeManager.isRecordToPhone {
                        self.overlayView.showOnlyRecord(true)
                    }
                }
            } else {
                self.recordTime = 0
                if let overlay = SocketManager.connectedDevice?.parameters.overlay, overlay.value == 1 {
                    self.overlayView.recordInfoButton.isHidden = true
                } else {
                    self.overlayView.isHidden = true
                }
            }
        }
        
        if timeoutForHidding == 0 {
            timeoutForHidding = 5
            hideAllPanels()
        }
        
        if timeoutForHidding > 0 {
            timeoutForHidding -= 1
        }
    }
    
    func formatRecordTime() -> String {
        let recordDate = Date(timeIntervalSince1970: recordTime)
        
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.hour, .minute, .second], from: recordDate)
        let hours = components.hour! - 3
        let minutes = components.minute!
        let seconds = components.second!
        
        let secondString = seconds < 10 ? "0\(seconds)" : "\(seconds)"
        let minuteString = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let hoursString = hours < 10 ? "0\(hours)" : "\(hours)"
        
        var recString = "\(minuteString):\(secondString)"
        if hours > 0 {
            recString = "\(hoursString):" + recString
        }
        return recString
    }
    
    func configureViews() {
        Logging.log(message: "[Viewfinder] Configure view")
        
        visualConstraints = SocketManager.connectedDevice?.parameters.constraints
        
        setupLeftPanel()
        extraFunctionsInLeftPanel()
        
        commonParametersView = ViewfinderEnumView(frame: .zero)
        setupRightPanel()
        warningLabel.text = "Viewfinder.FileViewMode.Title".localized() + "\n" + "Viewfinder.FileViewMode.Subtitle".localized()
        
        timer?.invalidate()
        timer = Timer(timeInterval: 1,
        target: self,
        selector: #selector(timerTick(sender:)),
        userInfo: nil,
        repeats: true)
        RunLoop.current.add(timer!, forMode: .common)

        parameterScrollView = ParametersScrollView(frame: rightPanelView.frame)
        parameterScrollView.blurred = true
        insertSubview(parameterScrollView, belowSubview: rightPanelView)
        
        parameterEnumView = ViewfinderEnumView(frame: rightPanelView.frame)
        parameterEnumView.setupView()
        parameterEnumView.blurred = true
        insertSubview(parameterEnumView, aboveSubview: parameterScrollView)

        commonParametersView.frame = rightPanelView.frame
        commonParametersView.setupView()
        commonParametersView.withSecondLineToLabels = true
        commonParametersView.alpha = 0
        commonParametersView.closeButton.addTarget(self, action: #selector(hideAllPanels), for: .touchUpInside)
        insertSubview(commonParametersView, aboveSubview: rightPanelView)
        
        commonParametersView.recordToPhoneSwitch.isOn = storeManager.isRecordToPhone
        
        if let overlay = SocketManager.connectedDevice?.parameters.overlay, overlay.value == 1 {
            overlayView.isHidden = false
        } else {
            overlayView.isHidden = true
        }
        
        if SocketManager.connectedDevice?.parameters.captureMode?.value == 0 {
            rightPanelView.recButton.isVideoMode = false
        } else if SocketManager.connectedDevice?.parameters.captureMode?.value == 1 {
            rightPanelView.recButton.isVideoMode = true
        } else {
            warningLabel.isHidden = false
            dissapearElementsBothPanels(dissapear: true)
        }
        
        if SocketManager.connectedDevice?.parameters.record?.value == 1, !isRecordToPhone {
            DispatchQueue.main.async {
                self.rightPanelView.recButton.pressed = true
            }
            isRecord = true
        } else {
            DispatchQueue.main.async {
                self.rightPanelView.recButton.pressed = false
            }
            isRecord = false
        }
        
        notificationCenter.addObserver(self,
                                       selector: #selector(paramsUpdated),
                                       name: NSNotification.Name(rawValue: "DeviceParamsUpdated"),
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(resetTimeout),
                                       name: NSNotification.Name ("PanelWasScrolled"),
                                       object: nil)
        
        // Left view
        
        let leftParametersEnumViewFrame = rightPanelView.frame
        leftParametersEnumView = ViewfinderEnumView(frame:
            CGRect(x: -(leftParametersEnumViewFrame.width - leftPanelView.frame.width),
                   y: 0,
                   width: leftParametersEnumViewFrame.width,
                   height: leftParametersEnumViewFrame.height))
        leftParametersEnumView.setupView()
        leftParametersEnumView.blurred = true
        leftParametersEnumView.direction = .right
        leftParametersEnumView.withTitle = true
        insertSubview(leftParametersEnumView, belowSubview: leftPanelView)
        
        leftParametersScrollView = ParametersScrollView(frame: leftParametersEnumViewFrame)
        leftParametersScrollView.blurred = true
        leftParametersScrollView.direction = .right
        leftParametersScrollView.isWithValueLabels = false
        leftParametersScrollView.isWithTitle = false
        leftParametersScrollView.isWithIcons = true
        leftParametersScrollView.isHidden = true
        leftParametersScrollView.animationOffset = 100
        leftParametersScrollView.icons = (top: UIImage(named: "scale_up_active")!.rotate(degree: 90), bottom: UIImage(named: "scale_down_active")!.rotate(degree: 90))
        insertSubview(leftParametersScrollView, belowSubview: leftParametersEnumView)
        
        motionPanelView = MotionDetectionPanelView(frame: CGRect(x: -15,
                                                                 y: 0,
                                                                 width: 80,
                                                                 height: leftPanelView.frame.height))
        motionPanelView?.setupView()
        motionPanelView?.accuracyAction = { button, value in
            button.type = .motionAccuracy
            self.showLeftScrollView(sender: button, newValue: value)
        }
        motionPanelView?.settingsAction = { button, value in
            self.showMotionSettingsView(sender: button, newValue: value)
        }
        
        motionSettingsView = MotionDetectionSettingsPanelView(frame: CGRect(x: -178,
                                                                            y: 0,
                                                                            width: 323,
                                                                            height: frame.height))
        motionSettingsView?.setupView()
    }
    
    @objc private func resetTimeout() {
        timeoutForHidding = 5
    }
    
    func updateView() {
        leftPanelView.updateButtonsValue()
        rightPanelView.updateButtonsValue()
        
        parameterEnumView.reloadWithoutOffset()
        commonParametersView.reloadWithoutOffset()
        parameterScrollView.updateScroll()
        
        if SocketManager.connectedDevice?.parameters.record?.value == 1 && !isRecordToPhone {
            rightPanelView.recButton.pressed = true
            isRecord = true
        } else if !isRecordToPhone {
            rightPanelView.recButton.pressed = false
            isRecord = false
        }
        
        if !warningLabel.isHidden {
            warningLabel.isHidden = true
            dissapearElementsBothPanels(dissapear: false)
        }
        
        if SocketManager.connectedDevice?.parameters.captureMode?.value == 0 {
            rightPanelView.recButton.isVideoMode = false
        } else if SocketManager.connectedDevice?.parameters.captureMode?.value == 1 {
            rightPanelView.recButton.isVideoMode = true
        } else {
            warningLabel.isHidden = false
            dissapearElementsBothPanels(dissapear: true)
        }
    }
    
    func updateViewForX(orientation: UIDeviceOrientation, stub: Bool = false) {
        switch orientation {
        case .landscapeLeft: updateViewForX(orientation: .landscapeRight)
        case .landscapeRight: updateViewForX(orientation: .landscapeLeft)
        default: break
        }
    }
    
    func updateViewForX(orientation: UIInterfaceOrientation) {
        hideAllPanels()
        
        var rightPanelOffset: CGFloat = 0.0
        var leftPanelOffset: CGFloat = 0.0
        switch orientation {
        case .landscapeRight:
            leftPanelOffset = 30
        case .landscapeLeft:
            rightPanelOffset = 30
        default: break
        }
        rightPanelView.frameX(frame.width - constWidthForPanel - rightPanelOffset)
        commonParametersView.frame = rightPanelView.frame
        parameterEnumView.frame = rightPanelView.frame
        parameterScrollView.frame = rightPanelView.frame
        guestModeRightPanelView?.frame = rightPanelView.frame
        
        leftPanelView.frameX(leftPanelOffset)
        if let _ = motionPanelView {
            motionSettingsView!.frame = leftPanelView.frame
            motionPanelView!.frame = leftPanelView.frame
            leftParametersScrollView.frame = leftPanelView.frame
            leftParametersEnumView.frame = motionPanelView!.frame
        }
        motionPanelView?.animationOffset = leftPanelOffset
        motionSettingsView?.animationOffset = leftPanelOffset
        overlayView.updateForX(orientation: orientation)
    }
    
    @objc func paramsUpdated() {
        DispatchQueue.main.async { [weak self] in
            self?.updateView()
        }
    }
    
    
    // MARK: Record to phone handler
    @objc func recordToPhoneUpdate(notification: Notification) {
        let recordOnDevice: Bool? = SocketManager.connectedDevice?.parameters.record?.value == 1
        switch (isRecordToPhone, rightPanelView.recButton.pressed, recordOnDevice) {
        case (true, false, _), (true, true, _), (false, false, false), (false, true, false):
            rightPanelView.recButton.pressed = false
            isRecord = false
        case (false, true, true), (false, false, true):
            rightPanelView.recButton.pressed = true
            isRecord = true
        default: break
        }
        
        recordCallback?(false)
    }
    
    @objc fileprivate func beginCapture(_ sender: RecButton) {
        guard !isRecordToPhone else {
            if (SocketManager.connectedDevice?.parameters.captureMode?.value == 0) {
                if let callback = snapshotCallback {
                    callback()
                    sender.pressed = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        sender.pressed = false
                    }
                }
            } else {
                if let callback = recordCallback {
                    sender.pressed = !sender.pressed
                    callback(sender.pressed)
                    
                    if sender.pressed {
                        self.isRecord = true
                    } else {
                        self.isRecord = false
                    }
                }
            }
            
            return
        }
        
        if (SocketManager.connectedDevice?.parameters.captureMode?.value == 0) {
            DispatchQueue.main.async {
                sender.pressed = true
                SocketManager.send(.doSnapshot).response { _, _ in
                    let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        sender.pressed = false
                    }
                }
            }
        } else {
            if !sender.pressed {
                sender.pressed = true
                SocketManager.send(.startRecord).response { [weak self] model, error in
                    if error == nil {
                        SocketManager.connectedDevice?.parameters.record?.value = 1
                        DispatchQueue.main.async { [weak self] in
                            self?.isRecord = true
                        }
                    } else {
                        SocketManager.connectedDevice?.parameters.record?.value = 0
                        DispatchQueue.main.async {
                            sender.pressed = false
                        }
                    }
                }
            } else {
                sender.pressed = false
                SocketManager.send(.stopRecord).response { [weak self] model, error in
                    if error == nil {
                        SocketManager.connectedDevice?.parameters.record?.value = 0
                        DispatchQueue.main.async { [weak self] in
                            self?.isRecord = false
                        }
                    } else {
                        DispatchQueue.main.async { 
                            sender.pressed = true
                        }
                    }
                }
            }
        }
    }

    
    @objc private func openSideMenu() {
        if let revealController = self.revealViewController {
            revealController.revealToggle(animated: true)
        }
    }
    
    @IBAction func goToFullscreen() {
        for view in [commonParametersView,
                     parameterScrollView,
                     parameterEnumView,
                     leftPanelView,
                     rightPanelView,
                     leftParametersEnumView,
                     motionPanelView,
                     motionSettingsView] as [UIView?] {
        view?.isHidden = !(view?.isHidden ?? false)
        }
    }
    
    func goToGuestMode() {
        guard SocketManager.isGuestMode else {
            guard SocketManager.connectedDevice?.parameters.captureMode?.value != 2 else {
                return
            }
            
            leftPanelView.isGuestAppereance = false
            
            guestModeRightPanelView?.removeFromSuperview()
            guestModeRightPanelView = nil
            return
        }
        hideAllPanels()
        leftPanelView.isGuestAppereance = true
        
        guestModeRightPanelView = GuestModeRightPanelView(frame: rightPanelView.frame)
        guestModeRightPanelView?.removeFromSuperview()
        
        addSubview(guestModeRightPanelView!)
    }
    
    @IBAction func tapOnStream(_ sender: UITapGestureRecognizer) {
        hideAllPanels()
    }
    
    @objc func hideAllPanels() {
        let realm = try? Realm()
        try? realm?.write {
            SocketManager.connectedDevice?.parameters.visual.value = -1.0
        }
        
        commonParametersView.disappear()
        parameterScrollView.displayed = false
        parameterEnumView.displayed = false
        rightPanelView.deselectAll()
        motionPanelView?.deselectAll()
        
        if let panelView =  motionPanelView {
            panelView.hide {
                panelView.frameX(-1000)
            }
        }
        if let _ = leftParametersScrollView {
            leftParametersScrollView.displayed = false
        }
        if let _ = motionSettingsView {
            motionSettingsView?.hide {
                self.motionSettingsView?.frameX(-CGFloat.infinity)
            }
        }

        if let _ = leftParametersEnumView {
            leftParametersEnumView.displayed = false
        }
    }
    
    func dissapearElementsBothPanels(dissapear: Bool) {
        guard !SocketManager.isGuestMode else {
            return
        }
        
        hideAllPanels()
        leftPanelView.isGuestAppereance = dissapear
        rightPanelView.isGuestAppereance = dissapear
    }
    
    // MARK: Temporary solution for ovveriding in restreaming
    func captureParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var videoModel = ParametersConfigurator.visualContentBy(type: .CaptureModeVideo)
        videoModel?.action = enumAction
        videoModel?.sender = sender
        videoModel?.model = SocketManager.connectedDevice?.parameters.captureMode

        var photoModel = ParametersConfigurator.visualContentBy(type: .CaptureModePhoto)
        photoModel?.action = enumAction
        photoModel?.sender = sender
        photoModel?.model = SocketManager.connectedDevice?.parameters.captureMode

        var streamModel: ParameterVisualContent? = ParametersConfigurator.visualContentBy(type: .CaptureModeStream)
        streamModel?.action = goToLivestreaming
        streamModel?.sender = sender
        streamModel?.model = SocketManager.connectedDevice?.parameters.captureMode
        
        // VERY VERY HOT FIX
        
        if let version =  SocketManager.connectedDevice?.info.version,
            SocketManager.isDevice18, version.major < 2 {
            streamModel = nil
        } else if SocketManager.isConnection(.demo) {
            streamModel = nil
        }

        return [photoModel, videoModel, streamModel].compactMap({
            return $0
        })
    }
    
    deinit {
        timer?.invalidate()
        Logging.log(message: "[Viewfinder Configurator] - Deinit")
    }
}

extension ViewfinderPanelsConfigurator {
    
    // MARK: Base methods for showing parameters
    
    fileprivate func showScrollParameterView(sender: ParameterButton, model: Parameter?, callBackAction: (() -> ())? ) {
        guard !parameterScrollView.displayed, let _ = model else {
            parameterScrollView.displayed = false

            rightPanelView.deselectAll()
            return
        }
        rightPanelView.deselectAll()
        sender.isSelected = true

        parameterScrollView.displayed = true
        parameterScrollView.model = model
        parameterScrollView.changeCallBack = callBackAction
        parameterScrollView.titleLabel.text = sender.title
        parameterScrollView.isWithIcons = false
        
        parameterEnumView.displayed = false

        debugPrint("Scroll View Opened")
    }
    
    fileprivate func showLeftScrollParameterView(sender: ParameterButton, model: Parameter, callBackAction: (() -> ())? ) {
        
        if let displayed = motionSettingsView?.displayed, displayed {
            motionSettingsView?.hide {
                self.motionSettingsView?.removeFromSuperview()
            }
        }
        
        guard !leftParametersScrollView.displayed else {
            leftParametersScrollView.displayed = false
            return
        }
//        leftPanelView.deselectAll()
        sender.isSelected = true
        
        leftParametersScrollView.frame = motionPanelView!.frame
        leftParametersScrollView.displayed = true
        leftParametersScrollView.model = model
        leftParametersScrollView.changeCallBack = callBackAction
        leftParametersScrollView.titleLabel.text = sender.title
        leftParametersScrollView.isWithIcons = true
        
        //parameterEnumView.displayed = false
        
        debugPrint("Scroll View Opened")
    }

    fileprivate func showFirstEnumParameterView(_ data: [ParameterVisualContent]) {
        hideAllPanels()
        
        commonParametersView.reload(data)
        commonParametersView.appear()
    }
    
    fileprivate func showSecondEnumParameterView(_ data: [ParameterVisualContent], sender: ParameterButton?) {
        guard !parameterEnumView.displayed else {
            parameterEnumView.displayed = false
            
            sender?.isSelected = false
            return
        }
        rightPanelView.deselectAll()
        sender?.isSelected = true
        
        parameterScrollView.displayed = false
        
        parameterEnumView.reload(data)
        parameterEnumView.displayed = true
        parameterEnumView.withTitle = true
        parameterEnumView.titleLabel.text = sender?.title
        
        debugPrint("Second Level View Opened")
    }
}

extension ViewfinderPanelsConfigurator {
    
    fileprivate func showMotionEnumView(sender: ParameterButton, newValue: Float?) {
        timeoutForHidding = 5
        
        if let displayed = motionPanelView?.displayed, displayed {
            if let _ = leftParametersScrollView {
                leftParametersScrollView.displayed = false
            }
            motionSettingsView?.displayed = false
            motionPanelView?.deselectAll()
            
            motionPanelView?.hide {
                self.motionPanelView?.removeFromSuperview()
            }
        } else {
            motionPanelView?.frame = CGRect(x: -15,
                                            y: 0,
                                            width: 80,
                                            height: leftPanelView.frame.height)
            insertSubview(motionPanelView!, belowSubview: leftPanelView)
            motionPanelView?.displayed = true
        }
    }
    
    // MARK: Setup for showing parameter methods
    fileprivate func showEnumView(sender: ParameterButton, newValue: Float?) {
        Logging.log(message: "[ViewfinderConfigurator] Sender: \(String(describing: sender.type))" +
            " newValue: \(String(describing: newValue))" +
            "title: \(String(describing: sender.title))",
            toFabric: true)
        
        guard let senderType = sender.type else {
            return
        }
        timeoutForHidding = 5
        
        // Hack for selection in common view
        if commonParametersView.isAppear {
            if var value = newValue {
                if parameterEnumView.displayed {
                    value = -1
                }
                
                let realm = try? Realm()
                try? realm?.write {
                    SocketManager.connectedDevice?.parameters.visual.value = value
                }
            }
            commonParametersView.reloadWithoutOffset()
        }
        commonParametersView.withSwitch = false
        switch senderType {
        case .CaptureModeCommon:
            commonParametersView.withSwitch = true
            
            showFirstEnumParameterView(captureParameters(sender: sender))
        case .ParametersCommon:
            showFirstEnumParameterView(commonParameters(sender: sender))
        case .IRCommon:
            showSecondEnumParameterView(irParameters(sender: sender), sender: sender)
        case .ColorsPaletteCommon:
            showSecondEnumParameterView(colorsPaletteParameters(sender: sender), sender: sender)
        case .RecognitionCommon:
            showSecondEnumParameterView(recognitionParameters(sender: sender), sender: sender)
        case .ReticleColorCommon:
            showSecondEnumParameterView(reticleColorParameters(sender: sender), sender: sender)
        case .DistancesCommon:
            showSecondEnumParameterView(distanceParameters(sender: sender), sender: sender)
        case .ReticleTypeCommon:
            showSecondEnumParameterView(reticleTypeParameters(sender: sender), sender: sender)
            
        default:
            debugPrint("Parameter type not found")
            return
        }
        
    }
    
    fileprivate func showScrollView(sender: ParameterButton, newValue: Float?) {
        Logging.log(message: "[ViewfinderConfigurator] Sender: \(String(describing: sender.type))" +
            " newValue: \(String(describing: newValue))" +
            "title: \(String(describing: sender.title))",
            toFabric: true)
        
        guard let senderType = sender.type else {
            return
        }
        timeoutForHidding = 5
        
        // TODO: Hack for selection in common view
        if commonParametersView.isAppear {
            if var value = newValue {
                if parameterScrollView.displayed {
                    value = -1
                }
                
                let realm = try? Realm()
                try? realm?.write {
                    SocketManager.connectedDevice?.parameters.visual.value = value
                }
            }
            commonParametersView.reloadWithoutOffset()
        }
        
        parameterScrollView.pipModel = nil
        
        switch senderType {
        case .ZoomCommon:
            parameterScrollView.pipModel = SocketManager.connectedDevice?.parameters.pip
            showScrollParameterView(sender: sender,
                                    model: SocketManager.connectedDevice?.parameters.zoom,
                                    callBackAction: scrollCallBack)
        case .BrightnessCommon:
            showScrollParameterView(sender: sender,
                                    model: SocketManager.connectedDevice?.parameters.brightness,
                                    callBackAction: scrollCallBack)
        case .ContrastCommon:
            showScrollParameterView(sender: sender,
                                    model: SocketManager.connectedDevice?.parameters.contrast,
                                    callBackAction: scrollCallBack)
        case .ReticleBrightnessCommon:
            showScrollParameterView(sender: sender,
                                    model: SocketManager.connectedDevice?.parameters.reticleBrightness,
                                    callBackAction: scrollCallBack)
        case .IRScrollable:
            showScrollParameterView(sender: sender,
                                    model: SocketManager.connectedDevice?.parameters.ir,
                                    callBackAction: scrollCallBack)
            
        default:
            Logging.log(message: "[Viewfinder configurator] - showScrollView Parameter not found")
            return
        }
        
    }
    
    fileprivate func showLeftScrollView(sender: ParameterButton, newValue: Float?) {
        timeoutForHidding = 5
        
        switch sender.type! {
        case .motionAccuracy:
            if let accuracy = SocketManager.connectedDevice?.parameters.motionAccuracy {
                showLeftScrollParameterView(sender: sender, model: accuracy, callBackAction: scrollCallBack)
            }
            
        default:
            debugPrint("Parameter type not found")
            return
        }
        
    }
    
    fileprivate func showMotionSettingsView(sender: ParameterButton, newValue: Float?) {
        timeoutForHidding = 5
        
        if let _ = leftParametersScrollView {
            leftParametersScrollView.displayed = false
        }
        if let displayed = motionSettingsView?.displayed, displayed {
            motionSettingsView?.hide {
                self.motionSettingsView?.removeFromSuperview()
            }
        } else {
            motionSettingsView?.frame = CGRect(x: -178,
                                              y: 0,
                                              width: 323,
                                              height: frame.height)
            insertSubview(motionSettingsView!, belowSubview: motionPanelView ?? leftPanelView)
            motionSettingsView?.displayed = true
        }
        
    }
    
    // MARK: Data for showing parameters
    private func irParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var visualContentArray: [ParameterVisualContent] = []
        
        Logging.log(message: "[ViewfinderConfigurator] Create IR Parameters", toFabric: true)
        
        if let model = SocketManager.connectedDevice?.parameters.ir {
            for index in 0...Int(model.limits.max) {
                let title = model.limits.localizedValues.map({ $0 })[index]
                let visualContent = ParameterVisualContent(type: .IRCommon,
                                                           model: model,
                                                           title: title.localized(),
                                                           isWithTitleValue: false,
                                                           isWithLocalizedValue: false,
                                                           normalImage: UIImage(named: "vf_ir." + String(index) ),
                                                           selectedImage: UIImage(named: "vf_ir_sel." + String(index) ),
                                                           action: enumAction,
                                                           sender: sender)
                visualContentArray.append(visualContent)
            }
        }
        
        return visualContentArray
    }
    
    // TODO: Uncomment when swift be able to support overriding in extensions
//    func captureParameters(sender: ParameterButton) -> [ParameterVisualContent] {
//        
//        var videoModel = ParametersConfigurator.visualContentBy(type: .CaptureModeVideo)
//        videoModel.action = enumAction
//        videoModel.sender = sender
//        videoModel.model = deviceConfig?.parameters.captureMode
//        
//        var photoModel = ParametersConfigurator.visualContentBy(type: .CaptureModePhoto)
//        photoModel.action = enumAction
//        photoModel.sender = sender
//        photoModel.model = deviceConfig?.parameters.captureMode
//        
//        var streamModel = ParametersConfigurator.visualContentBy(type: .CaptureModeStream)
//        streamModel.action = goToLivestreaming
//        streamModel.sender = sender
//        streamModel.model = deviceConfig?.parameters.captureMode
//        
//        return [photoModel, videoModel, streamModel]
//    }
    
    func motionParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var active = ParametersConfigurator.visualContentBy(type: .motionActivate)
        active?.action = motionActiveAction
        active?.sender = sender
        //active?.model = deviceConfig?.parameters.motionActive

        var accuracy = ParametersConfigurator.visualContentBy(type: .motionAccuracy)
        accuracy?.action = showLeftScrollView
        accuracy?.sender = sender
        //accuracy?.model = deviceConfig?.parameters.motionAccuracy

        var settings = ParametersConfigurator.visualContentBy(type: .motionSettings)
        settings?.action = goToMotionSettings
        settings?.sender = sender

        return [active, accuracy, settings].compactMap({
            return $0
        })
    }
    
    private func commonParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var visualContentArray: [ParameterVisualContent] = []
        sender.visualContentModel = ParameterVisualContent(type: .ParametersCommon,
                                                           model: SocketManager.connectedDevice?.parameters.visual,
                                              title: "",
                                              isWithTitleValue: false,
                                              isWithLocalizedValue: false,
                                              normalImage: nil,
                                              selectedImage: nil,
                                              action: nil,
                                              sender: nil)

        if let _parameters = visualConstraints["right"]?["level1"] {
            for (_, item) in _parameters.enumerated() {
                guard let parameter = item else {
                    continue
                }
                
                if var visualContent = ParametersConfigurator.visualContentBy(parameter: parameter) {
                    visualContent.action = selectorBy(type: visualContent.type)
                    visualContent.model = parameter
                    visualContent.sender = sender

                    visualContentArray.append(visualContent)
                }
            }
        }
        return visualContentArray
    }
    
    private func colorsPaletteParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var visualContentArray: [ParameterVisualContent] = []
        if let model = SocketManager.connectedDevice?.parameters.colorsPalette {
            for index in 0...Int(model.limits.max) {
                let title = model.limits.localizedValues.map({ $0 })[index]
                let visualContent = ParameterVisualContent(type: .ColorsPaletteCommon,
                                                           model: model,
                                                           title: title.localized(),
                                                           isWithTitleValue: false,
                                                           isWithLocalizedValue: false,
                                                           normalImage: UIImage(named: title + "_normal" ),
                                                           selectedImage: UIImage(named: title  + "_selected" ),
                                                           action: enumAction,
                                                           sender: sender)
                visualContentArray.append(visualContent)
            }
        }
        
        return visualContentArray
    }
    
    private func recognitionParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var visualContentArray: [ParameterVisualContent] = []
        if let model = SocketManager.connectedDevice?.parameters.recognition {
            for index in 0...Int(model.limits.max) {
                let title = model.limits.localizedValues.map({ $0 })[index]
                let visualContent = ParameterVisualContent(type: .RecognitionCommon,
                                                           model: model,
                                                           title: title.localized(),
                                                           isWithTitleValue: false,
                                                           isWithLocalizedValue: false,
                                                           normalImage: UIImage(named: "vf_mode." + String(index) ),
                                                           selectedImage: UIImage(named: "vf_mode_sel." + String(index) ),
                                                           action: enumAction,
                                                           sender: sender)
                visualContentArray.append(visualContent)
            }
        }
        
        return visualContentArray
    }
    
    private func reticleColorParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var visualContentArray: [ParameterVisualContent] = []
        if let model = SocketManager.connectedDevice?.parameters.reticleColor {
            for index in 0...Int(model.limits.max) {
                let title = model.limits.localizedValues.map({ $0 })[index]
                let visualContent = ParameterVisualContent(type: .ReticleColorCommon,
                                                           model: model,
                                                           title: title.localized(),
                                                           isWithTitleValue: false,
                                                           isWithLocalizedValue: false,
                                                           normalImage: UIImage(named: "ic_metka_color"),
                                                           selectedImage: UIImage(named: "ic_metka_color_sel"),
                                                           action: enumAction,
                                                           sender: sender)
                visualContentArray.append(visualContent)
            }
        }
        
        return visualContentArray
    }
    
    private func distanceParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var visualContentArray: [ParameterVisualContent] = []
        if let model = SocketManager.connectedDevice?.parameters.distances {
            for index in 0..<Int(model.distances.count) {
                let title = "\(model.distances[index])"
                let visualContent = ParameterVisualContent(type: .DistancesCommon,
                                                           model: model,
                                                           title: title.localized(),
                                                           isWithTitleValue: false,
                                                           isWithLocalizedValue: false,
                                                           normalImage: UIImage(named: "ic_pristrelka"),
                                                           selectedImage: UIImage(named: "ic_pristrelka_sel"),
                                                           action: enumAction,
                                                           sender: sender)
                visualContentArray.append(visualContent)
            }
        }
        
        return visualContentArray
    }
    
    private func reticleTypeParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        var visualContentArray: [ParameterVisualContent] = []
        if let model = SocketManager.connectedDevice?.parameters.reticleType {
            for index in 0...Int(model.limits.max) {
                let title = model.limits.localizedValues.map({ $0 })[index]
                let visualContent = ParameterVisualContent(type: .ReticleTypeCommon,
                                                           model: model,
                                                           title: title.localized(),
                                                           isWithTitleValue: false,
                                                           isWithLocalizedValue: false,
                                                           normalImage: UIImage(named: "ic_metka_type"),
                                                           selectedImage: UIImage(named: "ic_metka_type_sel"),
                                                           action: enumAction,
                                                           sender: sender)
                visualContentArray.append(visualContent)
            }
        }
        
        return visualContentArray
    }
}

extension ViewfinderPanelsConfigurator {
    
    fileprivate func scrollCallBack() {
        rightPanelView.updateButtonsValue()
        
        if commonParametersView.isAppear {
            commonParametersView.reloadWithoutOffset()
        }
    }
    
    func enumAction(sender: ParameterButton, newValue: Float?) {
        guard let model = sender.visualContentModel!.model else {
            return
        }
        timeoutForHidding = 5
        
        switch sender.type! {
        case .CaptureModePhoto, .CaptureModeVideo:
            commonParametersView.disappear()
            
            warningLabel.isHidden = true
            dissapearElementsBothPanels(dissapear: false)
            if SocketManager.connectedDevice?.parameters.captureMode?.value == 1, newValue == 0 {
                rightPanelView.recButton.pressed = false
                isRecord = false
            }
            
            if newValue == 0 {
                rightPanelView.recButton.isVideoMode = false
            } else {
                rightPanelView.recButton.isVideoMode = true
            }
            
            sender.visualContentModel?.model?.update(value: newValue ?? model.value) { success in
                sender.visualContentModel?.sender?.doActionAnimation()

                self.parameterEnumView.reloadWithoutOffset()
                self.commonParametersView.reloadWithoutOffset()
            }
            
        case .IRCommon,
             .ColorsPaletteCommon,
             .RecognitionCommon,
             .ReticleColorCommon,
             .DistancesCommon,
             .ReticleTypeCommon:

            sender.visualContentModel?.model?.update(value: newValue ?? model.value) { success in
                sender.visualContentModel?.sender?.doActionAnimation()

                self.parameterEnumView.reloadWithoutOffset()
                self.commonParametersView.reloadWithoutOffset()
            }
            
        default:
            break
        }
        
    }
    
    fileprivate func goToLivestreaming(sender: ParameterButton, newValue: Float?) {
        let storyboard = UIStoryboard(name: "Conditions", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as? RestreamingNavigationController
        
        
        if let live = SocketManager.connectedDevice?.parameters.live ?? SocketManager.connectedDevice?.parameters.stream, live.value == 1 {
            if #available(iOS 11.0, *) {
                vc?.route = .new
            } else {
                showRestreamingLimitedAlert()
                return
            }
        } else {
            vc?.route = .old
        }
        
        revealViewController!.frontViewController = vc
    }
    
    func showRestreamingLimitedAlert() {
        let alert = UIAlertController(title: "Stream.UsageLimitation.Alert.Title".localized(),
                                      message: "Stream.UsageLimitation.Alert.Message".localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                      style: .default,
                                      handler: nil))
        revealViewController?.frontViewController.present(alert, animated: true, completion: nil)
    }
    
    fileprivate func goToMotionSettings(sender: ParameterButton, newValue: Float?) {
        let storyboard = UIStoryboard(name: "Functions", bundle: nil)
        revealViewController!.frontViewController = storyboard.instantiateInitialViewController()
        if let functionsVC = (revealViewController!.frontViewController as? UINavigationController)?.viewControllers.first {
            functionsVC.viewDidLoad()
        }
        (revealViewController!.frontViewController as! UINavigationController).pushViewController(storyboard.instantiateViewController(withIdentifier: "MotionViewController"), animated: false)
    }
    
    fileprivate func displayOffAction(sender: ParameterButton, newValue: Float?) {
        let model = sender.visualContentModel!.model!
        let value = !NSNumber(value: model.value).boolValue
        
        sender.visualContentModel?.model?.update(value: NSNumber(value: value).floatValue ) { success in
            sender.doActionAnimation()
        }
    }
    
    fileprivate func motionActiveAction(sender: ParameterButton, newValue: Float?) {
        if let model = sender.visualContentModel?.model {
            let value = !NSNumber(value: model.value).boolValue
            sender.visualContentModel?.model?.value = NSNumber(value: value).floatValue
        }
    }
    
    fileprivate func shutterAction(sender: ParameterButton, newValue: Float?) {
        SocketManager.send(SocketCommand.shutter).response { _, _ in
            sender.startAnimation()
        }
    }
}
