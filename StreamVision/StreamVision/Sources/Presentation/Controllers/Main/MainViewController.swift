//
//  MainViewController.swift
//  YukonMobile
//
//  Created by Vladislav Chebotaryov on 11/19/15.
//  Copyright © 2015 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD
import NetworkExtension
import Alamofire

#if DEVBUILD
import SwiftKeychainWrapper
#endif

class MainViewController: UIViewController, SocketConnectionSubsriber,
                          UIPopoverPresentationControllerDelegate, RateManager,
                          StoreManager, DeviceManager {

    // Enums
    enum NavigationSegue: String {
        case Devices = "DeviceDetails"
        case Filemanager = "FileManager"
        case Viewfinder = "Viewfinder"
        case Stream = "Conditions"
        case Functions = "Functions"
        case BallisticCalculator = "BallisticCalculator"
    }
    
    private enum Status {
        case deviceConnected
        case guestMode
        case deviceDisconnected
        case demo
    }
    
    // Outlets
    @IBOutlet fileprivate weak var myDevicesButton    :StylizedButton!
    @IBOutlet fileprivate weak var viewfinderButton   :StylizedButton!
    @IBOutlet fileprivate weak var filesManagerButton :StylizedButton!
    @IBOutlet fileprivate weak var calcButton         :StylizedButton!
    @IBOutlet fileprivate weak var moreButton         :StylizedButton!
    @IBOutlet fileprivate weak var statusStackView    :UIStackView!
    
    @IBOutlet fileprivate weak var titleLabel         :UILabel!
    @IBOutlet fileprivate weak var curentVersionLabel :UILabel!
    
    @IBOutlet fileprivate weak var guestLabel: UILabel!
    @IBOutlet fileprivate weak var guestLabelHeightConstraint: NSLayoutConstraint!
    
    private var statusContent: (_ status: Status) -> [UIView] {
        return { status in
            switch status {
            case .deviceConnected:
                let connectedDeviceLabel = UILabel()
                connectedDeviceLabel.textAlignment = .center
                connectedDeviceLabel.textColor = .rgba(118, 146, 168, 1)
                connectedDeviceLabel.text = SocketManager.connectedDevice?.info.name
                return [connectedDeviceLabel]
            case .demo:
                let deviceNameLabel = UILabel()
                deviceNameLabel.textAlignment = .center
                deviceNameLabel.textColor = .rgba(118, 146, 168, 1)
                deviceNameLabel.font = UIFont(name: "Roboto-Regular", size: 14)
                deviceNameLabel.text = SocketManager.connectedDevice?.info.name
                
                let disconnectButton = ButtonWithActionBlock()
                disconnectButton.layer.cornerRadius = 2
                disconnectButton.layer.borderColor = UIColor.white.cgColor
                disconnectButton.layer.borderWidth = 1
                disconnectButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)
                disconnectButton.addAction(to: [.touchUpInside]) { [weak self] (sender) in
                    self?.connectDemo()
                }
                disconnectButton.setTitle("Main.Disconnect".localized(), for: .normal)
                disconnectButton.titleLabel?.minimumScaleFactor = 0.5
                disconnectButton.titleLabel?.adjustsFontSizeToFitWidth = true
                disconnectButton.titleLabel?.lineBreakMode = .byClipping
                
                return [deviceNameLabel, disconnectButton]
            case .guestMode:
                let guestLabel = UILabel()
                guestLabel.textAlignment = .center
                guestLabel.textColor = .rgba(118, 146, 168, 1)
                guestLabel.font = UIFont(name: "Roboto-Regular", size: 17)
                guestLabel.text = "Main.GuestMode".localized()
                
                return [guestLabel]
                
            case .deviceDisconnected:
                let unionLabel = UILabel()
                unionLabel.textAlignment = .center
                unionLabel.textColor = .rgba(118, 146, 168, 1)
                unionLabel.font = UIFont(name: "Roboto-Regular", size: 17)
                unionLabel.text = "Main.Union".localized()
                
                let connectDemoButton = ButtonWithActionBlock()
                connectDemoButton.addAction(to: [.touchUpInside]) { [weak self] (sender) in
                    self?.connectDemo()
                }
                connectDemoButton.layer.cornerRadius = 2
                connectDemoButton.layer.borderColor = UIColor.white.cgColor
                connectDemoButton.layer.borderWidth = 1
                connectDemoButton.setTitle("Main.TryDemoMode".localized(), for: .normal)
                connectDemoButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)
                connectDemoButton.titleLabel?.minimumScaleFactor = 0.5
                connectDemoButton.titleLabel?.adjustsFontSizeToFitWidth = true
                connectDemoButton.titleLabel?.lineBreakMode = .byClipping
                
                let guideButton = ButtonWithActionBlock()
                guideButton.addAction(to: [.touchUpInside]) { [weak self] (sender) in
                    self?.showConnectionGuide()
                }
                guideButton.layer.cornerRadius = 2
                guideButton.backgroundColor = .hex("1E75FE")
                guideButton.setTitle("Main.ConnectYourDevice".localized(), for: .normal)
                guideButton.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)
                guideButton.titleLabel?.minimumScaleFactor = 0.5
                guideButton.titleLabel?.adjustsFontSizeToFitWidth = true
                guideButton.titleLabel?.lineBreakMode = .byClipping
                
                return [guideButton, unionLabel, connectDemoButton]
            }
        }
    }

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEVBUILD
            let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]
            let build = Bundle.main.infoDictionary?["CFBundleVersion"]
            curentVersionLabel.text = "Verison: \(version!) Build: \(build!)"
            curentVersionLabel.isHidden = false
            
            let button: ButtonWithActionBlock = ButtonWithActionBlock(frame: .zero)
            button.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(button)
            NSLayoutConstraint.activate([
                button.topAnchor.constraint(equalTo: view.topAnchor),
                button.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                button.widthAnchor.constraint(equalToConstant: 100),
                button.heightAnchor.constraint(equalToConstant: 40)
                ])
        
            button.setTitle("Share log", for: .normal)
            button.addAction(to: .touchUpInside, actionBlock: { (sender) in
                if let logsFile = Logging.log(message: "Try share log file"), logsFile.exists {
                    let attachments = [logsFile.path.url]
                    let sharingActivity = UIActivityViewController(activityItems: attachments, applicationActivities: nil)
                    self.present(sharingActivity, animated: true, completion: nil)
                }
            })
            
            let wipeButton: ButtonWithActionBlock = ButtonWithActionBlock()
            wipeButton.translatesAutoresizingMaskIntoConstraints = false
            wipeButton.setTitle("Wipe keychain", for: .normal)
            wipeButton.addAction(to: .touchUpInside) { (button) in
                KeychainWrapper.wipeKeychain()
            }
            view.addSubview(wipeButton)
            NSLayoutConstraint.activate([
                wipeButton.topAnchor.constraint(equalTo: button.bottomAnchor),
                wipeButton.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                wipeButton.widthAnchor.constraint(greaterThanOrEqualToConstant: 100),
                wipeButton.heightAnchor.constraint(equalToConstant: 40)
            ])
        #endif
        
        // Migrate plsit to DB
        if !UserDefaults.standard.bool(forKey: "isMigrated") {
            SVProgressHUD.show()
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                MigrationService().migrateFromPlistToDB {
                    ConnectionService.reachability { (status) in
                        switch status {
                            case .reachable:
                            self?.deviceManager.fetch(completionHandler: {
                                SVProgressHUD.dismiss()
                            })
                            default: SVProgressHUD.dismiss()
                        }
                    }

                }
            }
        } else {
            // Fetch new devices
            deviceManager.fetch(completionHandler: nil)
        }
        
        // Update connection mode with store
        if SocketConnection.isDemoPreviosActive {
            SocketManager.connectionMode = .demo
        } else {
            if !SocketManager.isConnected {
                SocketManager.connectionMode = .normal
            }
        }
        
        DeviceStatisticsService.sendStatistics()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        SVProgressHUD.dismiss()

        navigationController?.setNavigationBarHidden(true, animated: false)
        
        if SocketManager.connectionStatus == .terminated {
            let alert = UIAlertController(title: "Main.Connection.Alert.Title".localized(), message: "Main.ConnectionIsLost".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            
            SocketManager.connectionMode = .normal
        }

        SocketManager.subscribe(self, notificationsType: [.DeviceConnected, .DeviceDisconnected, .DeviceAccessUpdated, .InvalidDevice])
        
        if SocketManager.isConnected {
            didFireNotification(notification: .DeviceConnected, model: SocketManager.connectedDevice)
        } else {
            didFireNotification(notification: .DeviceDisconnected, model: SocketManager.connectedDevice)
        }
        
        googleReport(action: "MainScreen_open")
        
        setupView()
        localizeView()
        
        guestLabel.textAlignment = .center
        guestLabel.textColor = .rgba(118, 146, 168, 1)
        guestLabel.font = UIFont(name: "Roboto-Regular", size: 17)
        guestLabel.text = "Main.GuestMode".localized()
        
        // Rate app
        rateManager.mainPresented(isValid: true)(self)
        
        SocketManager.send(.getDeviceInfo)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        SocketManager.unsubscribe(self)
    }

    // MARK: Setup view
    fileprivate func setupView() {
        viewfinderButton.isEnabled = SocketManager.isConnected || SocketManager.isConnection(.demo)
        myDevicesButton.isEnabled = storeManager.devices.count > 0
        
        if (FilesManager.shared.isLocalFilesExist &&
            storeManager.devices.count > 0 ) ||
            SocketManager.isConnected {
            filesManagerButton.isEnabled = true
        } else {
            filesManagerButton.isEnabled = false
        }
        
        if SocketManager.isConnection(.demo) {
            updateViewWithStatus(status: .demo)
        } else if SocketManager.connectionStatus == .connected {
            updateViewWithStatus(status: .deviceConnected)
        } else if SocketManager.connectionStatus == .disconnected {
            updateViewWithStatus(status: .deviceDisconnected)
        }
        
        if SocketManager.isGuestMode {
            guestLabelHeightConstraint.constant = 40
        } else {
            guestLabelHeightConstraint.constant = 0
        }
    }
    
    fileprivate func localizeView() {
        viewfinderButton.setTitle("Main.Viewfinder".localized(), for: .normal)
        myDevicesButton.setTitle("Main.MyDevices".localized(), for: .normal)
        filesManagerButton.setTitle("Main.FilesManager".localized(), for: .normal)
        moreButton.setTitle("Main.More".localized(), for: .normal)
        calcButton.setTitle("Main.BallisticCalculator".localized(), for: .normal)

        titleLabel.text = "Main.Title".localized()
        title = "Main.Navigation.Title".localized();
    }
    
    private func updateViewWithStatus(status: Status) {
        statusStackView.arrangedSubviews.forEach { (view) in
            view.removeFromSuperview()
        }
        
        for view in statusContent(status) {
            statusStackView.addArrangedSubview(view)
        }
    }
    
    // MARK: Socket connection notification
    internal func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        DispatchQueue.main.async { [weak self] in
            switch notification {
            case .DeviceConnected:
                if let _ = self, SocketManager.isConnection(.demo) {
                    self?.updateViewWithStatus(status: .demo)
                } else {
                    self?.updateViewWithStatus(status: .deviceConnected)
                }
            case .DeviceDisconnected:
                self?.updateViewWithStatus(status: .deviceDisconnected)
            case .InvalidDevice:
                self?.updateViewWithStatus(status: .deviceConnected)
                self?.presentInvalidDeviceWarning()
                
            default: break }
            
            self?.setupView()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        debugPrint("[\(type(of: self))] - Prepare segue")
        
        if let identifier = segue.identifier {
            if let navigationSegue = NavigationSegue(rawValue: identifier) {
                let destenationViewController = segue.destination as! BaseNavigationController
                destenationViewController.modalPresentationStyle = .fullScreen
                switch identifier {
                case "Conditions":
                    destenationViewController.setupWithStoryboardWithIdentifier(navigationSegue.rawValue) { viewController in
                        if let vc = viewController as? RestreamingNavigationController {
                            vc.route = sender as? RestreamingRoute
                        }
                    }
                default:
                    destenationViewController.setupWithStoryboardWithIdentifier(navigationSegue.rawValue)
                }
            }
        }
    }
    
    @IBAction func connectDemo() {
        if SocketManager.isConnection(.demo) {
            SocketManager.connectionMode = .normal
        } else {
            SocketManager.connectionMode = .demo
        }
    }
    
    fileprivate func showConnectionGuide() {
        let storyboard = UIStoryboard(name: "Guide", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()

        navigationController?.pushViewController(vc!, animated: true)
    }
    
    fileprivate func presentInvalidDeviceWarning() {
        let alert = UIAlertController(title: nil,
                                      message: "Main.BrokenDevice".localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                      style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func showPopover(sender: UIButton) {
        let popController = MoreViewController(items: [
            MoreItem(title: "Main.Stream".localized(), image: UIImage(named: "main_restreaming")!, action: { [weak self] in
                if let version =  SocketManager.connectedDevice?.info.version,
                   SocketManager.isDevice18, version.major < 2 {
                    let alert = UIAlertController(title: nil, message: "Restreaming.Blocked.Proj18".localized(), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    
                } else if let _ = SocketManager.connectedDevice?.parameters.live ?? SocketManager.connectedDevice?.parameters.stream {
                    if #available(iOS 11.0, *) {
                        self?.performSegue(withIdentifier: "Conditions", sender: RestreamingRoute.new)
                    } else {
                        self?.showRestreamingLimitedAlert()
                    }
                } else {
                    self?.performSegue(withIdentifier: "Conditions", sender: RestreamingRoute.old)
                }
                }, titleOffset: 22, imageOffset: 30, isEnabled: SocketManager.isConnected && !SocketManager.isConnection(.demo)),
            MoreItem(title: "Main.Settings".localized(), image: UIImage(named: "main_settings")!, action: { [weak self] in
                self?.performSegue(withIdentifier: "Functions", sender: nil)
                }, titleOffset: 10, imageOffset: 30)])
        
        // set the presentation style
        popController.modalPresentationStyle = UIModalPresentationStyle.popover
        
        // set up the popover presentation controller
        popController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.down
        popController.popoverPresentationController?.delegate = self
        popController.popoverPresentationController?.sourceView = sender
        popController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: sender.bounds.width, height: sender.bounds.height)
        
        popController.popoverPresentationController?.backgroundColor = .rgba(22, 46, 66, 1)
        
        // present the popover
        self.present(popController, animated: true, completion: nil)
    }
    
    func showRestreamingLimitedAlert() {
        let alert = UIAlertController(title: "Stream.UsageLimitation.Alert.Title".localized(),
                                      message: "Stream.UsageLimitation.Alert.Message".localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                      style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
