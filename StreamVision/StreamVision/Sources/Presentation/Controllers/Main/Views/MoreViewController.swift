//
//  MoreViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 11/13/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

struct MoreItem {
    let title: String?
    let image: UIImage
    let action: () -> ()
    
    let titleOffset: CGFloat
    let imageOffset: CGFloat
    
    let isEnabled: Bool
    
    init(title: String?,
         image: UIImage,
         action: @escaping () -> (),
         titleOffset: CGFloat = 0,
         imageOffset: CGFloat = 0,
         isEnabled: Bool = true) {
        
        self.title = title
        self.image = image
        self.action = action
        self.titleOffset = titleOffset
        self.imageOffset = imageOffset
        self.isEnabled = isEnabled
    }
}

final class MoreViewController: UIViewController {
    private let stackView = UIStackView()
    private var items: [MoreItem] = []
    
    
    init(items: [MoreItem]) {
        super.init(nibName: nil, bundle: nil)
        
        setupView()
        
        for item in items {
            let button = StylizedButton()
            button.setTitle(item.title, for: .normal)
            button.setImage(item.image, for: .normal)
            button.addAction(to: [.touchUpInside]) { sender in
                self.dismiss(animated: false, completion: nil)
                item.action()
            }
            button.titleYOffset = 20
            button.titleYOffset = item.titleOffset
            button.imageYOffset = item.imageOffset
            button.isEnabled = item.isEnabled
            button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 14)
            button.setTitleColor(.rgba(118, 146, 168, 1), for: .normal)
            
            button.titleLabel?.adjustsFontSizeToFitWidth = true
            button.titleLabel?.minimumScaleFactor = 0.5
            button.titleLabel?.lineBreakMode = .byWordWrapping
            
            button.translatesAutoresizingMaskIntoConstraints = false
            button.widthAnchor.constraint(equalToConstant: 100).isActive = true
            button.heightAnchor.constraint(equalToConstant: 100).isActive = true
            
            stackView.addArrangedSubview(button)
            self.items.append(item)
        }
        
        updateViewWidth()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setupView() {
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            //stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            stackView.topAnchor.constraint(equalTo: view.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
        stackView.alignment = .trailing
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 10)
        stackView.isLayoutMarginsRelativeArrangement = true
    }
    
    private func updateViewWidth() {
        self.preferredContentSize = CGSize(width: items.count * 110
            , height: 120)
    }

}
