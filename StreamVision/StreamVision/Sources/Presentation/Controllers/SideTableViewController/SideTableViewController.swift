//
//  SideTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

enum SideScreens: Int {
    case main = 0
    case devices = 1
    case files = 2
    case viewfinder = 3
    case stream = 4
    case functions = 5
    case bc = 6
}

class SideTableViewController: UITableViewController, StoreManager {
    // Variables
    var menuListNames: [String]!
    var selectedScreen: SideScreens? = nil
    private var textColor: UIColor = .white
    var isNightMode: Bool = false {
        didSet {
            if isNightMode {
                view.backgroundColor = .black
                textColor = storeManager.nightModeColor
            } else {
                textColor = .white
                view.backgroundColor = UIColor.colorFromHexString("000D16")
            }
            
            tableView.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        if let _ = selectedScreen {
            tableView.selectRow(at: IndexPath.init(row: selectedScreen!.rawValue, section: 0),
                                animated: false, scrollPosition: .none)
        }
        localizeView()
    }
    
    fileprivate func localizeView() {
        menuListNames = ["Stream Vision",
                         "Navigation.MyDevices".localized(),
                         "Navigation.FilesManager".localized(),
                         "Navigation.Viewfinder".localized(),
                         "Navigation.Stream".localized(),
                         "Navigation.Functions".localized(),
                         "Main.BallisticCalculator".localized(),
        ]
        tableView.reloadData()
    }
    
    fileprivate func disableCell(_ cell: SideMenuTableViewCell) {
        cell.iconImageView.image = cell.iconImageView.image!.withRenderingMode(.alwaysTemplate)
        
        if isNightMode {
            cell.iconImageView.tintColor = UIColor.colorFromHexString("721824")
            cell.titleLabel.isEnabled = true
            cell.titleLabel.textColor = UIColor.colorFromHexString("721824")
        } else {
            cell.iconImageView.tintColor = UIColor.gray
            cell.titleLabel.isEnabled = false
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! SideMenuTableViewCell
        cell.titleLabel.text = menuListNames[(indexPath as NSIndexPath).row]
        cell.titleLabel.textColor = textColor
        
        if isNightMode {
            cell.backgroundColor = .black
            cell.iconImageView.image = cell.iconImageView.image!.withRenderingMode(.alwaysTemplate)
            cell.tintColor = storeManager.nightModeColor
        } else {
            cell.iconImageView.image = cell.iconImageView.image!.withRenderingMode(.alwaysOriginal)
            cell.backgroundColor = UIColor.colorFromHexString("000D16")
        }
        
        if indexPath.item != SideScreens.main.rawValue {
            let colorView = UIView()
            if isNightMode {
                colorView.backgroundColor = .gray
            } else {
                colorView.backgroundColor = UIColor.colorFromHexString("1E75FE")
            }
            cell.selectedBackgroundView = colorView
            
            if ( indexPath.item != SideScreens.devices.rawValue ||
                storeManager.devices.count == 0) && (indexPath.item != SideScreens.files.rawValue) {
                if !SocketManager.isConnected && (indexPath.item != SideScreens.functions.rawValue) &&
                    indexPath.item != SideScreens.bc.rawValue {
                    disableCell(cell)
                }
            }
        } else {
            cell.selectionStyle = .none
            
            if isNightMode {
                cell.separatorView.backgroundColor = .gray
            } else {
                cell.separatorView.backgroundColor = .white
            }
        }
        
        if indexPath.item == SideScreens.files.rawValue {
            if (!FilesManager.shared.isLocalFilesExist && SocketManager.connectionMode != .demo && !SocketManager.isConnected)  || storeManager.devices.count == 0 {
                disableCell(cell)
            }
        }
        
        

        if indexPath.item == SideScreens.stream.rawValue {
            if let version =  SocketManager.connectedDevice?.info.version {
                if SocketManager.isConnection(.demo) || (SocketManager.isDevice18 && version.major < 2) {
                    disableCell(cell)
                }
            } else {
                disableCell(cell)
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedScreen = SideScreens(rawValue: indexPath.item)!
        if indexPath.item == SideScreens.main.rawValue {
            self.dismiss(animated: true, completion: nil)
        }
        
        if ( indexPath.item != SideScreens.devices.rawValue || storeManager.devices.count == 0)
            && indexPath.item != SideScreens.files.rawValue {
        if !SocketManager.isConnected && indexPath.item != SideScreens.functions.rawValue && indexPath.item != SideScreens.bc.rawValue {
            tableView.deselectRow(at: indexPath, animated: false)
            return
        }
        }
        
        if indexPath.item == SideScreens.files.rawValue {
            if (!FilesManager.shared.isLocalFilesExist && SocketManager.connectionMode != .demo && !SocketManager.isConnected) || storeManager.devices.count == 0 {
                tableView.deselectRow(at: indexPath, animated: false)
                return
            }
        }
        
        if indexPath.item == SideScreens.stream.rawValue {
            if let version =  SocketManager.connectedDevice?.info.version {
                if SocketManager.isConnection(.demo) || (SocketManager.isDevice18 && version.major < 2) {
                    tableView.deselectRow(at: indexPath, animated: false)
                    return
                }
            } else {
                tableView.deselectRow(at: indexPath, animated: false)
                return
            }
        }
        
        navigateTo(screen: SideScreens(rawValue: indexPath.item)!)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destenationViewController = segue.destination as? RestreamingNavigationController {
            destenationViewController.route = sender as? RestreamingRoute
        }
    }
    
    private func navigateTo(screen: SideScreens) {
        switch screen {
        case .devices:
            performSegue(withIdentifier: "DeviceDetails", sender: self)
        case .files:
            performSegue(withIdentifier: "FileManager", sender: self)
        case .viewfinder:
            performSegue(withIdentifier: "Viewfinder", sender: self)
        case .stream:
            if let version =  SocketManager.connectedDevice?.info.version,
               SocketManager.isDevice18, version.major < 2 {
                showRestreamingProj18Alert()
            } else if let live = SocketManager.connectedDevice?.parameters.live ?? SocketManager.connectedDevice?.parameters.stream, live.value == 1 {
                if #available(iOS 11.0, *) {
                    performSegue(withIdentifier: "Conditions", sender: RestreamingRoute.new)
                } else {
                    self.showRestreamingLimitedAlert()
                }
            } else {
                performSegue(withIdentifier: "Conditions", sender: RestreamingRoute.old)
            }
        case .functions:
            performSegue(withIdentifier: "Functions", sender: self)
        case .bc:
            performSegue(withIdentifier: "BallisticCalculator", sender: self)
        default: break
        }
    }
    
    func showRestreamingLimitedAlert() {
        let alert = UIAlertController(title: "Stream.UsageLimitation.Alert.Title".localized(),
                                      message: "Stream.DeviceUsageLimitation.Alert.Message".localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                      style: .default,
                                      handler: nil))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let vc = ((appDelegate.window?.rootViewController?.presentedViewController as? BaseNavigationController)?.frontViewController as? UINavigationController)?.topViewController
        vc?.present(alert, animated: true, completion: nil)
    }
    
    func showRestreamingProj18Alert() {
        let alert = UIAlertController(title: nil,
                                      message: "Restreaming.Blocked.Proj18".localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil))
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let vc = ((appDelegate.window?.rootViewController?.presentedViewController as? BaseNavigationController)?.frontViewController as? UINavigationController)?.topViewController
        vc?.present(alert, animated: true, completion: nil)
    }

}
