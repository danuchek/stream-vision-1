//
//  SideMenuTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/1/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var separatorView: UIView!
}
