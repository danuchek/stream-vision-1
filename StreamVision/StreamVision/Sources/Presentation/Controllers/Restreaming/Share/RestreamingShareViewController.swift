//
//  RestreamingShareViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 11/15/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class RestreamingShareViewController: UIViewController {
    @IBOutlet fileprivate weak var shareButton: UIButton!
    @IBOutlet fileprivate weak var startStreamButton: UIButton!
    @IBOutlet fileprivate weak var shareTitleLabel: UILabel!
    @IBOutlet fileprivate weak var textViewField: UITextField!
    
    var liveModel: LiveAuthModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        localizeView()
        
        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        viewTapGestureRecognizer.numberOfTapsRequired = 1
        viewTapGestureRecognizer.isEnabled = true
        viewTapGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(viewTapGestureRecognizer)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc func viewTapped() {
        textViewField.resignFirstResponder()
    }
    
    private func localizeView() {
        textViewField.text = "Restreaming.Sharing.MessagePrefix".localized() + " " + liveModel.live!.shareUrl
        title = "Restreaming.Sharing.Title".localized()
    }
    
    @IBAction func shareAction() {
        //[liveModel.live!.shareUrl as AnyObject,
        let items = [textViewField.text ?? ""] as [Any]
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func showStream() {
        let navigationController = navigateToStream(animated: true) as! UINavigationController
        let restreamingVC = navigationController.viewControllers.first as! RestreamingViewController
        restreamingVC.authModel = liveModel
    }

}
