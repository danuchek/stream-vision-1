//
//  RestreamingFacebookShareViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/15/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

class RestreamingFacebookShareViewController: UIViewController, UITextFieldDelegate {
    
    // Outlets
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textViewTitleLabel: UILabel!
    @IBOutlet weak var shareTitleLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var liveButton: UIButton!
    @IBOutlet weak var signOutButton: UIButton!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    // Const
    let textViewLength: Int = 100
    
    // Auth service
    var authService: FBAuthService!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        localizeView()
    }
    
    private func setupView() {
        liveButton.layer.cornerRadius = 5
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.clipsToBounds = true
        
        textField.delegate = self
        shareButton.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        
        if let userId = authService.userId {
            let urlString = "https://graph.facebook.com/\(userId)/picture?type=square"
            let url = URL(string: urlString)
            avatarImageView.sd_setImage(with: url,
                                        placeholderImage: #imageLiteral(resourceName: "account_facebook_avatar"))
        }
        
        liveButton.titleLabel?.adjustsFontSizeToFitWidth = true
        liveButton.titleLabel?.minimumScaleFactor = 0.5
        liveButton.titleLabel?.lineBreakMode = .byClipping
    }
    
    private func localizeView() {
        textViewTitleLabel.text = "Restreaming.Facebook.Description.Hint".localized()
        shareTitleLabel.text = "Restreaming.Facebook.Share.Streaming.Link".localized()
        shareButton.setTitle("Restreaming.Facebook.Share.Streaming.Link".localized(), for: .normal)
        liveButton.setTitle("Restreaming.Facebook.Share.Go.Live".localized(), for: .normal)
        signOutButton.setTitle("Restreaming.Facebook.Share.Sign.Out".localized(), for: .normal)
        
        textField.attributedPlaceholder =
            NSAttributedString(string: "Restreaming.Facebook.Share.TextView.Placeholder".localized(),
                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.colorFromHexString("2D3F4F").withAlphaComponent(0.4)])
    }
    
    // MARK: Delegate
    
    func textView(_ textView: UITextView,
                  shouldChangeTextIn range: NSRange,
                  replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count < textViewLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    @IBAction func dismissKeyboard() {
        textField.resignFirstResponder()
    }
    
    // MARK: Actions
    
    @objc private func shareAction() {
        var items = [] as [Any]
        if let shareUrl = authService.model?.live?.shareUrl {
            items.append("Restreaming.Sharing.MessagePrefix".localized() + " " + shareUrl)
        }
        
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func toLive() {
        SVProgressHUD.show()
        authService.updateDescription(text: textField.text) { [weak self] in
            self?.showStream()
            SVProgressHUD.dismiss()
        }
    }
    
    func showStream() {
        guard let liveModel = authService.model else {
            return
        }
        let navigationController = navigateToStream(animated: true) as! UINavigationController
        let restreamingVC = navigationController.viewControllers.first as! RestreamingViewController
        restreamingVC.authModel = liveModel
    }
    
    @IBAction func logOut() {
        let alert = UIAlertController(title: "Restreaming.Facebook.SignOutDialog.Title".localized(), message: "Restreaming.Facebook.SignOutDialog.Text".localized(), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                      style: .default,
                                      handler: { [weak self] action in
                                        self?.authService.logOut()
                                        self?.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                      style: .cancel,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}
