//
//  RestreamingAccountsViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 10/2/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

class RestreamingAccountsViewController: UIViewController, UITableViewDelegate {
    enum AccountType: String {
        case youtube = "toYoutubeAccountSegue"
        case share = "toShareSegue"
    }
    
    @IBOutlet weak var loginFacebookButton: UIButton!
    @IBOutlet weak var loginGoogleButton: UIButton!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    let authService: FBAuthService = FBAuthService()
    let googleService: GoogleAuthService = GoogleAuthService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Google analytics
        googleReport(action: "New_RestreamingSelectChannel_open")
        localizeView()
    }
    
    private func localizeView() {
        title = "Restreaming.Accounts.Title".localized()
        
        if authService.isAuthorized {
            loginFacebookButton.setTitle("Restreaming.Account.Facebook.Continue.Button".localized(), for: .normal)
        } else {
            loginFacebookButton.setTitle("Restreaming.Account.Facebook.Login.Button".localized(), for: .normal)
        }
        
        if googleService.isAuthorized {
            loginGoogleButton.setTitle("Restreaming.Account.Youtube.Continue.Button".localized(), for: .normal)
        } else {
            loginGoogleButton.setTitle("Restreaming.Account.Youtube.Login.Button".localized(), for: .normal)
        }
        
        subtitleLabel.text = "Restreaming.Accounts.Subtitle".localized()
        descriptionLabel.text = "Restreaming.Accounts.Description".localized()
    }
    
    private func setupView() {
        loginFacebookButton.layer.cornerRadius = 5
        loginGoogleButton.layer.cornerRadius = 5
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch AccountType(rawValue: segue.identifier!)! {
        case .share:
            if let shareViewController = segue.destination as? RestreamingFacebookShareViewController {
                shareViewController.authService = sender as? FBAuthService
            }
        case .youtube:
            let accountViewController = segue.destination as! RestreamingAccountViewController
            accountViewController.authService = GoogleAuthService()
        }
    }
    
    @IBAction func showFBAccount(sender: UIButton) {
        if authService.isAuthorized {
            SVProgressHUD.show()
            authService.updateRtmpUrl { [weak self] (model) in
                if let _ = model, let weakSelf = self  {
                    self?.performSegue(withIdentifier: AccountType.share.rawValue, sender: weakSelf.authService)
                } else {
                    self?.linkCreationErrorAlert()
                }
                SVProgressHUD.dismiss()
            }
        } else {
            authService.login { [weak self] (model, error) in
                if let _ = model, error == nil, let weakSelf = self {
                    self?.performSegue(withIdentifier: AccountType.share.rawValue, sender: weakSelf.authService)
                } else {
                    // TODO: Error
                }
                SVProgressHUD.dismiss()
            }
        }
    }
    
    private func linkCreationErrorAlert() {
        let alert = UIAlertController(title: nil,
                                      message: "Restreaming.Facebook.Error.Create.Link".localized(),
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                      style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
