//
//  YoutubeValidationViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 10/20/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class YoutubeValidationViewController: UIViewController {
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    var authService: LiveAuthServiceProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup view
        localizeView()
        
        let closeButton = UIButton(type: .custom)
        closeButton.setImage(UIImage(named: "validation_close"), for: .normal)
        closeButton.addTarget(self, action: #selector(closeViewAction), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: closeButton)
        
        authService.presenterViewController = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Google analytics
        googleReport(action: "New_RestreamingYoutubeValidation_open")
    }
    
    fileprivate func localizeView() {
        title = "Restreaming.Account.VerificationYoutube.Title".localized()
        
        subTitleLabel.text = "Restreaming.Account.VerificationYoutube.SubTitle".localized()
        descriptionLabel.text = "Restreaming.Account.VerificationYoutube.Description".localized()
        startButton.setTitle("Restreaming.Account.VerificationYoutube.VerifyButton".localized(), for: .normal)
    }

    @objc private func closeViewAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openYoutubeValidationAction(sender: ActivityButton) {
        sender.showIndicator()
        
        authService.login { (liveModel, error) in
            sender.hideIndicator()
            
            if let _ = error {
                UIApplication.shared.open(
                    URL(string: "https://m.youtube.com/live_streaming_signup")!, options: [:], completionHandler: nil)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
