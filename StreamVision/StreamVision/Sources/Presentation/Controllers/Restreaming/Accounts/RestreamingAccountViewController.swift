//
//  RestreamingChooseAccountViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 10/2/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

class RestreamingAccountViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    enum CellType: Int {
        case account = 0
        case new
    }
    
    @IBOutlet weak var tableView: UITableView!
    var authService: LiveAuthServiceProtocol!
    private var data: [LiveAuthModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Restreaming.Account.Title".localized()
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.allowsMultipleSelectionDuringEditing = false;
        authService.presenterViewController = self
        
        if authService.isAuthorized {
            SVProgressHUD.show()
            
            authService.login() { _, _ in
                self.reloadData()
                SVProgressHUD.dismiss()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Google analytics
        googleReport(action: "New_RestreamingAccount-\(authService.description)_open")
    }
    
    private func reloadData() {
        data = [authService.model].compactMap({ return $0 })
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if data.count + 1 == indexPath.row + 1  {
            let cell = UITableViewCell()
            cell.textLabel?.textColor = .rgba(0, 118, 255, 1)
            cell.backgroundColor = .clear
            cell.textLabel?.text = !authService.isAuthorized ?
            "Restreaming.Account.AddAccount".localized() : "Restreaming.Account.SwitchAccount".localized()
            cell.tag = indexPath.row
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCellIdentifier") as! AccountCell
        cell.configure(user: data.last!.user)
        cell.accessoryType = .disclosureIndicator
        cell.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        
        if data.count + 1 == indexPath.row + 1  {
            return .none
        }
        
        return .delete
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            data.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            authService.logOut()
            reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cellIndex = (indexPath.row == data.count) ? 1 : indexPath.row
        
        switch CellType(rawValue: cellIndex)! {
        case .account:
            guard let _ = authService.model?.live else {
                performSegue(withIdentifier: "YoutubeValidationSegue", sender: nil)
                break
            }
            
            performSegue(withIdentifier: "ShareSegue", sender: nil)
        case .new:
            if authService.isAuthorized {
                authService.logOut()
                reloadData()
            }
            
            authService.login(completion: { (model, error) in
                self.reloadData()
                SVProgressHUD.dismiss()
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "YoutubeValidationSegue":
            let navigationViewController = segue.destination as! UINavigationController
            let viewController = navigationViewController.viewControllers.first as! YoutubeValidationViewController
            viewController.authService = authService
        case "ShareSegue":
            let viewController = segue.destination as! RestreamingShareViewController
            viewController.liveModel = authService.model!
            
        default: break
        }
    }

}
