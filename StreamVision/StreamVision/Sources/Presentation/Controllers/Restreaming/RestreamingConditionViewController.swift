//
//  RestreamingConditionViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/18/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import STZPopupView
import SVProgressHUD

class RestreamingConditionViewController: UIViewController {
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var deviceImageView: UIImageView!
    
    // Variables
    fileprivate let connection: SocketManager = SocketManager.sharedManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let revealController = self.revealViewController() {
            view.addGestureRecognizer(revealController.panGestureRecognizer())
            
            let rearViewController = revealController.rearViewController!
            (rearViewController as! SideTableViewController).selectedScreen = .stream
        }
        
        // Setup view
        localizeView()
        showInternetAlert()
        
        title = "Restreaming.StartScreen.Title".localized()
        deviceImageView.image = SocketManager.connectedDevice?.info.image
        
        let menu = MenuButton()
        menu.addTarget(self, action: #selector(openSideMenu), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Google analytics
        googleReport(action: "New_RestreamingConditions_open")
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(willEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        // Setup navigation
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc fileprivate func willEnterForeground() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            self.showInternetAlert()
        })
    }
    
    fileprivate func showInternetAlert() {
        guard !UserDefaults.standard.bool(forKey: "Streaming.Internet.Usage.Accept") else {
            return
        }
        let internetWarningAlert = UIAlertController(title: "Restreaming.WarningAlert.Title".localized(),
                                                     message: "Restreaming.WarningAlert.Message".localized(),
                                                     preferredStyle: .alert)
        internetWarningAlert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                                     style: .cancel) { _ in
            self.goToMain()
            })
        internetWarningAlert.addAction(
            UIAlertAction(title: "Restreaming.WarningAlert.Accept".localized(),
                          style: .default) { _ in
                                UserDefaults.standard.set(true, forKey: "Streaming.Internet.Usage.Accept")
        })
        present(internetWarningAlert, animated: true, completion: nil)
    }
    
    fileprivate func showNoInternetAlert() {
        let internetWarningAlert = UIAlertController(title: "Restreaming.NoInternetWarning.Title".localized(),
                                                     message: "Restreaming.NoInternetWarning.Message".localized(),
                                                     preferredStyle: .alert)
        internetWarningAlert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(),
                                                     style: .cancel,
                                                     handler: nil))
        internetWarningAlert.addAction(
        UIAlertAction(title: "Restreaming.NoInternetWarning.GoToSettings".localized(), style: .default) { _ in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })
        present(internetWarningAlert, animated: true, completion: nil)
    }
    
    fileprivate func localizeView() {
        subTitleLabel.text = "Restreaming.StartScreen.SubTitle".localized()
        descriptionLabel.text = "Restreaming.StartScreen.Description".localized()
        notesLabel.text = "Restreaming.StartScreen.Notes".localized()
        startButton.setTitle("Restreaming.StartScreen.StartButton".localized(), for: .normal)
    }
    
    // MARK: Navigation Actions
    @IBAction func goToMain() {
        navigateToMain(animated: true)
    }

    @IBAction func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }
    
    @IBAction func checkConditionAction(sender: ActivityButton) {
        sender.showIndicator()
        Logging.log(message: "[LOG] - Check internet")
        ConnectionService.reachability { (status) in
            switch status {
            case .notReachable: self.showNoInternetAlert()
            case .reachable:    self.performSegue(withIdentifier: "AccountsScreenSegue", sender: nil)
            }
            sender.hideIndicator()
        }
    }

}
