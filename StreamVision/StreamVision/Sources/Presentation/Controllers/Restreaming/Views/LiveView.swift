//
//  LiveView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 2/6/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class LiveView: UIView {
    @IBOutlet weak var view: UIView!
    @IBOutlet private weak var prefixLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        xibSetup(with: frame)
    }
    
    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(with: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0 , y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        
        localizeView()
    }

    private func localizeView() {
        prefixLabel.text = "Restreaming.Preview.Live".localized() + ":"
    }
}
