//
//  LockView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 2/6/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class LockView: UIView, StoreManager {
    @IBOutlet weak var view: UIView!
    @IBOutlet private weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var lockTitleLabel: UILabel!
    @IBOutlet weak var unlockTitleLabel: UILabel!

    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup(with: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    fileprivate func xibSetup(with frame: CGRect) {
        view = loadViewFromNib()
        view.frame = CGRect(x: 0 , y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        
        if storeManager.isNightMode {
            arrowImageView.image = UIImage(named: "restreaming_row_unlock")?.withRenderingMode(.alwaysTemplate)
            arrowImageView.tintColor = storeManager.nightModeColor
        }
        
        localizeView()
        
        lockTitleLabel.textColor = .white
        unlockTitleLabel.textColor = .white
    }
    
    private func localizeView() {
        lockTitleLabel.text = "Restreaming.Preview.Lock.Title".localized()
        unlockTitleLabel.text = "Restreaming.Preview.UnLock.Title".localized()
    }

}
