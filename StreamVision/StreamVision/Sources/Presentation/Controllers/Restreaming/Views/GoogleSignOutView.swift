//
//  GoogleSignOutView.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class GoogleSignOutView: UIView {
    // Outlets
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var signOutButton: UIButton!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userEmailLabel: UILabel!
    
    // MARK: Setup view
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup(with: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    fileprivate func xibSetup(with frame: CGRect) {
        let view = loadViewFromNib()
        view.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        
        localizeView()
    }
    
    fileprivate func localizeView() {
        signOutButton.setTitle("Restreaming.Preconditions.AccountPopup.SignOut".localized(), for: .normal)
        titleLabel.text = "Restreaming.Preconditions.AccountPopup.Title".localized()
        descriptionLabel.text = "Restreaming.Preconditions.AccountPopup.Description".localized()
    }
    
    // MARK: Update view
    
    func updateViewFromAuthModel(_ authModel: LiveAuthModel) {
        userEmailLabel.text = authModel.user.email
        userNameLabel.text = authModel.user.name
    }
    
}
