//
//  RestreamingViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/28/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class RestreamingViewController: ViewfinderViewController {
    // Outlets
    @IBOutlet fileprivate weak var restreamingConfigurator: RestreamingPanelsConfigurator!
    var authModel: LiveAuthModel!
    
    override func viewDidLoad() {
        Logging.log(message: "[Restream] viewDidLoad")
        updateViewConfigurator()
        
        super.viewDidLoad()
    }
    
    func updateViewConfigurator() {
        Logging.log(message: "[Restream] Update view configurator")
        
        viewConfigurator = restreamingConfigurator
        restreamingConfigurator.presenterViewController = self
        
        Logging.log(message: "[Restream] \(String(describing: viewConfigurator))")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Logging.log(message: "Restream Appear")
        // Google analytics
        googleReport(action: "RestreamingViewfinder_open")
        
        // Setup left menu panel
        if let revealController = self.revealViewController() {
            view.removeGestureRecognizer(revealController.panGestureRecognizer())
        }
        
        SocketManager.send(.getDeviceInfo)
    }
    
    override func setupStream() {
        streamHandler = StreamHandler(motionDetectionView: motionDetectionView)
        streamHandler.didFirstFrameObtained = { resolution in
            let viewWidth = self.view.frame.width -
                (self.viewConfigurator.leftPanelView.frame.width + self.viewConfigurator.rightPanelView.frame.width)
            let viewHeight = self.view.frame.height
            let k1 = CGFloat(resolution.w) / CGFloat(resolution.h)
            if (viewWidth / k1 <= viewHeight) {
                self.streamHeightLayout.constant = viewWidth / k1
                self.streamWidthLayout.constant = viewWidth
            } else {
                self.streamHeightLayout.constant = viewHeight
                self.streamWidthLayout.constant = k1 * viewHeight
            }
            self.streamHorisontalLayout.constant = (self.viewConfigurator.leftPanelView.frame.width + (viewWidth / 2)) - self.view.center.x
        }
        
        updateStreamService( authModel.live!.rtmpUrl )
        Logging.log(message: "[Restream] Stream setup - OK")
    }

    func updateStreamService(_ rtmpUrl: String) {
        let isH264 = SocketManager.connectedDevice?.parameters.stream?.isH264
        Logging.log(message: "[Restream] Stream setup with h264 - \(String(describing: isH264))")
        
        streamService = StreamService(mode: .restreaming(rtmp: rtmpUrl, streamHandler, isH264: isH264 ?? false))
        streamService.start()
    }
}
