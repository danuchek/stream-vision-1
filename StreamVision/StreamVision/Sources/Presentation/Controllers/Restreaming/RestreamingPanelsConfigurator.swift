//
//  RestreamingPanelsConfigurator.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/15/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class RestreamingPanelsConfigurator: ViewfinderPanelsConfigurator {
    @IBOutlet weak var liveView: LiveView!
    
    weak var presenterViewController: RestreamingViewController!
    private var lockView: LockView!
    private var brightness: CGFloat = 0.0
    private var streamTime: Float = 0.0
    override var isRecordToPhone: Bool {
        return false
    }
    
    override func setupLeftPanel() {
        super.setupLeftPanel()
        Logging.log(message: "[Restream] Setup left panel")
        
        leftPanelView.menuButton.removeTarget(self, action: nil, for: .touchUpInside)
        leftPanelView.menuButton.addTarget(self, action: #selector(showWarningAlert), for: .touchUpInside)
        
        if var unlockVisualContent = ParametersConfigurator.visualContentBy(type: .Lock) {
            unlockVisualContent.action = lock
            leftPanelView.addButton(visualContent: unlockVisualContent)
        }
    }
    
    override func extraFunctionsInLeftPanel() {
        Logging.log(message: "[Restream] Extra functions left panel")
        
        if var motionVisualContent =
            ParametersConfigurator.visualContentBy(type: .Options) {
            
            Logging.log(message: "[Restream] Options visual content got")
            motionVisualContent.action = showOptionsEnumView
            leftPanelView.addButtonToTop(visualContent: motionVisualContent)
        }
    }
    
    fileprivate func showOptionsEnumView(sender: ParameterButton, newValue: Float?) {
        timeoutForHidding = 5
        leftParametersEnumView.displayed = !leftParametersEnumView.displayed
        
        leftParametersEnumView.frameWidth(rightPanelView.frame.width)
        leftParametersEnumView.reload(optionsParameters())
    }

    @objc fileprivate func showWarningAlert() {
        let alert = UIAlertController(title: "Restreaming.StopPreview.Message".localized(),
                                      message: nil,
                                      preferredStyle: .alert)
        let stopAction = UIAlertAction(title: "Restreaming.Preview.Stop".localized(), style: .cancel) { action in
            
            self.presenterViewController.navigateToMain(animated: true)
        }
        alert.addAction(stopAction)
        let cancelAction = UIAlertAction(title: "General.Alert.Cancel".localized(), style: .default) { action in
            
        }
        alert.addAction(cancelAction)
        presenterViewController.present(alert, animated: true, completion: nil)
    }
    
    override func timerTick(sender: Timer) {
        super.timerTick(sender: sender)
        
        updateTimeLabel()
    }
    
    func updateTimeLabel() {
        streamTime += 1
        let recTimeDate = Date(timeIntervalSince1970: Double(streamTime))
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        let formatterString = ((streamTime > 3600) ? "HH:" : "") + "mm:ss"
        formatter.dateFormat = formatterString
        liveView.timeLabel.text = formatter.string(from: recTimeDate)
    }
    
    override func configureViews() {
        super.configureViews()
        Logging.log(message: "[Restream] Configure view")
        
        leftParametersEnumView.titleLabel.text = "Restreaming.Preview.Options.Title".localized()
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(unlockView))
        
        lockView = LockView(frame: CGRect(x: 0, y: 0,
                                          width: frame.width, height: frame.height))
        lockView.addGestureRecognizer(gesture)
        addSubview(lockView)
        
        lockView.isHidden = true
        if storeManager.isNightMode {
            liveView.view.backgroundColor = .black
        }
    }
    
    private func lock(sender: ParameterButton, newValue: Float?) {
        lockView.isHidden = false
        
        brightness = UIScreen.main.brightness
        UIScreen.main.brightness = 0.1
    }
    
    @objc private func unlockView() {
        lockView.isHidden = true
        
        UIScreen.main.brightness = brightness
    }
    
    override func captureParameters(sender: ParameterButton) -> [ParameterVisualContent] {
        commonParametersView.withSwitch = false
        var result = super.captureParameters(sender: sender)
        result.removeLast()
        return result
    }
}

extension RestreamingPanelsConfigurator {
    // MARK: Parameters
    
    func optionsParameters() -> [ParameterVisualContent] {
        Logging.log(message: "[Restream] Options parameters")
        
        var share = ParametersConfigurator.visualContentBy(type: .Share)
        share?.action = sharingAction
        
        Logging.log(message: "[Restream] Share button \(String(describing: share))")

        return [share].compactMap { return $0 }
    }
}

extension RestreamingPanelsConfigurator {
    // MARK: Actions
    
    func sharingAction(sender: ParameterButton, newValue: Float?) {
        var sharingItems = [AnyObject]()
        Logging.log(message: "[Restream] Sharing actions")
        
        sharingItems.append(presenterViewController.authModel.live!.shareUrl as AnyObject)
        let activityViewController = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
        presenterViewController.present(activityViewController, animated: true, completion: nil)
    }
}
