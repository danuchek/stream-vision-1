//
//  HotspotGuideTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/11/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Foundation

class HotspotGuideTableViewController: UITableViewController {
    //Constants
    fileprivate let menuHeight: CGFloat = 36
    
    // Outlets
    @IBOutlet fileprivate weak var subtitleLabel: UILabel!
    @IBOutlet fileprivate weak var step1Label: UILabel!
    @IBOutlet fileprivate weak var step2Label: UILabel!
    @IBOutlet fileprivate weak var step3Label: UILabel!
    @IBOutlet fileprivate weak var step4Label: UILabel!
    @IBOutlet fileprivate weak var step5Label: UILabel!
    
    //UI Variables
    fileprivate let menuView: UIView = UIView(frame: CGRect.zero)
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "Old_RestreamingHotspotGuide_open")
    }
    
    // MARK: View configuration
    fileprivate func localizeView() {
        title = "Restreaming.HotspotGuide.Navigation.Title".localized()
        subtitleLabel.text = "Restreaming.YoutubeEnabling.Subtitle".localized()
        step1Label.text = "restreaming-hotspotguide-step1".localized()
        step2Label.text = "Restreaming.HotspotGuide.Step2".localized()
        step3Label.text = "Restreaming.HotspotGuide.Step3".localized()
        step4Label.text = "Restreaming.HotspotGuide.Step4".localized()
        step5Label.text = "Restreaming.HotspotGuide.Step5".localized()
    }
    
    fileprivate func configureTableView() {
        tableView.estimatedRowHeight = 44
        tableView.contentInset = UIEdgeInsets(top: menuHeight, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    // MARK: Scroll Delegate methods
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //Set [Y] position for menuView when table scrolled
        menuView.frameY(tableView.contentOffset.y)
    }
    
    // MARK: Actions
    @objc fileprivate func backAction() {
        dismiss(animated: true, completion: nil)
    }

}
