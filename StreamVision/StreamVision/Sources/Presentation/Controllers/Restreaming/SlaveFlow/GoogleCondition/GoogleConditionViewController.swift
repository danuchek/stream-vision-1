//
//  GoogleConditionViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/20/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class GoogleConditionTableViewController: UITableViewController {
    //Constants
    fileprivate let menuHeight: CGFloat = 36
    
    // Outlets
    @IBOutlet fileprivate weak var subtitleLabel: UILabel!
    @IBOutlet fileprivate weak var step1TitleLabel: UILabel!
    @IBOutlet fileprivate weak var step1LinkButton: UIButton!
    @IBOutlet fileprivate weak var step2TitleLabel: UILabel!
    @IBOutlet fileprivate weak var step2SubtitleLabel: UILabel!
    @IBOutlet fileprivate weak var step3TitleLabel: UILabel!
    @IBOutlet fileprivate weak var checkStreamingButton: UIButton!
    
    //UI Variables
    fileprivate let menuView: UIView = UIView(frame: CGRect.zero)
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        configureTableView()
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Google analytics
        googleReport(action: "Old_RestreamingGoogleActivationGuide_open")
    }
    
    // MARK: View configuration
    fileprivate func setupView() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        title = "Restreaming.YoutubeEnabling.Title".localized()
    }
    
    fileprivate func localizeView() {
        subtitleLabel.text = "Restreaming.YoutubeEnabling.Subtitle".localized()
        step1TitleLabel.text = "Restreaming.YoutubeEnabling.Step1.Title".localized()
        step1LinkButton.setTitle("Restreaming.YoutubeEnabling.Step1.SighUpLink".localized(), for: .normal)
        step2TitleLabel.text = "Restreaming.YoutubeEnabling.Step2.Title".localized()
        step2SubtitleLabel.text = "Restreaming.YoutubeEnabling.Step2.Subtitle".localized()
        step3TitleLabel.text = "Restreaming.YoutubeEnabling.Step3.Title".localized()
    }
    
    fileprivate func configureTableView() {
        tableView.estimatedRowHeight = 44
        tableView.contentInset = UIEdgeInsets(top: menuHeight, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    // MARK: Scroll Delegate methods
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //Set [Y] position for menuView when table scrolled
        menuView.frameY(tableView.contentOffset.y)
    }
    
    @IBAction func dissmiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openUrl() {
        if let url = URL(string: "https://m.youtube.com/live_streaming_signup") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

}
