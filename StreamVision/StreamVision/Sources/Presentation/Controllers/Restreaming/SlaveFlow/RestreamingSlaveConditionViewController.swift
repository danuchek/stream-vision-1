//
//  RestreamingConditionViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/18/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import STZPopupView
import SVProgressHUD

class RestreamingSlaveConditionViewController: UIViewController {
    // Outlets
    @IBOutlet fileprivate weak var signOut: UIButton!
    @IBOutlet fileprivate weak var letsContinueButton: UIButton!
    @IBOutlet fileprivate weak var slaveConditionImage: UIImageView!
    @IBOutlet fileprivate weak var googleConditionImage: UIImageView!
    @IBOutlet fileprivate weak var shareConditionImage: UIImageView!
    @IBOutlet fileprivate weak var slaveDescriptionLabel: UILabel!
    @IBOutlet fileprivate weak var gooleDescriptionLabel: UILabel!
    @IBOutlet fileprivate weak var shareDescriptionLabel: UILabel!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var subtitleLabel: UILabel!
    
    // Variables
    fileprivate var clientAuth: GoogleAuthService = GoogleAuthService()
    var authModel: LiveAuthModel!
    var conditionModel = ConditionModel() {
        didSet {
            slaveConditionImage.isHighlighted = conditionModel.slaveCondition
            googleConditionImage.isHighlighted = conditionModel.googleCondition
            if conditionModel.slaveCondition {
                slaveDescriptionLabel.text = "Restreaming.Preconditions.Condition.HotSpot.Off".localized()
            }
        }
    }
    fileprivate let connection: SocketManager = SocketManager.sharedManager
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let revealController = self.revealViewController() {
            view.addGestureRecognizer(revealController.panGestureRecognizer())
            
            let rearViewController = revealController.rearViewController!
            (rearViewController as! SideTableViewController).selectedScreen = .stream
        }
        
        // Setup view
        localizeView()
        clientAuth.presenterViewController = self
        if SocketManager.connectionMode == .slave {
            conditionModel.slaveCondition = true
        }
        
        showInternetAlert()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Google analytics
        googleReport(action: "Old_RestreamingConditions_open")
        
        // Setup navigation
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Check if autorized
        if clientAuth.isAuthorized && SocketManager.connectionMode == .slave {
            letsContinueButton.setTitle("Restreaming.Preconditions.BlueButton.LetsContinue".localized(), for: .normal)
            
            SVProgressHUD.show()
            clientAuth.signIn({ authModel, error in
                if error != .auth {
                    self.updateViewWithAuthModel(authModel)
                    
                    if self.clientAuth.isAbleToStream {
                        self.conditionModel.googleCondition = true
                    }
                } else {
                    self.showErrorAlert()
                }
                
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            })
        }
    }
    
    fileprivate func showInternetAlert() {
        let internetWarningAlert = UIAlertController(title: "Restreaming.WarningAlert.Title".localized(), message: "Restreaming.WarningAlert.Message".localized(), preferredStyle: .alert)
        internetWarningAlert.addAction(UIAlertAction(title: "General.Alert.Cancel".localized(), style: .cancel) { _ in
            self.goToMain()
            })
        internetWarningAlert.addAction(UIAlertAction(title: "Restreaming.WarningAlert.Accept".localized(), style: .default, handler: nil))
        present(internetWarningAlert, animated: true, completion: nil)
    }
    
    fileprivate func localizeView() {
        slaveDescriptionLabel.text = "Restreaming.Preconditions.Condition.HotSpot.On".localized()
        gooleDescriptionLabel.text = "Restreaming.Preconditions.Condition.YouTube.Off".localized()
        shareDescriptionLabel.text = "Restreaming.Preconditions.Condition.Share.Description".localized()
        signOut.setTitle("Restreaming.Preconditions.Condition.YouTube.On.SignOut".localized(), for: .normal)
        letsContinueButton.setTitle("Restreaming.Preconditions.BlueButton.GetStarted".localized(), for: .normal)
        titleLabel.text = "Restreaming.Preconditions.Title.Default".localized()
        subtitleLabel.text = "Restreaming.Preconditions.Subtitle.Default".localized()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShareCondition" {
            let destinationViewController = segue.destination as! RestreamingShareViewController
            destinationViewController.liveModel = authModel
        }
    }
    
    fileprivate func updateViewWithAuthModel(_ authModel: LiveAuthModel?) {
        guard let _ = authModel else {
            return
        }
        self.authModel = authModel
        self.signOut.isHidden = false
        self.gooleDescriptionLabel.text = "\("Restreaming.Preconditions.Condition.YouTube.On.Prefix".localized()) \(authModel!.user.email!)"
    }
    
    fileprivate func showErrorAlert() {
        let alert = UIAlertController(title: nil, message: "Restreaming.Preconditions.ChekAcountSettings".localized(), preferredStyle: .alert)
        let okAction = UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    fileprivate func showYoutubeErrorAlert() {
        let alert = UIAlertController(title: nil, message: "Restreaming.Preconditions.ChekYoutubeSettings".localized(), preferredStyle: .alert)
        let okAction = UIAlertAction(title: "General.Alert.Ok".localized(), style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: Navigation Actions
    @IBAction func goToMain() {
        navigateToMain(animated: true)
    }

    @IBAction func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }
    
    @IBAction func nextCondition() {
        if !conditionModel.slaveCondition {
            performSegue(withIdentifier: "SlaveConnection", sender: nil)
            
        } else if !conditionModel.googleCondition {
            clientAuth.signIn() { authModel, error in
                if error == .broadcast {
                    self.updateViewWithAuthModel(authModel)
                    self.performSegue(withIdentifier: "GoogleCondition", sender: nil)
                } else if error == .youtube {
                    self.showYoutubeErrorAlert()
                } else if error == nil {
                    self.updateViewWithAuthModel(authModel)
                    self.conditionModel.googleCondition = true
                } else {
                    self.showErrorAlert()
                }
            }
        } else {
            self.performSegue(withIdentifier: "ShareCondition", sender: nil)
        }
    }
    
    // MARK: Sign out action
    
    @IBAction func signOutAction() {
        let popupView = createSignOutPopup()
        self.presentPopupView(popupView)
    }
    
    fileprivate func createSignOutPopup() -> GoogleSignOutView {
        let popupView = GoogleSignOutView(frame: CGRect(x: 0,y: 0,width: 314,height: 204))
        popupView.center = view.center
        popupView.updateViewFromAuthModel(authModel)
        popupView.signOutButton.addTarget(self, action: #selector(popUpSignOut), for: .touchUpInside)
        return popupView
    }
    
    @objc fileprivate func popUpSignOut() {
        clientAuth.logOut()
        signOut.isHidden = true
        conditionModel.googleCondition = false
        dismissPopupView()
        localizeView()
    }

}
