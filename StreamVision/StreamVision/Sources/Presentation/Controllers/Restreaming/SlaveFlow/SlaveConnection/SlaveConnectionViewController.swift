//
//  RestreamingSlaveConnectionViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/18/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

class SlaveConnectionViewController: UIViewController, ScanLANDelegate {
    //Outlets
    @IBOutlet fileprivate weak var arrowsImageView: UIImageView!
    @IBOutlet fileprivate weak var successView: UIView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var subtitleLabel: UILabel!
    @IBOutlet fileprivate weak var successLabel: UILabel!
    
    //Variables
    fileprivate var scanLan: ScanLAN!
    fileprivate var ipAdresses: [String] = []
    fileprivate var findingIp: Bool = false
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let menu = MenuButton()
        menu.addTarget(self, action: #selector(goToConditions), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)
        
        subscribeToNotifications()
        setupSanLan()
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Google analytics
        googleReport(action: "Old_RestreamingSlaveConnection_open")
        
        scanLan.startScan()
        arrowsImageView.rotate360Degrees()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        
        destroyScanLan()
    }
    
    // MARK: Setup view
    fileprivate func subscribeToNotifications() {
        let defaultCenter = NotificationCenter.default
        defaultCenter.addObserver(self, selector: #selector(applicationDidEnterBackgroundNotification),
                                  name: UIApplication.didEnterBackgroundNotification, object: nil)
        defaultCenter.addObserver(self, selector: #selector(applicationWillEnterForeground),
                                  name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    func localizeView() {
        title = "Restreaming.SlaveCheking.Title".localized()
        subtitleLabel.text = "Restreaming.SlaveCheking.Subtitle".localized()
        successLabel.text = "Restreaming.SlaveCheking.Success".localized()
    }
    
    // MARK: Setup ScanLan
    fileprivate func setupSanLan() {
        scanLan = ScanLAN(delegate: self)
    }
    
    fileprivate func destroyScanLan() {
        if let _ = scanLan {
            scanLan.stopScan()
        }
        scanLan = nil
    }
    
    // MARK: Slave connect logic
    @objc fileprivate func applicationWillEnterForeground() {
        setupSanLan()
        scanLan.startScan()

        UIView.setAnimationsEnabled(true)
        arrowsImageView.rotate360Degrees()
    }
    
    @objc fileprivate func applicationDidEnterBackgroundNotification() {
        destroyScanLan()
    }
    
    // MARK: Delegate methods
    func scanLANDidFindNewAdrress(_ address: String!, havingHostName hostName: String!) {
        ipAdresses.append(address)
        
        if (!findingIp) {
            findingIp = true
            scanIp(address)
        }

        Logging.log(message: "Scan ip")
    }
    
    func scanIp(_ ip: String){
        SocketManager.tryConnectionWithIP(ip) { connected in
            if connected {
                SVProgressHUD.show()
                DispatchQueue.main.async {
                    self.successView.isHidden = false
                    SocketManager.connectionMode = .slave
                    self.destroyScanLan()

                    SocketManager.isDataUpdatedBlock = { _ in
                        SVProgressHUD.dismiss()
                        (self.navigationController?.viewControllers[0] as! RestreamingSlaveConditionViewController).conditionModel.slaveCondition = true
                        
                        self.popToConditions()
                    }

                }
                
                self.arrowsImageView.layer.removeAllAnimations()
                self.arrowsImageView.isHighlighted = true
            } else {
                if self.ipAdresses.count > 0 {
                    self.scanIp(self.ipAdresses.first!)
                    self.ipAdresses.removeFirst()
                    if self.ipAdresses.count == 0 {
                        self.findingIp = false
                    }
                }
            }
        }
    }
    
    func hotspotUnavailable() {
        debugPrint("Hotspot does't active")
    }
    
    // MARK: Actions
    @objc fileprivate func goToConditions() {
        SocketManager.connectionMode = .normal
        popToConditions()
    }

    fileprivate func popToConditions() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
}
