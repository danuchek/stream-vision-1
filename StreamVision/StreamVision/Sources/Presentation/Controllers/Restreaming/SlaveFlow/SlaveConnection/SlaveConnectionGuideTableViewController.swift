//
//  RestreamingSlaveConnectionGuideTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/18/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

class SlaveConnectionGuideTableViewController: UITableViewController, UITextFieldDelegate {
    //Outlets
    @IBOutlet fileprivate weak var copyPasswordButton:  UIButton!
    @IBOutlet fileprivate weak var changePasswordButton:UIButton!
    @IBOutlet fileprivate weak var cantFindItButton:    UIButton!
    @IBOutlet fileprivate weak var checkSharingButton:  UIButton!
    @IBOutlet fileprivate weak var condition1Label:     UILabel!
    @IBOutlet fileprivate weak var condition2Label:     UILabel!
    @IBOutlet fileprivate weak var condition3Label:     UILabel!
    @IBOutlet fileprivate weak var condition4Label:     UILabel!
    @IBOutlet fileprivate weak var conditionInfoLabel:  UILabel!
    @IBOutlet fileprivate weak var conditionTitleLabel: UILabel!

    //Constants
    fileprivate let menuHeight: CGFloat = 36
    
    //Variables
    fileprivate var passwordString = "1234567890"
    fileprivate var changeAction: UIAlertAction!
    
    //UI Variables
    fileprivate let menuView: UIView = UIView(frame: CGRect.zero)

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        localizeView()
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Google analytics
        googleReport(action: "Old_RestreamingSlaveConnectionGuide_open")
    }
    
    // MARK: View configuration
    fileprivate func setupView() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        copyPasswordButton.layer.borderWidth = 1
        copyPasswordButton.layer.cornerRadius = 2
        copyPasswordButton.layer.borderColor = UIColor.colorFromHexString("#169BD5").cgColor
        
        copyPasswordButton.titleLabel?.adjustsFontSizeToFitWidth = true
        copyPasswordButton.titleLabel?.minimumScaleFactor = 0.5
        copyPasswordButton.titleLabel?.lineBreakMode = .byClipping
        
        changePasswordButton.titleLabel?.adjustsFontSizeToFitWidth = true
        changePasswordButton.titleLabel?.minimumScaleFactor = 0.5
        changePasswordButton.titleLabel?.lineBreakMode = .byClipping
        
        cantFindItButton.titleLabel?.adjustsFontSizeToFitWidth = true
        cantFindItButton.titleLabel?.minimumScaleFactor = 0.5
        cantFindItButton.titleLabel?.lineBreakMode = .byClipping
    }
    
    fileprivate func localizeView() {
        condition1Label.text = "Restreaming.SlaveConnection.Step1".localized()
        condition2Label.text = "Restreaming.SlaveConnection.Step2".localized()
        condition4Label.text = "Restreaming.SlaveConnection.Step4".localized()
        condition3Label.text = "\("Restreaming.SlaveConnection.SetupPassword.Prefix".localized()) \(passwordString)"
        cantFindItButton.setTitle("Restreaming.SlaveConnection.Step2.Subtext".localized(), for: .normal)
        copyPasswordButton.setTitle("Restreaming.SlaveConnection.Step3.CopyPassword".localized(), for: .normal)
        changePasswordButton.setTitle("Restreaming.SlaveConnection.Step3.ChangePassword".localized(), for: .normal)
        checkSharingButton.setTitle("Restreaming.SlaveConnection.CheckSharing".localized(), for: .normal)
        conditionInfoLabel.text = "Restreaming.SlaveConnection.Note".localized()
    }
    
    fileprivate func configureTableView() {
        tableView.estimatedRowHeight = 44
        tableView.contentInset = UIEdgeInsets(top: menuHeight, left: 0, bottom: 0, right: 0)
        tableView.scrollIndicatorInsets = tableView.contentInset
    }
    
    // MARK: Scroll Delegate methods
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //Set [Y] position for menuView when table scrolled
        menuView.frameY(tableView.contentOffset.y)
    }
    
    // MARK: Table Delegate methods
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // MARK: Actions
    @IBAction func copyPasswordToPastboard() {
        let pastBoard = UIPasteboard.general
        pastBoard.string = passwordString
    }
    
    @IBAction func changePassword() {
        let changePasswordAlertController = UIAlertController(title: "Restreaming.SlaveConnection.Alert.Title".localized(), message: nil, preferredStyle: .alert)
        changeAction = UIAlertAction(title: "Restreaming.SlaveConnection.AlertAction.Change".localized(), style: .default, handler: { [weak self] action in
            if let weakSelf = self {
                weakSelf.passwordString = changePasswordAlertController.textFields!.first!.text!
                weakSelf.localizeView()
            }
        })
        changePasswordAlertController.addAction(changeAction)
        
        let cancel = UIAlertAction(title: "General.Alert.Cancel".localized(), style: .cancel, handler: nil)
        changePasswordAlertController.addAction(cancel)
        changePasswordAlertController.addTextField { (textField) in
            textField.borderStyle = .none
            textField.delegate = self
            textField.text = self.passwordString
        }
        
        present(changePasswordAlertController, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        changeAction.isEnabled = range.location + -range.length > 6
        return true
    }
    
    @IBAction func switchDeviceToSlaveMode() {
        SVProgressHUD.show()
        view.isUserInteractionEnabled = false

        let hotspotName = UIDevice.current.name.components(separatedBy: " ").joined(separator: "%20")

        SocketManager.connectionMode = .slaveConnection
        SocketManager.send(.setSlaveWifi, params: ["pass":passwordString,"ssid":hotspotName]).response { [weak self] _, _ in
            if let weakSelf = self {
                SocketManager.connectionStatus = .disconnected
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    weakSelf.performSegue(withIdentifier: "SlaveConnection", sender: nil)
                }
                weakSelf.view.isUserInteractionEnabled = true
            }
        }
    }
}
