//
//  RestreamingNavigationViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/9/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import UIKit

enum RestreamingRoute: Int {
    case new
    case old
}

class RestreamingNavigationController: UINavigationController {
    var route: RestreamingRoute!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch route! {
        case .new:
            performSegue(withIdentifier: "toNewRestreaming", sender: nil)
        case .old:
            performSegue(withIdentifier: "toOldRestreaming", sender: nil)
        }
    }
}
