//
//  UserAgreementsViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/29/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//
import WebKit

final class UserAgreementsViewController: UIViewController {
    private var webView: WKWebView!
    var fileName: String!
    var screenTitle: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = screenTitle
        webView = WKWebView(frame: view.frame)
        view.addSubview(webView)
        
        let htmlFile = Bundle.main.path(forResource: fileName, ofType: "html")
        let html = try? String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
        webView.loadHTMLString(html!, baseURL: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "SettingsUserAgreements_open")
    }

}
