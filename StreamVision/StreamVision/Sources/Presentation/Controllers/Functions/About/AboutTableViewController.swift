//
//  AboutTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/25/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import MessageUI

class AboutTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    enum Identifiers: String {
        case version = "Version"
        case updated = "Updated"
        case contact = "Contact"
        case privacy = "Privacy"
        case eula = "Eula"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "SettingsAbout_open")
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        switch Identifiers(rawValue: cell.reuseIdentifier!)! {
        case .version:
            cell.textLabel?.text = "Functions.About.Version".localized()
            cell.detailTextLabel?.text = Bundle.main.object(
                forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        case .updated:
            cell.textLabel?.text = "Functions.About.Updated".localized()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss zzz yyyy"
            dateFormatter.locale = Locale(identifier: "en-GB")
            //            [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en-GB"]];
            let date = dateFormatter.date(from:
                Bundle.main.object(forInfoDictionaryKey: "BuildDateString") as! String)
            dateFormatter.dateFormat = "dd MMM, YYYY"
            cell.detailTextLabel?.text = dateFormatter.string(from: date!)
        case .contact:
            cell.textLabel?.text = "Functions.About.ContactSupport".localized()
        case .privacy:
            cell.textLabel?.text = "Functions.About.PrivacyPolicy".localized()
        case .eula:
            cell.textLabel?.text = "Functions.About.Eula".localized()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath)!
        
        switch Identifiers(rawValue: cell.reuseIdentifier!)! {
        case .contact: sendEmail()
        default: break
        }
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch Identifiers(rawValue: segue.identifier!)! {
        case .privacy:
            let destViewController = segue.destination as! UserAgreementsViewController
            destViewController.fileName = "privacy_policy"
            destViewController.screenTitle = "Functions.About.PrivacyPolicy".localized()
        case .eula:
            let destViewController = segue.destination as! UserAgreementsViewController
            destViewController.fileName = "eula"
            destViewController.screenTitle = "Functions.About.Eula".localized()
        default: break
        }
    }
    
    @IBAction func sendEmail() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["support@pulsar-nv.com"])
        
        return mailComposerVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

}
