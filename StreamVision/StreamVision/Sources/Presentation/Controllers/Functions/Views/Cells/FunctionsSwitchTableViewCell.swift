//
//  FunctionsSwitchTableViewCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/12/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

class FunctionsSwitchWithDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var cellSwitch: UISwitch!
    
    var cellAction: ((_ isOn: Bool) -> ())?
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if let action = cellAction {
            action(sender.isOn)
        }
    }
}

class FunctionsSwitchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var cellSwitch: UISwitch!
    
    var cellAction: ((_ isOn: Bool) -> ())?
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if let action = cellAction {
            action(sender.isOn)
        }
    }
}
