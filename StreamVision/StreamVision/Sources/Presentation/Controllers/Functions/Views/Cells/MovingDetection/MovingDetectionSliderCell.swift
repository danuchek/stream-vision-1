//
//  MovingDetectionSwipeCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/24/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class MovingDetectionSliderCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var sliderView: UISlider!
    
    var sliderValueChanged: ((Float) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        sliderView.minimumValue = 0
        sliderView.maximumValue = 1
    }
    
    @IBAction func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began: break
            case .moved: break
            case .ended: sliderValueChanged?(slider.value)
            default: break
            }
        }
    }

}
