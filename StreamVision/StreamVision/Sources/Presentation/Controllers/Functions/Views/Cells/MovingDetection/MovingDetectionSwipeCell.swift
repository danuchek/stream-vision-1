//
//  MovingDetectionSwipeCell.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/24/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class MovingDetectionSwipeCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var switchView: UISwitch!

    var switchValueChanged: ((Bool) -> ())?
    
    @IBAction func switchValueChanged(sender: UISwitch) -> () {
        switchValueChanged?(sender.isOn)
    }

}
