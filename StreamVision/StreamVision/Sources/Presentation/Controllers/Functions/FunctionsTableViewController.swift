//
//  FunctionsTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/31/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import Localize_Swift
import MessageUI
import RealmSwift

class FunctionsTableViewController: UITableViewController,
                                    MFMailComposeViewControllerDelegate {
    enum Sections: Int {
        case section0 = 0
        case section1 = 1
        case section2 = 2
    }
    
    enum ReuseIdentifier: String {
        case language = "Language"
        case movingDetection = "Motion"
        case rotate = "Rotate"
        case nightmode = "Nightmode"
        case privacy = "Privacy"
        case eula = "Eula"
        case whatsNew = "WhatsNew"
        case support = "Support"
    }
    
    // Variables
    fileprivate var dataSource: [Sections: [String] ] {
        get {
            
            return [.section2 : ["Settings.Whats.New.In".localized(),
                                 "Functions.About.ContactSupport".localized(),
                                 "Functions.Language.Title".localized()],
                    .section1 : ["Functions.About.PrivacyPolicy".localized(),
                                 "Functions.About.Eula".localized()],
                    .section0 : [//"Functions.RotateVideo".localized(),
                                 "Functions.NightMode.Title".localized(),
                                 "Functions.MotionDetection.Title".localized()]
                   ]
        }
    }
    var model = MovingDetectionModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let revealController = self.revealViewController() {
            view.addGestureRecognizer(revealController.panGestureRecognizer())
            let rearViewController = revealController.rearViewController!
            (rearViewController as! SideTableViewController).selectedScreen = .functions
        }
        
        let menu = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        menu.setImage(UIImage(named: "navigation_menu"), for: UIControlState())
        menu.setImage(UIImage(named: "navigation_menu_tap"), for: UIControlState.highlighted)
        menu.addTarget(self, action: #selector(openSideMenu), for: UIControlEvents.touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)
        
        let realm = try! Realm()
        if let motionModel = realm.objects(MovingDetectionModel.self).last {
            model = motionModel
        } else {
            try! realm.write {
                realm.add(model)
            }
        }
        
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        googleReport(action: "Settings_open")
        
        tableView.reloadData()
        localizeView()
    }

    fileprivate func localizeView() {
        navigationItem.title = "Functions.Title".localized()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = dataSource[Sections(rawValue: section)!] {
            return data.count
        }
        return 0
    }
    
    // MARK: Data source
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        let reuseIdentifier = ReuseIdentifier(rawValue: cell.reuseIdentifier!)!
        
        let dataModel = dataSource[Sections(rawValue: indexPath.section)!]!
        switch reuseIdentifier {
        case .language:
            cell.textLabel?.text = dataModel[indexPath.row]
            cell.detailTextLabel?.text = Localize.currentLanguage()
            
        case .movingDetection:
            cell.textLabel?.text = dataModel[indexPath.row]
            cell.detailTextLabel?.text = model.isMovingDetectionActive ?
                "Functions.MotionDetection.IsActive.On".localized() :
                "Functions.MotionDetection.IsActive.Off".localized()
            
        case .rotate:
            let rotateCell = cell as? FunctionsSwitchTableViewCell
            rotateCell?.titleLabel.text = dataModel[indexPath.row]
            rotateCell?.cellSwitch.isOn = StoreManager.sharedStore.isInvertedVideoStream
            rotateCell?.cellAction = { isOn in
                StoreManager.sharedStore.isInvertedVideoStream = isOn
            }
            
        case .nightmode:
            let nightMode = cell as? FunctionsSwitchWithDetailsTableViewCell
            nightMode?.titleLabel.text = dataModel[indexPath.row]
            nightMode?.detailsLabel.text = "Functions.NightMode.Subtitle".localized()
            nightMode?.cellSwitch.isOn = StoreManager.sharedStore.isNightMode
            nightMode?.cellAction = { isOn in
                StoreManager.sharedStore.isNightMode = isOn
            }
            
        case .privacy:
            cell.textLabel?.text = dataModel[indexPath.row]
        case .eula:
            cell.textLabel?.text = dataModel[indexPath.row]
        case .whatsNew:
            if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? NSString {
                cell.textLabel?.text = String(format: dataModel[indexPath.row], version)
            }
        case .support:
            cell.textLabel?.text = dataModel[indexPath.row]
        }
        return cell

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath)!
        
        switch ReuseIdentifier(rawValue: cell.reuseIdentifier!)! {
        case .support: sendEmail()
        case .whatsNew:
            let alert = UIAlertController(title: "iOS.Notifications.WhatsNewAlert.Title".localized(),
                                          message: "iOS.Notifications.WhatsNewAlert.Text".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default,
                                          handler: nil))
            present(alert, animated: true, completion: nil)
        default: break
        }
    }
    
    @IBAction func sendEmail() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["support@pulsar-nv.com"])
        
        return mailComposerVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Navigation
    @objc fileprivate func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Functions.Viewfinder".localized()
        case 1: return "Functions.PrivacyConditions".localized()
        case 2: return "Functions.About.Title".localized()
        default: return "Section not found"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch ReuseIdentifier(rawValue: identifier)! {
            case .privacy:
                let destViewController = segue.destination as! UserAgreementsViewController
                destViewController.fileName = "privacy_policy"
                destViewController.screenTitle = "Functions.About.PrivacyPolicy".localized()
            case .eula:
                let destViewController = segue.destination as! UserAgreementsViewController
                destViewController.fileName = "eula"
                destViewController.screenTitle = "Functions.About.Eula".localized()
            default: break
            }
        }
    }

}
