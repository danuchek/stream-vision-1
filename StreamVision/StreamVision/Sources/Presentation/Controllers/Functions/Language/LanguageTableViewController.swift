//
//  LanguageTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/1/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import Localize_Swift

class LanguageTableViewController: UITableViewController {
    // Variables
    fileprivate var languages: [String] = Localize.availableLanguages().sorted(stable: true)
    fileprivate var defaultSetting = "Functions.Language.Default"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        languages.insert(defaultSetting, at: 0)
        
        localizeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        googleReport(action: "SettingsLanguages_open")
    }
    
    fileprivate func localizeView() {
        navigationItem.title = "Functions.Language.Title".localized()
        navigationItem.rightBarButtonItem = UIBarButtonItem()
        navigationController?.navigationBar.backItem?.title = "Functions.Title".localized()
    }
    
    // MARK: Data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    // MARK: Section
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let langCode = languages[indexPath.row]
        
        let cell = tableView.dequeueReusableCell( withIdentifier: "LanguageCell", for: indexPath )
        
        if UserDefaults.standard.value(forKey: "LCLCurrentLanguageKey") == nil {
            cell.accessoryType = defaultSetting == langCode ? .checkmark : .none
        } else {
            cell.accessoryType = Localize.currentLanguage() == langCode ? .checkmark : .none
        }
        
        if let langName = NSLocale(localeIdentifier: langCode).displayName(forKey: NSLocale.Key.identifier,
                                                                           value: langCode) {
            cell.textLabel?.text = String(langName.first!).capitalized + langName.dropFirst()
        } else {
            cell.textLabel?.text = Bundle.main.localizedString(forKey: defaultSetting, value: nil, table: nil)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let langCode = languages[indexPath.row]
        if langCode == defaultSetting {
            Localize.resetCurrentLanguageToDefault()
            UserDefaults.standard.set(nil, forKey: "LCLCurrentLanguageKey")
        } else {
            Localize.setCurrentLanguage(langCode)
        }
        localizeView()
        tableView.reloadData()
    }
}
