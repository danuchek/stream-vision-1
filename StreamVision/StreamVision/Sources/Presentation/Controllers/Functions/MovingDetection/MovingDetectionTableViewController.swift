//
//  MovingDetectionTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/24/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import RealmSwift
import MediaPlayer

extension MovingDetectionTableViewController {
    func startObservingVolumeChanges() {
        audioSession.addObserver(self, forKeyPath: Observation.VolumeKey, options: [.initial, .new], context: &Observation.Context)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &Observation.Context {
            if keyPath == Observation.VolumeKey, let _ = (change?[NSKeyValueChangeKey.newKey] as? NSNumber)?.floatValue {
                tableView.reloadData()
            }
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    func stopObservingVolumeChanges() {
        audioSession.removeObserver(self, forKeyPath: Observation.VolumeKey, context: &Observation.Context)
    }
}

class MovingDetectionTableViewController: UITableViewController {
    var model: MovingDetectionModel!
    let audioSession = AVAudioSession.sharedInstance()
    fileprivate struct Observation {
        static let VolumeKey = "outputVolume"
        static var Context = 0
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        if let motionModel = realm.objects(MovingDetectionModel.self).last {
            model = motionModel
        } else {
            model = MovingDetectionModel()
            try! realm.write {
                realm.add(model)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        do {
            try audioSession.setActive(true)
            startObservingVolumeChanges()
        } catch {
            print("Failed to activate audio session")
        }
        
        googleReport(action: "SettingsMotionDetection_open")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopObservingVolumeChanges()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        switch indexPath.row {
        case 0:
            let enableMovingDetectionCell = cell as! MovingDetectionSwipeCell
            enableMovingDetectionCell.titleLabel.text = "Functions.MotionDetection.Activation.Title".localized()
            enableMovingDetectionCell.subTitleLabel.text = "Functions.MotionDetection.Activation.Subtitle".localized()
            enableMovingDetectionCell.switchView.isOn = model.isMovingDetectionActive
            enableMovingDetectionCell.switchValueChanged = { value in
                let realm = try! Realm()
                try! realm.write {
                    self.model.isMovingDetectionActive = value
                }
            }
        case 1:
            let detectionAccuracyCell = cell as! MovingDetectionSliderCell
            detectionAccuracyCell.titleLabel.text = "Functions.MotionDetection.Accuracy.Title".localized()
            detectionAccuracyCell.subTitleLabel.text = "Functions.MotionDetection.Accuracy.Subtitle".localized()
            detectionAccuracyCell.sliderView.minimumValue = 0.2
            detectionAccuracyCell.sliderView.maximumValue = 0.8
            detectionAccuracyCell.sliderView.value = Float(model.detectionAccuracy)
            detectionAccuracyCell.sliderValueChanged = { value in
                let realm = try! Realm()
                try! realm.write {
                    self.model.detectionAccuracy = value
                }
            }
            
        case 2:
            let soundIndicationCell = cell as! MovingDetectionSwipeCell
            soundIndicationCell.titleLabel.text = "Functions.MotionDetection.Sound.Title".localized()
            soundIndicationCell.switchView.isOn = model.isSoundIndication
            soundIndicationCell.switchValueChanged = { value in
                let realm = try! Realm()
                try! realm.write {
                    self.model.isSoundIndication = value
                }
            }
            
        case 3:
            let soundVolumeCell = cell as! MovingDetectionSliderCell
            soundVolumeCell.titleLabel.text = "Functions.MotionDetection.Volume.Title".localized()
            soundVolumeCell.sliderView.value = audioSession.outputVolume
            soundVolumeCell.sliderValueChanged = { value in
                let realm = try! Realm()
                try! realm.write {
                    MPVolumeView.setVolume(value)
                }
            }
            
        case 4:
            let vibroIndicationCell = cell as! MovingDetectionSwipeCell
            vibroIndicationCell.titleLabel.text = "Functions.MotionDetection.Vibro.Title".localized()
            vibroIndicationCell.switchView.isOn = model.isVibroIndication
            vibroIndicationCell.switchValueChanged = { value in
                let realm = try! Realm()
                try! realm.write {
                    self.model.isVibroIndication = value
                }
            }
            
        default: break
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
