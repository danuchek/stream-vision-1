//
//  FunctionsTableViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/31/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import Localize_Swift
import MessageUI
import RealmSwift
import SVProgressHUD

extension FunctionsViewController: StoreManager {}
class FunctionsViewController: UIViewController,
                               MFMailComposeViewControllerDelegate,
                               UITableViewDelegate,
                               UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    enum Sections: Int {
        case viewfinder
        case privacy
        case about
    }
    
    enum ReuseIdentifier: String {
        case language = "Language"
        case movingDetection = "Motion"
        case rotate = "Rotate"
        case nightMode = "Nightmode"
        case privacy = "Privacy"
        case eula = "Eula"
        case whatsNew = "WhatsNew"
        case support = "Support"
    }
    
    typealias DataSourceItems = [Sections : [ReuseIdentifier: FunctionsCellProtocol] ]
    var dataSourceItemsDictionary: DataSourceItems = [:]
    var model = MovingDetectionModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        if let motionModel = realm.objects(MovingDetectionModel.self).last {
            model = motionModel
        } else {
            try! realm.write {
                realm.add(model)
            }
        }
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataSourceItemsDictionary = getDataSourceItems()
        googleReport(action: "Settings_open")
        
        tableView.reloadData()
        localizeView()
    }
    
    fileprivate func localizeView() {
        navigationItem.title = "Functions.Title".localized()
    }
    
    fileprivate func setupView() {
        if let revealController = self.revealViewController() {
            view.addGestureRecognizer(revealController.panGestureRecognizer())
            let rearViewController = revealController.rearViewController!
            (rearViewController as! SideTableViewController).selectedScreen = .functions
        }
        
        let menu = MenuButton()
        menu.addTarget(self, action: #selector(openSideMenu),
                       for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menu)
        
        tableView.estimatedRowHeight = 140
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    fileprivate func getDataSourceItems() -> DataSourceItems {
        var sections: DataSourceItems = [:]
        
        sections[.about] = [.whatsNew: WhatsNewCellModel(),
                            .support: AboutCellModel(),
                            .language: LanguageCellModel()]
        
        sections[.privacy] = [.privacy: PrivacyCellModel(),
                              .eula: EulaCellModel()]
        
        sections[.viewfinder] = [//.rotate: RotateVideoCellModel(),
                                 .nightMode: NightModeCellModel(),
                                 .movingDetection: MotionDetectionCellModel()]
        return sections
    }
    
    // MARK: Data source
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return dataSourceItemsDictionary[ Sections(rawValue: section)! ]!.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceItemsDictionary.keys.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)!
        let data = dataSourceItemsDictionary[section]!
        
        let reuseIdentifier = Array(data.keys).sorted(by: { $0.rawValue > $1.rawValue }, stable: true)[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier.rawValue, for: indexPath)
        let dataModel = data[reuseIdentifier]!
        let title = dataModel.title.localized()
        
        switch reuseIdentifier {
        case .language:
            cell.textLabel?.text = title
            if UserDefaults.standard.value(forKey: "LCLCurrentLanguageKey") == nil {
                cell.detailTextLabel?.text = "Functions.Language.Default".localized()
            } else {
                cell.detailTextLabel?.text = Localize.currentLanguage()
            }
        case .movingDetection:
            cell.textLabel?.text = title
            cell.detailTextLabel?.text = model.isMovingDetectionActive ?
                "Functions.MotionDetection.IsActive.On".localized() :
                "Functions.MotionDetection.IsActive.Off".localized()
        case .rotate:
            let rotateCell = cell as? FunctionsSwitchTableViewCell
            rotateCell?.titleLabel.text = title
            rotateCell?.cellSwitch.isOn = storeManager.isInvertedVideoStream
            rotateCell?.cellAction = { [weak self] isOn in
                self?.storeManager.isInvertedVideoStream = isOn
            }
        case .nightMode:
            let nightMode = cell as? FunctionsSwitchWithDetailsTableViewCell
            nightMode?.titleLabel.text = title
            nightMode?.detailsLabel.text = "Functions.NightMode.Subtitle".localized()
            nightMode?.cellSwitch.isOn = storeManager.isNightMode
            nightMode?.cellAction = { [weak self] isOn in
                self?.storeManager.isNightMode = isOn
            }
            
        case .privacy:
            cell.textLabel?.text = title
        case .eula:
            cell.textLabel?.text = title
        case .whatsNew:
            if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? NSString {
                cell.textLabel?.text = String(format: title.replacingOccurrences(of: "%s", with: "%@"), version)
            }
        case .support:
            cell.textLabel?.text = title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath)!
        
        switch ReuseIdentifier(rawValue: cell.reuseIdentifier!)! {
        case .support:
            sendEmail()
        case .whatsNew:
            let alert = UIAlertController(title: "iOS.Notifications.WhatsNewAlert.Title".localized(),
                                          message: "iOS.Notifications.WhatsNewAlert.Text".localized(),
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "General.Alert.Ok".localized(),
                                          style: .default,
                                          handler: nil))
            present(alert, animated: true, completion: nil)
        case .privacy:
            let destViewController = UserAgreementsViewController()
            destViewController.fileName = "privacy_policy"
            destViewController.screenTitle = "Functions.About.PrivacyPolicy".localized()
            navigationController?.pushViewController(destViewController, animated: true)
        case .eula:
            let destViewController = UserAgreementsViewController()
            destViewController.fileName = "eula"
            destViewController.screenTitle = "Functions.About.Eula".localized()
            navigationController?.pushViewController(destViewController, animated: true)
            
        default: break
        }
    }
    
    @IBAction func sendEmail() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["support@pulsar-nv.com"])
        
        return mailComposerVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    // MARK: Navigation
    
    @objc fileprivate func openSideMenu() {
        if let revealController = self.revealViewController() {
            revealController.revealToggle(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Functions.Viewfinder".localized()
        case 1: return "Functions.PrivacyConditions".localized()
        case 2: return "Functions.About.Title".localized()
        default: return "Section not found"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            switch ReuseIdentifier(rawValue: identifier)! {
            case .privacy:
                let destViewController = segue.destination as! UserAgreementsViewController
                destViewController.fileName = "privacy_policy"
                destViewController.screenTitle = "Functions.About.PrivacyPolicy".localized()
            case .eula:
                let destViewController = segue.destination as! UserAgreementsViewController
                destViewController.fileName = "eula"
                destViewController.screenTitle = "Functions.About.Eula".localized()
            default: break
            }
        }
    }
    
}
