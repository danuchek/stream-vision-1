//
//  ViewController.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/22/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import SWRevealViewController

extension UINavigationController {
    override open var childForHomeIndicatorAutoHidden: UIViewController? {
        return topViewController
    }
}

class BaseNavigationController: SWRevealViewController {

    func setupWithStoryboardWithIdentifier(_ identifier: String) {
        Logging.log(message: "[BaseNavigation] Init with \(identifier)", toFabric: true)
        
        let storyboard = UIStoryboard(name: identifier, bundle: nil)
        frontViewController = storyboard.instantiateInitialViewController()
    }
    
    func setupWithStoryboardWithIdentifier(_ identifier: String, setup block: (UIViewController?) -> () ) {
        Logging.log(message: "[BaseNavigation] Init with \(identifier)", toFabric: true)
        
        let storyboard = UIStoryboard(name: identifier, bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        block(vc)
        frontViewController = vc
    }
    
        override open var childForHomeIndicatorAutoHidden: UIViewController? {
            return frontViewController
        }
}
