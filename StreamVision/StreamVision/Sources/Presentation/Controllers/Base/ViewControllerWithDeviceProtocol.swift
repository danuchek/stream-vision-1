//
//  ViewControllerWithDeviceProtocol.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/27/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

protocol ViewControllerWithDeviceProtocol {
    var deviceModel: VirtualDevice! { get set }
}
