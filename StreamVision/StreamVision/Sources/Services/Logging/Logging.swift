//
//  Logging.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/30/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

import FileKit
import FirebaseCrashlytics

class LogOperation: ConcurrentOperation {
    let message: String
    let logsFile: TextFile
    
    init(logsFile: TextFile, message: String) {
        self.logsFile = logsFile
        self.message = message
    }
    
    override func main() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            if let logsFile = self?.logsFile, !logsFile.exists {
                try? logsFile.create()
                try? "[\(Date())] ===== Start logs =====" |>> logsFile
                
                if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String {
                    try? "VERSION \(version)" |>> logsFile
                }
                if let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String {
                    try? "BUILD \(build)" |>> logsFile
                }
            } else if let logsFile = self?.logsFile {
                try? "[\(Date())] \(String(describing: self?.message));" |>> logsFile
            }
            self?.finish()
        }
    }
}

class LoggingQueue: OperationQueue {
    override init() {
        super.init()
        
        maxConcurrentOperationCount = 1
    }
}

class Logging {
    static let logQueue = LoggingQueue()
    
    @discardableResult
    class func log(message: String,
                   toFile: Bool = true,
                   toFabric: Bool = false) -> TextFile? {
        
        if toFabric {
            Crashlytics.crashlytics().log(format: "%@", arguments: getVaList([message]))
        }

        return Logging.log(message: message, toFile: toFile)
    }
    
    @discardableResult
    class func log(message: String, toFile: Bool = true) -> TextFile? {
        var logsFile: TextFile? = nil
        #if DEVBUILD
        logsFile = TextFile(path: Path.userHome + "/Documents/" + "logs.txt")
        
        debugPrint(message)
        if let logsFile = logsFile, toFile {
            Logging.logQueue.addOperation(LogOperation(logsFile: logsFile, message: message))
        }
        #endif
        
        return logsFile
    }
}
