//
//  entropy.hpp
//  MotionDetectionResearch
//
//  Created by Vladislav Chebotaryov on 6/9/17.
//  Copyright © 2017 Vladislav Chebotaryov. All rights reserved.
//

#ifndef entropy_hpp
#define entropy_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

cv::Mat myEntropy(cv::Mat seq, int histSize);
float calcEntropy(cv::Mat seq, cv::Size size, int index);

#endif /* entropy_hpp */
