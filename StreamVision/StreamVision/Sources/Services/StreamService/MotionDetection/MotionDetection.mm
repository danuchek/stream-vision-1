//
//  MotionDetection.m
//  MotionDetectionResearch
//
//  Created by Vladislav Chebotaryov on 6/9/17.
//  Copyright © 2017 Vladislav Chebotaryov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MotionDetection.hpp"
#import <opencv2/bioinspired/bioinspired.hpp>
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/opencv.hpp>
#import "Entropy.hpp"
#import <vector>
#include <iostream>
#include <stdio.h>
#import <CoreGraphics/CoreGraphics.h>

#define MD_DEBUG NO
#define IMAGE_SIZE 120

@interface MotionDetection() {
    cv::Ptr<cv::bioinspired::Retina> cvRetina;
}

@property (nonatomic, assign) int frameNumber;

@end

@implementation MotionDetection

static float meanBuffer[512];
static float entropyBuffer[512];

- (void)setupRetinaWidth:(NSInteger)width height:(NSInteger)height {
    self.isSetuped = YES;
    
    cvRetina = createRetina(cv::Size(IMAGE_SIZE, IMAGE_SIZE), false, cv::bioinspired::RETINA_COLOR_BAYER, false, 10.0, 10.0);
    
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [searchPaths objectAtIndex:0];
    documentPath = [documentPath stringByAppendingString:@"/RetinaDefaultParameters.xml"];
    const char *path = [documentPath UTF8String];
    
    cvRetina->write(path);
    // загруждаем настройки
    cvRetina->setup(path);
    // очищаем буфер
    cvRetina->clearBuffers();
    
    cv::bioinspired::RetinaParameters params = cvRetina->getParameters();
    // Цветной или ЧБ
    params.OPLandIplParvo.colorMode = 0;
    // Чувствительность фоторецепторов
    params.OPLandIplParvo.photoreceptorsLocalAdaptationSensitivity = 1;
    // Фильтра шумов в кадрах
    params.OPLandIplParvo.photoreceptorsTemporalConstant = .1;
    // Что то для фильтра шума в пикселях
    params.OPLandIplParvo.photoreceptorsSpatialConstant = 0.4; // 0.2
    // 0.6 до 1 какая -то чувтсивтельность
    params.OPLandIplParvo.ganglionCellsSensitivity = 0.7; // 0.7
    // Хз что
    params.IplMagno.V0CompressionParameter = 5; // 0.6
    // Хз что
    params.IplMagno.amacrinCellsTemporalCutFrequency = 5;
    cvRetina->setup(params);
     
}

- (void)processImage:(UIImage *)image {
    cv::Mat matImage;
    cv::Mat imgTemp;
    
    cv::Mat rawImage;
    cv::Mat grayImage;
    cv::Mat gaussianImage;
    
    cv::Mat parvoImageMat;
    float medianEntropy, medianMean; // отфильтрованные значения
    
    UIImageToMat(image, rawImage);
    
    cv::cvtColor(rawImage, grayImage, cv::COLOR_BGRA2GRAY);
    GaussianBlur(grayImage, gaussianImage, cv::Size(21, 21), 0);
    
    resize(gaussianImage, matImage, cv::Size(IMAGE_SIZE, IMAGE_SIZE));
    
    cvRetina->run(matImage);
    
    cv::Mat retinaOutputMagno;
    cvRetina->getMagno(retinaOutputMagno);
    cvRetina->getParvo(parvoImageMat);
    
    if (self.frameNumber < 512) {
        self.frameNumber++;
    }
    
    // получаем среднее значение яркости всех пикселей
    float mean = cv::mean(retinaOutputMagno)[0];
    // получаем энтропию
    
    int histSize = 256;
    //int hist_w = 300; int hist_h = 300;
    //int bin_w = cvRound( (double) hist_w/histSize );
    
    cv::Mat src, hist;
    hist = myEntropy(retinaOutputMagno, histSize);
    float entropy = calcEntropy(hist, retinaOutputMagno.size(), histSize);
    
    if (self.frameNumber >= 2) {
        // фильтруем значения энтропии
        // сначала сдвинем буфер значений
        // энтропии и запишем новый элемент
        for (int i = self.frameNumber - 1; i > 0; i--) {
            entropyBuffer[i] = entropyBuffer[i - 1];
        }
        entropyBuffer[0] = entropy;
        // фильтруем значения средней яркости
        // сначала сдвинем буфер значений
        // средней яркости и запишем новый элемент
        for (int i = self.frameNumber - 1; i > 0; i--) {
            meanBuffer[i] = meanBuffer[i - 1];
        }
        meanBuffer[0] = mean;
        // для фильтрации применим медианный фильтр
        medianEntropy = getMedianArrayf(entropyBuffer, self.frameNumber);
        medianMean = getMedianArrayf(meanBuffer, self.frameNumber);
    } else {
        medianEntropy = entropy;
        medianMean = mean;
    }
    
    //если средняя яркость не очень высокая, то на изображении движение, а не шум
    //if (medianMean >= mean) {
    //если энтропия меньше медианы, то на изображении движение, а не шум
    if (mean < 60)
    if ((medianEntropy * 1.2) >= entropy) {
        
        // делаем пороговое преобразование
        // как правило, области с движением достаточно яркие
        // поэтому можно обойтись и без медианы средней яркости
        // cv::threshold(retinaOutputMagno, imgTemp,150, 255.0, CV_THRESH_BINARY);
        // пороговое преобразование с учетом медианы средней яркости
        cv::threshold(retinaOutputMagno, imgTemp, 100, 255.0, CV_THRESH_BINARY);
        // найдем контуры
        std::vector<std::vector<cv::Point>> contours;
        cv::findContours(imgTemp, contours, CV_RETR_EXTERNAL,
                         CV_CHAIN_APPROX_SIMPLE);
        if (contours.size() > 0) {
            // если контуры есть
//            arrayBB.resize(contours.size());
            // найдем ограничительные рамки
            
            cv::Mat contoursMat = cv::Mat(matImage.size(), CV_8UC4);
            cv::cvtColor(contoursMat, contoursMat, CV_BGR2RGBA, 4);
            contoursMat = cv::Scalar(0,0,0,0);
            
            NSInteger findCounters = 0;
            for(std::vector<std::vector<cv::Point>>::iterator it = contours.begin(); it != contours.end(); ++it) {
                std::vector<cv::Point> contour = *it;
                
                /*
                int numberOfChanges = contourArea(contour);
                NSLog(@"%i", numberOfChanges);
                if (numberOfChanges < 100) {
                    continue;
                }
                 */


                cv::Rect rect = boundingRect(contour);
                float min = (self.objectSizeCoefficient * IMAGE_SIZE * IMAGE_SIZE) - 0.3 * IMAGE_SIZE * IMAGE_SIZE;
                float max = (self.objectSizeCoefficient * IMAGE_SIZE * IMAGE_SIZE) + 0.3 * IMAGE_SIZE * IMAGE_SIZE;
                float rectSquare = rect.width * rect.height;
                NSLog(@"%f", rectSquare);
                if ( rectSquare <= min || rectSquare >= max ) {
                    continue;
                }
                
                cv::rectangle(contoursMat,
                              cv::Point(rect.x, rect.y),
                              cv::Point(rect.x + rect.width, rect.y + rect.height),
                              cv::Scalar(255, 0, 0),
                              1);
                findCounters++;
            }
            
            resize(contoursMat,
                   contoursMat,
                   rawImage.size(),
                   0, 0,
                   cv::INTER_NEAREST);
            //image = image + contoursMat;
            addWeighted(rawImage, 1, contoursMat, 1, 0, rawImage);
            
            if (MD_DEBUG) {
                resize(imgTemp,
                       imgTemp,
                       cv::Size(100,100),
                       0, 0,
                       cv::INTER_NEAREST);
            }
            
            if (findCounters > 0) {
                self.movingDetectionCallBack();
            }
            
            contoursMat.release();
        }
    }
    
    if (MD_DEBUG) {
    resize(retinaOutputMagno,
           retinaOutputMagno,
           cv::Size(100,100),
           0, 0,
           cv::INTER_NEAREST);
    
    resize(gaussianImage,
           gaussianImage,
           cv::Size(100,100),
           0, 0,
           cv::INTER_NEAREST);
    
    resize(parvoImageMat,
           parvoImageMat,
           cv::Size(100,100),
           0, 0,
           cv::INTER_NEAREST);
    }
    
    @autoreleasepool {
    UIImage *tempImage = MatToUIImage(rawImage);
    if (MD_DEBUG) {
        UIImage *image2 = MatToUIImage(retinaOutputMagno);
        UIImage *hist2 = MatToUIImage(imgTemp);
        UIImage *parvo = MatToUIImage(parvoImageMat);
        UIImage *original = MatToUIImage(gaussianImage);
        tempImage = [self imageByCombiningImage:tempImage withImage:image2 withImage2:hist2 parvo:(UIImage*)parvo original:original];
    }
    
    self.renderedImageCallBack(tempImage);
    }
}

- (UIImage*)imageByCombiningImage:(UIImage*)firstImage
                        withImage:(UIImage*)secondImage
                       withImage2:(UIImage*)image2
                            parvo:(UIImage*)parvo
                         original:(UIImage*)original {
    UIImage *image = nil;
    UIGraphicsBeginImageContextWithOptions(firstImage.size, NO, [[UIScreen mainScreen] scale]);
    
    [firstImage drawAtPoint:CGPointMake(0,0)];
    [secondImage drawAtPoint:CGPointMake(0,0)];
    [image2 drawAtPoint:CGPointMake(100,0)];
    [parvo drawAtPoint:CGPointMake(200,0)];
    [original drawAtPoint:CGPointMake(300,0)];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

// быстрая сортировка массива
template<typename aData>
void quickSort(aData* a, long l, long r) {
    long i = l, j = r;
    aData temp, p;
    p = a[ l + (r - l)/2 ];
    do {
        while ( a[i] < p ) i++;
        while ( a[j] > p ) j--;
        if (i <= j) {
            temp = a[i]; a[i] = a[j]; a[j] = temp;
            i++; j--;
        }
    } while ( i<=j );
    if ( i < r )
        quickSort(a, i, r);
    if ( l < j )
        quickSort(a, l , j);
};

float getMedianArrayf(float* data, unsigned long nData) {
    float medianData;
    float mData[nData];
    unsigned long i;
    if (nData == 0)
        return 0;
    if (nData == 1) {
        medianData = data[0];
        return medianData;
    }
    for (i = 0; i != nData; ++i) {
        mData[i] = data[i];
    }
    quickSort(mData, 0, nData - 1);
    medianData = mData[nData >> 1];
    return medianData;
};

@end
