//
//  MotionDetection.h
//  MotionDetectionResearch
//
//  Created by Vladislav Chebotaryov on 6/9/17.
//  Copyright © 2017 Vladislav Chebotaryov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MotionDetection : NSObject
typedef void (^RenderedImageCallback)(UIImage*);
typedef void (^MovingDetectionCallback)(void);

- (void)setupRetinaWidth:(NSInteger)width height:(NSInteger)height;
- (void)processImage:(UIImage *)image;

@property (nonatomic, assign) BOOL isSetuped;
@property (nonatomic, assign) float objectSizeCoefficient;
@property (nonatomic, copy) RenderedImageCallback renderedImageCallBack;
@property (nonatomic, copy) MovingDetectionCallback movingDetectionCallBack;

@end
