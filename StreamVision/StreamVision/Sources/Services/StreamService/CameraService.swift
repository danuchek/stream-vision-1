//
//  CameraService.swift
//  RealStream
//
//  Created by Vladislav Chebotaryov on 2/22/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import AVFoundation

// TODO: Rewrite service
class CameraService {
    let inputDevice = AVCaptureDevice.default(for: AVMediaType.video)
    
    var session: AVCaptureSession
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    init?(with layer: CALayer) {
        guard let inputDevice = self.inputDevice,
              let captureInput = try? AVCaptureDeviceInput(device: inputDevice) else {
                debugPrint("[\(type(of: self))] - Can't connect to camera")
                return nil
        }
        session = AVCaptureSession()
        session.sessionPreset = AVCaptureSession.Preset.high
        if session.canAddInput(captureInput) {
            session.addInput(captureInput)
        }
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = layer.frame
        layer.masksToBounds = true
        layer.insertSublayer(videoPreviewLayer!, at: 0)
        
        let angle = Double.pi / 2;  //rotate 180°, or 1 π radians
        videoPreviewLayer?.transform = CATransform3DMakeRotation(CGFloat(angle), 0.0, 0.0, 1.0);
        videoPreviewLayer?.frame = CGRect(x: 0.0,
                                          y: layer.frame.origin.y,
                                          width: layer.frame.width,
                                          height: layer.frame.height)
        
        let orientation = UIDevice.current.orientation
        updatePreviewLayerWith(orientation)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(orientationWillChange),
                                               name: UIApplication.willChangeStatusBarOrientationNotification,
                                               object: nil)
    }
    
    @objc func orientationWillChange(notification: Notification) {
        if let statusBarOrientation = notification.userInfo?[UIApplication.statusBarOrientationUserInfoKey] as? Int,
            let orientation = UIDeviceOrientation(rawValue: statusBarOrientation) {
            updatePreviewLayerWith(orientation)
        }
    }
    
    func updatePreviewLayerWith(_ orientation: UIDeviceOrientation) {
        switch orientation {
        case .landscapeLeft: videoPreviewLayer?.connection?.videoOrientation = .portraitUpsideDown
        case .landscapeRight: videoPreviewLayer?.connection?.videoOrientation = .portrait
        default: break
        }
    }
    
    func play() {
        session.startRunning()
    }
    
    func stop() {
        session.stopRunning()
    }
    
}
