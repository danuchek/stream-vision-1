//
//  SreamService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/28/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Foundation
import FileKit

enum Initializations {
    case viewfinder(StreamHandler, isH264: Bool)
    case player(path: String, StreamHandler)
    case restreaming(rtmp: String, StreamHandler, isH264: Bool)
    case camera(layer: CALayer?)
    case convert(path: String, sku: String, StreamHandler, callback: StreamService.ConvertorParserCallback?)
}


class StreamService: NSObject, VLCMediaDelegate, DeviceManager {
    typealias FrameExtractorDst = (_ streamHandler: StreamHandler) -> String
    typealias AudioExtractorDst = (_ streamHandler: StreamHandler) -> String
    typealias ConvertorParserCallback = (_ width: Int, _ height: Int, _ length: Int, _ fps: Int) -> Void
    
    var vlcMediaPlayer: VLCMediaPlayer? = VLCMediaPlayer()
    fileprivate var frameExtractorDst: FrameExtractorDst = { streamHandler in
        return "video-prerender-callback=\(streamHandler.frameExtractor.videoPrerenderCallbackPointer)," +
            "video-postrender-callback=\(streamHandler.frameExtractor.videoPostrenderCallbackPointer)," +
        "video-data=\(streamHandler.frameExtractor.videoDataPointer)"
    }
    fileprivate var audioExtractorDst: AudioExtractorDst = { streamHandler in
        return "audio-prerender-callback=\(streamHandler.audioExtractor.audioPrerenderCallbackPointer)," +
            "audio-postrender-callback=\(streamHandler.audioExtractor.audioPostrenderCallbackPointer),audio-data=\(streamHandler.audioExtractor.audioDataPointer)"
    }
    var cameraService: CameraService!
    var parserCallback: ConvertorParserCallback?
    var sku: String = ""
    
    init(mode: Initializations) {
        super.init()
        VLCLibrary.shared().debugLogging = true;
        
        switch mode {
        
        case let .viewfinder(streamHandler, isH264):
            let media = sdpMedia(isH264: isH264)
            let soutParameter = "#transcode{vcodec=MJPG, acodec=mp3}:duplicate{dst=\"smem{" + frameExtractorDst(streamHandler) + "\"}"

            media?.addOptions(["sout": soutParameter])
            vlcMediaPlayer?.media = media
            
        case let .restreaming(rtmp, streamHandler, isH264):
            let media = sdpMedia(isH264: isH264)
            let soutParameter =
                "#transcode{vcodec=MJPG, acodec=mp3}:duplicate{dst=\"smem{" + frameExtractorDst(streamHandler) + "}\",dst=\"transcode{vcodec=h264,vb=512," +
                    "venc=x264{profile=baseline,preset=ultrafast,tune=zerolatency}," +
                    "acodec=mp3,ab=128,channels=1,samplerate=44100,threads=4}" +
            ":standard{mux=flv,dst=\(rtmp),access=rtmp}\"}"
            
            media?.addOptions(["sout": soutParameter])
            vlcMediaPlayer?.media = media
            
        case let .player(path, streamHandler):
            let playerMedia = VLCMedia(path: path)
            let soutParameter = "#transcode{vcodec=MJPG, acodec=mp3}:duplicate{dst=\"smem{" + frameExtractorDst(streamHandler) + "}\"}"
            playerMedia?.addOptions(["sout": soutParameter])
            vlcMediaPlayer?.media = playerMedia

            
        case let .camera(layer):
            cameraService = CameraService(with: layer!)
            cameraService?.play()
            
        case let .convert(path, sku, streamHandler, parserCallback):
            Logging.log(message: "[VLC] Convert initialized")
            self.parserCallback = parserCallback
            self.sku = sku
            
            vlcMediaPlayer?.audio.volume = 0
            let playerMedia = VLCMedia(path: path)
            let soutParameter = "#transcode{vcodec=MJPG, acodec=s16l}:smem{\(frameExtractorDst(streamHandler)),\(audioExtractorDst(streamHandler))}"
            playerMedia?.addOptions(["sout": soutParameter])
            playerMedia?.delegate = self
            
            playerMedia?.synchronousParse()
            
            vlcMediaPlayer?.media = playerMedia
            vlcMediaPlayer?.currentAudioTrackIndex = -1
        }
    }
    
    fileprivate func sdpMedia(isH264: Bool) -> VLCMedia? {
        let sdpPath = Bundle.main.path(forResource: isH264 ? "stream" : "stream-old", ofType: "sdp")
        let sdpURL = URL(fileURLWithPath: sdpPath!)
        let media = VLCMedia(url: sdpURL)
        media?.addOptions([":network-caching": 100])
        
        return media
    }
    
    func drawableView(_ drawableView: UIView, viewAngle: Double) {
        self.drawableView(drawableView)
        
        drawableView.layer.transform = CATransform3DMakeRotation(CGFloat(viewAngle), 0, 0.0, 1.0);
    }
    
    func drawableView(_ drawableView: UIView) {
        vlcMediaPlayer?.drawable = drawableView
    }
    
    // MARK: Global
    
    func start() {
        Logging.log(message: "[VLC] Play executed")
        
        vlcMediaPlayer?.play()
    }
    
    func mediaDidFinishParsing(_ aMedia: VLCMedia) {
        Logging.log(message: "[VLC] Parsing media finish")
        
        if aMedia.tracksInformation.count > 0 {
            let videoStats = (aMedia.tracksInformation[0] as? [String: Any])
            if let width = videoStats?["width"] as? Int,
                let height = videoStats?["height"] as? Int {
                let videoLength = aMedia.length.intValue
                parserCallback?(width, height, Int(videoLength), deviceManager.deviceFPS(by: sku))
            }
        } else {
            Logging.log(message: "[VLC] Media not parsed")
            // TODO: Show error
        }
    }
    
    func stop() {
        Logging.log(message: "[VLC] Stop vlc")
        
        vlcMediaPlayer?.delegate = nil
        vlcMediaPlayer?.stop()
        vlcMediaPlayer?.media = nil
        vlcMediaPlayer = nil
        parserCallback = nil
    }
    
    func pause() {
        vlcMediaPlayer?.pause()
    }
    
    func doSnapshot(path: Path) {
        vlcMediaPlayer?.saveVideoSnapshot(at: path.rawValue, withWidth: 0, andHeight: 0)
    }
    
    deinit {
        print("[VLC] Deinitialization")
    }
}
