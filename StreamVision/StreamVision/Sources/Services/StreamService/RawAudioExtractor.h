//
//  RawAudioExtractor.h
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/19/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RawAudioExtractorDelegate

-(void)didObtainAudio:(NSData * _Nonnull)data
             channels:(NSInteger)channels
                 rate:(NSInteger)rate
              samples:(NSInteger)samples
                  bps:(NSInteger)bps
                  pts:(NSInteger)pts;
    
@end

@interface RawAudioExtractor : NSObject

@property (nullable ,weak, nonatomic) id<RawAudioExtractorDelegate> delegate;

@property (readonly, assign, nonatomic) long long int audioPrerenderCallbackPointer;
@property (readonly, assign, nonatomic) long long int audioPostrenderCallbackPointer;
@property (readonly, assign, nonatomic) long long int audioDataPointer;

@end
