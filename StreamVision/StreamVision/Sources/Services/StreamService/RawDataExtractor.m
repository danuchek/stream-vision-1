//
//  FrameExtractor.m
//  StreamVision
//
//  Created by Oleksii Shvachenko on 12.12.16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

#import "RawDataExtractor.h"

@interface RawDataExtractor()

- (void)processFrame:(NSData * _Nonnull)image withWidth:(NSInteger)width andHeight:(NSInteger)height pts:(NSInteger)pts;

@end

static unsigned int videoBufferSize = 0;
static uint8_t *videoBuffer = 0;

static void cbVideoPrerender(void *p_video_data, uint8_t **pp_pixel_buffer, int size) {
    if (size > videoBufferSize || !videoBuffer)
    {
        free(videoBuffer);
        videoBuffer = (uint8_t *) malloc(size);
        videoBufferSize = size;
    }
    
    *pp_pixel_buffer = videoBuffer;
}

static void cbVideoPostrender(void *p_video_data, uint8_t *p_pixel_buffer
                              , int width, int height, int pixel_pitch, int size, int64_t pts) {
    NSData *data = [NSData dataWithBytes:p_pixel_buffer length:size];

    RawDataExtractor *instance = (__bridge RawDataExtractor *)p_video_data;
    [instance processFrame:data withWidth:width andHeight:height pts:pts];
}

@implementation RawDataExtractor

- (NSInteger)videoPrerenderCallbackPointer {
    return (long long int)(intptr_t)(void*)&cbVideoPrerender;
}

- (NSInteger)videoPostrenderCallbackPointer {
    return (long long int)(intptr_t)(void*)&cbVideoPostrender;
}

- (NSInteger)videoDataPointer {
    return (long long int)(intptr_t)(__bridge void*)self;
}

- (void)processFrame:(NSData * _Nonnull)image withWidth:(NSInteger)width andHeight:(NSInteger)height pts:(NSInteger)pts {
    [self.delegate didObtainNewFrame: image width:width height:height pts: pts];
}

@end
