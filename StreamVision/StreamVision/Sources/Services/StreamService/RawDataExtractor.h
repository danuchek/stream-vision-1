//
//  FrameExtractor.h
//  StreamVision
//
//  Created by Oleksii Shvachenko on 12.12.16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RawDataExtractorDelegate

- (void)didObtainNewFrame:(NSData * _Nonnull)imageData width:(NSInteger) width height:(NSInteger)height pts:(NSInteger)pts;

@end

@interface RawDataExtractor : NSObject

@property (nullable ,weak, nonatomic) id<RawDataExtractorDelegate> delegate;

@property (readonly, assign, nonatomic) NSInteger videoPrerenderCallbackPointer;
@property (readonly, assign, nonatomic) NSInteger videoPostrenderCallbackPointer;
@property (readonly, assign, nonatomic) NSInteger videoDataPointer;

@end
