//
//  RawAudioExtractor.m
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/19/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

#import "RawAudioExtractor.h"

@interface RawAudioExtractor()

-(void)didObtainAudio:(NSData * _Nonnull)data
             channels:(NSInteger)channels
                 rate:(NSInteger)rate
              samples:(NSInteger)samples
                  bps:(NSInteger)bps
                  pts:(NSInteger)pts;

@end

static unsigned long videoBufferSize = 0;
static uint8_t *videoBuffer = 0;

static void cbAudioPrerender(void* p_audio_data, uint8_t** pp_pcm_buffer , size_t size) {
    if (size > videoBufferSize || !videoBuffer)
    {
        free(videoBuffer);
        videoBuffer = (uint8_t *) malloc(size);
        videoBufferSize = size;
    }
    
    *pp_pcm_buffer = videoBuffer;
}

static void cbAudioPostrender(void* p_audio_data,
                              uint8_t* p_pcm_buffer,
                              unsigned int channels,
                              unsigned int rate,
                              unsigned int nb_samples,
                              unsigned int bits_per_sample,
                              size_t size, int64_t pts) {
    NSData *data = [NSData dataWithBytes:p_pcm_buffer length:size];
    RawAudioExtractor *instance = (__bridge RawAudioExtractor *)p_audio_data;
    
    [instance didObtainAudio:data channels:channels rate:rate samples:nb_samples bps:bits_per_sample pts:pts];
}

@implementation RawAudioExtractor

- (long long int)audioPrerenderCallbackPointer {
    return (long long int)(intptr_t)(void*)&cbAudioPrerender;
}

- (long long int)audioPostrenderCallbackPointer {
    return (long long int)(intptr_t)(void*)&cbAudioPostrender;
}

- (long long int)audioDataPointer {
    return (long long int)(intptr_t)(__bridge void*)self;
}

-(void)didObtainAudio:(NSData * _Nonnull)data
             channels:(NSInteger)channels
                 rate:(NSInteger)rate
              samples:(NSInteger)samples
                  bps:(NSInteger)bps
                  pts:(NSInteger)pts {
    [self.delegate didObtainAudio:data channels:channels rate:rate samples:samples bps:bps pts:pts];
    NSLog(@"Audio. Channels=%li,\n rate=%li,\n samples=%li,\n bps=%li,\n pts=%li,", (long)channels, rate, samples, bps, pts);
}

@end
