//
//  ConverterHandler.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/2/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

class ConverterHandler: StreamHandler, VLCMediaPlayerDelegate {
    var currentFile: FileModel?
    var isConvertingEnd: Bool = true
    var finished: (() -> ())?
    
    var isConverting: (_ file: FileModel) -> Bool {
        return { file in
            return file == self.currentFile
        }
    }
    
    override init() {}
    
    override func didObtainNewFrame(_ imageData: Data, width: Int, height: Int, pts: Int) {
        isConvertingEnd = false
        FilesManager.shared.encodeFrame(imageData, pts: pts)
        Logging.log(message: "[VLC] New frame: PTS=\(pts)", toFile: false)
    }
    
    override func didObtainAudio(_ data: Data, channels: Int, rate: Int, samples: Int, bps: Int, pts: Int) {
        Logging.log(message: "[VLC] Audio received", toFile: false)
        FilesManager.shared.encodeAudio(data: data, channels: channels, rate: rate, samples: samples, bps: bps, pts: pts)
    }
    
    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        if let player = aNotification.object as? VLCMediaPlayer {
            Logging.log(message: "[VLC] MEDIA IS PLAYING - \(player.isPlaying)")
            Logging.log(message: "[VLC] mediaPlayerStateChanged - \(player.state)")
            
            if player.isPlaying == false && !isConvertingEnd {
                Logging.log(message: "[VLC] Player is not plaing")
                
                isConvertingEnd = true
                FilesManager.shared.endConverting {
                    self.currentFile = nil
                    self.finished?()
                }
            }
        }
    }
    
    func cancelConverting() {
        currentFile = nil
        isConvertingEnd = true
    }
}
