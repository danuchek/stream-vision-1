//
//  StreamHandler.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/19/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation
import MediaPlayer
import RealmSwift

/// Use for getting image from stream for current time
class StreamHandler: RawDataExtractorDelegate, RawAudioExtractorDelegate {
    let frameExtractor = RawDataExtractor()
    let audioExtractor = RawAudioExtractor()
    var model: MovingDetectionModel!
    private let motionDetection = MotionDetection()
    private var withMotion: Bool = true
    
    fileprivate var queue = DispatchQueue(label: "MD", attributes: []);
    
    var snapshot: UIImage?
    var motionDetectionView: UIImageView?
    var player: AVAudioPlayer!
    
    var didFirstFrameObtained: ((_ resolution: (w: Int, h: Int) ) -> Void)?
    var resolution: (w: Int, h: Int) = (0, 0)
    
    // MARK: Init
    convenience init(motionDetectionView: UIImageView, withMotion: Bool = true) {
        self.init()
        self.motionDetectionView = motionDetectionView
        self.withMotion = withMotion
    }
    
    init() {
        let realm = try! Realm()
        if let motionModel = realm.objects(MovingDetectionModel.self).last {
            model = motionModel
        } else {
            model = MovingDetectionModel()
            try! realm.write {
                realm.add(model)
            }
        }
        
        if let fileUrl = URL(string: "System/Library/Audio/UISounds/Tink.caf") {
            player = try? AVAudioPlayer(contentsOf: fileUrl)
        }
        model = realm.objects(MovingDetectionModel.self).last!
        self.motionDetection.movingDetectionCallBack = {
            DispatchQueue.main.async {
                if self.model.isVibroIndication {
                    AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                }
                if self.model.isSoundIndication {
                    self.player?.play()
                }
            }
        }
        self.motionDetection.objectSizeCoefficient = self.model.detectionAccuracy
        
        frameExtractor.delegate = self
        audioExtractor.delegate = self
    }
    
    // MARK: Delegate
    func didObtainAudio(_ data: Data,
                        channels: Int,
                        rate: Int,
                        samples: Int,
                        bps: Int,
                        pts: Int) {
        
    }
    
    func didObtainNewFrame(_ imageData: Data, width: Int, height: Int, pts: Int) {
        guard let image = UIImage(data: imageData as Data) else {
            return
        }
        
        resolution = (width, height)
        
        if let callback = didFirstFrameObtained {
            DispatchQueue.main.async {
                callback((width, height))
            }
            didFirstFrameObtained = nil
        }
        
        DispatchQueue.global().async {
            FilesManager.shared.encodeFrame(imageData, pts: pts)
        }
        
        DispatchQueue.main.async {
            if self.model.isMovingDetectionActive, self.withMotion {
                if !self.motionDetection.isSetuped {
                    self.motionDetection.setupRetinaWidth(width, height: height)
                    self.motionDetection.objectSizeCoefficient = self.model.detectionAccuracy
                    self.motionDetection.renderedImageCallBack = { image in
                        DispatchQueue.main.async {
                            self.motionDetectionView?.image = image
                        }
                    }
                } else {
                    self.queue.async {
                        self.motionDetection.processImage(image)
                    }
                }
            } else {
                self.motionDetectionView?.image = image
            }
            self.motionDetection.objectSizeCoefficient = self.model.detectionAccuracy
        }
        
        snapshot = image
    }
}
