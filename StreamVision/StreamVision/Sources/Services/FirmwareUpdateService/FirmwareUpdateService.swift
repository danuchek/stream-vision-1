//
//  FirmwareUpdateService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/25/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Foundation
import FastSocket
import FileKit
import RealmSwift

class FirmwareUpdateService: FirmwareUpdaterDelegate, StoreManager {
    enum FinishResult {
        case finishUpdate
        case receivedUpdate
        case errorUpdate(NSError)
    }
    
    // Types
    typealias InitializeBlock = (_ totalIterations: Int) -> ()
    typealias ProgressBlock = (_ iteration: Int) -> ()
    typealias FinishBlock = (_ result: FinishResult) -> ()
    
    // Variables
    fileprivate var deviceSKU: String!
    
    fileprivate let fileManager = FilesManager.shared
    fileprivate var firmwareUpdater: FirmwareUpdater!
    
    fileprivate var initializeBlock: InitializeBlock?
    fileprivate var progressBlock: ProgressBlock?
    fileprivate var finishBlock: FinishBlock?
    
    var lastCheckForUpdates: Date? {
        get {
            return UserDefaults.standard.object(forKey: "lastCheckForUpdate") as? Date
        }
    }
    
    // TODO: Update to firm to device
    func removeFirmwareForDevice(_ device: VirtualDevice) -> Bool {
        guard device.hasDownloadedFirmware else {
            return false
        }
        do {
            try fileManager.pathToFirmwareFromDevice(device).deleteFile()
        } catch {
            return false
        }
        return true
    }
    
    func downloadFirmwareForDevice(_ device: VirtualDevice, progressBlock: @escaping (_ percentage: Float) -> (), finishBlock: @escaping ConnectionService.FinishBlock) {
        ConnectionService.downloadFirmwareForDevice(device, progressBlock: progressBlock, finishBlock: finishBlock)
    }
    
    func cancelCurrentDownloadFirmware() {
        ConnectionService.cancelDownloadFirmware()
    }
    
    func checkForUpdatesForDevice(_ device: VirtualDevice,
                                  completion: @escaping (_ device: VirtualDevice) -> () ) {
        checkForUpdatesForDevices([device]) { (devices) in
            completion(devices.last!)
        }
    }
    
    func checkForUpdatesForDevices(_ devices: [VirtualDevice], completion: @escaping (_ devices: [VirtualDevice]) -> () ) {
        ConnectionService.checkForUpdatesForDevices(devices, completion: completion)
    }
    
    func uploadFirmwareToDevice(_ initializeBlock: InitializeBlock?,
                                progressBlock: ProgressBlock?,
                                finishBlock: FinishBlock? ) {
        self.initializeBlock = initializeBlock
        self.progressBlock = progressBlock
        self.finishBlock = finishBlock
        
        Logging.log(message: "[Firmware service] - Update start", toFile: true)
        
        let networkSettings = SocketManager.sharedManager.settings
        guard let connectedDevice = SocketManager.connectedDevice else {
            finishBlock?(.errorUpdate(NSError(domain: "Device disconnected", code: 0, userInfo: nil)))
            Logging.log(message: "[Firmware service] - Error device disconnected", toFile: true)
            return
        }
        
        if fileManager.firmwareExistFromDevice(connectedDevice) {
            Logging.log(message: "[Firmware service] - Firmware exist", toFile: true)
            
            firmwareUpdater = FirmwareUpdater(filePath: fileManager.pathToFirmwareFromDevice(connectedDevice),
                                              networkSettings: networkSettings,
                                              delegate: self, isOldApi: !SocketManager.isDevice18)
        } else {
            finishBlock?(.errorUpdate(NSError(domain: "Initialization error", code: -1, userInfo: nil)))
        }
    }
    
    func socketEvent(_ event: SocketEvent) {
        switch event {
            
        case let .initialize(iterations, totalBytes):
            Logging.log(message: "Initialyzed \(iterations), total bytes: \(totalBytes)")
            initializeBlock?(iterations)
            deviceSKU = SocketManager.connectedDevice?.info.sku
            SocketManager.sharedManager.sendCommand(.fwUpdate).response { _, _ in
                self.firmwareUpdater.uploadFirmwareToDevice()
            }
            
        case let .connection(connected):
            Logging.log(message: "Soket connected: \(connected)")
            if !connected {
                finishBlock?(.errorUpdate(NSError(domain: "Connection error", code: -1, userInfo: nil)))
            }
        
        case let .send(bytes, leftBytes, iteration):
            Logging.log(message: "Send: \(bytes), left: \(leftBytes), iteration: \(iteration)", toFile: true)
            if let _ = progressBlock {
                self.progressBlock!(iteration)
            }
            
        case let .receive(message):
            Logging.log(message: "[Firmware updater] - Device say: \(message)", toFile: true)
            
        case let .finish(result):
            let deviceBySKU = storeManager.devices.filter() {
                return $0.info.sku == self.deviceSKU
            }.last!
            _ = try? FilesManager.shared.pathToFirmwareFromDevice(deviceBySKU).deleteFile()
            
            switch result {
            case let .errorUpdate(error):
                Logging.log(message: "[Firmware updater] - Uploading fail: \(error.domain)", toFile: true)
            default:
                let realm = try! Realm()
                try? realm.write {
                    deviceBySKU.info.version = deviceBySKU.firmware.version
                }
                storeManager.store(deviceBySKU)
                Logging.log(message: "[Firmware updater] - Success uploading", toFile: true)
            }
            finishBlock?(result)
        }
    }
    
    
    
}
