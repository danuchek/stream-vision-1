//
//  FirmwareUpdater.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/26/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import FileKit
import FastSocket

enum SocketEvent {
    case initialize(iterations: Int, totalBytes: Int)
    case connection(_: Bool)
    case send(bytes: Int, leftBytes: Int, iteration: Int)
    case receive(message: String)
    case finish(result: FirmwareUpdateService.FinishResult)
}

protocol FirmwareUpdaterDelegate {
    func socketEvent(_ event: SocketEvent)
}

class FirmwareUpdater {
    typealias SoketData = Array<Array<UInt8>>
    
    // Variables
    fileprivate var extractedFirmwareData: SoketData?
    fileprivate var socket: FastSocket = FastSocket()
    fileprivate let buffer = 8192
    fileprivate var host: String?
    fileprivate var port: String?
    fileprivate var firmwareSize = 0
    fileprivate var timer: Timer?
    fileprivate var timeoutUploading = 30
    fileprivate var connectionFailed: Bool = false
    fileprivate var isOldApi: Bool = false
    var delegate: FirmwareUpdaterDelegate
    
    required init(filePath: Path,
                  networkSettings: SocketConnectionSettings,
                  delegate: FirmwareUpdaterDelegate,
                  isOldApi: Bool) {
        self.delegate = delegate
        self.host = networkSettings.baseSocketAddress
        self.port = networkSettings.baseSocketPort
        self.isOldApi = isOldApi
        Logging.log(message: "[Firmware updater] - Uploader initialized", toFile: true)
        
        DispatchQueue.global(qos: .background).async {
            let firmware = File<NSData>(path: filePath)
            var firmwareData: NSData?
            do {
                try firmwareData = firmware.read()
            } catch {
                Logging.log(message: "[Firmware updater] - Firmware corrupt", toFile: true)
            }
            
            if let firmwareData = firmwareData {
                Logging.log(message: "[Firmware updater] - Data OK", toFile: true)
                Logging.log(message: "[Firmware updater] - Firmware size \(firmwareData.length)", toFile: true)
                
                self.firmwareSize = firmwareData.length
                self.extractedFirmwareData = self.extractSoketDataFromFirmwareData(firmwareData as Data)
                DispatchQueue.main.async {
                    self.delegate.socketEvent(.initialize(iterations: self.extractedFirmwareData!.count,
                                                          totalBytes: firmwareData.length))
                }
            } else {
                DispatchQueue.main.async {
                    self.delegate.socketEvent(
                        .finish(result: .errorUpdate(NSError(domain: "Firmware data corrupted", code: -1, userInfo: nil))))
                }
            }
        }
    }
    
    fileprivate func extractSoketDataFromFirmwareData(_ firmwareData: Data) -> SoketData {
        var byteArray = [UInt8](repeating: 0, count: firmwareData.count)
        (firmwareData as NSData).getBytes(&byteArray, length: firmwareData.count)
        
        var composedBytes: Array<Array<UInt8>> = []
        var tempBuffer: Array<UInt8> = []
        byteArray.forEach { byte in
            tempBuffer.append(byte)
            if tempBuffer.count == buffer {
                composedBytes.append(tempBuffer)
                tempBuffer = []
            }
        }
        
        Logging.log(message: "[Firmware updater] - Firmware data was extracted", toFile: true)
        return composedBytes
    }
    
    func uploadFirmwareToDevice() {
        guard connectFromSocket() else {
            return
        }
        Logging.log(message: "[Firmware updater] - Upload begin", toFile: true)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(uploadTimeout), userInfo: nil, repeats: true)
        connectionFailed = false
        DispatchQueue.global(qos: .background).async {
            Logging.log(message: "[Firmware updater] - Queue started", toFile: true)
            
            var iteration = 0
            self.extractedFirmwareData!.forEach { byteArray in
                Logging.log(message: "[Firmware updater] - Iteration \(iteration)", toFile: true)
                guard !self.connectionFailed else {
                    Logging.log(message: "[Firmware updater] - Connection failed", toFile: true)
                    return
                }
                iteration += 1
                self.timeoutUploading = 30
                let sendedBytes = self.sendSoketData(byteArray)
                let leftBytes = self.firmwareSize - (iteration * self.buffer)
                
                var (_, receiveMessage): (Int, String) = (0, "")
                if self.isOldApi {
                    (_, receiveMessage) = self.receiveSoketData()
                } else if leftBytes == 0 {
                    (_, receiveMessage) = self.receiveSoketData()
                }
                
                Logging.log(message: "[Firmware updater] - Device say \(receiveMessage) and bytes \(leftBytes) left", toFile: true)
                DispatchQueue.main.async {
                    self.delegate.socketEvent(.send(bytes: sendedBytes, leftBytes: leftBytes, iteration: iteration))
                    self.delegate.socketEvent(.receive(message: receiveMessage))
                }
                if leftBytes == 0 {
                    Logging.log(message: "[Firmware updater] - Left bytes is zero - \(receiveMessage)", toFile: true)
                    
                    let (_, endMessage) = self.receiveSoketData()
                    self.closeConnectionImmediatelyWithMessage(endMessage)
                }
                
                Logging.log(message: "[Firmware updater] - Upload message - \(receiveMessage)", toFile: true)
                Logging.log(message: "[Firmware updater] - Upload iteration - \(iteration), bytes left - \(leftBytes)", toFile: true)
                
                if self.isOldApi, receiveMessage.contains("END") {
                    self.closeConnection(message: receiveMessage)
                    return
                }
            }

        }
    }
    
    private func closeConnection(message: String) {
        Logging.log(message: "[Firmware updater] - Connection close with \(message)", toFile: true)
        
        self.connectionFailed = true
        self.closeConnectionImmediatelyWithMessage(message)
    }
    
    @objc fileprivate func uploadTimeout() {
        timeoutUploading -= 1
        if timeoutUploading == 0 {
            Logging.log(message: "[Firmware updater] - Upload timeout", toFile: true)
            
            socket.close()
            timer?.invalidate()
            connectionFailed = true
            
            Logging.log(message: "[Firmware updater] - I will close connection with ERR", toFile: true)
            self.closeConnectionImmediatelyWithMessage("ERR")
        }
    }
    
    fileprivate func closeConnectionImmediatelyWithMessage(_ soketMessage: String) {
        Logging.log(message: "[Firmware updater] - Connection will close with message - \(soketMessage)", toFile: true)
        
        guard soketMessage.contains("END") else {
            DispatchQueue.main.async { [weak self] in
                self?.callFinishWithErrorMessage(soketMessage)
            }
            return
        }
        let (_, receiveMessage) = self.receiveSoketData()
        
        DispatchQueue.main.async { [weak self] in
            self?.callFinishWithErrorMessage(receiveMessage)
        }
    }
    
    fileprivate func callFinishWithErrorMessage(_ message: String) {
        timer?.invalidate()
        DispatchQueue.main.async {
            self.delegate.socketEvent(.receive(message: message))
            
            let error = NSError(domain: "Uploading fail", code: -1, userInfo: nil)
            if message.contains("OK") {
                Logging.log(message: "Close with - OK", toFile: true)
                self.delegate.socketEvent(.finish(result: .finishUpdate))
            } else {
                if message.contains("END") {
                    Logging.log(message: "Close with - double end detected")
                    let (_, receiveMessage) = self.receiveSoketData()
                    if receiveMessage.contains("OK") {
                        Logging.log(message: "Close with - OK")
                        self.delegate.socketEvent(.finish(result: .receivedUpdate))
                    } else {
                        Logging.log(message: "Close with - error coz ok no found | \(receiveMessage)")
                        self.delegate.socketEvent(.finish(result: .errorUpdate(error)))
                    }
                } else {
                    Logging.log(message: "Close with - error coz next end no found | \(message)")
                    self.delegate.socketEvent(.finish(result: .errorUpdate(error)))
                }
            }
        }
    }
    
    fileprivate func sendSoketData(_ bytes: Array<UInt8>) -> Int {
        let sendingBytes: UnsafeMutablePointer<UInt8> = UnsafeMutablePointer(mutating: bytes)
        return socket.sendBytes(sendingBytes, count: buffer)
    }
    
    fileprivate func receiveSoketData() -> (Int, String) {
        let receiveBuf = UnsafeMutableRawPointer.allocate(byteCount: 5, alignment: 5)
        let receiveBytes = socket.receiveBytes(receiveBuf, limit: 5)
        Logging.log(message: "Bytes from device \(receiveBytes)", toFile: true)
        
        if receiveBytes < 0 {
            return (receiveBytes, "ERR")
        }
        let receiveMessage = String(data: Data(bytes: receiveBuf, count: receiveBytes), encoding: String.Encoding.utf8)
//        receiveBuf.deallocate(bytes: 4, alignedTo: 4)
        receiveBuf.deallocate()
        return (receiveBytes, receiveMessage ?? "ERR")
    }
    
    fileprivate func connectFromSocket() -> Bool {
        socket = FastSocket(host: host!, andPort: port!)
        let connected = socket.connect()
        delegate.socketEvent(.connection(connected))
        return connected
    }
}
