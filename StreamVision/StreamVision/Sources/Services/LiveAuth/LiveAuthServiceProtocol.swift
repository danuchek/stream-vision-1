//
//  LiveAuthServiceProtocol.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/19/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

protocol LiveAuthServiceProtocol {
    var presenterViewController: UIViewController? { get set }
    var model: LiveAuthModel? { get }
    var isAuthorized: Bool { get }
    var isAbleToStream: Bool { get }
    
    func login( completion: ( (LiveAuthModel?, LiveErrorValidation.LiveError?) -> () )? )
    func logOut()
    
    var description: String { get }
}
