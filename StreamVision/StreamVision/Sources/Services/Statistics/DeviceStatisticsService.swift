//
//  DeviceStatisticsService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/18/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit
import RealmSwift
import FileKit
import Zip

class StatisticRawModel: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var sku: String = ""
    @objc dynamic var serial: String = ""
    
    var fullPath: String {
        return basePath + "\(id)/"
    }
    
    let basePath = (Path.userHome + "/Library/" + "statistics/raw/").rawValue
    override static func primaryKey() -> String? {
        return "id"
    }
}

class DeviceStatisticsService {
    
    /// Try collect statistics
    ///
    /// Firstly we need check if device connected.
    /// After that we must download journal and info file if they exist
    /// *Note* - You also need remove this files
    class func collectStatistics() {
        Logging.log(message: "[STATS] Collecting begin")
        
        if SocketManager.isConnected {
            Logging.log(message: "[STATS] Device connected")
            
            // Get files list
            SocketManager.send(.getStatisticsList, params: ["1:/": nil]).response { model, error in
                Logging.log(message: "[STATS] File list for statistics received")
                
                if let statisticsFilesModel = model as? DeviceStatisticsModel {
                    Logging.log(message: "[STATS] Statistics exist")
                    
                    for file in statisticsFilesModel.files where file.name.contains("stt") {
                        let realm = try! Realm()
                        let count = (realm.objects(StatisticRawModel.self).last?.id ?? 0) + 1
                        Logging.log(message: "[STATS] Stt exist")
                        
                        // Download statistics
                        SocketManager.download(params:
                            ["size": String(file.size), "path":"1:/\(file.name)"]).response { responseModel, error in
                                if let dataModel = responseModel as? Data {
                                    DispatchQueue.main.async {
                                        if let device = SocketManager.connectedDevice {
                                            let statisticModel = StatisticRawModel()
                                            statisticModel.id = count
                                            statisticModel.sku = device.info.sku
                                            statisticModel.serial = device.info.serial
                                            
                                            let path = Path(statisticModel.fullPath) + "\(file.name)"
                                            try? Path(statisticModel.fullPath).createDirectory(withIntermediateDirectories: true)
                                            try? dataModel.write(to: path)
                                            
                                            let realm = try! Realm()
                                            try! realm.write {
                                                realm.add(statisticModel, update: .all)
                                            }
                                        }
                                        SocketManager.send(.delete, params: ["path":"1:/\(file.name)"])
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    /// Try send statistics
    ///
    /// For statistics need internet and so we will check it
    class func sendStatistics() {
        ConnectionService.reachability { (status) in
            if status == .reachable {
                DeviceStatisticsService.sendStatisticsWithInternet()
            }
        }
    }
    
    /// Try send statistics recursively
    fileprivate class func sendStatisticsWithInternet() {
        let realm = try! Realm()
        guard let statsModel = realm.objects(StatisticRawModel.self).sorted(by: { lhs, rhs in return lhs.id > rhs.id }).last else {
            return
        }
        
        let statsForZip = [Path(statsModel.fullPath + "jrnl.stt"),
                           Path(statsModel.fullPath + "info.stt")].compactMap { path in
                            return path.exists ? path.url : nil
        }
        
        let currentZipPath = (FilesManager.shared.defaultDir + "\(statsModel.id).zip")
        if currentZipPath.exists {
            try? currentZipPath.deleteFile()
        }
        
        if let destinationURL = try? Zip.quickZipFiles(statsForZip, fileName: "\(statsModel.id)") {
            ConnectionService.sendStatistic(sku: statsModel.sku, serial: statsModel.serial, zipURL: destinationURL) { error in
                try? Path(url: destinationURL)?.deleteFile()
                if error == nil {
                    DispatchQueue.main.async {
                        for statUrl in statsForZip {
                            try? Path(url: statUrl)?.deleteFile()
                        }
                        let realm = try! Realm()
                        try! realm.write {
                            realm.delete(statsModel)
                        }
                        DeviceStatisticsService.sendStatistics()
                    }
                }
            }
        }
    }
}
