//
//  NetworkConnection.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/2/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Alamofire
import FileKit
import RealmSwift
import Reachability

extension String: ParameterEncoding {

    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }

}

class NetworkConnection: StoreManager {
    
    // TODO: Refactore to CONFIG
    #if DEVBUILD
        let baseUrl: String = "http://stage.streamvisionsupport.lt"
    #else
        let baseUrl: String = "https://streamvisionsupport.lt"
    #endif
    static var serverUrl = NetworkConnection().baseUrl + "/api/v1"
    
    var uploadStatisticUrl: String {
        return baseUrl + "/api/v1/device/statistic/upload"
    }
    
    // Types
    typealias InitializeBlock = (_ totalIterations: Int) -> ()
    typealias ProgressBlock = (_ percentage: Float) -> ()
    typealias FinishBlock = (_ error: NSError?) -> ()
    
    // Variables
    fileprivate let networkManager = Alamofire.Session()
    fileprivate var currentDownloadTask: Alamofire.Request?
    
    func sendPushToken(_ token: String) {
        let device = UIDevice.current
        let vendor = device.identifierForVendor
        let uuid = UUID().uuidString
        
        let params = ["deviceId":vendor!.uuidString, "vendorId":uuid, "typeId":"1", "token":token]
        AF.request("\(baseUrl)/api/v1/device/add",
            method: .post,
            parameters: params,
            encoding: JSONEncoding.default).response { response in
                Logging.log(message: "Device registred for pushes - \(token)")
        }
    }
    
    func checkForUpdatesForDevice(_ device: VirtualDevice,
                                  completion: @escaping (_ device: VirtualDevice) -> ()) {
        checkForUpdatesForDevices([device]) { (devices) in
            completion(devices.last!)
        }
    }
    
    func checkForUpdatesForDevices(_ devices: [VirtualDevice],
                                   completion: @escaping (_ devices: [VirtualDevice]) -> () ) {
        let devicesDictionary = devices.map { return
            ["sku": $0.info.sku,
             "series": $0.info.serial,
             "deviceVersion": $0.info.hw,
             "version": $0.info.version.composedVersion()] }
        let paramsJsonData = try! JSONSerialization.data(withJSONObject: devicesDictionary, options:[])
        let paramsJsonString = String(data: paramsJsonData, encoding: String.Encoding.utf8)
        
        AF.request("\(baseUrl)/api/v1/firmware/list",
                          method: .post,
                          parameters: nil,
                          encoding: paramsJsonString!,
                          headers: ["Content-Type":"application/json"])
            .responseString { response in
                if let responsedData = response.data , (response.data?.count)! > 0 {
                    guard let fetchedUpdatesDictionary =
                        try? JSONSerialization.jsonObject(with: responsedData,
                                                          options: .mutableContainers) as? Array<Dictionary<String,AnyObject>> else {
                                                            completion(devices)
                                                            return
                    }
                    
                    let realm = try! Realm()
                    for device in devices where !device.isInvalidated {
                        try! realm.write {
//                        device.deviceUpdateInfoModel?.lastCheckForUpdates = Date()
                            device.firmware.lastCheckForUpdates = Date()
                        
                        var isUpdated = false
                            fetchedUpdatesDictionary.forEach { updates in
                            let model = CheckForUpdatesModel(checkDict: updates as NSDictionary)
                            
                            if let sku = model.sku, let hardwareVersion = model.hardwareVersion, device.info.sku == sku &&
                                device.info.hw == hardwareVersion {
                                device.firmware = VirtuaFirmwareInfo()
                                device.firmware.version = model.version
                                device.firmware.sizeInBytes = model.sizeInBytes!
                                device.firmware.crc32 = model.crc32!
                                device.firmware.firmwareDescription = model.firmwareDescription ?? ""
                                isUpdated = true
                            }
                        }
                            if !isUpdated {
                                device.firmware.version = device.info.version
                            }
                        }
                    }
                }
                self.storeManager.store(devices)
                UserDefaults.standard.set(NSDate(), forKey: "lastCheckForUpdate")
                UserDefaults.standard.synchronize()
                completion(devices)
        }
    }
    
    func downloadFirmwareForDevice(_ device: VirtualDevice, progressBlock: @escaping ProgressBlock, finishBlock: @escaping FinishBlock) {

        let firmwareUrl: String = "\(baseUrl)/api/v1/firmware/download/\(device.info.sku)/\(device.firmware.version!.composedVersion())/\(device.info.hw)"
        Logging.log(message: "Download firm started - \(firmwareUrl)")
        
        let destination: DownloadRequest.Destination = { _, _ in
            var fileUrl: URL!
            DispatchQueue.main.sync {
                fileUrl = URL(fileURLWithPath: FilesManager.shared.pathToFirmwareFromDevice(device).rawValue)
            }
            return (fileUrl, [.createIntermediateDirectories, .removePreviousFile])
        }
        
        currentDownloadTask = networkManager.download(firmwareUrl, to: destination)
            .downloadProgress() { progress in
                Logging.log(message: "Firm progress - \(progress)")
                let progressPercentage = Float(progress.fractionCompleted)
                DispatchQueue.main.async {
                    progressBlock(progressPercentage)
                }
            }.response { response in
                self.currentDownloadTask = nil
                
                let error: Error? = response.error
                if error == nil {
                    Logging.log(message: "Firm ready")
                    let file = File<NSData>(path: FilesManager.shared.pathToFirmwareFromDevice(device) )
                    let fileData = try? file.read()
                    if let data = fileData {
                        let localCrc32 = data.crc32()
                        var remoteCrc32: UInt32 = 0;
                        Scanner(string: device.firmware.crc32).scanHexInt32(&remoteCrc32);
                        
                        if localCrc32 != remoteCrc32 {
                            Logging.log(message: "Firm crc error - localcrc(\(localCrc32) - remotecrc(\(remoteCrc32)")
                            
                            _ = try? file.delete()
                            DispatchQueue.main.async {
                                finishBlock(NSError(domain: "crc_corrupted", code: 0, userInfo: nil))
                            }
                            return
                        }
                    }
                }

                DispatchQueue.main.async {
                    finishBlock(response.error as NSError?)
                }
        }
    }
    
    func sendStatistic(sku: String!, serial: String!, zipURL: URL!, completion: ConnectionService.FinishBlock?) {
        AF.upload(multipartFormData: { (multipartFormData) in
            let device = UIDevice.current
            multipartFormData.append(device.identifierForVendor!.uuidString.data(using: .utf8)!,
                                     withName: "token")
            multipartFormData.append(sku.data(using: .utf8)!, withName: "sku")
            multipartFormData.append(serial.data(using: .utf8)!, withName: "serialNumber")
            multipartFormData.append(zipURL, withName: "statisticFile")
        }, to: self.uploadStatisticUrl,
           method:.post).responseJSON { (response) in
            completion?(response.error)
        }
    }
    
    func cancelDownloadFirmware() {
        currentDownloadTask?.cancel()
    }
    
    
    func reachability(with host: String = "https://www.google.com", _ completion: @escaping (_ status: ReachabilityStatus) -> () ) {
        let reachabilityManager = try? Reachability(hostname: host)
        reachabilityManager?.allowsCellularConnection = true
        
        if let reach = reachabilityManager {
            if reach.connection != .unavailable {
                Logging.log(message: "[LOG] Internet rechable")
                completion(.reachable)
            } else {
                Logging.log(message: "[LOG] Internet not rechable")
                completion(.notReachable)
            }
        } else {
            Logging.log(message: "[LOG-Error] Internet not rechable")
            completion(.notReachable)
        }
    }

}
