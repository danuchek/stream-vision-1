//
//  ConnectionManager.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/8/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Alamofire
import FileKit
import SVProgressHUD

/**
 ConnectionStatus
 
 - Connected: When application have stable connection with Device
 - NotResponse: When application have fails with connection
 - Disconnected: When application have disconnected from Device
 - Terminated: When application had connection but it was disconnected
 */

enum ReachabilityStatus {
    case notReachable
    case reachable
}

/**
 ConnectionManager
 
 Manager for connection with device. Across this manager go on all communication with connected device.
 
 - sharedConnection : Singleton
 - Status: Use for current status of connection with device
 - sendCommand(_:) : Use for sending command to device. Command has diffirent initializers for your need
 
 */
class ConnectionService {
    //
    static let networkConnection = NetworkConnection()
    
    // Types
    typealias InitializeBlock = (_ totalIterations: Int) -> ()
    typealias ProgressBlock = (_ percentage: Float) -> ()
    typealias FinishBlock = (_ error: Error?) -> ()

    // MARK: Networking
    class func sendPushToken(_ token: String) {
        networkConnection.sendPushToken(token)
    }
    
    class func checkForUpdatesForDevice(_ device: VirtualDevice, completion: @escaping (_ device: VirtualDevice) -> ()) {
        networkConnection.checkForUpdatesForDevice(device, completion: completion)
    }
    
    class func checkForUpdatesForDevices(_ devices: [VirtualDevice], completion: @escaping (_ devices: [VirtualDevice]) -> () ) {
        networkConnection.checkForUpdatesForDevices(devices, completion: completion)
    }
    
    class func downloadFirmwareForDevice(_ device: VirtualDevice, progressBlock: @escaping ProgressBlock, finishBlock: @escaping FinishBlock) {
        networkConnection.downloadFirmwareForDevice(device, progressBlock: progressBlock, finishBlock: finishBlock)
    }
    
    class func cancelDownloadFirmware() {
        networkConnection.cancelDownloadFirmware()
    }
    
    class func reachability(_ completion: @escaping (_ status: ReachabilityStatus) -> () ) {
        networkConnection.reachability(completion)
    }
    
    class func sendStatistic(sku: String!, serial: String!, zipURL: URL!, completion: FinishBlock? = nil) {
        networkConnection.sendStatistic(sku: sku, serial: serial, zipURL: zipURL, completion: completion)
    }

}
