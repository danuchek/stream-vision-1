//
//  FileDownloadService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/17/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import FileKit

class FileDownloadService {
    typealias Progress = (_ downloadProgress: ResponseProgress) -> ()
    typealias FinishFile = (_ finish: ResponseProgressFile) -> ()
    typealias Finish = (_ error: NSError?) -> ()
    
    struct ResponseProgress {
        let folder: FolderModel
        let file: FileModel
        let totalBytes: Float
    }
    
    struct ResponseProgressFile {
        let folder: FolderModel
        let file: FileModel
    }
    
    struct GroupDownload {
        let folders: [FolderModel]
        let progress: Progress
        let finishFile: FinishFile
        let finish: Finish
    }
    
    struct GroupDelete {
        let folders: [(FolderModel, Bool, FileManagerDataSource.FileLocation)]
        let progress: Progress
        let finish: Finish
    }
    
    enum DownloadStatus {
        case ready
        case downloading
    }
    
    private var downloadQueue: [GroupDownload] = []
    private let groupDownloadService = GroupDownloadService()
    private let groupDeleteService = GroupDeleteService()
    
    private var downloadStatus: DownloadStatus = .ready
    
    func downloadFolders(folders: [FolderModel],
                         progress: @escaping Progress,
                         fileFinished: @escaping FinishFile,
                         finish: @escaping Finish ) {
        
        let groupDownload = GroupDownload(folders: folders,
                                          progress: progress,
                                          finishFile: fileFinished,
                                          finish: { error in
            self.downloadQueue.removeAll()
            finish(error)
        })
        downloadQueue.append(groupDownload)
        startDownload()
    }
    
    func deleteFiles(folders: [(FolderModel, Bool, FileManagerDataSource.FileLocation)],
                     device: DeviceModel,
                     progress: @escaping Progress,
                     finish: @escaping Finish) {
        let groupDelete = GroupDelete(folders: folders, progress: progress, finish: { error in
            finish(error)
        })
        groupDeleteService.device = device
        groupDeleteService.operation = groupDelete
        groupDeleteService.delete()
    }
    
    private func startDownload() {
        switch downloadStatus {
        case .ready:
            if let operation = downloadQueue.last {
                self.groupDownloadService.operation = operation
                self.groupDownloadService.download()
            }
        case .downloading: break
        }
    }
    
    func cancellDownload() {
        downloadQueue.removeAll()
        groupDownloadService.cancellDownload()
    }
    
    func cancellDelete() {
        groupDeleteService.cancellDeleting()
    }
    
}

class GroupDeleteService {
    var operation: FileDownloadService.GroupDelete! {
        didSet {
            folders = operation.folders
            progress = operation.progress
            finish = operation.finish
            
            totalRemoved = 0
            folders.forEach() { folder in
                self.totalFiles += folder.0.files.count
            }
        }
    }
    
    private var folders: [(FolderModel, Bool, FileManagerDataSource.FileLocation)]!
    private var currentFolder: (FolderModel, Bool, FileManagerDataSource.FileLocation)!
    private var progress: FileDownloadService.Progress!
    private var finish: FileDownloadService.Finish!
    private var totalFiles: Int = 0
    private var totalRemoved: Int = 0
    
    private var cancelled: Bool = false
    var device: DeviceModel!
    
    private func nextDelete() {
        delete()
    }
    
    func delete() {
        guard folders.count > 0 else {
            return
        }
        
        currentFolder = folders.last!
        let file = self.currentFolder.0.files.last!
        
        if currentFolder.2 == .remote {
            SocketManager.send(.delete, params: ["path":"1:/\(currentFolder.0.name!)/\(file.name!)"])
                .response { model, error in
                    guard !self.cancelled else {
                        self.cancelled = false
                        self.finishDeletingWith(file, error)
                        return
                    }
                    
                    self.resolveFoldersQueue(file: file)
                    
                    if self.folders.count == 0 {
                        self.finishDeletingWith(file, error)
                    } else {
                        self.totalRemoved += 1
                        self.progress( FileDownloadService.ResponseProgress(folder: self.currentFolder.0,
                                                                            file: file,
                                                                            totalBytes: Float(self.totalRemoved) / Float(self.totalFiles) ) )
                        
                        self.nextDelete()
                    }
            }
        } else if currentFolder.2 == .local {
            let pathToFile = FilesManager.sharedFileManager.pathToLocalFile(file,
                                                                            folder: self.currentFolder.0,
                                                                            device: self.device)
            let pathToInfoFile = FilesManager.sharedFileManager.pathToLocalInfoFile(file,
                                                                                    folder: self.currentFolder.0,
                                                                                    device: self.device)
            DispatchQueue.global().async {
                _ = try? pathToFile.deleteFile()
                _ = try? pathToInfoFile.deleteFile()
            }
            
            self.resolveFoldersQueue(file: file)
            if self.folders.count == 0 {
                self.finishDeletingWith(file, nil)
            } else {
                self.totalRemoved += 1
                self.progress( FileDownloadService.ResponseProgress(folder: self.currentFolder.0,
                                                                    file: file,
                                                                    totalBytes: Float(self.totalRemoved) / Float(self.totalFiles) ) )
                
                self.nextDelete()
            }
        }
    }
    
    fileprivate func finishDeletingWith(_ file: FileModel, _ error: NSError?) {
        self.progress( FileDownloadService.ResponseProgress(folder: self.currentFolder.0,
                                                            file: file,
                                                            totalBytes: error == nil ? 1 : -1 ) )
        self.finish(error)
    }
    
    fileprivate func resolveFoldersQueue(file: FileModel) {
        if let folder = self.folders.last {
            guard let index = self.folders.index(where: { $0.0 == folder.0 && $0.2 == folder.2 }) else {
                return
            }
            
            var mutableFolder = self.folders[index]
            let fileIndex = mutableFolder.0.files.index(of: file)
            mutableFolder.0.files.remove(at: fileIndex!)
            self.folders[index] = mutableFolder
            
            if mutableFolder.0.files.count == 0 {
                if mutableFolder.1 {
                    if mutableFolder.2 == .remote {
                        SocketManager.send(.delete, params: ["path":"1:/\(mutableFolder.0.name!)"])
                    } else if mutableFolder.2 == .local {
                        _ = try? FilesManager.sharedFileManager.pathToLocalFolder(currentFolder.0,
                                                                                  device: device).deleteFile()
                        _ = try? FilesManager.sharedFileManager.pathToLocalInfoFolder(currentFolder.0,
                                                                                      device: device).deleteFile()
                    }
                }
                self.folders.removeLast()
            }
        }
    }
    
    func cancellDeleting() {
        cancelled = true
        SocketManager.cancellDeleting()
    }
    
}

class GroupDownloadService {
    var operation: FileDownloadService.GroupDownload! {
        didSet {
            folders = operation.folders
            progress = operation.progress
            finish = operation.finish
            finishFile = operation.finishFile
            
            self.totalReaded = 0
            self.totalBytes = 0
            operation.folders.forEach() { folder in
                self.totalBytes += Float(folder.folderSize())
            }
        }
    }
    
    private var folders: [FolderModel]!
    private var currentFolder: FolderModel!
    private var progress: FileDownloadService.Progress!
    private var finish: FileDownloadService.Finish!
    private var finishFile: FileDownloadService.FinishFile!
    private var totalBytes: Float = 0.0
    private var totalReaded: Float = 0.0
    
    private var cancelled: Bool = false
    var withFolder: Bool = false

    private func nextDownload() {
        download()
    }
    
    func download() {
        guard folders.count > 0 else {
            return
        }
        currentFolder = folders.last!
        let file = Array(self.currentFolder.files).last!
        SocketManager.download(params: ["path":"1:/\(self.currentFolder.name!)/\(file.name!)", "size": "\(file.size!)"])
            .progress { bytesReaded in
                self.totalReaded += bytesReaded.floatValue
                DispatchQueue.main.async {
                    self.progress(FileDownloadService.ResponseProgress(folder: self.currentFolder!,
                                                                       file: file,
                                                                       totalBytes: (self.totalReaded / self.totalBytes)))
                }
            }.response { model, error in
                guard !self.cancelled else {
                    self.cancelled = false
                    self.finishDownloadWith(file, error)
                    return
                }
                
                if error == nil, model != nil {
                    if let dataModel = model as? Data {
                        self.saveFile(folder: self.currentFolder, file: file, model: dataModel)
                    } else {
                        debugPrint("File model is not DATA!!!")
                    }
                }
                if let folder = self.folders.last {
                    let index = self.folders.index(of: folder)
                    var mutableFolder = self.folders[index!]
                    if let fileIndex = mutableFolder.files.index(of: file) {
                        mutableFolder.files.remove(at: fileIndex)
                    }
                    self.folders[index!] = mutableFolder
                    
                    if mutableFolder.files.count == 0 {
                        self.folders.removeLast()
                    }
                }
                
                if error == nil {
                    self.finishFile(FileDownloadService.ResponseProgressFile(folder: self.currentFolder!, file: file) )
                }
                
                if self.folders.count == 0 {
                    DispatchQueue.main.async {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.finishDownloadWith(file, error)
                        }
                    }
                } else {
                    self.nextDownload()
                }
        }
    }
    
    fileprivate func finishDownloadWith(_ file: FileModel, _ error: NSError?) {
        self.progress( FileDownloadService.ResponseProgress(folder: self.currentFolder!,
                                                            file: file,
                                                            totalBytes: error == nil ? 1 : -1 ) )
        self.finish(error)
    }
    
    func cancellDownload() {
        cancelled = true
        SocketManager.cancellDownload()
    }
    
    fileprivate func saveFile(folder: FolderModel, file: FileModel, model: Data, completion: (() -> ())? = nil) {
        guard let device = SocketManager.connectedDevice else {
            return
        }
        
        let fileManager = FilesManager.sharedFileManager
        let folderPath = fileManager.pathToLocalFolder(folder, device: device)
        _ = try? folderPath.createDirectory()
        DispatchQueue.main.async {
            fileManager.saveModelInfoFor(folder: folder)
            fileManager.saveModelInfoFor(folder: folder, file: file)
        }
        
        DispatchQueue.global().async {
                do {
                    try File<Data>(path: folderPath + "/\(file.name!)").write(model)
                } catch {
                    debugPrint("Folder moldel - \(folder.name!), File model - \(file.name!), Cant be save")
                }
        }
    }
}
