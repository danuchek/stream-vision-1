//
//  FileDownloadService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/17/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import FileKit
import RealmSwift

class DownloadingRequest: Object {
    @objc dynamic var id: Date = Date()
    
    @objc dynamic var deviceName: String = ""
    @objc dynamic var folderName: String = ""
    @objc dynamic var folderDate: Date = Date()
    @objc dynamic var fileName: String = ""
    @objc dynamic var fileDate: Date = Date()
    @objc dynamic var fileSize: Int = 0
    
    var requestHash: String {
        return deviceName + folderName + fileName
    }
}

class FileOperationsService: SocketConnectionSubsriber {
    typealias Progress = (_ downloadProgress: DownloadProgress) -> ()
    typealias DeleteProgress = (_ downloadProgress: ResponseProgress) -> ()
    typealias Finish = (_ error: NSError?) -> ()
    typealias FinishFile = (_ finish: DownloadResponse) -> ()
    typealias DeleteFinish = (_ error: NSError?) -> ()

    struct DownloadProgress {
        let request: DownloadingRequest
        let totalBytes: Float
    }

    struct DownloadResponse {
        let request: DownloadingRequest
        let folder: FolderModel
        let file: FileModel
        let error: Error?
    }
    
    struct ResponseProgress {
        let request: DeleteRequest
        let totalBytes: Float
    }
    
    struct ResponseProgressFile {
        let folder: FolderModel
        let file: FileModel
    }

    
    struct DeleteRequest {
        let deviceName: String
        let folderName: String
        let folderDate: Date
        var fileName: String
        let fileDate: Date
        let fileSize: Int
        
        let isFolder: Bool
        var location: FilesDataSource.FileLocation
        
        let progress: DeleteProgress
        let finish: DeleteFinish

        var fileHash: String {
            return deviceName + folderName + fileName
        }
    }
    static let FileDownloadedNotification = "FileIsDownloaded"

    private let groupDownloadService = GroupDownloadService()
    private let groupDeleteService = GroupDeleteService()
    private var downloadingFiles: [String] {
        return groupDownloadService.requests.map({ request in
            request.requestHash
        })
    }
    
    private var progressSubsribers: [String : FilesManager.DownloadingProgressCallback] = [:]
    private var finishSubsribers: [String : FilesManager.DownloadingFinishCallback] = [:]
    
    var isDownloading: Bool {
        return groupDownloadService.status == .downloading
    }
    
    var isCanBeResumed: Bool {
        return groupDownloadService.requests.count > 0 && groupDownloadService.status == .interupted
    }
    
    init() {
        SocketManager.subscribe(self, notificationType: .DeviceDisconnected)
    }
    
    func didFireNotification(notification: SocketConnection.Notifications, model: Any?) {
        switch notification {
        case .DeviceDisconnected:
            groupDownloadService.status = .interupted
        default: break

        }
    }
    
    func subscribeOnDonwloadingProgress(fileHash: String, callback: @escaping FilesManager.DownloadingProgressCallback ) {
        progressSubsribers[fileHash] = callback
    }
    
    func subscribeOnDownloadingFinish(fileHash: String, callback: @escaping FilesManager.DownloadingFinishCallback ) {
        finishSubsribers[fileHash] = callback
    }
    
    func isDownloading(hash: String) -> Bool {
        return groupDownloadService.currentRequest?.requestHash == hash
    }
    
    func isInDownloadingQueue(hash: String) -> Bool {
        return downloadingFiles.contains(hash)
    }
    func resumeDownloading() {
        groupDownloadService.status = .ready
        groupDownloadService.resume()
    }
    
    func stopDownloading() {
        if groupDownloadService.status != .interupted {
            groupDownloadService.requests = []
        }
        if isDownloading {
            groupDownloadService.stop()
        }
        finishSubsribers = [:]
        progressSubsribers = [:]
    }

    func downloadFolders(folders: [FolderModel], for device: VirtualDevice) {
        var requests: [DownloadingRequest] = []
        folders.forEach { (folder) in
            requests.append(contentsOf:
                folder.files.map({ (file) -> DownloadingRequest in
                    let request = DownloadingRequest()
                    request.id = Date()
                    request.folderName = folder.name
                    request.folderDate = folder.date
                    request.fileName = file.name
                    request.fileSize = file.size
                    request.fileSize = file.size
                    request.deviceName = device.deviceString
                    return request
                }))
        }
        download(with: requests)
    }
    
    private func download(with requests: [DownloadingRequest]) {
        Logging.log(message: "[FS] Download: \(requests)", toFabric: true)
        if groupDownloadService.status == .interupted {
            groupDownloadService.status = .ready
        }
        
        groupDownloadService.progress = { downloadProgress in
            let callback = self.progressSubsribers[downloadProgress.request.requestHash]
            DispatchQueue.main.async {
                callback?(downloadProgress.totalBytes, downloadProgress.request.requestHash)
            }
            
        }
        
        groupDownloadService.finishFile = { response in
            Logging.log(message: "[FS] File with hash:\(response.request.requestHash) finished", toFabric: true)
            let hash = response.request.requestHash
            self.finishSubsribers[hash]?(response.folder, response.file, response.request.requestHash, response.error)
            self.finishSubsribers.removeValue(forKey: hash)
            self.progressSubsribers.removeValue(forKey: hash)
        }
        
        self.groupDownloadService.download(with: requests)
    }

    func deleteFiles(folders: [(folder: FolderModel, withFolder: Bool)],
                     device: VirtualDevice,
                     location: FilesDataSource.FileLocation,
                     progress: @escaping DeleteProgress,
                     finish: @escaping DeleteFinish) {
        var requests: [DeleteRequest] = []
        folders.forEach { (data) in
                data.folder.files.forEach({ file in
                    var request =
                    DeleteRequest(deviceName: device.deviceString,
                                  folderName: data.folder.name,
                                  folderDate: data.folder.date,
                                  fileName: file.name,
                                  fileDate: file.date,
                                  fileSize: file.size,
                                  isFolder: false,
                                  location: location,
                                  progress: progress,
                                  finish: finish)
                    if location == .mixed {
                        if file.isLocal {
                            requests.append(
                                DeleteRequest(deviceName: device.deviceString,
                                              folderName: data.folder.name,
                                              folderDate: data.folder.date,
                                              fileName: file.name,
                                              fileDate: file.date,
                                              fileSize: file.size,
                                              isFolder: false,
                                              location: .local,
                                              progress: progress,
                                              finish: finish))
                        }
                        request.location = .remote
                    }
                    // Update for converted and remote file comparence
                    if request.fileName.contains("mov") && request.location == .remote {
                        request.fileName = request.fileName.replacingOccurrences(of: "mov", with: "avi")
                    }
                    if (request.location == .remote && file.isRemote) || request.location == .local {
                        requests.append(request)
                    }
                })
            if data.withFolder {
                    var request =
                        DeleteRequest(deviceName: device.deviceString,
                                      folderName: data.folder.name,
                                      folderDate: data.folder.date,
                                      fileName: "",
                                      fileDate: Date(),
                                      fileSize: 0,
                                      isFolder: data.withFolder,
                                      location: location,
                                      progress: progress,
                                      finish: finish)
                if location == .mixed  {
                    if data.folder.isLocal {
                        requests.append(
                            DeleteRequest(deviceName: device.deviceString,
                                          folderName: data.folder.name,
                                          folderDate: data.folder.date,
                                          fileName: "",
                                          fileDate: Date(),
                                          fileSize: 0,
                                          isFolder: data.withFolder,
                                          location: .local,
                                          progress: progress,
                                          finish: finish))
                    }
                    request.location = .remote
                }
                requests.append(request)
            }
        }
        groupDeleteService.delete(with: requests)
    }
    
    func cancelDownload(by hash: String) {
        Logging.log(message: "[FS] Cancel download by hash: \(hash)", toFabric: true)
    
        self.finishSubsribers.removeValue(forKey: hash)
        self.progressSubsribers.removeValue(forKey: hash)
        groupDownloadService.cancelDownload(by: hash)
    }

    func cancellAllDownload() {
        Logging.log(message: "[FS] Cancel download", toFabric: true)
        
        groupDownloadService.cancellDownload()
    }

    func cancellDelete() {
        Logging.log(message: "[FS] Cancel delete", toFabric: true)
        
        groupDeleteService.cancellDeleting()
    }

}

// MARK: - Group delete service

class GroupDeleteService {
    private var requests: [FileOperationsService.DeleteRequest] = []
    private var currentRequest: FileOperationsService.DeleteRequest!
    private var progress: FileOperationsService.DeleteProgress!
    private var finish: FileOperationsService.DeleteFinish!
    private var totalFiles: Int = 0
    private var totalRemoved: Int = 0

    private var cancelled: Bool = false
    var device: VirtualDevice!

    private func nextDelete() {
        delete()
    }
    
    func delete(with requests: [FileOperationsService.DeleteRequest]) {
        self.requests.insert(contentsOf: requests, at: self.requests.count)
        delete()
    }
    
    func deleteFolder(with request: FileOperationsService.DeleteRequest) {
        Logging.log(message: "[FS] Delete folder - requests: \(requests)", toFabric: true)
        
        switch request.location {
        case .remote:
            SocketManager.send(.delete, params: ["path":"1:/\(request.folderName)"]).response { model, error in
                if self.requests.count > 0 {
                    self.delete()
                } else {
                    self.finishDeletingWith(request, error)
                }
            }
            self.requests.removeFirst()
        case .local:
            let pathToFile = FilesManager.shared.defaultDir + "/\(request.deviceName)/\(request.folderName)"
            let pathToInfoFile = Path(pathToFile.rawValue + ".folderinfo")
           
            try? pathToFile.deleteFile()
            try? pathToInfoFile.deleteFile()
            self.requests.removeFirst()
            
            if requests.count > 0 {
                self.delete()
            } else {
                self.finishDeletingWith(request, nil)
            }
        default: break
        }
    }

    func delete() {
        Logging.log(message: "[FS] Delete - requests: \(requests)", toFabric: true)
        
        guard let currentRequest = requests.first else {
            return
        }
        guard !currentRequest.isFolder else {
            deleteFolder(with: currentRequest)
            return
        }

        self.currentRequest = currentRequest
        if currentRequest.location == .remote {
            SocketManager.send(.delete, params: ["path":"1:/\(currentRequest.folderName)/\(currentRequest.fileName)"])
                .response { model, error in
                    guard !self.cancelled else {
                        self.cancelled = false
                        self.finishDeletingWith(currentRequest, error)
                        return
                    }
                    
                    if self.requests.count > 0 {
                        self.requests.removeFirst()
                    }
                    if self.requests.count == 0 {
                        self.finishDeletingWith(currentRequest, error)
                    } else {
                        self.totalRemoved += 1
                        currentRequest.progress( FileOperationsService.ResponseProgress(request: currentRequest,
                                                                            totalBytes: Float(self.totalRemoved) / Float(self.totalFiles) ) )
                        self.nextDelete()
                    }
            }
        } else if currentRequest.location == .local {
            let pathToFile = FilesManager.shared.defaultDir + "/\(currentRequest.deviceName)/\(currentRequest.folderName)/\(currentRequest.fileName)"
            let pathToInfoFile = Path(pathToFile.rawValue + ".fileinfo")
            _ = try? pathToFile.deleteFile()
            _ = try? pathToInfoFile.deleteFile()

            self.requests.removeFirst()
            if self.requests.count == 0 {
                self.finishDeletingWith(currentRequest, nil)
            } else {
                self.totalRemoved += 1
                currentRequest.progress( FileOperationsService.ResponseProgress(request: currentRequest,
                                                                    totalBytes: Float(self.totalRemoved) / Float(self.totalFiles) ) )
                self.nextDelete()
            }
        }
    }

    fileprivate func finishDeletingWith(_ request: FileOperationsService.DeleteRequest, _ error: NSError?) {
        request.progress( FileOperationsService.ResponseProgress(request: request,
                                                            totalBytes: error == nil ? 1 : -1 ) )
        request.finish(error)
    }

    func cancellDeleting() {
        Logging.log(message: "[FS] Cancell deleting - requests: \(requests)", toFabric: true)
        
        cancelled = true
        SocketManager.cancellDeleting()
    }

}

// MARK: - Group download service

final class GroupDownloadService {
    enum DownloadStatus {
        case ready
        case downloading
        case interupted
    }
    
    var requests: [DownloadingRequest] = []
    fileprivate var currentRequest: DownloadingRequest!
    var progress: FileOperationsService.Progress?
    var finishFile: FileOperationsService.FinishFile?
    private var totalBytes: Float = 0.0
    private var totalReaded: Float = 0.0
    
    private var cancelled: Bool = false 
    var status: DownloadStatus = .ready

    private func nextDownload() {
        self.totalReaded = 0
        download()
    }
    
    func download(with requests: [DownloadingRequest]) {
        self.requests.insert(contentsOf: requests, at: self.requests.count)
        if status != .downloading {
            download()
        }
    }
    
    func resume() {
        totalReaded = 0
        download()
    }

    func download() {
        guard status != .interupted else {
            return
        }
        guard let currentRequest = requests.first else {
            status = .ready
            return
        }
        status = .downloading

        Logging.log(message: "[FS] New request started - \(currentRequest)", toFabric: true)
        self.currentRequest = currentRequest
        
        // Demo mode stub
        guard !currentRequest.fileName.contains("demo") else {
            if currentRequest.fileName.contains("photo") {
                saveFile(request: currentRequest, path: Path(Bundle.main.path(forResource: "demo_vid", ofType: ".avi", inDirectory: nil)!).rawValue)
            } else {
                saveFile(request: currentRequest, path: Path(Bundle.main.path(forResource: "demo_scr", ofType: ".jpg", inDirectory: nil)!).rawValue)
            }
            self.requests.removeFirst()
            self.nextDownload()
            return
        }
        
        Logging.log(message: "[FS] Download start \(currentRequest.fileName) size(\(currentRequest.fileSize))", toFabric: true)
        SocketManager.download(params: ["path":"1:/\(self.currentRequest.folderName)/\(currentRequest.fileName)", "size": "\(currentRequest.fileSize)"])
            .progress { bytesReaded in
                self.totalReaded += bytesReaded.floatValue
                DispatchQueue.main.async {
                    if self.currentRequest.fileSize != 0 {
                        self.progress?(
                        FileOperationsService.DownloadProgress(request: self.currentRequest,
                                                               totalBytes: (self.totalReaded / Float(self.currentRequest.fileSize))))
                    }
                }
            }.response { model, error in
                Logging.log(message: "[FS] Request response - \(String(describing: model)), error -\(String(describing: error))", toFabric: true)
                guard self.status != .interupted else {
                    return
                }
                self.cancelled = false

                if error == nil, model != nil {
                    if let dataModel = model as? Data, let filePath = String(data: dataModel, encoding: .utf8) {
                        self.saveFile(request: currentRequest, path: filePath)
                    } else {
                        Logging.log(message: "[FS] File model is not DATA!!!", toFabric: true)
                    }
                } else {
                    let folder = FolderModel(name: currentRequest.folderName,
                                             size: 0,
                                             date: currentRequest.folderDate)
                    let file = FileModel(name: currentRequest.fileName,
                                         size: currentRequest.fileSize,
                                         date: currentRequest.fileDate,
                                         path: nil)
                    let error = NSError(domain: "Downloading error", code: 0, userInfo: nil)
                    self.finishFile?(FileOperationsService.DownloadResponse(request: currentRequest,
                                                                            folder: folder,
                                                                            file: file,
                                                                            error: error))
                }

                if self.requests.count == 0 {
                    self.status = .ready
                } else {
                    if let index = self.requests.firstIndex(of: currentRequest) {
                        self.requests.remove(at: index)
                    }
                    self.nextDownload()
                }
        }
    }
    
    func cancelDownload(by hash: String) {
        Logging.log(message: "[FS] Cancell downloading - request: \(requests)", toFabric: true)
        
        if self.currentRequest.requestHash == hash {
            cancellDownload()
            status = .ready
        } else {
            if let index = requests.firstIndex(where: { (request) -> Bool in
                return request.requestHash == hash }) {
                requests.remove(at: index)
            }
        }
    }

    func cancellDownload() {
        Logging.log(message: "[FS] Cancell downloading - request: \(requests)", toFabric: true)
        
        cancelled = true
        SocketManager.cancellDownload()
    }
    
    func stop() {
        Logging.log(message: "[FS] Stop downloading", toFabric: true)
        
        status = .interupted
        SocketManager.cancellDownload()
    }

    fileprivate func saveFile(request: DownloadingRequest, path: String) {
        guard let device = SocketManager.connectedDevice else {
            return
        }
        Logging.log(message: "[FS] Save file by request - \(String(describing: currentRequest))", toFabric: true)
        
        let fileManager = FilesManager.shared
        var folder = FolderModel(name: request.folderName, size: 0, date: request.folderDate)
        let file = FileModel(name: request.fileName, size: request.fileSize, date: request.fileDate, path: path)
        fileManager.saveModelInfoFor(folder: folder, file: file)
        
        let pathToTempFile = Path(path)
        let tempFile = File<Data>(path: pathToTempFile)
        
        var newPath: Path!
        if !Thread.isMainThread {
            DispatchQueue.main.sync {
                newPath = fileManager.pathToFile(file, folder: folder, device: device)
            }
        } else {
            newPath = fileManager.pathToFile(file, folder: folder, device: device)
        }
        Logging.log(message: "[FS] I try to move file to \(String(describing: newPath))", toFabric: true)
        Logging.log(message: "[FS] Old file exist:\(tempFile.exists)", toFabric: true)
        Logging.log(message: "[FS] Old file desc:\(tempFile.description)", toFabric: true)
        do {
            Logging.log(message: "[FS] I move temp file with size - \(String(describing: tempFile.size))", toFabric: true)
            try tempFile.move(to: newPath)
        } catch {
            Logging.log(message: "[FS] I can't move file", toFabric: true)
            Logging.log(message: "[FS] Reason \(error)", toFabric: true)
            
        }
        
        DispatchQueue.main.async {
            folder.path = fileManager.pathToLocalFolder(folder, device: device).rawValue
            file.path = newPath.rawValue
        
            self.finishFile?(FileOperationsService.DownloadResponse(request: self.currentRequest,
                                                                    folder: folder,
                                                                    file: file,
                                                                    error: nil))
        }
    }
}
