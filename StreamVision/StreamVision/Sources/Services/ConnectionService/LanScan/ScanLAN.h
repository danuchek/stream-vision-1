//
//  ScanLAN.h
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/2/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ScanLANDelegate <NSObject>

@optional
- (void)scanLANDidFindNewAdrress:(NSString *)address havingHostName:(NSString *)hostName;
- (void)scanLANDidFinishScanning;
- (void)hotspotUnavailable;
@end

@interface ScanLAN : NSObject

@property(nonatomic,weak) id<ScanLANDelegate> delegate;

- (id)initWithDelegate:(id<ScanLANDelegate>)delegate;
- (void)startScan;
- (void)stopScan;

@end
