//
//  ClientAuthService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/22/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import GoogleAPIs
import GoogleSignIn


class GoogleAuthService: NSObject, GIDSignInDelegate, LiveAuthServiceProtocol {
    typealias CompletionBlock = (_ authModel: LiveAuthModel?, _ error: LiveErrorValidation.LiveError?) -> ()
    typealias YoutubeCompletion = (_ authModel: LiveStreamingModel?, _ error: LiveErrorValidation.LiveError?) -> ()
    
    weak var presenterViewController: UIViewController? {
        didSet {
            googleSignIn.presentingViewController = presenterViewController
        }
    }
    fileprivate let googleSignIn = GIDSignIn.sharedInstance()!
    fileprivate var completion: CompletionBlock?
    private var userToken: String!
    
    // Scopes with default youtube scope
    var scopes: Array<String> = ["https://www.googleapis.com/auth/youtube"] {
        didSet {
            googleSignIn.scopes = scopes
        }
    }
    
    // MARK: Base model
    public private(set) var model: LiveAuthModel?
    
    // MARK: Protocol
    var isAuthorized: Bool {
        return googleSignIn.currentUser != nil || googleSignIn.hasPreviousSignIn()
    }
    
    var isAbleToStream: Bool {
        return model?.live != nil
    }
    
    override var description: String {
        return "Google"
    }
    
    // MARK: Init
    override init() {
        super.init()
        
        googleSignIn.scopes = scopes
        googleSignIn.delegate = self
        googleSignIn.shouldFetchBasicProfile = true
//        googleSignIn.serverClientID =
//        "917495371193-754nhismdjrhes1dcn1blguksbdhbve9.apps.googleusercontent.com"
        //GIDGoogleUser.profileData
    }
    
    // MARK: Protocol methods
    func login(completion: ((LiveAuthModel?, LiveErrorValidation.LiveError?) -> ())?) {
        signIn(completion)
    }
    
    func logOut() {
        googleSignIn.signOut()
        googleSignIn.disconnect()
        model = nil
    }
    
    // MARK: Google delegate methods
    func signInSilently(_ completion: CompletionBlock? ) {
        self.completion = completion
        
        signIn(completion)
//        googleSignIn.signInSilently()
    }
    
    func signIn(_ completion: CompletionBlock? ) {
        self.completion = completion
        
        if (googleSignIn.hasPreviousSignIn()) {
//            signInSilently(completion)
            googleSignIn.signIn()
        } else {
            googleSignIn.signIn()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        presenterViewController?.present(viewController, animated: false, completion: nil)
    }

    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        var resultModel: LiveAuthModel?
        if let user = user {
            resultModel = LiveAuthModel(user: LiveUserModel(userId: user.userID,
                                                            token: user.authentication.idToken,
                                                            name: user.profile.name,
                                                            email: user.profile.email),
                                        live: nil)
            self.model = resultModel
        }
        if error == nil && user != nil {
            userToken = user.authentication.accessToken
            loadYoutubeModel { model, error in
                if let streamingModel = model, error == nil {
                    resultModel?.live = streamingModel
                    self.model = resultModel
                }
                self.completion?(resultModel, error)
            }
        } else {
            completion?(nil ,LiveErrorValidation.validate(error as NSError))
        }
    }
    
    fileprivate func loadYoutubeModel(completion: YoutubeCompletion?) {
        let youtubeManager = Youtube()
        youtubeManager.mine = true
        youtubeManager.id = nil
        youtubeManager.broadcastType = YoutubeLiveBroadcastsBroadcastType.Persistent

        youtubeManager.fetcher.accessToken = userToken
        youtubeManager.listLiveBroadcasts("id,contentDetails,snippet") { (liveBroadcastlistResponse, error) in
            if let _ = error {
                completion?(nil, LiveErrorValidation.validate(error! as NSError))
            } else {
                let broadcast = liveBroadcastlistResponse!.items[0]
                if let broadcastId = broadcast.id {
                    
                    let shareUrl = "http://youtu.be/\(broadcastId)"
                    
                    //Setup manager
                    youtubeManager.id = broadcast.contentDetails.boundStreamId
                    youtubeManager.mine = nil
                    
                    youtubeManager.listLiveStreams("cdn,snippet") { liveStreamlistResponse, error in
                        if let _ = error {
                            completion?(nil, LiveErrorValidation.validate(error! as NSError))
                        } else {
                            let liveStream = liveStreamlistResponse!.items[0]
                            let ingestionInfo = liveStream.cdn.ingestionInfo
                            let rtmp = "\(ingestionInfo!.ingestionAddress!)/\(ingestionInfo!.streamName!)"
                            
                            let liveModel = LiveStreamingModel(rtmpUrl: rtmp, shareUrl: shareUrl, expirationDate: nil, liveId: nil)
                            completion?(liveModel, nil)
                        }
                    }
                } else {
                    completion?(nil, LiveErrorValidation.validate(NSError(domain: "", code: 0, userInfo: nil)))
                }
            }
        }
    }
    
}
