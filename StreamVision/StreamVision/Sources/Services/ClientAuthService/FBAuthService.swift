//
//  FacebookAuthService.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/19/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import SVProgressHUD
//import FBSDKLoginKit
//import FBSDKCoreKit
import FacebookLogin
import FacebookCore

class FBAuthService: LiveAuthServiceProtocol {
    weak var presenterViewController: UIViewController?
    public private(set) var model: LiveAuthModel?
    private let fbLoginManager = LoginManager()
    
    var userId: Int? {
        if let id = Profile.current?.userID {
            return Int(id)
        }
        return nil
    }

    // MARK: Protocol
    
    var isAuthorized: Bool {
        return Profile.current != nil
    }
    
    var isAbleToStream: Bool {
        return model?.live != nil
    }
    
    var description: String {
        return "Facebook"
    }
    
    init() {
        Profile.enableUpdatesOnAccessTokenChange(true)
        fbLoginManager.defaultAudience = .everyone
        updateUserModel()
    }
    
    private func updateUserModel() {
        if let profile = Profile.current {
            if model == nil {
                model = LiveAuthModel()
            }
            if let id = AccessToken.current?.userID,
                let token = AccessToken.current?.tokenString,
                let name = profile.name {
                model?.user = LiveUserModel(userId: id,
                                            token: token,
                                            name: name,
                                            email: nil)
            }
        }
    }
    
    func updateDescription(text: String?, completion: @escaping () -> Void) {
        var parameters: [String: String] = [:]
        if let _ = text {
            parameters["description"] = text!
        }
        
        if let liveId = model?.live?.liveId {
            GraphRequest(graphPath: "\(liveId)",
                parameters: parameters,
                httpMethod: .post).start { (connection, graphResult, error) in
                    completion()
            }
        }
    }
    
    func login(completion: ((LiveAuthModel?, LiveErrorValidation.LiveError?) -> ())?) {
        guard !isAuthorized else {
            updateRtmpUrl({ (liveModel) in
                completion?(liveModel, nil)
            })
            return
        }
        fbLoginManager.logIn(permissions: ["publish_video"],
                                  from: presenterViewController) { (loginResult, error) in
                                    SVProgressHUD.show()
                                    if let result = loginResult, !result.isCancelled {
                                        self.updateRtmpUrl({ (model) in
                                            completion?(model, nil)
                                        })
                                    } else {
                                        completion?(nil, nil)
                                        SVProgressHUD.dismiss()
                                    }
        }
    }
    
    private func fbLogin() {
        fbLoginManager.logIn(permissions: ["publish_video"],
                             from: presenterViewController) { (loginResult, error) in
                                SVProgressHUD.show()
                                if let result = loginResult, !result.isCancelled {
                                    
                                } else {
                                    
                                }
                                
        }
    }
    
    func updateRtmpUrl(_ completion: ( (_ model: LiveAuthModel?) -> () )?) {
        guard let userId = AccessToken.current?.userID else {
            logOut()
            completion?(nil)
            return
        }
        
        var parameters: [String:String] = [:]
        if let dataParameters = try? JSONSerialization.data(withJSONObject: ["value":"EVERYONE"],
                                                            options: .prettyPrinted) {
            if let json = String(data: dataParameters, encoding: .utf8) {
                parameters["privacy"] = json
            }
        }
        
        GraphRequest(graphPath: "\(userId)/live_videos",
            parameters: parameters,
            httpMethod: .post).start { (connection, graphResult, error) in
                guard let liveIdString = (graphResult as? [String: Any])?["id"] as? String,
                    let liveId = Int(liveIdString), error == nil else {
                        self.logOut()
                        completion?(nil)
                        return
                }
                
                GraphRequest(graphPath: "\(liveIdString)",
                    parameters: ["fields" : "permalink_url,stream_url,status"],
                    httpMethod: .get).start { (connection, graphResult, error) in
                        if let _ =  error {
                            print("Some error received \(error!)")
                        } else {
                            if let result = graphResult as? [String : Any] {
                                let streamUrl = result["stream_url"] as? String
                                let shareUrl = result["permalink_url"] as! String
                                self.updateUserModel()
                                self.model?.live = LiveStreamingModel(rtmpUrl: streamUrl ?? "",
                                                                      shareUrl: "https://www.facebook.com/" + shareUrl,
                                                                      expirationDate: Date(timeIntervalSinceNow: 14400), liveId: liveId)
                            }
                            completion?(self.model)
                        }
                }
        }
    }
    
    
    
    func logOut() {
        model = nil
        let storage = HTTPCookieStorage.shared
        storage.cookies?.forEach() {
            storage.deleteCookie($0)
        }
        UserDefaults.standard.synchronize()
        fbLoginManager.logOut()
    }
}

