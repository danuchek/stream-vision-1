//
//  ClientAuthValidation.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/22/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

class LiveErrorValidation {
    
    enum LiveError: Int {
        case auth
        case broadcast
        case youtube
        
        case unnowned
    }
    
    class func validate(_ error: NSError) -> LiveError {
        switch error.code {
        case 100500: return .youtube
        case 401, -5, -1009, -1001: return .auth
            
        default: return .broadcast
        }
    }
}
