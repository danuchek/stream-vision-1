//
//  String+Loco.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/12/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

import Localize_Swift

let LCLBaseBundle = "Base"

extension String {

    func localized(for langCode: String) -> String {
        let bundle: Bundle = .main
        if let path = bundle.path(forResource: langCode, ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        }
        else if let path = bundle.path(forResource: LCLBaseBundle, ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: self, value: nil, table: nil)
        }
        return self
    }
    
}
