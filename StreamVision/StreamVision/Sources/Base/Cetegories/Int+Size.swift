//
//  Int+Size.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/11/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

extension Int {
    func toStringBytes() -> String {
        return "\(self) Bytes"
    }
    func toStringKBytes() -> String {
        return "\(self/1000) KB"
    }
    func toStringMBytes() -> String {
        return "\(self/1000000) MB"
    }
    func toStringUniversalMemoryUnit() -> String {
        switch self {
        case 1000...1000000: return self.toStringKBytes()
        case let x where x > 1000000: return toStringMBytes()
        default:
            return self.toStringBytes()
        }
    }
}
