//
//  UIImage+Rotate.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/4/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

extension UIImage {
    func rotate(degree: Double) -> UIImage {
        let radians = CGFloat(degree*Double.pi)/180.0
        let rotatedSize = self.size
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(rotatedSize, false, scale)
        let bitmap = UIGraphicsGetCurrentContext()
        bitmap!.translateBy(x: (rotatedSize.width / 2), y: (rotatedSize.height / 2));
        bitmap!.rotate(by: radians);
        bitmap!.scaleBy(x: 1.0, y: -1.0);
        bitmap!.draw(self.cgImage!, in: CGRect(x: -(self.size.width / 2) + 1, y: -self.size.height / 2 , width: self.size.width-2, height: self.size.height-1))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
