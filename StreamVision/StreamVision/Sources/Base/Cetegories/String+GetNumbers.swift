//
//  String+GetNumbers.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/6/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

prefix operator <---

extension String {
    
    static prefix func <---<T>(lhs: String) -> T? {
        guard let data = lhs.data(using: .utf8) else {
            return nil
        }
        guard let result = try? JSONSerialization.jsonObject(with: data) else {
            return nil
        }
        return result as? T
    }
    
    func getStringNumbers() -> String? {
        let numbers = self.filter {
            let number = String($0)
            return
                Int(number) != nil
            }.map {
                return String($0) }
        let resultString = numbers.joined(separator: "")
        return resultString.count > 0 ? resultString : nil
    }

    func getIntNumbers() -> Int? {
        guard let numbersString = getStringNumbers() else {
            return nil
        }
        return Int(numbersString)
    }
}

extension String {
    func match(regex: String) -> [String] {
        guard let regex = try? NSRegularExpression(pattern: regex, options: []) else { return [] }
        let nsString = self as NSString
        let results  = regex.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))
        return results.map ({ result in
            (0..<result.numberOfRanges).map {
                result.range(at: $0).location != NSNotFound
                    ? nsString.substring(with: result.range(at: $0))
                    : ""
            }
        }).reduce([], +)
    }
    
//    func match(regex: String) -> [String] {
//        do {
//            let regex = try NSRegularExpression(pattern: regex)
//            let nsString = self as NSString
//            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
//            return results.map { nsString.substring(with: $0.range)}
//        } catch let error {
//            print("invalid regex: \(error.localizedDescription)")
//            return []
//        }
//    }
}
