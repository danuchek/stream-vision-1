//
//  Array+RemoveExept.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/12/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

extension Array {    
    mutating func removeExept(_ includedElement: (Element?) -> Bool) -> [Element] {
        var temp: Element?
        self.forEach {
            if includedElement($0) {
                temp = $0
            }
        }
        removeAll()
        if let exeptObject = temp {
           append(exeptObject)
        }
        
        return self
    }
}
