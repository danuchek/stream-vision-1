//
//  UINavigationBar+CustomHeight.swift
//  RealStream
//
//  Created by Vladislav Chebotaryov on 2/9/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

extension UINavigationBar {
    open override func sizeThatFits(_ size: CGSize) -> CGSize {
        if let superView = self.superview {
            return CGSize(width: (superView.frame.width), height: 44)
        }
        return size
    }
}
