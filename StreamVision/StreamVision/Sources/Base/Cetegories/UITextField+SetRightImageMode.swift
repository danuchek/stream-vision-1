//
//  UITextField.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 2/25/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

extension UITextField {
    func setRightImageMode(_ mode: CGFloat) {
        self.rightViewMode = UITextField.ViewMode(rawValue: Int(mode))!
    }
}
