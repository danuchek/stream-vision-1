//
//  CALayer+ColorFromUIColor.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/23/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

extension CALayer {
    func setBorderColorFromUIColor(_ color: UIColor) {
        self.borderColor = color.cgColor
    }
}
