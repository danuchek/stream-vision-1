//
//  UIVIewController+GAnalitics.swift
//  RealStream
//
//  Created by Vladislav Chebotaryov on 1/21/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import Firebase

extension UIViewController {
    func googleReport(action name: String) {
        Analytics.logEvent("screen_open", parameters: [
            "name": name as NSObject,
        ])
    }
}
