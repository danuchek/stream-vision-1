//
//  NSData+ConvertToken.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/8/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Foundation

extension Data {
    func convertPushTokenToString() -> String {
        return map { String(format: "%.2hhx", $0) }.joined()
    }
}
