//
//  UIViewController+Navigation.swift
//  RealStream
//
//  Created by Vladislav Chebotaryov on 2/2/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import SVProgressHUD

enum ScreensEnum : String {
    case main = "Main"
    case stream = "Restreaming"
    case functions = "Functions"
    case conditions = "Conditions"
}

extension UIViewController {
    typealias Completion = () -> Void
    
    func isNavigateFromMain() -> Bool {
        if let navigation = navigationController {
            let rootVC = navigation.viewControllers.first!

            return rootVC is MainViewController
        }

        return false
    }

    func navigateToMain(animated: Bool) { navigateToMain(animated: animated, completion: nil) }
    func navigateToMain(animated: Bool, completion: Completion? ) {
        guard self.revealViewController() == nil else {
            let rearViewController = revealViewController().rearViewController!
            (rearViewController as! SideTableViewController).dismiss(animated: animated, completion: nil)
            return
        }
        _ = navigateTo(.main, animation: animated, completionBlock: completion)
    }
    
    @discardableResult
    func navigateToFunctions(animated: Bool,  completion: Completion? = nil) -> UIViewController {
        return navigateTo(.functions, animation: animated, completionBlock: completion)
    }
    
    @discardableResult
    func navigateToConditions(animated: Bool,  completion: Completion? = nil) -> UIViewController {
        return navigateTo(.conditions, animation: animated, completionBlock: completion)
    }
    
    @discardableResult
    func navigateToStream(animated: Bool,  completion: Completion? = nil ) -> UIViewController {
        return navigateTo(.stream, animation: animated, completionBlock: completion)
    }
    
    fileprivate func navigateTo(_ screen: ScreensEnum, animation: Bool, completionBlock: Completion?) -> UIViewController {
        let viewController: UIViewController = UIStoryboard(name: screen.rawValue, bundle: nil).instantiateInitialViewController()!
        
        if let revealController = self.revealViewController() {
            revealController.pushFrontViewController(viewController, animated: true)
        }
        return viewController
    }
}
