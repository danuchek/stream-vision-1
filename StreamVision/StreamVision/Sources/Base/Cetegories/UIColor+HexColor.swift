//
//  String+hex.swift
//  RealStream
//
//  Created by Vladislav Chebotaryov on 2/18/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

extension UIColor {
    
    typealias hexType = (_ hex: String) -> UIColor
    @nonobjc static var hex: hexType {
        return { hex in
            return UIColor.colorFromHexString(hex)
        }
    }
    
    typealias RGBAColorType = (_ r: CGFloat, _ g: CGFloat, _ b: CGFloat, _ a: CGFloat) -> UIColor
    @nonobjc static var rgba: RGBAColorType {
        return { r, g, b, a in
            return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
        }
    }
    
    // Creates a UIColor from a Hex string.
    @nonobjc static func colorFromHexString(_ hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}
