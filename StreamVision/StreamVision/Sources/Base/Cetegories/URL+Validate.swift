//
//  URL+Validate.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/11/20.
//  Copyright © 2020 Ciklum. All rights reserved.
//

extension URL {
    var isValid: Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: self.absoluteString)
        return result
    }
}
