//
//  UISlider+TintForMaxImage.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 11/28/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

extension UISlider {
    
    func setMinimumValueImageTint(color: UIColor) {
        minimumValueImage = tintImage(minimumValueImage, color: color)
    }
    
    func setMaximumValueImageTint(color: UIColor) {
        maximumValueImage = tintImage(maximumValueImage, color: color)
    }
    
    private func tintImage(_ image: UIImage?, color: UIColor) -> UIImage? {
        guard let inputImage = image else {
            return nil
        }
        var resultImage: UIImage = inputImage.withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(inputImage.size, false, resultImage.scale)
        color.set()
        resultImage.draw(in: CGRect(x: 0, y: 0, width: inputImage.size.width, height: inputImage.size.height))
        resultImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resultImage
    }
    
}

