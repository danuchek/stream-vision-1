//
//  UIImage+SaveToGallery.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/1/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Photos

extension UIImage {
    
    func saveToGallery(_ completion: @escaping (_ error: NSError?) -> Void) {
        checkAlbum { assetsCollection in
            if let collection = assetsCollection {
                self.saveImage(collection) { error in
                    if let _error = error {
                        completion(_error)
                    } else {
                        completion(nil)
                    }
                }
            } else {
                completion(NSError(domain: "Album couldn't create", code: 0, userInfo: nil))
            }
        }
    }
    
    func checkAlbum(_ completion: @escaping (_ assetsCollection: PHAssetCollection?) -> Void) {
        var collection: PHAssetCollection?
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", "SaveImageActivity.AlbumName".localized())
        collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions).firstObject
        
        //if we don't have a special album for this app yet then make one
        var assetCollectionPlaceholder: PHObjectPlaceholder!
        if collection == nil {
            PHPhotoLibrary.shared().performChanges({
                let createAlbumRequest : PHAssetCollectionChangeRequest =
                    PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: "SaveImageActivity.AlbumName".localized())
                assetCollectionPlaceholder = createAlbumRequest.placeholderForCreatedAssetCollection
                }) { success, error in
                    if success {
                        let collectionFetchResult = PHAssetCollection.fetchAssetCollections(withLocalIdentifiers: [assetCollectionPlaceholder.localIdentifier], options: nil)
                        completion(collectionFetchResult.firstObject)
                    } else {
                        completion(nil)
                    }
            }
        } else {
            completion(collection)
        }
    }
    
    func saveImage(_ assetsCollection: PHAssetCollection, completion: @escaping (_ error: NSError?) -> Void) {
        var photosAsset: PHFetchResult<PHAsset>!
        PHPhotoLibrary.shared().performChanges({
            let assetRequest = PHAssetChangeRequest.creationRequestForAsset(from: self)
            let assetPlaceholder = assetRequest.placeholderForCreatedAsset
            photosAsset = PHAsset.fetchAssets(in: assetsCollection, options: nil)
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: assetsCollection, assets: photosAsset)
            let array = NSArray(array: [assetPlaceholder!])
            albumChangeRequest!.addAssets(array)
            }) { success, error in
                if success {
                    completion(nil)
                } else if let _ = error {
                    completion(NSError(domain: "Image couldn't save", code: 0, userInfo: nil))
                }
        }
    }
}
