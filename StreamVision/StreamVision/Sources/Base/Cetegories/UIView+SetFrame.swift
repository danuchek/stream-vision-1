//
//  UIView+SetFrame.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/18/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Foundation

extension UIView {
    func frameX(_ x: CGFloat) {
        frame = CGRect(x: x, y: frame.origin.y, width: frame.width, height: frame.height)
    }
    func frameY(_ y: CGFloat) {
        frame = CGRect(x: frame.origin.x, y: y, width: frame.width, height: frame.height)
    }
    func frameWidth(_ w: CGFloat) {
        frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: w, height: frame.height)
    }
    func frameHeight(_ h: CGFloat) {
        frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: h)
    }
}
