//
//  Dictionary+Switch.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 10/2/19.
//  Copyright © 2019 Ciklum. All rights reserved.
//

extension Dictionary {
    mutating func switchKey(fromKey: Key, toKey: Key) {
        if let entry = removeValue(forKey: fromKey) {
            self[toKey] = entry
        }
    }
}
