//
//  NSDate+CurrentDate.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/23/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import Localize_Swift

extension Date {
    
    func formatted(with format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func deviceConnectionStyledDateString() -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    func monthString() -> String {
        let formatter = DateFormatter()
        let currentApplicationLocale: Locale = Locale(identifier: Localize.currentLanguage())
        formatter.locale = currentApplicationLocale
        formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM", options: 0, locale: currentApplicationLocale)!
        return formatter.string(from: self)
    }

    func shortMonthString() -> String {
        let formatter = DateFormatter()
        let currentApplicationLocale: Locale = Locale(identifier: Localize.currentLanguage())
        formatter.locale = currentApplicationLocale
        formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "MMM", options: 0, locale: currentApplicationLocale)!
        return formatter.string(from: self)
    }

    func dayString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        return formatter.string(from: self)
    }

    func fullDateString() -> String {
        let formatter = DateFormatter()
        let currentApplicationLocale: Locale = Locale(identifier: Localize.currentLanguage())
        formatter.locale = currentApplicationLocale
        formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "MMMM dd YYYY, hh:mm a", options: 0, locale: currentApplicationLocale)!
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        return formatter.string(from: self)
    }

}
