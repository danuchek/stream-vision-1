//
// Created by Alex Jav on 20.02.17.
//

#ifndef YUKON_OPTICS_ANDROID_BULLET_H
#define YUKON_OPTICS_ANDROID_BULLET_H

typedef struct {
    int n;
    double *V;
    double *Cd;
    double length;
    double cal;
    double weight;
    double m;
    double Sref;
} Bullet;

Bullet *makeBullet(int n,
                   double *v,
                   double *cd,
                   int vLen,
                   int cdLen,
                   double length,
                   double cal,
                   double weight,
                   double m,
                   double Sref);

void freeBullet(Bullet *bullet);

#endif //YUKON_OPTICS_ANDROID_BULLET_H
