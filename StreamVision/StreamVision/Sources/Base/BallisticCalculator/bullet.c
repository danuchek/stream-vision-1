//
// Created by Alex Jav on 20.02.17.
//

#include "bullet.h"
#include <stdlib.h>

Bullet *makeBullet(int n,
                   double *v,
                   double *cd,
                   int vLen,
                   int cdLen,
                   double length,
                   double cal,
                   double weight,
                   double m,
                   double Sref) {
    Bullet* bullet = (Bullet*)malloc(sizeof(Bullet));
    bullet->Cd = malloc(cdLen * sizeof(double));
    for (int i = 0; i < cdLen; i++) {
        bullet->Cd[i] = cd[i];
    }
    bullet->V = malloc(vLen * sizeof(double));
    for (int i = 0; i < vLen; i++) {
        bullet->V[i] = v[i];
    }
    bullet->n = n;
    bullet->length = length;
    bullet->cal = cal;
    bullet->weight = weight;
    bullet->Sref = Sref;
    bullet->m = m;
    return bullet;
}

void freeBullet(Bullet* bullet) {
    free(bullet->Cd);
    free(bullet->V);
    free(bullet);
}
