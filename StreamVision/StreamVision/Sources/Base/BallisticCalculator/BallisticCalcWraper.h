//
//  BallisticCalcWraper.h
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/12/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "bullet.h"

@interface CalculatedDistance : NSObject
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) double elevation;
@property (nonatomic, assign) double windage;
@property (nonatomic, assign) BOOL isValid;
@end

@implementation CalculatedDistance
- (nullable instancetype)initWithDistance:(double)distance
                       elevation:(double)elevation
                         windage:(double)windage
                           valid:(BOOL)isValid {
    self = [super init];
    if (self) {
        self.distance = distance;
        self.elevation = elevation;
        self.windage = windage;
        self.isValid = isValid;
    }
    return self;
}
@end

@interface BallisticCalcWraper : NSObject

+ (nonnull NSArray <CalculatedDistance *> *)calculateDistancesByRowsCount:(int)rowsCount
                                                              min:(double)minDistance
                                                              max:(double)maxDistance
                                                           bullet:(nonnull Bullet *)bullet
                                                        windSpeed:(double)windSpeed
                                                        windAngle:(double)windAngle
                                                           muzzle:(double)muzzle
                                                           sights:(double)sights
                                                      temperature:(double)temperature
                                                          presure:(double)presure
                                                         humidity:(double)humidity
                                                actualTemperature:(double)actualTemperature
                                                    actualPresure:(double)actualPresure
                                                   actualHumidity:(double)actualHumidity
                                                            alpha:(double)alpha
                                                        zeroRange:(double)zeroRange
                                                            twist:(double)twist
                                                       rightTwist:(int)rightTwist
                                                          useGyro:(int)useGyro;

+ (nonnull CalculatedDistance *)calculateByDistance:(double)distance
                                             bullet:(nonnull Bullet *)bullet
                                          windSpeed:(double)windSpeed
                                          windAngle:(double)windAngle
                                             muzzle:(double)muzzle
                                             sights:(double)sights
                                        temperature:(double)temperature
                                            presure:(double)presure
                                           humidity:(double)humidity
                                  actualTemperature:(double)actualTemperature
                                      actualPresure:(double)actualPresure
                                     actualHumidity:(double)actualHumidity
                                              alpha:(double)alpha
                                          zeroRange:(double)zeroRange
                                              twist:(double)twist
                                         rightTwist:(int)rightTwist
                                            useGyro:(int)useGyro;

@end
