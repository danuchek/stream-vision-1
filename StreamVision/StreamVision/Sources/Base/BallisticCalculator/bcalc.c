#include "bcalc.h"
#include <math.h>
#include <stdlib.h>

void freeSolution(Solution* sol) {
    for (int i=0; i<sol->n; i++)
        free(sol->array[i]);
    free(sol->array);
    free(sol->timePoints);
    free(sol);
}

double getBulletCdForSpeed(Bullet* bullet, double M) {
    double x0 = 0.0, x1 = 0.0, y0 = 0.0, y1 = 0.0;
    for (int i = 0; i < bullet->n; i++) {
        double curr = bullet->V[i];
        double next;
        if (i == bullet->n - 1) {
            next = curr + 10000.0;
        } else {
            next = bullet->V[i+1];
        }
        if (curr <= M && next > M) {
            x0 = curr;
            x1 = next;
            y0 = bullet->Cd[i];
            if (i == bullet->n - 1) {
                y1 = y0;
            } else {
                y1 = bullet->Cd[i+1];
            }
            break;
        }
    }
    return (y0 + (y1-y0)*(M-x0)/(x1-x0));
}

/* This function evaluates the values of the derivatives from eqs (1)-(6) */
void flightModel(double x, double *y, double *dydx, double* par, Bullet* bullet) {
    // Variables:
    //       y[0] <- Vx
    //       y[1] <- x		Range
    //       y[2] <- Vy
    //       y[3] <- y		Elevation
    //       y[4] <- Vz
    //       y[5] <- z		Windage
    
    // Parameters:
    //      p[0] <- Wx
    //      p[1] <- Wy
    //      p[2] <- Wz
    //      p[3] <- muzzle velocity
    //      p[4] <- temperature
    //      p[5] <- barometric pressure at launch site
    //      p[6] <- relative humidity
    //      p[7] <- shooting angle
    
    double Wx = par[0];
    double Wy = par[1];
    double Wz = par[2];
    double T  = par[4];
    double p  = par[5];
    double Rh = par[6];
    double sa = par[7];
    
    /* Compute air speed */
    double V = sqrt( (y[0] - Wx) * (y[0] - Wx) +
                    (y[2] - Wy) * (y[2] - Wy) +
                    (y[4] - Wz) * (y[4] - Wz) );
    
    double Vx = y[0];
    double Vy = y[2];
    double Vz = y[4];
    
    /* Compute bullet elevation above the launch site */
    double h = y[1]*sin(sa) + y[3]*cos(sa);
    
    /* Atmospheric calculations */
    /* 1, compute saturated vapour pressure, simplified formula */
    double psat = 610.78 * pow(10.0,(7.5*(T - 273.15))/(T - 35.85));
    /* 2, actual vapour pressure */
    double pv = Rh * psat;
    /* 3, compute dry air pressure */
    double pd = (p - pv);
    /* 4, compute molar mass of air mixture */
    double M = (pd/p)*0.0289644 + (pv/p)*0.018016;
    /* 5, compute temperature around the bullet */
    double Th = T - 0.0065*h;
    /* 6, compute air pressure around the bullet */
    double ph = p*pow(T/Th,9.80665*M/(8.31432*(-0.0065)));
    /* 7, compute air density around the bullet */
    double rhoh = (ph*M)/(8.31432*Th);
    /* 8, compute speed of sound around the bullet */
    double vs = sqrt(1.4*8.31432*Th/M);
    
    double drag = -0.5/bullet->m * rhoh * bullet->Sref * getBulletCdForSpeed(bullet,  V/vs) * V;
    /* Compute acceleration and velocity */
    dydx[0] = drag*(Vx-Wx) - 9.80665*sin(sa);  /* Vx */
    dydx[1] = y[0];       /* Sx */
    dydx[2] = drag*(Vy-Wy) - 9.80665*cos(sa);  /* Vy */
    dydx[3] = y[2];   /* Sy */
    dydx[4] = drag*(Vz-Wz); /* Vz */
    dydx[5] = y[4];   /* Sz */
}

/* General fourth order Runge Kutta */
void rungekutta(double *y, double *dydx, double x, double h, double *yout, double* par, Bullet* bullet,
                void (*d) (double x, double *y, double* dxdy, double* par, Bullet* bullet))

{
    
    int             i;
    double    xmid, halfstep, h6, *dy_mid, *dy_trial, *y_trial;
    
    
    dy_mid = (double *) calloc(6, sizeof(double));
    dy_trial = (double *) calloc(6, sizeof(double));
    y_trial = (double *) calloc(6, sizeof(double));
    
    halfstep = h * 0.5;
    h6 = h / 6.0;
    xmid = x + halfstep;
    
    for (i = 0; i < 6; i++)
        y_trial[i] = y[i] + halfstep * dydx[i];
    
    (*d) (xmid, y_trial, dy_trial,par,bullet);
    
    for (i = 0; i < 6; i++)
        y_trial[i] = y[i] + halfstep * dy_trial[i];
    
    (*d) (xmid, y_trial, dy_mid,par,bullet);
    for (i = 0; i < 6; i++) {
        y_trial[i] = y[i] + h * dy_mid[i];
        dy_mid[i] += dy_trial[i];
    }
    
    (*d) (x + h, y_trial, dy_trial,par,bullet);
    for (i = 0; i < 6; i++)
        yout[i] = y[i] + h6 * (dydx[i] + dy_trial[i] + 2.0 * dy_mid[i]);
    
    free(y_trial);
    free(dy_trial);
    free(dy_mid);
}

Solution* Solve(double *vstart, double x1, double* par, Bullet* bullet,
                void (*d) (double x, double *y, double* dxdy, double* par, Bullet* bullet)) {
    /* Initial values , Integrate from x1 , ODE parameters, Bullet description,
     * User-supplied function to compute derivatives */
    
    int   i, k;
    double x, h;
    
    double speed = par[3];
    int nstep = NSTEP_SIZE;
    
    Solution* res = (Solution*)malloc(sizeof(Solution));
    res->timePoints = (double*)malloc((nstep + 1) * sizeof(double));
    res->array = (double**)malloc((nstep + 1)*sizeof(double*));
    double *vcopy, *vout, *dvcopy;
    
    /* Allocate work space */
    vcopy = (double *) calloc(6, sizeof(double));
    vout = (double *) calloc(6, sizeof(double));
    dvcopy = (double *) calloc(6, sizeof(double));
    
    /* Copy initial values */
    res->array[0] = (double*)malloc(6*sizeof(double));
    for (i = 0; i < 6; i++)
        res->array[0][i] = vcopy[i] = vstart[i];
    
    res->timePoints[0] = x1;
    res->n = 1;
    x = x1;
    h = 0.5/speed;
    
    for (k = 0; k < nstep; k++) {
        
        /* Update y's */
        (*d) (x, vcopy, dvcopy,par,bullet);
        rungekutta(vcopy, dvcopy, x, h, vout, par, bullet, d);
        if ((x+h) == x) {
            break;
        }
        
        /* Update x */
        x += h;
        res->timePoints[k + 1] = x;
        res->n++;
        /* Copy result */
        res->array[k+1] = (double*)malloc(6*sizeof(double));
        for (i = 0; i < 6; i++)
            res->array[k + 1][i] = vcopy[i] = vout[i];
    }
    
    /* Free work space */
    free(dvcopy);
    free(vout);
    free(vcopy);
    return (res);
}

void gyroCorrections(Solution* solution, // Existing trajectory
                     Bullet* bullet, // Projectile description
                     double twist, // Twist rate of the barrel
                     int rightTwist, // 1 for right twist
                     double T, // Temperature
                     double p, // Barometric pressure
                     double Wz // Crosswind component 
                     ) {
    double SG = 30.0*bullet->weight/
                ((twist)*(twist)*(bullet->length)*
                (1.0+(bullet->length/bullet->cal)*
                (bullet->length/bullet->cal)));
    /* Get muzzle velocity */
    double Vxm = solution->array[0][0];
    double Vym = solution->array[0][2];
    double Vzm = solution->array[0][4];
    double Vm = sqrt(Vxm*Vxm + Vym*Vym + Vzm*Vzm);
    
    /* Atmosphere corrected stability factor */
    double SG1 = SG * pow(Vm/853.44, 1.0/3.0) * 
                 (T/288.15) * (101320.75/p);

    for (int i=0; i < solution->n; i++) {
        /* Calculate gyroscopic drift and apply to Sz */
        double time = solution->timePoints[i];
        double drift = 0.03175*(SG1+1.2)*pow(time,1.83);
        if (rightTwist) {
            solution->array[i][5] -= drift;
        } else {
            solution->array[i][5] += drift;
        }
        /* Calculate Magnus lift and apply to Sy */
        double magnus = solution->array[i][1]*
            tan((Wz*M_PI*
            (SG1 -0.24*(bullet->length/bullet->cal)+3.2)
            )/482802.4);
        if (rightTwist) {
            solution->array[i][3] -= magnus;
        } else {
            solution->array[i][3] += magnus;
        }
    }
}

/* Linear interpolation */
double linint(double x0, double y0, double x1, double y1, 
              double x)
{
    return (x - x0) * (y1 - y0) / (x1 - x0) + y0;
}

/* calculate updated vertical zeroing angle */
double zeroVert(Solution* sol, double zeroRange, 
                double zeroAngle)
{
    int   i;
    double t;
    double dx, dy;
    
    // Skip initial part of trajectory
    // until we reach zeroing Range
    for (i=1; i<sol->n && sol->array[i][1] < zeroRange; i++);
    
    if (i >= NSTEP_SIZE) {
        return NAN;
    }
    
    // arrival time for zeroing range
    t = linint(sol->array[i - 1][1], sol->timePoints[i-1], 
        sol->array[i][1], sol->timePoints[i], zeroRange);
       
    // S_x on the trajectory (should be close to zeroRange)
    dx = linint(sol->timePoints[i-1], sol->array[i - 1][1], 
         sol->timePoints[i], sol->array[i][1], t);
    
    // S_y on the trajectory
    dy = linint(sol->timePoints[i-1], sol->array[i - 1][3], 
         sol->timePoints[i], sol->array[i][3], t);
    
    //t    double    0.104033705145405
    //dy    double    0.0014418425759871224
    
    //t    double    0.10403372653594009
    //dy    double    0.0014220188963492545
    
    //t    double    0.10403367825894948
    //dy    double    0.0014418607507880486
         
    // update vertical zeroing angle
    double angle = zeroAngle + atan(-dy / dx);
    return (angle);
}

/* calculate updated horizontal zeroing angle */
double zeroHor(Solution* sol, double zeroRange, 
                double zeroAngle)
{
    int   i;
    double t;
    double dx, dz;
    
    // Skip initial part of trajectory
    // until we reach zeroing Range
    for (i=1; i<sol->n && sol->array[i][1] < zeroRange; i++);
    
    if (i >= NSTEP_SIZE) {
        return NAN;
    }
    
    // arrival time for zeroing range
    t = linint(sol->array[i - 1][1], sol->timePoints[i-1], 
        sol->array[i][1], sol->timePoints[i], zeroRange);
       
    // S_x on the trajectory (should be close to zeroRange)
    dx = linint(sol->timePoints[i-1], sol->array[i - 1][1], 
         sol->timePoints[i], sol->array[i][1], t);
    
    // S_z on the trajectory
    dz = linint(sol->timePoints[i-1], sol->array[i - 1][5], 
         sol->timePoints[i], sol->array[i][5], t);
         
    // update horizontal zeroing angle
    double angle = zeroAngle + atan(-dz / dx);
    return (angle);
}

Solution* exeption(Bullet* bullet) {
    int t0 = 0; /* Set start time to 0.0 sec */
    double *vstart;
    double *par;
    
    /* allocate the variables */
    vstart = (double *) calloc(6, sizeof(double));
    par = (double *) calloc(8, sizeof(double));
    
    for (int i = 0; i < 6; i++) {
        vstart[i] = 0;
    }
    
    for (int i = 0; i < 8; i++) {
        par[i] = 0;
    }
    
    Solution* emtyResult = Solve(vstart, t0, par, bullet, flightModel);
    free(vstart);
    free(par);
    
    return emtyResult;
}

Solution* calculateTrajectory(
             Bullet* bullet,   // Bullet description
             double windSpeed, // Wind speed
             double windAngle, // Wind angle, clockwise
             double mv,        // Muzzle velocity
             double sh,        // Sights height
             double T,         // Temperature
             double p,         // Barometric pressure
             double Rh,        // Relative humidity
             double Tz,        // Actual temperature
             double Pz,        // Actual pressure
             double Rhz,       // Actual humidity
             double alpha,     // Shooting angle
             double zeroRange, // Zeroing range
             double twist,     // Barrel twist rate
             int rightTwist,   // Barrel twist direction
             int useGyro       // Gyroscopic corrections
             ) {

    int t0 = 0; /* Set start time to 0.0 sec */
    double *vstart;
    double *par;
    
    /* allocate the variables */
    vstart = (double *) calloc(6, sizeof(double));
    par = (double *) calloc(8, sizeof(double));
    
    /* First zeroing approximation */
    // Parameters:
    //      p[0] <- headwind
    //      p[1] <- liftwind
    //      p[2] <- sidewind
    //      p[3] <- muzzle velocity
    //      p[4] <- temperature
    //      p[5] <- barometric pressure
    //      p[6] <- relative humidity
    //      p[7] <- shooting angle
    par[0] = 0;
    par[1] = 0;
    par[2] = 0;
    par[3] = mv;
    par[4] = Tz;
    par[5] = Pz;
    par[6] = Rhz;
    par[7] = 0;
    /* vstart vector:
     [0] = Vx (velocity downrange)
     [1] = Xx (position downrange)
     [2] = Vy (vertical)
     [3] = Xy
     [4] = Vz (transverse)
     [5] = Xz
     */
    vstart[0] = mv;
    vstart[1] = 0.0;
    vstart[2] = 0.0;
    vstart[3] = -sh;
    vstart[4] = 0.0;
    vstart[5] = 0.0;
    
    Solution* zero = Solve(vstart, t0, par, bullet, 
                                 flightModel);
    if (useGyro) {
        gyroCorrections(zero, 
                    bullet, 
                    twist,
                    rightTwist,
                    T,
                    p,
                    0 // Crosswind component 
                    );
    }

    double theta01 = zeroVert(zero, zeroRange, 0.0);
    double thetag1 = zeroHor(zero, zeroRange, 0.0);
    if (thetag1 != thetag1 || theta01 != theta01) {
        return exeption(bullet);
    }
                 
    freeSolution(zero);
    
    /* Second zeroing approximation */
    // Parameters:
    //      p[0] <- headwind
    //      p[1] <- liftwind
    //      p[2] <- sidewind
    //      p[3] <- muzzle velocity
    //      p[4] <- temperature
    //      p[5] <- barometric pressure at launch site
    //      p[6] <- relative humidity
    //      p[7] <- shooting angle
    par[0] = 0;
    par[1] = 0;
    par[2] = 0;
    par[3] = mv;
    par[4] = Tz;
    par[5] = Pz;
    par[6] = Rhz;
    par[7] = theta01; 
    /* vstart vector:
     [0] = Vx (velocity downrange)
     [1] = Xx (position downrange)
     [2] = Vy (vertical)
     [3] = Xy
     [4] = Vz (transverse)
     [5] = Xz
     */
    vstart[0] = mv*cos(theta01)*cos(thetag1);
    vstart[1] = 0.0;
    vstart[2] = mv*sin(theta01)*cos(thetag1);
    vstart[3] = -sh;
    vstart[4] = mv*sin(thetag1);
    vstart[5] = 0.0;
    
    zero = Solve(vstart, t0, par, bullet, 
                                 flightModel);
    if (useGyro) {
        gyroCorrections(zero, 
                    bullet, 
                    twist,
                    rightTwist,
                    T,
                    p,
                    0 // Crosswind component 
                    );
    }
    
    double theta0s = zeroVert(zero, zeroRange, theta01);
    double thetags = zeroHor(zero, zeroRange, thetag1);
    freeSolution(zero);
    
    /* calculate actual bullet trajectory */
    // Parameters:
    //      p[0] <- headwind
    //      p[1] <- liftwind
    //      p[2] <- sidewind
    //      p[3] <- muzzle velocity
    //      p[4] <- temperature
    //      p[5] <- barometric pressure at launch site
    //      p[6] <- relative humidity
    //      p[7] <- shooting angle
    par[0] = -cos(windAngle)*cos(theta0s+alpha)*windSpeed;
    par[1] = cos(windAngle)*sin(theta0s+alpha)*windSpeed;
    par[2] = sin(windAngle)*windSpeed;
    par[3] = mv;
    par[4] = T;
    par[5] = p;
    par[6] = Rh;
    par[7] = theta0s+alpha;
    /* vstart vector:
     [0] = Vx (velocity downrange)
     [1] = Xx (position downrange)
     [2] = Vy (vertical)
     [3] = Xy
     [4] = Vz (transverse)
     [5] = Xz
     */
    vstart[0] = mv*cos(theta0s)*cos(thetags);
    vstart[1] = 0.0;
    vstart[2] = mv*sin(theta0s)*cos(thetags);
    vstart[3] = -sh;
    vstart[4] = mv*sin(thetags);
    vstart[5] = 0.0;
    Solution* result = Solve(vstart, t0, par, bullet, 
                                 flightModel);
    if (useGyro) {
        gyroCorrections(result, 
                    bullet, 
                    twist,
                    rightTwist,
                    T,
                    p,
                    par[2] // Crosswind component
                    );
    }
    free(vstart);
    free(par);
    return result;   
}
