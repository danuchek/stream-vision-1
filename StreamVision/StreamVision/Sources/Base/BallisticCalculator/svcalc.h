//
// Created by Alex Jav on 01.03.17.
//

#ifndef YUKON_OPTICS_ANDROID_SVCALC_H
#define YUKON_OPTICS_ANDROID_SVCALC_H

#include "bcalc.h"

static const int INDEX_RESULT_DISTANCE = 0;
static const int INDEX_RESULT_ELEVATION = 1;
static const int INDEX_RESULT_WINDAGE = 2;
static const int INDEX_RESULT_IS_VALID = 3;
static const int RESULT_ONE_ARRAY_SIZE = 4;

void freeResultOneArray(double *array);
void freeResultTableArray(double **array, int rowsCount);

double** computeTable(
        int rowsCount,
        double minDistance,
        double maxDistance,
        Bullet* bullet,   // Bullet description
        double windSpeed, // Wind speed
        double windAngle, // Wind angle, clockwise
        double mv,        // Muzzle velocity
        double sh,        // Sights height
        double T,         // Temperature
        double p,         // Barometric pressure
        double Rh,        // Relative humidity
        double Tz,        // Actual temperature
        double Pz,        // Actual pressure
        double Rhz,       // Actual humidity
        double alpha,     // Shooting angle
        double zeroRange, // Zeroing range
        double twist,     // Barrel twist rate
        int rightTwist,   // Barrel twist direction
        int useGyro       // Gyroscopic corrections
);

double* computeOneDistance(
        double distance,
        Bullet* bullet,   // Bullet description
        double windSpeed, // Wind speed
        double windAngle, // Wind angle, clockwise
        double mv,        // Muzzle velocity
        double sh,        // Sights height
        double T,         // Temperature
        double p,         // Barometric pressure
        double Rh,        // Relative humidity
        double Tz,        // Actual temperature
        double Pz,        // Actual pressure
        double Rhz,       // Actual humidity
        double alpha,     // Shooting angle
        double zeroRange, // Zeroing range
        double twist,     // Barrel twist rate
        int rightTwist,   // Barrel twist direction
        int useGyro       // Gyroscopic corrections
);

#endif //YUKON_OPTICS_ANDROID_SVCALC_H
