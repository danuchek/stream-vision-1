//  bcalc.h
//  Copyright © 2016 Nuwaste Studios. All rights reserved.

#ifndef bcalc_h
#define bcalc_h

#include "bullet.h"

static const int NSTEP_SIZE = 8000;

typedef struct {
    int n;
    double *timePoints;
    double **array;
} Solution;

void freeSolution(Solution* sol);
Solution* calculateTrajectory(
        Bullet* bullet,   // Bullet description
        double windSpeed, // Wind speed
        double windAngle, // Wind angle, clockwise
        double mv,        // Muzzle velocity
        double sh,        // Sights height
        double T,         // Temperature (zeroing)
        double p,         // Barometric pressure (zeroing)
        double Rh,        // Relative humidity (zeroing)
        double Tz,        // Actual temperature
        double Pz,        // Actual pressure
        double Rhz,       // Actual humidity
        double alpha,     // Shooting angle
        double zeroRange, // Zeroing range
        double twist,     // Barrel twist rate
        int rightTwist,   // Barrel twist direction
        int useGyro       // Gyroscopic corrections
);

#endif /* bcalc_h */
