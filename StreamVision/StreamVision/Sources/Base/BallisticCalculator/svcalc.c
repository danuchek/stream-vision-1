//
// Created by Alex Jav on 01.03.17.
//


#include "svcalc.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static int INDEX_SOLUTION_Sx = 1;
static int INDEX_SOLUTION_Sy = 3;
static int INDEX_SOLUTION_Sz = 5;

static double** newResultTableArray(int rowsCount) {
    return malloc(rowsCount * sizeof(double*));
}

static double* newResultOneArray() {
    return malloc(RESULT_ONE_ARRAY_SIZE * sizeof(double));
}

void freeResultOneArray(double *array) {
    free(array);
}

void freeResultTableArray(double **array, int rowsCount) {
    for (int i = 0; i < rowsCount; i++) {
        freeResultOneArray(array[i]);
    }
    free(array);
}

static void setupValues(double *destResultArray, double elevation, double windage, int isValid) {
    destResultArray[INDEX_RESULT_ELEVATION] = elevation;
    destResultArray[INDEX_RESULT_WINDAGE] = windage;
    destResultArray[INDEX_RESULT_IS_VALID] = isValid;
}

static double getInterpolated(double **solutionArray, int indexFirstAbove, int solutionIndex, double multiplier) {
    // min = -0.011973550315617301 /// 9.3
    // max = -0.011725428556256743
    // multi = 0.97027140440128911
    ////////////////////////////////
    
    // min = -0.011427385464208191 /// 11.0
    // max = -0.011182230606077221
    // multi = 0.98480975771479828
    ////////////////////////////////
    
    double min = solutionArray[indexFirstAbove - 1][solutionIndex];
    double max = solutionArray[indexFirstAbove][solutionIndex];
    return (max - min) * multiplier + min;
}

static void computeOneDistanceInner(double distance, int *startIndexRef, double *destResultArray, Solution *solution) {
    destResultArray[INDEX_RESULT_DISTANCE] = distance;
    if (((int)distance) == 0) {
        setupValues(destResultArray, 0, 0, 1);
        return;
    }
    
    int currentSolutionIndex = *startIndexRef;
    
    while (1) {
        if (currentSolutionIndex == solution->n) {
            setupValues(destResultArray, 0, 0, 0);
            break;
        }
        
        double currentSx = solution->array[currentSolutionIndex][INDEX_SOLUTION_Sx];
        if (currentSx > distance) {
            double previousSx = solution->array[currentSolutionIndex - 1][INDEX_SOLUTION_Sx];
            double multiplier = (distance - previousSx) / (currentSx - previousSx); // 0.98480975771479828
                                                                                    // 0.97022485543756864
                                                                                    // 0.97025363355083161
            // (50 - 49.524094499671193) / (50.014590472963391 - 49.524094499671193)
            // (50 - 49.524091956324433) / (50.014587879314256 - 49.524091956324433)
            
            double interpolatedSy = getInterpolated(solution->array,
                                                    currentSolutionIndex,
                                                    INDEX_SOLUTION_Sy,
                                                    multiplier);
            double interpolatedSz = getInterpolated(solution->array,
                                                    currentSolutionIndex,
                                                    INDEX_SOLUTION_Sz,
                                                    multiplier);
            
            double eleRad = atan(interpolatedSy / distance); // interpolatedSy = -0.011732883589600519 //11.0
                                                            //interpolatedSy = -0.011185954567769624 //9.3
            double windRad = atan(interpolatedSz / distance);
            
            setupValues(destResultArray, eleRad, windRad, 1);
            
            break;
        }
        
        currentSolutionIndex++;
    }
    
    *startIndexRef = currentSolutionIndex;
}

double** computeTable(
                      int rowsCount,
                      double minDistance,
                      double maxDistance,
                      Bullet* bullet,   // Bullet description
                      double windSpeed, // Wind speed
                      double windAngle, // Wind angle, clockwise
                      double mv,        // Muzzle velocity
                      double sh,        // Sights height
                      double T,         // Temperature
                      double p,         // Barometric pressure
                      double Rh,        // Relative humidity
                      double Tz,        // Actual temperature
                      double Pz,        // Actual pressure
                      double Rhz,       // Actual humidity
                      double alpha,     // Shooting angle
                      double zeroRange, // Zeroing range
                      double twist,     // Barrel twist rate
                      int rightTwist,   // Barrel twist direction
                      int useGyro       // Gyroscopic corrections
) {
    Solution *solution = calculateTrajectory(bullet,
                                             windSpeed,
                                             windAngle,
                                             mv,
                                             sh,
                                             T, p, Rh,
                                             Tz, Pz, Rhz,
                                             alpha,
                                             zeroRange,
                                             twist, rightTwist, useGyro);
    
    double **resultArray = newResultTableArray(rowsCount + 1);
    double step = (maxDistance - minDistance) / (rowsCount ?: 1) ?: 1;
    int currentSolutionIndex = 1;
    
    for (double i = minDistance, j = 0; j <= rowsCount; i+=step, j++ ) {
        int arrayIndex = j;
        resultArray[arrayIndex] = newResultOneArray();
        computeOneDistanceInner(i, &currentSolutionIndex, resultArray[arrayIndex], solution);
    }
    
    freeSolution(solution);
    
    return resultArray;
}

double* computeOneDistance(
                           double distance,
                           Bullet* bullet,   // Bullet description
                           double windSpeed, // Wind speed
                           double windAngle, // Wind angle, clockwise
                           double mv,        // Muzzle velocity
                           double sh,        // Sights height
                           double T,         // Temperature
                           double p,         // Barometric pressure
                           double Rh,        // Relative humidity
                           double Tz,        // Actual temperature
                           double Pz,        // Actual pressure
                           double Rhz,       // Actual humidity
                           double alpha,     // Shooting angle
                           double zeroRange, // Zeroing range
                           double twist,     // Barrel twist rate
                           int rightTwist,   // Barrel twist direction
                           int useGyro       // Gyroscopic corrections
) {
    Solution *solution = calculateTrajectory(bullet,
                                             windSpeed,
                                             windAngle,
                                             mv,
                                             sh,
                                             T, p, Rh,
                                             Tz, Pz, Rhz,
                                             alpha,
                                             zeroRange,
                                             twist, rightTwist, useGyro);
    
    double *smallResultArray = newResultOneArray();
    int index = 0;
    computeOneDistanceInner(distance, &index, smallResultArray, solution);
    
    freeSolution(solution);
    freeBullet(bullet);
    
    return smallResultArray;
}
