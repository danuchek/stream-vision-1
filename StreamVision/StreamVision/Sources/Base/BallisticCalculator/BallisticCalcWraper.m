//
//  BallisticCalcWraper.m
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/12/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

#import "BallisticCalcWraper.h"
#import "svcalc.h"

@implementation BallisticCalcWraper

+ (nonnull CalculatedDistance *)calculateByDistance:(double)distance
                                             bullet:(nonnull Bullet *)bullet
                                          windSpeed:(double)windSpeed
                                          windAngle:(double)windAngle
                                             muzzle:(double)muzzle
                                             sights:(double)sights
                                        temperature:(double)temperature
                                            presure:(double)presure
                                           humidity:(double)humidity
                                  actualTemperature:(double)actualTemperature
                                      actualPresure:(double)actualPresure
                                     actualHumidity:(double)actualHumidity
                                              alpha:(double)alpha
                                          zeroRange:(double)zeroRange
                                              twist:(double)twist
                                         rightTwist:(int)rightTwist
                                            useGyro:(int)useGyro {
    double *result = computeOneDistance(
                                   distance,
                                   bullet,
                                   windSpeed,
                                   windAngle,
                                   muzzle,
                                   sights,
                                   temperature,
                                   presure,
                                   humidity,
                                   actualTemperature,
                                   actualPresure,
                                   actualHumidity,
                                   alpha,
                                   zeroRange,
                                   twist,
                                   rightTwist,
                                   useGyro);
    CalculatedDistance *calculatedDistance = [[CalculatedDistance alloc] initWithDistance:result[0]
                                                                                elevation:result[1]
                                                                                    windage:result[2]
                                                                                    valid:result[3]];
    return calculatedDistance;
}

+ (nonnull NSArray <CalculatedDistance *> *)calculateDistancesByRowsCount:(int)rowsCount
                                                                      min:(double)minDistance
                                                                      max:(double)maxDistance
                                                                   bullet:(nonnull Bullet *)bullet
                                                                windSpeed:(double)windSpeed
                                                                windAngle:(double)windAngle
                                                                   muzzle:(double)muzzle
                                                                   sights:(double)sights
                                                              temperature:(double)temperature
                                                                  presure:(double)presure
                                                                 humidity:(double)humidity
                                                        actualTemperature:(double)actualTemperature
                                                            actualPresure:(double)actualPresure
                                                           actualHumidity:(double)actualHumidity
                                                                    alpha:(double)alpha
                                                                zeroRange:(double)zeroRange
                                                                    twist:(double)twist
                                                               rightTwist:(int)rightTwist
                                                                  useGyro:(int)useGyro {
    NSMutableArray *calculatedDistances = [[NSMutableArray alloc] init];
    double **result = computeTable(rowsCount,
                                   minDistance,
                                   maxDistance,
                                   bullet,
                                   windSpeed,
                                   windAngle,
                                   muzzle,
                                   sights,
                                   temperature,
                                   presure,
                                   humidity,
                                   actualTemperature,
                                   actualPresure,
                                   actualHumidity,
                                   alpha,
                                   zeroRange,
                                   twist,
                                   rightTwist,
                                   useGyro);
    for (int i = 0; i <= rowsCount; i++) {
        double *distance = result[i];
        CalculatedDistance *calculatedDistance = [[CalculatedDistance alloc] initWithDistance:distance[0]
                                                                                    elevation:distance[1]
                                                                                      windage:distance[2]
                                                                                        valid:distance[3]];
        free(result[i]);
        [calculatedDistances addObject:calculatedDistance];
    }
    free(result);
    
    return calculatedDistances;
}

@end
