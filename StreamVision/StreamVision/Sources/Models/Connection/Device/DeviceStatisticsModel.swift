//
//  DeviceStatisticsModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 12/19/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class DeviceStatisticsModel: CommandModelProtocol {
    var files: [FileModel] = []
    
    required init?(_ data: Data) {
        guard let jsonDictionary = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
            return nil
        }
        
        
        if let jsonFiles = jsonDictionary as? [AnyHashable: Any] {
            guard let filesList = jsonFiles["filelist"] as? [AnyObject] else {
                return nil
            }
            
            filesList.forEach() {
                if let name = $0["file"] as? String, let size = $0["size"] as? Int, size > 0 {
                    let file = FileModel(name: name, size: size)
                    files.append(file)
                }
            }
        }
    }
}
