//
//  DeviceFoldersModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/17/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

/**
 DeviceFoldersModel
 Class model contain folders with files from device

 - folders
 */
class DeviceFoldersModel: CommandModelProtocol {
    var folders: Array<FolderModel> = []

    required init?(_ data: Data) {
        guard let jsonDictionary = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
            return nil
        }
            
        if let jsonFolders = jsonDictionary as? Dictionary<AnyHashable, Any> {
            guard let folderList = jsonFolders["filelist"] as? Array<AnyObject> else {
                return nil
            }
            
            folderList.forEach() {
                guard let folderName = $0["folder"] as? String,
                      let folderSize = $0["size"] as? Int,
                      let folderDate = $0["date"] as? String,
                      folderName.contains("img")
                else {
                    print("Parse folder error \($0)")
                    return
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yy HH:mm"
                let formattedDate = dateFormatter.date(from: folderDate) ?? Date()
                var folder = FolderModel(name: folderName, size: folderSize, date: formattedDate)
                folder.isRemote = true
                folders.append(folder)
            }
//            folders.sort { (lhs: FolderModel, rhs: FolderModel) -> Bool in
//                lhs.name > rhs.name
//            }
        }
    }
}
