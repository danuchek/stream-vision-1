//
//  FirmwareVersion.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 10/20/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit
import RealmSwift

func ==(left: FirmwareVersion, right: FirmwareVersion) -> Bool {
    return left.composedVersion() == right.composedVersion()
}

func >(left: FirmwareVersion, right: FirmwareVersion) -> Bool {
    return left.composedVersion() > right.composedVersion()
}

func <(left: FirmwareVersion, right: FirmwareVersion) -> Bool {
    return left.composedVersion() < right.composedVersion()
}

func >=(left: FirmwareVersion, right: FirmwareVersion) -> Bool {
    return left.composedVersion() >= right.composedVersion()
}

func <=(left: FirmwareVersion, right: FirmwareVersion) -> Bool {
    return left.composedVersion() <= right.composedVersion()
}

extension FirmwareVersion {
    func composedVersion() -> Int {
        var composedPatch = ""
        if patch < 10 {
            composedPatch = "00\(patch)"
        } else if patch >= 10 && patch < 100 {
            composedPatch = "0\(patch)"
        } else {
            composedPatch = "\(patch)"
        }

        var composedMinor = ""
        if minor < 10 {
            composedMinor = "0\(minor)"
        } else {
            composedMinor = "\(minor)"
        }

        var composedMajor = ""
        if minor < 10 {
            composedMajor = "0\(major)"
        } else {
            composedMajor = "\(major)"
        }

        return Int("\(composedMajor)\(composedMinor)\(composedPatch)")!
    }
}

class FirmwareVersion: Object {
    @objc dynamic var versionString: String = "00.00.000"
    @objc dynamic var major: Int = 0
    @objc dynamic var minor: Int = 0
    @objc dynamic var patch: Int = 0
    @objc dynamic var latter: String = "A"
    
    class func instanceFrom(_ firmwareVersion: String) -> FirmwareVersion {
        let instance = FirmwareVersion()
        guard firmwareVersion.contains(".") else {
            instance.versionString = FirmwareVersion.createStringVersionBy(version: firmwareVersion)
            
            let version = FirmwareVersion.instanceFrom(instance.versionString)
            instance.major = version.major
            instance.minor = version.minor
            instance.patch = version.patch
            
            return instance
        }
        
        //firmwareVersion
        let versions = firmwareVersion.components(separatedBy: ".")
        instance.major = Int(versions[0]) ?? 0
        instance.minor = Int(versions[1]) ?? 0
        instance.patch = Int(versions[2]) ?? 0
        
        if versions.count > 3 {
            instance.latter = versions[3]
        }
        
        instance.versionString = FirmwareVersion.createVersionStringFrom(instance)
        return instance
    }
    
    class func createStringVersionBy(version: String) -> String {
        var firmware = version
        if firmware.count < 7 {
            while firmware.count != 7 {
                firmware = "0\(firmware)"
            }
        }
        firmware.insert(".", at: firmware.index(firmware.startIndex, offsetBy: 4))
        firmware.insert(".", at: firmware.index(firmware.startIndex, offsetBy: 2))
        return "\(firmware)"
    }
    
    class func createVersionStringFrom(_ instance: FirmwareVersion) -> String {
        return "\(instance.major).\(String(format: "%01d", instance.minor ) ).\( String(format: "%03d", instance.patch) )"
    }
}
