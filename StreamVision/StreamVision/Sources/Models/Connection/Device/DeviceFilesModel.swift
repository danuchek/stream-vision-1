//
//  DeviceFilesModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/21/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

/**
 DeviceFilesModel
 Class model contain files from device

 - files
 */
class DeviceFilesModel: CommandModelProtocol {
    var files: [FileModel] = []

    required init?(_ data: Data) {
        guard let jsonDictionary = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) else {
            return nil
        }
        
        if let jsonFiles = jsonDictionary as? Dictionary<AnyHashable, Any> {
            let filesList = jsonFiles["filelist"] as! Array<AnyObject>

            filesList.forEach() {
                guard let name = $0["file"] as? String,
                      let size = $0["size"] as? Int,
                      let fileDate = $0["date"] as? String else {
                        return
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yy HH:mm"
                let date = dateFormatter.date(from: fileDate) ?? Date()
                let file = FileModel(name: name, size: size, date: date)
                file.isRemote = true
                
                if file.name.contains("avi") ||
                   file.name.contains("mp4") ||
                   file.name.contains("mov") ||
                   file.name.contains("jpg"), file.size > 0 {
                   files.append(file)
                }
            }
        }
    }
}
