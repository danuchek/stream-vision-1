//
//  Device.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/10/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import RealmSwift

extension DeviceModel {
//    func deviceString() -> String {
//        var result: String!
//
//        if Thread.isMainThread {
//            result = composedDeviceString()
//        } else {
//            DispatchQueue.main.sync {
//                result = composedDeviceString()
//            }
//        }
//
//        return result
//    }
    
//    private func composedDeviceString() -> String {
//        return "\(config.name).\(deviceInfoModel.sku).\(deviceInfoModel.serial)"
//    }
}

class DeviceModel: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var deviceInfoModel: DeviceInfoModel!
    @objc dynamic var deviceUpdateInfoModel: DeviceUpdateInfoModel!
//    var config: DeviceConfig!
    
//    var hasDownloadedFirmware: Bool {
//        get {
//            return FilesManager.shared.firmwareExistFromDevice(self)
//        }
//    }
//    var hasNewestFirmware: Bool {
//        get {
//            guard let _ = deviceUpdateInfoModel?.actualFirmwareVersion else {
//                return false
//            }
//            return deviceUpdateInfoModel.actualFirmwareVersion! > deviceInfoModel.firmwareVersion!
//        }
//    }
    
    static func ==(lhs: DeviceModel, rhs: DeviceModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func !=(lhs: DeviceModel, rhs: DeviceModel) -> Bool {
        return lhs.id != rhs.id
    }
    
    override public func isEqual(_ object: Any?) -> Bool {
        return self.id == (object as? DeviceModel)?.id
    }
    
    override public var hash: Int {
        return id.hashValue
    }
    
//    class func instanceFrom(_ data: Data) -> DeviceModel? {
////        STUB For check something
////        let data = DeviceDemoGetDeviceInfoResponse(sku: .custom,
////                                                   serial: .s9000960,
////                                                   version: .v0000000F,
////                                                   hardware: .ptv013).responseData!
//        guard let response = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers),
//            let convertedResponse = response as? [String: AnyObject],
//            let sku = convertedResponse["sku"] as? String,
//            let firmware = convertedResponse["softwareVersion"] as? String,
//            let hardware = convertedResponse["hardwareVersion"] as? String
//        else {
//            return nil
//        }
//
//        var deviceParameters = convertedResponse["Parameters"] as? [String: AnyObject]
//        deviceParameters?["magnification"] = convertedResponse["magnification"]
//        if let commandsArray = convertedResponse["Commands"] as? [String] {
//            for command in commandsArray {
//                deviceParameters?[command] = true as AnyObject
//            }
//        }
//
//        let device = DeviceModel()
//        device.deviceInfoModel = DeviceInfoModel()
//        device.deviceInfoModel.sku = sku
//        device.deviceInfoModel.hardwareVersion = hardware
//
//        // Safe check if serial number has incorrect type
//        if let serial = convertedResponse["serialNumber"] as? Int {
//            device.deviceInfoModel.serial = "\(serial)"
//        } else if let serial = convertedResponse["serialNumber"] as? String {
//            device.deviceInfoModel.serial = serial
//        } else {
//            device.deviceInfoModel.serial = "000000"
//        }
//
//        device.deviceInfoModel.connectionDate = Date()
//
//        let version = FirmwareVersion.instanceFrom(firmware)
//        device.deviceInfoModel.firmwareVersion = version
//
//        device.firmware = DeviceUpdateInfoModel()
//        device.firmware.version = version
//        device.config = DeviceConfigurator.configure(sku: sku,
//                                                     version: version,
//                                                     deviceFunctions: deviceParameters)
//        device.id = device.deviceString()
//
//        return device
//    }
    
    override static func ignoredProperties() -> [String] {
        return ["hasDownloadedFirmware",
                "hasNewestFirmware",
                "deviceConfig"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
