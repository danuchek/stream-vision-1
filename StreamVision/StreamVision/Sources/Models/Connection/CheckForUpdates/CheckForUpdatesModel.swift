//
//  CheckForUpdatesModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/7/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

class CheckForUpdatesModel {
    var hardwareVersion: String?
    var sizeInBytes: Int?
    var crc32: String?
    var sku: String?
    var firmwareDescription: String?
    var version: FirmwareVersion?
    
    required init(checkDict: NSDictionary) {
        hardwareVersion = checkDict["deviceVersion"] as? String
        sizeInBytes = checkDict["sizeInBytes"] as? Int
        crc32 = checkDict["crc32"] as? String
        sku = checkDict["sku"] as? String
        firmwareDescription = checkDict["description"] as? String
        if let versionFromServer = checkDict["version"] as? Int {
            version = FirmwareVersion.instanceFrom(String(versionFromServer))
        }
    }
}
