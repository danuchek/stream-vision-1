//
//  CommandModelProtocol.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/13/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

protocol CommandModelProtocol {
    init?(_ data: Data)
}
