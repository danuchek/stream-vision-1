//
//  ModelParser.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/13/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

class ModelParser: DeviceManager {
    class func parseDataBy(_ type: SocketCommand, data: Data?) -> AnyObject? {
        guard let socketData = data, socketData.count > 0 else {
            return nil
        }
        
        switch type {
        case .getParams:
            return socketData as AnyObject//DeviceParameters.parseDeviceGetParameters(socketData) as AnyObject
        case .getDeviceInfo:
            guard let response = try? JSONSerialization.jsonObject(with: socketData, options: .mutableContainers),
                let convertedResponse = response as? [String: AnyObject],
                let sku = convertedResponse["sku"] as? String,
                let firmware = convertedResponse["softwareVersion"] as? String,
                let hardware = convertedResponse["hardwareVersion"] as? String
            else { return nil }
            
            let serial = (convertedResponse["serialNumber"] as? String) ?? "\((convertedResponse["serialNumber"] as? Int) ?? 0)"
            
            var deviceParameters = convertedResponse["Parameters"] as? [String: AnyObject]
            deviceParameters?["magnification"] = convertedResponse["magnification"]
            if let commandsArray = convertedResponse["Commands"] as? [String] {
                for command in commandsArray {
                    deviceParameters?[command] = true as AnyObject
                }
            }
            
            if var functionsDict = deviceParameters {
                if var ir = functionsDict["IR"] as? [String: AnyObject] {
                    ir["values"] = [0,1,2,3] as AnyObject
                    functionsDict["IR"] = ir as AnyObject
                }
                
                if var zoom = functionsDict["Zoom"] as? [String: AnyObject] {
                    if let min = zoom["min"] as? Int,
                        let max = zoom["max"] as? Int,
                        let step = zoom["step"] as? Double {
                        let multiplier = (max / min)
                        zoom["multiplier"] = multiplier as AnyObject
                        zoom["step"] = (step / 1000.0) as AnyObject
                        
                        let newMax = (max / 1000)
                        zoom["max"] = (newMax * multiplier) as AnyObject
                    }
                    zoom["min"] = functionsDict["magnification"]
                    functionsDict["Zoom"] = zoom as AnyObject
                }
                
                if var reticleColor = functionsDict["ReticleColor"] as? [String: AnyObject] {
                    if let values = reticleColor["values"] as? [Int] {
                        reticleColor["values"] = values.map {
                            MigrationService().setupLocalizedValue(value: "\($0)",
                                values: MigrationService.ReticleColor18.ordered)
                            } as AnyObject
                        functionsDict["ReticleColor"] = reticleColor as AnyObject
                    }
                }
                
                if let _ = functionsDict["DateTime"] as? String {
                    functionsDict["DateTime"] = true as AnyObject
                }
                deviceParameters = functionsDict
            }
            
            let deviceManager = ModelParser().deviceManager
            var deviceTemplate: VirtualDeviceTemplate!
            if !Thread.isMainThread {
                DispatchQueue.main.sync {
                    deviceTemplate = deviceManager.getDevice(by: sku, version: FirmwareVersion.instanceFrom(firmware))
                }
            } else {
                deviceTemplate = deviceManager.getDevice(by: sku, version: FirmwareVersion.instanceFrom(firmware))
            }
            
            var device: VirtualDevice!
            if !Thread.isMainThread {
                DispatchQueue.main.sync {
                    device = VirtualDevice.from(template: deviceTemplate)
                }
            } else {
                device = VirtualDevice.from(template: deviceTemplate)
            }
            
            let group = DispatchGroup()
            group.enter()
            deviceManager.update {
                device.info.invalidSku = sku
                device.info.serial = serial
                device.info.hw = hardware
                device.info.version = FirmwareVersion.instanceFrom(firmware)
                
                if let deviceParameters = deviceParameters {
                    device.templateParameters.api.type.update(rawParameters: deviceParameters,
                                                              parameters: &device.parameters)
                }
                group.leave()
            }
            group.wait()
            
            return device
        case .getFoldersList:
            return DeviceFoldersModel(socketData)
        case .getStatisticsList:
            return DeviceStatisticsModel(socketData)
        case .getFilesList:
            return DeviceFilesModel(socketData)
        case .download:
            return socketData as AnyObject?
        default:
            return nil
        }
    }
}
    
//    class func parseDataBy<T: RawRepresentable & SocketCommandDescriptionProtocol>(_ type: T, data: Data?) -> AnyObject? {
//        guard let socketData = data, socketData.count > 0 else {
//            return nil
//        }
//
//        if let socketType = type as? SocketCommand {
//            switch socketType {
//            case .getParams:
//                return nil //DeviceParameters.parseDeviceGetParameters(socketData) as AnyObject
//            case .getDeviceInfo:
//                return DeviceModel.instanceFrom(socketData)
//            case .getFoldersList:
//                return DeviceFoldersModel(socketData)
//            case .getStatisticsList:
//                return DeviceStatisticsModel(socketData)
//            case .getFilesList:
//                return DeviceFilesModel(socketData)
//            case .download:
//                return socketData as AnyObject?
//            default:
//                return nil
//            }
//        }
//
//        return nil
//    }
//}
