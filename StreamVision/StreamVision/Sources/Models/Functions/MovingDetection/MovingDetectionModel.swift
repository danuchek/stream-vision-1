//
//  MovingDetectionModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 4/24/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import RealmSwift

class MovingDetectionModel: Object {
    @objc dynamic var isMovingDetectionActive = false
    @objc dynamic var detectionAccuracy: Float = 0.0
    @objc dynamic var isSoundIndication = false
    @objc dynamic var soundVolume: Float = 0.0
    @objc dynamic var isVibroIndication = false
}
