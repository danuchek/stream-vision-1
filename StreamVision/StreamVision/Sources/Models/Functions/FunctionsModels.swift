//
//  FunctionsModels.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 9/2/18.
//  Copyright © 2018 Ciklum. All rights reserved.
//

protocol FunctionsCellProtocol {
    var title: String { get }
    func subTitle() -> String
}

extension FunctionsCellProtocol {
    func subTitle() -> String {
        return ""
    }
}

struct WhatsNewCellModel: FunctionsCellProtocol {
    let title: String = "Settings.Whats.New.In"
}

struct AboutCellModel: FunctionsCellProtocol {
    let title: String = "Functions.About.ContactSupport"
}

struct LanguageCellModel: FunctionsCellProtocol {
    let title: String = "Functions.Language.Title"
}

struct PrivacyCellModel: FunctionsCellProtocol {
    let title: String = "Functions.About.PrivacyPolicy"
}

struct EulaCellModel: FunctionsCellProtocol {
    let title: String = "Functions.About.Eula"
}


struct RotateVideoCellModel: FunctionsCellProtocol {
    let title: String = "Functions.RotateVideo"
}

struct NightModeCellModel: FunctionsCellProtocol {
    let title: String = "Functions.NightMode.Title"
}

struct MotionDetectionCellModel: FunctionsCellProtocol {
    let title: String = "Functions.MotionDetection.Title"
}

struct SignInCellModel: FunctionsCellProtocol {
    let title: String = "Functions.SignIn"
}

struct RegistrationCellModel: FunctionsCellProtocol {
    let title: String = "Functions.Registration.Cell.Title"
    func subTitle() -> String {
        return "Functions.Registration.Cell.SubTitle"
    }
}

struct AccountCellModel: FunctionsCellProtocol {
    let title: String = "Account"
}

