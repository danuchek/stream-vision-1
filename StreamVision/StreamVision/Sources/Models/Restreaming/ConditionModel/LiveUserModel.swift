//
//  LiveUserModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/22/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

struct LiveUserModel {
    let userId: String
    let token: String
    let name: String
    let email: String?
}
