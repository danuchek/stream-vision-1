//
//  AuthLiveModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/19/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

struct LiveStreamingModel {
    var rtmpUrl: String
    var shareUrl: String
    var expirationDate: Date?
    var liveId: Int?
}
