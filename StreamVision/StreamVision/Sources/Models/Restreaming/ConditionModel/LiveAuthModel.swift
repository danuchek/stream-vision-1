//
//  LiveAuthModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 5/22/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

struct LiveAuthModel {
    var user: LiveUserModel!
    var live: LiveStreamingModel?
}
