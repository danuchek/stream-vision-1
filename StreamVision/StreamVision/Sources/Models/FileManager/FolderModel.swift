//
//  FolderModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/15/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

func == (lhs: FolderModel, rhs: FolderModel) -> Bool {
    return lhs.name == rhs.name
}

extension FolderModel: Hashable, Equatable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(name.hash)
    }
}

extension FolderModel {
    mutating func mergeFolderWith(folder: FolderModel) {
        files.append(contentsOf: folder.files)
    }
}

struct FolderModel {
    
    @objc(_TtCV12StreamVision11FolderModel17FolderModelHelper)class FolderModelHelper: NSObject, Codable, NSCoding {
        let name: String
        let size: Int
        let date: Date
        
        init(name: String, size: Int, date: Date) {
            self.name = name
            self.size = size
            self.date = date
        }
        
        // MARK: NSCoding
        required convenience init?(coder aDecoder: NSCoder) {
            guard
            let decodedName =  aDecoder.decodeObject(forKey: "name") as? String
            else { return nil }
            
            //let decodedSize = aDecoder.decodeInteger(forKey: "size") as? Int
            let decodedDate =  aDecoder.decodeObject(forKey: "date") as? Date
            self.init(name: decodedName, size: 0, date: decodedDate ?? Date())
        }
        
        func encode(with aCoder: NSCoder) {
            aCoder.encode(name, forKey: "name")
            aCoder.encode(size, forKey: "size")
            aCoder.encode(date, forKey: "date")
        }
    }
    
    enum FileExtension {
        case video
        case photo
    }
    
    let name: String
    let size: Int
    let date: Date
    var path: String?

    var files: [FileModel] = []
    
    var isLocal: Bool {
        return path != nil ? true : false
    }
    var isRemote: Bool = false
    
    var folderToData: Data {
        return NSKeyedArchiver.archivedData(withRootObject: FolderModelHelper(name: name, size: size, date: date))
    }
    
    init(name: String, size: Int = 0, date: Date = Date()) {
        self.name = name
        self.size = size
        self.date = date
    }
    
    init(from folder: FolderModel) {
        name = folder.name
        size = folder.size
        date = folder.date
        path = folder.path
        isRemote = folder.isRemote
    }
    
    init(folderModelHelper: FolderModelHelper) {
        name = folderModelHelper.name
        size = folderModelHelper.size
        date = folderModelHelper.date
    }
    
    func numberOfFiles() -> Int {
        return files.count
    }
    
    func folderSize() -> Int {
        return files.map { $0.size }.reduce(0) { $0 + $1 }
    }
}
