//
//  FileModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/15/16.
//  Copyright © 2016 Ciklum. All rights reserved.
//

import UIKit

enum FileType {
    case image
    case video
    case converted
}

extension FileModel {
    override var hash: Int {
        return (name.hash) ^ (size.hashValue) ^ (date.hashValue)
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        return nameWithoutType == (object as? FileModel)?.nameWithoutType// && size == (object as? FileModel)?.size
    }
    
}

class FileModel: NSObject, NSCoding {
    enum FileLocation {
        case local
        case remote
        case both
    }
    
    var nameWithoutType: String {
        return name.components(separatedBy: ".")[0]
    }
    var name: String
    var size: Int
    var date: Date
    var path: String?
    
    init(name: String, size: Int, date: Date = Date(), path: String? = nil) {
        self.name = name
        self.size = size
        self.date = date
        self.path = path
    }
    
    var isLocal: Bool {
        return path != nil
    }
    var isRemote: Bool = false
    
    var fileType: FileType {
        if name.contains("avi") {
            return .video
        } else if name.contains("mov") || name.contains("mp4") {
            return .converted
        }
        return .image
    }
    
    var fileToData: Data {
        return NSKeyedArchiver.archivedData(withRootObject: self)
    }
    
    var location: FileLocation {
        switch (isRemote, isLocal) {
        case (false, true):
            return .local
        case (true, false):
            return .remote
        default:
            return .both
        }
    }
    
    // MARK: NSCoding
    convenience required init?(coder aDecoder: NSCoder) {
        guard let decodedName = aDecoder.decodeObject(forKey: "name") as? String,
        let decodedSize = aDecoder.decodeObject(forKey: "size") as? Int,
        let decodedDate = aDecoder.decodeObject(forKey: "date") as? Date
        else { return nil }
        
        self.init(name: decodedName, size: decodedSize, date: decodedDate)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        
        // Hack for migration from old vesions
        let optionalSize: Int? = size
        aCoder.encode(optionalSize, forKey: "size")
        aCoder.encode(date, forKey: "date")
    }
}

