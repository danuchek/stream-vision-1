//
//  ParametersConfigurator.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/22/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit



class ParametersConfigurator {
    enum ViewfinderParameterType {
        
        // First level
        case CaptureModeCommon
        case IRCommon
        case ZoomCommon
        case BrightnessCommon
        case DisplayOffCommon
        case RecordParameter
        case RecognitionCommon
        case ColorsPaletteCommon
        case ReticleColorCommon
        case ReticleBrightnessCommon
        case ContrastCommon
        case DistancesCommon
        case ReticleTypeCommon
        case PresetCommon
        case ShutterCommon
        
        case ParametersCommon
        
        // Second
        case CaptureModeVideo
        case CaptureModePhoto
        case CaptureModeStream
        case PiP
        case IRScrollable
        
        // Third
        case Lock
        case Options
        case Share
        case Motion
        
        case motionActivate
        case motionAccuracy
        case motionSettings
        case motionSepparator
    }
    
    class func visualContentBy(parameter: Parameter) -> ParameterVisualContent? {
        
        switch parameter {
        // Buttons
        case is CaptureMode:
            var captureMode = ParameterVisualContent(type: .CaptureModeCommon,
                                                     title: "Viewfinder.Parameters.Modes",
                                                     isWithTitleValue: false,
                                                     isWithLocalizedValue: false)
            captureMode.pressAnimationAction = { sender in
                if let model = sender.visualContentModel?.model, model.value == 0 {
                    sender.setImage(UIImage(named: "vf_capture_mode.0"), for: .normal)
                    sender.setImage(UIImage(named: "vf_capture_mode_sel.0"), for: .selected)
                } else {
                    sender.setImage(UIImage(named: "vf_capture_mode.1"), for: .normal)
                    sender.setImage(UIImage(named: "vf_capture_mode_sel.1"), for: .selected)
                }
            }
            return captureMode
            
        case is Zoom:
            return ParameterVisualContent(type: .ZoomCommon,
                                          title: "Viewfinder.Parameters.Zoom",
                                          isWithTitleValue: true,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "vf_zoom"),
                                          selectedImage: UIImage(named: "vf_zoom_sel"))
        case is IR:
            var ir = ParameterVisualContent(type: .IRCommon,
                                            title: "Viewfinder.Parameters.IR",
                                            isWithTitleValue: true,
                                            isWithLocalizedValue: true,
                                            normalImage: UIImage(named: "vf_ir.3"),
                                            selectedImage: UIImage(named: "vf_ir_sel.3"))
            ir.pressAnimationAction = { sender in
                let model = sender.visualContentModel!.model as! IR
                
                sender.setTitleColor(.gray, for: .disabled)
                if let status = model.irStatus, status.value == 0.0 {
                    sender.isEnabled = false
                } else {
                    sender.isEnabled = true
                }
                sender.setTitleValue(floatValue: model.value, for: .normal)
            }
            return ir
            
            
        case is Brightness:
            return ParameterVisualContent(type: .BrightnessCommon,
                                          title: "Viewfinder.Parameters.Brightness".localized(),
                                          isWithTitleValue: true,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "vf_brightness"),
                                          selectedImage: UIImage(named: "vf_brightness_sel"))
            
        case is DisplayOff:
            var displayOff = ParameterVisualContent(type: .DisplayOffCommon,
                                                    title: "Viewfinder.Parameters.DisplayEnabled.On",
                                                    isWithTitleValue: false,
                                                    isWithLocalizedValue: false,
                                                    normalImage: UIImage(named: "vf_displayon"),
                                                    selectedImage: UIImage(named: "vf_displayon_sel"))
            displayOff.autoSelectAfterPress = false
            displayOff.pressAnimationAction = { sender in
                if let model = sender.visualContentModel?.model, model.value == 1 {
                    sender.setTitle("Viewfinder.Parameters.DisplayEnabled.On".localized(), for: .normal)
                    sender.setImage(UIImage(named: "vf_displayon"), for: .normal)
                    sender.setImage(UIImage(named: "vf_displayon_sel"), for: .highlighted)
                } else {
                    sender.setTitle("Viewfinder.Parameters.DisplayEnabled.Off".localized(), for: .normal)
                    sender.setImage(UIImage(named: "vf_displayoff"), for: .normal)
                    sender.setImage(UIImage(named: "vf_displayoff_sel"), for: .highlighted)
                }
            }
            return displayOff
            
        case is Record:
            return ParameterVisualContent(type: .RecordParameter,
                                          title: "Record",
                                          isWithTitleValue: false,
                                          isWithLocalizedValue: false)
        case is Recognition:
            var recognition = ParameterVisualContent(type: .RecognitionCommon,
                                                     title: "Viewfinder.Parameters.WorkMode",
                                                     isWithTitleValue: true,
                                                     isWithLocalizedValue: true,
                                                     normalImage: UIImage(named: "vf_recognition"),
                                                     selectedImage: UIImage(named: "vf_recognition_sel"))
            recognition.pressAnimationAction = { sender in
                let model = sender.visualContentModel!.model!
                
                sender.setTitleValue(floatValue: model.value, for: .normal)
            }
            return recognition
        case is ColorsPalette:
            var colors = ParameterVisualContent(type: .ColorsPaletteCommon,
                                                title: "Viewfinder.Parameters.ColorSchema",
                                                isWithTitleValue: true,
                                                isWithLocalizedValue: true,
                                                normalImage: UIImage(named: "vf_colors_schema"),
                                                selectedImage: UIImage(named: "vf_colors_schema_sel"))
            colors.pressAnimationAction = { sender in
                let model = sender.visualContentModel!.model!
                
                sender.setTitleValue(floatValue: model.value, for: .normal)
            }
            return colors
            
        case is Distance:
            return ParameterVisualContent(type: .DistancesCommon,
                                          title: "Viewfinder.Parameters.Distance",
                                          isWithTitleValue: true,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "ic_pristrelka"),
                                          selectedImage: UIImage(named: "ic_pristrelka_sel"))
        case is ReticleBrightness:
            return ParameterVisualContent(type: .ReticleBrightnessCommon,
                                          title: "Viewfinder.Parameters.MarkerBrightness",
                                          isWithTitleValue: true,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "ic_metka_brightness"),
                                          selectedImage: UIImage(named: "ic_metka_brightness_sel"))
        case is ReticleColor:
            return ParameterVisualContent(type: .ReticleColorCommon,
                                          title: "Viewfinder.Parameters.MarkerColor",
                                          isWithTitleValue: true,
                                          isWithLocalizedValue: true,
                                          normalImage: UIImage(named: "ic_metka_color"),
                                          selectedImage: UIImage(named: "ic_metka_color_sel"))
        case is Contrast:
            return ParameterVisualContent(type: .ContrastCommon,
                                          title: "Viewfinder.Parameters.Contrast",
                                          isWithTitleValue: true,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "vf_contrast"),
                                          selectedImage: UIImage(named: "vf_contrast_sel"))
        case is ReticleType:
            return ParameterVisualContent(type: .ReticleTypeCommon,
                                          title: "Viewfinder.Parameters.MarkerType",
                                          isWithTitleValue: true,
                                          isWithLocalizedValue: true,
                                          normalImage: UIImage(named: "ic_metka_type"),
                                          selectedImage: UIImage(named: "ic_metka_type_sel"))
        case is Shutter:
            var shutter = ParameterVisualContent(type: .ShutterCommon,
                                                 title: "Viewfinder.Parameters.Calibrate",
                                                 isWithTitleValue: false,
                                                 isWithLocalizedValue: false,
                                                 normalImage: UIImage(named: "vf_shutter"),
                                                 selectedImage: UIImage(named: "vf_shutter_sel"))
            shutter.autoSelectAfterPress = false
            return shutter
            
        case is Preset:
            let preset = ParameterVisualContent(type: .PresetCommon,
                                                title: "",
                                                isWithTitleValue: false,
                                                isWithLocalizedValue: false,
                                                normalImage: nil,
                                                selectedImage: nil)
            return preset
        default:
            return nil
        }
    }
    
    class func visualContentBy(type: ViewfinderParameterType) -> ParameterVisualContent? {
        switch type {
        
        case .IRScrollable:
            var ir = ParameterVisualContent(type: .IRScrollable,
                                            title: "Viewfinder.Parameters.IR",
                                            isWithTitleValue: true,
                                            isWithLocalizedValue: false,
                                            normalImage: UIImage(named: "vf_ir.3"),
                                            selectedImage: UIImage(named: "vf_ir_sel.3"))
            ir.pressAnimationAction = { sender in
                let model = sender.visualContentModel!.model!
                
                sender.setTitleValue(floatValue: model.value, for: .normal)
            }
            return ir
            
        // Parameters
            
        case .ParametersCommon:
            return ParameterVisualContent(type: .ParametersCommon,
                                          title: "Viewfinder.Parameters".localized(),
                                          isWithTitleValue: false,
                                          isWithLocalizedValue: false)
        
        case .CaptureModePhoto:
            return ParameterVisualContent(type: .CaptureModePhoto,
                                          title: "Viewfinder.PhotoMode".localized(),
                                          isWithTitleValue: false,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "vf_capture_mode.0"),
                                          selectedImage: UIImage(named: "vf_capture_mode_sel.0"))
        
        case .CaptureModeVideo:
            return ParameterVisualContent(type: .CaptureModeVideo,
                                          title: "Viewfinder.VideoMode".localized(),
                                          isWithTitleValue: false,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "vf_capture_mode.1"),
                                          selectedImage: UIImage(named: "vf_capture_mode_sel.1"))
            
        case .CaptureModeStream:
            return ParameterVisualContent(type: .CaptureModeStream,
                                          title: "Stream",
                                          isWithTitleValue: false,
                                          isWithLocalizedValue: false,
                                          normalImage: UIImage(named: "vf_capture_mode.2"),
                                          selectedImage: UIImage(named: "vf_capture_mode.2"))

        case .PiP:
            return ParameterVisualContent(type: .PiP,
                                          title: "PiP",
                                          isWithTitleValue: false,
                                          isWithLocalizedValue: false)
            
        case .Lock:
            var lock = ParameterVisualContent(type: .Lock,
                                                 title: "Restreaming.Preview.Lock.Button",
                                                 isWithTitleValue: false,
                                                 isWithLocalizedValue: false,
                                                 normalImage: UIImage(named: "restreaming_lock"),
                                                 selectedImage: UIImage(named: "restreaming_lock_sel"))
            lock.autoSelectAfterPress = false
            return lock
            
        case .Options:
            var options = ParameterVisualContent(type: .Options,
                                              title: "Restreaming.Preview.Options.Title",
                                              isWithTitleValue: false,
                                              isWithLocalizedValue: false,
                                              normalImage: UIImage(named: "rs_options"),
                                              selectedImage: UIImage(named: "rs_options_sel"))
            options.autoSelectAfterPress = false
            return options
            
        case .Motion:
            var options = ParameterVisualContent(type: .Motion,
                                                 title: "Viewfinder.Preview.Motion.Title",
                                                 isWithTitleValue: false,
                                                 isWithLocalizedValue: false,
                                                 normalImage: UIImage(named: "motion_det_active"),
                                                 selectedImage: UIImage(named: "motion_det_sel"))
            options.autoSelectAfterPress = false
            return options
            
        case .motionActivate:
            var motionActivate = ParameterVisualContent(type: .motionActivate,
                                                 title: "Viewfinder.Preview.MotionPanel.Active",
                                                 isWithTitleValue: false,
                                                 isWithLocalizedValue: false,
                                                 normalImage: UIImage(named: "motion_det_active"),
                                                 selectedImage: UIImage(named: "motion_det_sel"))
            motionActivate.pressAnimationAction = { sender in
                    if let model = sender.visualContentModel?.model, model.value == 1 {
                        sender.setTitle("Viewfinder.Preview.MotionPanel.Active.On".localized(), for: .normal)
                    } else {
                        sender.setTitle("Viewfinder.Preview.MotionPanel.Active.Off".localized(), for: .normal)
                    }
            }
            return motionActivate
            
        case .motionSepparator:
            let motionSepparator = ParameterVisualContent(type: .motionSepparator,
                                                        title: "Viewfinder.Preview.MotionPanel.Active",
                                                        isWithTitleValue: false,
                                                        isWithLocalizedValue: false,
                                                        normalImage: nil,
                                                        selectedImage: nil)
            return motionSepparator
            
        case .motionAccuracy:
            var motionAccuracy = ParameterVisualContent(type: .motionAccuracy,
                                                        title: "Viewfinder.Preview.MotionPanel.Accuracy",
                                                 isWithTitleValue: false,
                                                 isWithLocalizedValue: false,
                                                 normalImage: UIImage(named: "scale_active"),
                                                 selectedImage: UIImage(named: "scale_sel"))
            motionAccuracy.autoSelectAfterPress = true
            return motionAccuracy
            
        case .motionSettings:
            var motionSettings = ParameterVisualContent(type: .motionSettings,
                                                        title: "Viewfinder.MotionPanel.Sound.Vibration",
                                                 isWithTitleValue: false,
                                                 isWithLocalizedValue: false,
                                                 normalImage: UIImage(named: "motion_det_settings_active"),
                                                 selectedImage: UIImage(named: "motion_det_settings_sel"))
            motionSettings.autoSelectAfterPress = true
            return motionSettings
            
        case .Share:
            var share = ParameterVisualContent(type: .Share,
                                                 title: "FileManager.Actions.Share",
                                                 isWithTitleValue: false,
                                                 isWithLocalizedValue: false,
                                                 normalImage: UIImage(named: "rs_share"),
                                                 selectedImage: UIImage(named: "rs_share_sel"))
            share.autoSelectAfterPress = false
            return share
            
        default: return nil
        }
    }
}
