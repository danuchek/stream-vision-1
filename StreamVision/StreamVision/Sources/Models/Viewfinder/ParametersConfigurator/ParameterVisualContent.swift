//
//  ParameterVisualContent.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 1/22/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

struct ParameterVisualContent {
    typealias ParameterAction = (_ sender: ParameterButton, _ newValue: Float?) -> ()

    let type: ParametersConfigurator.ViewfinderParameterType
    var model: Parameter?

    var title: String
    let isWithTitleValue: Bool
    let isWithLocalizedValue: Bool

    let normalImage: UIImage?
    let selectedImage: UIImage?

    var action: ParameterAction!
    var sender: ParameterButton?

    var pressAnimationAction : ( (_ sender: ParameterButton) -> () )?
    var autoSelectAfterPress : Bool = true

    init(type: ParametersConfigurator.ViewfinderParameterType,
         model: Parameter?,
         title: String,
         isWithTitleValue: Bool,
         isWithLocalizedValue: Bool,
         normalImage: UIImage?,
         selectedImage: UIImage?,
         action: ParameterAction!,
         sender: ParameterButton?) {

        self.type = type
        self.title = title
        self.isWithTitleValue = isWithTitleValue
        self.isWithLocalizedValue = isWithLocalizedValue
        self.normalImage = normalImage
        self.selectedImage = selectedImage
        self.model = model
        self.action = action
        self.sender = sender

        Logging.log(message: "Init visual model - \(self.type), \(self.title)", toFabric: true)
    }

    init(type: ParametersConfigurator.ViewfinderParameterType,
         title: String,
         isWithTitleValue: Bool,
         isWithLocalizedValue: Bool,
         normalImage: UIImage?,
         selectedImage: UIImage?) {

        self.type = type
        self.title = title
        self.isWithTitleValue = isWithTitleValue
        self.isWithLocalizedValue = isWithLocalizedValue
        self.normalImage = normalImage
        self.selectedImage = selectedImage

        Logging.log(message: "Init visual model - \(self.type), \(self.title)", toFabric: true)
    }

    init(type: ParametersConfigurator.ViewfinderParameterType,
         title: String,
         isWithTitleValue: Bool,
         isWithLocalizedValue: Bool) {

        self.type = type
        self.title = title
        self.isWithTitleValue = isWithTitleValue
        self.isWithLocalizedValue = isWithLocalizedValue
        self.normalImage = nil
        self.selectedImage = nil

        Logging.log(message: "Init visual model - \(self.type), \(self.title)", toFabric: true)
    }
}
