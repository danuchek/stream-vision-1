//
//  BallisticCalcParameterProtocol.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/21/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

extension BallisticCalcParameterProtocol {
    var rawValue: String {
        let decimal = selectedLimit.pattern.decimal
        return "\(String(format: "%.\(decimal)f", value.value))"
    }
    
    var absValue: String {
        let decimal = selectedLimit.pattern.decimal
        return "\(String(format: "%.\(decimal)f", abs(value.value)))"
    }
    
    var localizedValue: String {
        let decimal = selectedLimit.pattern.decimal
        return "\(String(format: "%.\(decimal)f", value.value)) " + UnitsConvertor.localizedUnitsBy(value.unit as! UnitsProtocol)
    }
    
    var localizedType: String {
        return UnitsConvertor.localizedUnitsBy(value.unit as! UnitsProtocol)
    }
    
    var selectedLimit: (limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType) {
        return limits[value.unit.rawValue as! Int]
    }
    
    static func createParameter<T: BallisticCalcParameterProtocol, D>(_ ModelClass: T.Type) -> T where T.UnitsType == D, D.RawValue == Int {
        return ModelClass.init(value: (ModelClass.defaultValue, unit: ModelClass.UnitsType(rawValue: 0)!))
    }
    
    static func createParameter<T: BallisticCalcParameterProtocol, D>(_ value: Double, _ ModelClass: T.Type) -> T where T.UnitsType == D, D.RawValue == Int {
        return ModelClass.init(value: (value, unit: ModelClass.UnitsType(rawValue: 0)!))
    }
}

protocol BallisticCalcParameterProtocol {
    associatedtype UnitsType: RawRepresentable
    static var defaultValue: Double { get }
    
    var value: (value: Double, unit: UnitsType) { get set }
    var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] { get set }
    init(value: (value: Double, unit: UnitsType))
}

protocol BallisticCalcRangeParameterProtocol: BallisticCalcParameterProtocol {
    var range: (value: (Double, Double), unit: UnitsType) { get set }
    init(range: (value: (Double, Double), unit: UnitsType))
}
