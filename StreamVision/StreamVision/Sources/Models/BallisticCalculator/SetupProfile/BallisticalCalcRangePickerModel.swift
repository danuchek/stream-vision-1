//
//  BallisticalCalcRangePickerModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 7/11/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

struct BallisticalCalcRangePickerModel {
    var range: (Double, Double)
    var title: String
    var step: Int
    var selected: Int
    var data: [(max: Double, min: Double, units: String)]
    
    var didSelectValue: (_ value: (Double, Double), _ units: Int) -> (Double, Double)?
    var editFinished: (_ canceled: Bool) -> ()
}
