//
//  BallisticalCalcValuePickerModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 6/20/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

extension BallisticalCalcValuePickerModel {
    func componentsCount() -> Int {
        return (isWithType  ?  1:  0)
             + (isWithValue ?  1:  0)
             + (isInteger   ?  0:  2)
    }
}

struct BallisticalCalcValuePickerModel {
    var value: Double
    var title: String
    var selected: Int
    var isWithType: Bool
    var isWithValue: Bool
    var isInteger: Bool
    var data: [(max: Double, min: Double, units: String)]
    
    var didSelectValue: (_ value: Double, _ units: Int) -> Double?
    var editFinished: (_ canceled: Bool) -> ()
}
