//
//  SetOutdoorModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/16/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

extension SetOutdoorModel {
    struct TemperatureModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Temperature
        static var defaultValue: Double = 16
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 50, min: -50), pattern: (integer: 2, decimal: 0), units: .c),
             (limits: (max: 122, min: -58), pattern: (integer: 3, decimal: 0), units: .f)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct PressureModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Pressure
        static var defaultValue: Double = 29.92
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 32, min: 16), pattern: (integer: 2, decimal: 2), units: .inHg),
             (limits: (max: 812.7999, min: 406.3999), pattern: (integer: 3, decimal: 4), units: .mmHg),
             (limits: (max: 108.364, min: 54.182), pattern: (integer: 3, decimal: 3), units: .kPa),
             (limits: (max: 15.71, min: 7.85), pattern: (integer: 2, decimal: 2), units: .PSI),
             (limits: (max: 1083.64, min: 541.82), pattern: (integer: 4, decimal: 2), units: .mBar)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct HumidityModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Humidity
        static var defaultValue: Double = 50
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 100, min: 5), pattern: (integer: 3, decimal: 0), units: .percent)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
}

struct SetOutdoorModel {
    var temperature: TemperatureModel?
    var pressure: PressureModel?
    var humidity: HumidityModel?
    
    var actualTemperature: TemperatureModel?
    var actualPressure: PressureModel?
    var actualHumidity: HumidityModel?
}
