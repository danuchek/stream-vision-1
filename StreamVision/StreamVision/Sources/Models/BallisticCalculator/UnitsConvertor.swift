//
//  UnitsConvertor.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/15/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

protocol UnitsProtocol {}

extension UnitsConvertor {
    enum Weight: Int, UnitsProtocol {
        case grain
        case gram
    }
    
    enum Speed: Int, UnitsProtocol {
        case ms
        case fps
        case mph
    }
    
    enum Measure: Int, UnitsProtocol {
        case mm
        case inch
        case m
    }
    
    enum Distance: Int, UnitsProtocol {
        case m
        case yd
    }
    
    enum Direction: Int, UnitsProtocol {
        case left
        case right
    }
    
    enum Temperature: Int, UnitsProtocol {
        case c
        case f
        case k
    }
    
    enum Pressure: Int, UnitsProtocol {
        case inHg
        case mmHg
        case kPa
        case PSI
        case mBar
        case hPa
        case pA
    }
    
    enum Humidity: Int, UnitsProtocol {
        case percent
    }
    
    enum RawUnits: Int, UnitsProtocol {
        case raw
    }
    
    enum Angle: Int, UnitsProtocol {
        case degree
    }
    
    enum Correction: Int, UnitsProtocol {
        case moa = 0
        case clicks
        case mil
        case cm
        case inches
    }
    
    enum Profile: Int, UnitsProtocol {
        case g1 = 0
        case g2
        case g5
        case g6
        case g7
        case g8
        case gs
        case gp
    }
    
    enum ClickValue: Int, UnitsProtocol {
        case halfMoa
        case oneQuarterMoa
        case oneEighthMoa
        case oneTenthMil
        case twoTenthMil
    }
}

extension UnitsConvertor {
    static var localizedUnitsBy: (_ units: UnitsProtocol) -> String {
        return { units in
            switch units {
            case is Weight: return UnitsConvertor.localizedWeight(units as! Weight)
            case is Speed: return UnitsConvertor.localizedSpeed(units as! Speed)
            case is Measure: return UnitsConvertor.localizedMeasure(units as! Measure)
            case is Distance: return UnitsConvertor.localizedDistance(units as! Distance)
            case is Direction: return UnitsConvertor.localizedDirection(units as! Direction)
            case is Temperature: return UnitsConvertor.localizedTemperature(units as! Temperature)
            case is Pressure: return UnitsConvertor.localizedPressure(units as! Pressure)
            case is Humidity: return UnitsConvertor.localizedHumidity(units as! Humidity)
            case is RawUnits: return UnitsConvertor.localizedRaw(units as! RawUnits)
            case is Profile: return UnitsConvertor.localizedProfile(units as! Profile)
            case is Angle: return UnitsConvertor.localizedAngle(units as! Angle)
            case is ClickValue: return UnitsConvertor.localizedClickValue(units as! ClickValue)
            case is Correction: return UnitsConvertor.localizedCorrection(units as! Correction)
            default:
                assert(true, "Error type \(self)")
                return "Error type"
            }
        }
    }
    
    static var allLocalizedUnitsBy: (_ units: UnitsProtocol) -> [String] {
        return { units in
            switch units {
            case is Weight: return UnitsConvertor.allLocalizedWeight
            case is Speed: return  UnitsConvertor.allLocalizedSpeed
            case is Measure: return UnitsConvertor.allLocalizedMeasure
            case is Distance: return UnitsConvertor.allLocalizedDistance
            case is Direction: return UnitsConvertor.allLocalizedDirection
            case is Temperature: return UnitsConvertor.allLocalizedTemperature
            case is Pressure: return UnitsConvertor.allLocalizedPressure
            case is Humidity: return UnitsConvertor.allLocalizedHumidity
            case is Profile: return UnitsConvertor.allLocalizedProfile
            case is Angle: return UnitsConvertor.allLocalizedAngle
            case is ClickValue: return UnitsConvertor.allLocalizedClickValue
            case is Correction: return UnitsConvertor.allLocalizedCorrection
            default:
                assert(true, "Error type \(self)")
                return ["Error type"]
            }
        }
    }
    
    static var localizedWeight: (_ value: Weight) -> String {
        return { value in
            switch value {
            case .grain: return "BallisticCalc.Units.Grain".localized()
            case .gram: return "BallisticCalc.Units.Gram".localized()
            }
        }
    }
    static var allLocalizedWeight: [String] {
        return ["BallisticCalc.Units.Grain".localized(),
                "BallisticCalc.Units.Gram".localized()]
    }
    
    static var localizedSpeed: (_ value: Speed) -> String {
        return { value in
            switch value {
            case .fps: return "BallisticCalc.Units.FootPerSecond".localized()
            case .mph: return "BallisticCalc.Units.MilePerHour".localized()
            case .ms: return "BallisticCalc.Units.MetrePerSecond".localized()
            }
        }
    }
    static var allLocalizedSpeed: [String] {
    return ["BallisticCalc.Units.MetrePerSecond".localized(),
            "BallisticCalc.Units.FootPerSecond".localized(),
            "BallisticCalc.Units.MilePerHour".localized()]
    }
    
    static var localizedMeasure: (_ value: Measure) -> String {
        return { value in
            switch value {
            case .inch: return "BallisticCalc.Units.Inch".localized()
            case .m: return "BallisticCalc.Units.Metre".localized()
            case .mm: return "BallisticCalc.Units.Millimetre".localized()
            }
        }
    }
    static var allLocalizedMeasure: [String] {
        return ["BallisticCalc.Units.Millimetre".localized(),
                "BallisticCalc.Units.Inch".localized(),
                "BallisticCalc.Units.Metre".localized()]
    }
    
    static var localizedDistance: (_ value: Distance) -> String {
        return { value in
            switch value {
            case .m: return "BallisticCalc.Units.Metre".localized()
            case .yd: return "BallisticCalc.Units.Yard".localized()
            }
        }
    }
    static var allLocalizedDistance: [String] {
        return ["BallisticCalc.Units.Metre".localized(),
                "BallisticCalc.Units.Yard".localized()]
    }
    
    static var localizedTemperature: (_ value: Temperature) -> String {
        return { value in
            switch value {
            case .c: return "BallisticCalc.Units.DegreeCelsius".localized()
            case .f: return "BallisticCalc.Units.DegreeFahrenheit".localized()
            case .k: return "BallisticCalc.Units.DegreeKelvin".localized()
            }
        }
    }
    static var allLocalizedTemperature: [String] {
        return ["BallisticCalc.Units.DegreeCelsius".localized(),
                "BallisticCalc.Units.DegreeFahrenheit".localized(),
                "BallisticCalc.Units.DegreeKelvin".localized()]
    }
    
    static var localizedDirection: (_ value: Direction) -> String {
        return { value in
            switch value {
            case .left: return "BallisticCalc.SetRifle.TwistType.Left".localized()
            case .right: return "BallisticCalc.SetRifle.TwistType.Right".localized()
            }
        }
    }
    static var allLocalizedDirection: [String] {
        return ["BallisticCalc.SetRifle.TwistType.Left".localized(),
                "BallisticCalc.SetRifle.TwistType.Right".localized()]
    }
    
    static var localizedPressure: (_ value: Pressure) -> String {
        return { value in
            switch value {
            case .hPa: return "BallisticCalc.Units.Hectopascal".localized()
            case .inHg: return "BallisticCalc.Units.InchOfMercury".localized()
            case .pA: return "BallisticCalc.Units.Pascal".localized()
            case .mmHg: return "BallisticCalc.Units.mmHg".localized()
            case .kPa: return "BallisticCalc.Units.kPa".localized()
            case .PSI: return "BallisticCalc.Units.PSI".localized()
            case .mBar: return "BallisticCalc.Units.mbar".localized()
            }
        }
    }
    static var allLocalizedPressure: [String] {
        return ["BallisticCalc.Units.InchOfMercury".localized(),
                "BallisticCalc.Units.mmHg".localized(),
                "BallisticCalc.Units.kPa".localized(),
                "BallisticCalc.Units.PSI".localized(),
                "BallisticCalc.Units.mbar".localized(),
                "BallisticCalc.Units.Hectopascal".localized(),
                "BallisticCalc.Units.Pascal".localized()]
    }
    
    static var localizedHumidity: (_ value: Humidity) -> String {
        return { value in
            switch value {
            case .percent: return "BallisticCalc.Units.Percent".localized()
            }
        }
    }
    static var allLocalizedHumidity: [String] {
        return ["BallisticCalc.Units.Percent".localized()]
    }
    
    static var localizedRaw: (_ value: RawUnits) -> String {
        return { value in
            switch value {
            case .raw: return "".localized()
            }
        }
    }
    
    static var localizedProfile: (_ value: Profile) -> String {
        return { value in
            return BallisticTable.stringBy(value)
        }
    }
    static var allLocalizedProfile: [String] {
        return ["G1", "G2", "G5", "G6", "G7", "G8" ,"GS", "GP"]
    }
    
    static var localizedAngle: (_ value: Angle) -> String {
        return { value in
            switch value {
            case .degree: return "BallisticCalc.Units.Degree".localized()
            }
        }
    }
    static var allLocalizedAngle: [String] {
        return ["BallisticCalc.Units.Degree".localized()]
    }
    
    static var localizedClickValue: (_ value: ClickValue) -> String {
        return { value in
            switch value {
            case .halfMoa: return "BallisticCalc.Units.1/2Moa".localized()
            case .oneEighthMoa: return "BallisticCalc.Units.1/8Moa".localized()
            case .oneQuarterMoa: return "BallisticCalc.Units.1/4Moa".localized()
            case .oneTenthMil: return "BallisticCalc.Units.1/10Mil".localized()
            case .twoTenthMil: return "BallisticCalc.Units.2/10Mil".localized()
            }
        }
    }
    static var allLocalizedClickValue: [String] {
        return ["BallisticCalc.Units.1/2Moa".localized(),
                "BallisticCalc.Units.1/4Moa".localized(),
                "BallisticCalc.Units.1/8Moa".localized(),
                "BallisticCalc.Units.1/10Mil".localized(),
                "BallisticCalc.Units.2/10Mil".localized()]
    }
    
    static var localizedCorrection: (_ value: Correction) -> String {
        return { value in
            switch value {
            case .moa: return "BallisticCalc.Units.MOA".localized()
            case .clicks: return "BallisticCalc.Units.Clicks".localized()
            case .mil: return "BallisticCalc.Units.Mildots".localized()
            case .cm: return "BallisticCalc.Units.Cm".localized()
            case .inches: return "BallisticCalc.Units.Inch".localized()
            }
        }
    }
    static var allLocalizedCorrection: [String] {
        return ["BallisticCalc.Units.MOA".localized(),
                "BallisticCalc.Units.Clicks".localized(),
                "BallisticCalc.Units.Mildots".localized(),
                "BallisticCalc.Units.Cm".localized(),
                "BallisticCalc.Units.Inch".localized()]
    }
}

class UnitsConvertor {
    class func convertUnitsBy<T: RawRepresentable>(value: (Double, T) ,destUnits: T) -> (Double, T) where T.RawValue == Int {
            switch value.1 {
            case is Weight:
                let result = weight((value.0, value.1 as! Weight), destUnits as! UnitsConvertor.Weight)
                return (result.0, result.1 as! T)
            case is Speed:
                let result = speed((value.0, value.1 as! Speed), destUnits as! Speed)
                return (result.0, result.1 as! T)
            case is Measure:
                let result = measure((value.0, value.1 as! Measure), destUnits as! Measure)
                return (result.0, result.1 as! T)
            case is Distance:
                let result = distance((value.0, value.1 as! Distance), destUnits as! Distance)
                return (result.0, result.1 as! T)
            case is Temperature:
                let result = temperature((value.0, value.1 as! Temperature), destUnits as! Temperature)
                return (result.0, result.1 as! T)
            case is Pressure:
                let result = pressure((value.0, value.1 as! Pressure), destUnits as! Pressure)
                return (result.0, result.1 as! T)
            case is Direction:
                let result = direction((value.0, value.1 as! Direction), destUnits as! Direction)
                return (result.0, result.1 as! T)
            case is Profile:
                let result = profile((value.0, value.1 as! Profile), destUnits as! Profile)
                return (result.0, result.1 as! T)
            case is ClickValue:
                let result = clickValue((value.0, value.1 as! ClickValue), destUnits as! ClickValue)
                return (result.0, result.1 as! T)
            case is Correction:
                return (value.0, destUnits)
            default: return value
                
            }
    }
    
    static var speed: ((Double, Speed) ,Speed) -> (Double, Speed) {
        get {
            return { value ,units in
                switch (value.1, units) {
                case (.ms, .mph): return (UnitsConvertor.ms_mph(value.0), .mph)
                case (.mph, .ms): return (UnitsConvertor.mph_ms(value.0), .ms)
                case (.fps, .mph): return (UnitsConvertor.fps_mph(value.0), .mph)
                case (.fps, .ms): return (UnitsConvertor.fps_ms(value.0), .ms)
                case (.mph, .fps): return (UnitsConvertor.mph_fps(value.0), .fps)
                case (.ms, .fps): return (UnitsConvertor.ms_fps(value.0), .fps)
                default: return value
                }
            }
        }
    }
    
    static var weight: ((Double, Weight), Weight) -> (Double, Weight) {
        get {
            return { value ,units in
                switch (value.1, units) {
                case (.gram, .grain): return (UnitsConvertor.gram_grain(value.0), .grain)
                case (.grain, .gram): return (UnitsConvertor.grain_gram(value.0), .gram)
                default: return value
                }
            }
        }
    }
    
    static var measure: ((Double, Measure) ,Measure) -> (Double, Measure) {
        get {
            return { value ,units in
                switch (value.1, units) {
                case (.inch, .mm): return (UnitsConvertor.inch_mm(value.0), .mm)
                case (.inch, .m): return (UnitsConvertor.inch_m(value.0), .m)
                case (.mm, .inch): return (UnitsConvertor.mm_inch(value.0), .inch)
                case (.m, .inch): return (UnitsConvertor.m_inch(value.0), .inch)
                case (.m, .mm): return (UnitsConvertor.m_mm(value.0), .mm)
                case (.mm, .m): return (UnitsConvertor.mm_m(value.0), .m)
                default: return value
                }
            }
        }
    }
    
    static var distance: ((Double, Distance) ,Distance) -> (Double, Distance) {
        get {
            return { value ,units in
                switch (value.1, units) {
                case (.m, .yd): return (UnitsConvertor.meter_yard(value.0), .yd)
                case (.yd, .m): return (UnitsConvertor.yard_meter(value.0), .m)
                default: return value
                }
            }
        }
    }
    
    static var temperature: ((Double, Temperature) ,Temperature) -> (Double, Temperature) {
        get {
            return { value ,units in
                switch (value.1, units) {
                case (.c, .f): return (UnitsConvertor.celsium_fahrenheit(value.0), units)
                case (.f, .c): return (UnitsConvertor.fahrenheit_celsium(value.0), units)
                case (.c, .k): return (UnitsConvertor.celsium_kelvin(value.0), units)
                case (.f, .k): return (UnitsConvertor.fahrenheit_kelvin(value.0), units)
                case (.k, .f): return (UnitsConvertor.kelvin_fahrenheit(value.0), units)
                case (.k, .c): return (UnitsConvertor.kelvin_celcium(value.0), units)
                default: return value
                }
            }
        }
    }

    static var pressure: ((Double, Pressure) ,Pressure) -> (Double, Pressure) {
        get {
            return { value ,units in
                switch (value.1, units) {
                case (.hPa, .inHg): return (UnitsConvertor.hPa_inHg(value.0), units)
                case (.hPa, .pA): return (UnitsConvertor.hPa_pa(value.0), units)
                case (.hPa, .mmHg): return (UnitsConvertor.hPa_mmHg(value.0), units)
                case (.hPa, .kPa): return (UnitsConvertor.hPa_kPa(value.0), units)
                case (.hPa, .PSI): return (UnitsConvertor.hPa_PSI(value.0), units)
                case (.hPa, .mBar): return (value.0, units)
                    
                case (.inHg, .hPa): return (UnitsConvertor.inHg_hPa(value.0), units)
                case (.inHg, .pA): return (UnitsConvertor.inHg_pA(value.0), units)
                case (.inHg, .mmHg): return (UnitsConvertor.inHg_mmHg(value.0), units)
                case (.inHg, .kPa): return (UnitsConvertor.inHg_kPa(value.0), units)
                case (.inHg, .PSI): return (UnitsConvertor.inHg_PSI(value.0), units)
                case (.inHg, .mBar): return (UnitsConvertor.inHg_mBar(value.0), units)
                    
                case (.pA, .inHg): return (UnitsConvertor.pA_inHg(value.0), units)
                case (.pA, .hPa): return (UnitsConvertor.pA_hPa(value.0), units)
                case (.pA, .mmHg): return (UnitsConvertor.pA_mmHg(value.0), units)
                case (.pA, .kPa): return (UnitsConvertor.pA_kPa(value.0), units)
                case (.pA, .PSI): return (UnitsConvertor.pA_PSI(value.0), units)
                case (.pA, .mBar): return (UnitsConvertor.pA_mBar(value.0), units)
                    
                case (.mmHg, .hPa): return (UnitsConvertor.mmHg_hPa(value.0), units)
                case (.mmHg, .inHg): return (UnitsConvertor.mmHg_inHg(value.0), units)
                case (.mmHg, .kPa): return (UnitsConvertor.mmHg_kPa(value.0), units)
                case (.mmHg, .PSI): return (UnitsConvertor.mmHg_PSI(value.0), units)
                case (.mmHg, .mBar): return (UnitsConvertor.mmHg_mBar(value.0), units)
                case (.mmHg, .pA): return (UnitsConvertor.mmHg_pA(value.0), units)
                    
                case (.kPa, .hPa): return (UnitsConvertor.kPa_hPa(value.0), units)
                case (.kPa, .inHg): return (UnitsConvertor.kPa_inHg(value.0), units)
                case (.kPa, .mmHg): return (UnitsConvertor.kPa_mmHg(value.0), units)
                case (.kPa, .PSI): return (UnitsConvertor.kPa_PSI(value.0), units)
                case (.kPa, .mBar): return (UnitsConvertor.kPa_mBar(value.0), units)
                case (.kPa, .pA): return (UnitsConvertor.kPa_pA(value.0), units)
                    
                case (.PSI, .hPa): return (UnitsConvertor.PSI_hPa(value.0), units)
                case (.PSI, .inHg): return (UnitsConvertor.PSI_inHg(value.0), units)
                case (.PSI, .mmHg): return (UnitsConvertor.PSI_mmHg(value.0), units)
                case (.PSI, .kPa): return (UnitsConvertor.PSI_kPa(value.0), units)
                case (.PSI, .mBar): return (UnitsConvertor.PSI_mBar(value.0), units)
                case (.PSI, .pA): return (UnitsConvertor.PSI_pA(value.0), units)
                    
                case (.mBar, .hPa): return (UnitsConvertor.mBar_hPa(value.0), units)
                case (.mBar, .inHg): return (UnitsConvertor.mBar_inHg(value.0), units)
                case (.mBar, .mmHg): return (UnitsConvertor.mBar_mmHg(value.0), units)
                case (.mBar, .kPa): return (UnitsConvertor.mBar_kPa(value.0), units)
                case (.mBar, .PSI): return (UnitsConvertor.mBar_PSI(value.0), units)
                case (.mBar, .pA): return (UnitsConvertor.mBar_pA(value.0), units)
                    
                default: return value
                }
            }
        }
    }
    
    static var direction: ((Double, Direction), Direction) -> (Double, Direction) {
        get {
            return { value ,units in
                return (value.0, units)
            }
        }
    }
    
    static var profile: ((Double, Profile), Profile) -> (Double, Profile) {
        get {
            return { value ,units in
                return (value.0, units)
            }
        }
    }
    
    static var clickValue: ((Double, ClickValue), ClickValue) -> (Double, ClickValue) {
        get {
            return { value ,units in
                return (value.0, units)
            }
        }
    }
    
    // Convertation
    class func mm_inch(_ mm: Double) -> Double {
        return mm * 0.0393701
    }
    
    class func m_inch(_ m: Double) -> Double {
        return m * 39.3701
    }
    
    class func inch_mm(_ inch: Double) -> Double {
        return inch * 25.4
    }
    
    class func gram_grain(_ gram: Double) -> Double {
        return gram * 15.4323584
    }
    
    class func grain_gram(_ grain: Double) -> Double {
        return grain / 15.4323584
    }
    
    class func meter_yard(_ meter: Double) -> Double {
        return (meter * 0.0936133) + meter
    }
    
    class func yard_meter(_ yard: Double) -> Double {
        return yard - (yard * 0.0856)
    }
    
    class func celsium_fahrenheit(_ celsium: Double) -> Double {
        return celsium * 9/5+32
    }
    
    class func fahrenheit_celsium(_ fahrenheit: Double) -> Double {
        return (fahrenheit - 32) * 5/9
    }
    
    class func celsium_kelvin(_ celsium: Double) -> Double {
        return celsium + 273.15
    }
    
    class func kelvin_fahrenheit(_ kelvin: Double) -> Double {
        return kelvin * 9/5 - 459.67
    }
    
    class func kelvin_celcium(_ kelvin: Double) -> Double {
        return kelvin * 9/5 - 459.67
    }
    
    class func fahrenheit_kelvin(_ fahrenheit: Double) -> Double {
        return (fahrenheit + 459.67) * 5/9
    }
    
    class func ms_fps(_ ms: Double) -> Double {
        return ms * 3.28084
    }
    
    class func mph_fps(_ mph: Double) -> Double {
        return mph * 1.466667
    }
    
    class func ms_mph(_ ms: Double) -> Double {
        return ms * 2.23694
    }
    
    class func mph_ms(_ ms: Double) -> Double {
        return ms * 0.44704
    }
    
    class func fps_mph(_ mph: Double) -> Double {
        return mph / 1.466667
    }
    
    class func fps_ms(_ fps: Double) -> Double {
        return fps * 0.3048
    }
    
    class func mm_m(_ mm: Double) -> Double {
        return mm * 0.001
    }
    
    class func m_mm(_ m: Double) -> Double {
        return m / 0.001
    }
    
    class func inch_m(_ inch: Double) -> Double {
        return inch * 0.0254
    }
    
    // Pressure
    
    //
    class func hPa_pa(_ hPa: Double) -> Double {
        return hPa * 100
    }
    
    class func hPa_mmHg(_ hPa: Double) -> Double {
        return hPa * 0.75006156130264
    }
    
    class func hPa_kPa(_ hPa: Double) -> Double {
        return hPa * 0.1
    }
    
    class func hPa_PSI(_ hPa: Double) -> Double {
        return hPa * 0.0145038
    }
    
    class func hPa_inHg(_ hPa: Double) -> Double {
        return 0.02953 * hPa
    }
    
    class func hPa_mBar(_ hPa: Double) -> Double {
        return UnitsConvertor.inHg_mBar(UnitsConvertor.hPa_inHg(hPa))
    }
    
    //
    class func inHg_hPa(_ inHg: Double) -> Double {
        return inHg * 33.8639
    }
    
    class func inHg_mmHg(_ inHg: Double) -> Double {
        return inHg * 25.399999704976
    }
    
    class func inHg_kPa(_ inHg: Double) -> Double {
        return inHg * 3.38639
    }
    
    class func inHg_PSI(_ inHg: Double) -> Double {
        return inHg * 0.491154
    }
    
    class func inHg_mBar(_ inHg: Double) -> Double {
        return inHg * 33.8639
    }
    
    class func inHg_pA(_ inHg: Double) -> Double {
        return inHg * 3386.39
    }
    
    //
    class func pA_inHg(_ pA: Double) -> Double {
        return (pA * 0.0005906) / 2
    }
    
    class func pA_hPa(_ pA: Double) -> Double {
        return pA * 0.01
    }
    
    class func pA_mmHg(_ pA: Double) -> Double {
        return UnitsConvertor.inHg_mmHg(UnitsConvertor.pA_inHg(pA))
    }
    
    class func pA_kPa(_ pA: Double) -> Double {
        return UnitsConvertor.inHg_kPa(UnitsConvertor.pA_inHg(pA))
    }
    
    class func pA_PSI(_ pA: Double) -> Double {
        return UnitsConvertor.inHg_PSI(UnitsConvertor.pA_inHg(pA))
    }
    
    class func pA_mBar(_ pA: Double) -> Double {
        return UnitsConvertor.inHg_mBar(UnitsConvertor.pA_inHg(pA))
    }
    
    //
    class func  mmHg_hPa(_ mmHg: Double) -> Double {
        return mmHg * 1.3332239
    }
    
    class func  mmHg_inHg(_ mmHg: Double) -> Double {
        return UnitsConvertor.hPa_inHg(UnitsConvertor.mmHg_hPa(mmHg))
    }
    
    class func  mmHg_kPa(_ mmHg: Double) -> Double {
        return UnitsConvertor.inHg_kPa(UnitsConvertor.mmHg_inHg(mmHg))
    }
    
    class func  mmHg_PSI(_ mmHg: Double) -> Double {
        return UnitsConvertor.inHg_PSI(UnitsConvertor.mmHg_inHg(mmHg))
    }
    
    class func  mmHg_mBar(_ mmHg: Double) -> Double {
        return UnitsConvertor.inHg_mBar(UnitsConvertor.mmHg_inHg(mmHg))
    }
    
    class func  mmHg_pA(_ mmHg: Double) -> Double {
        return UnitsConvertor.inHg_pA(UnitsConvertor.mmHg_inHg(mmHg))
    }
    
    //
    class func  kPa_inHg(_ kPa: Double) -> Double {
        return kPa * 0.2953
    }
    
    class func  kPa_hPa(_ kPa: Double) -> Double {
        return UnitsConvertor.inHg_hPa(UnitsConvertor.kPa_inHg(kPa))
    }
    
    class func  kPa_mmHg(_ kPa: Double) -> Double {
        return UnitsConvertor.inHg_mmHg(UnitsConvertor.kPa_inHg(kPa))
    }
    
    class func  kPa_pA(_ kPa: Double) -> Double {
        return UnitsConvertor.inHg_pA(UnitsConvertor.kPa_inHg(kPa))
    }
    
    class func  kPa_mBar(_ kPa: Double) -> Double {
        return UnitsConvertor.inHg_mBar(UnitsConvertor.kPa_inHg(kPa))
    }
    
    class func  kPa_PSI(_ kPa: Double) -> Double {
        return UnitsConvertor.inHg_PSI(UnitsConvertor.kPa_inHg(kPa))
    }
    
    //
    class func  PSI_inHg(_ PSI: Double) -> Double {
        return PSI * 2.03602
    }
    
    class func  PSI_hPa(_ PSI: Double) -> Double {
        return UnitsConvertor.inHg_hPa(UnitsConvertor.PSI_inHg(PSI))
    }
    
    class func  PSI_pA(_ PSI: Double) -> Double {
        return UnitsConvertor.inHg_pA(UnitsConvertor.PSI_inHg(PSI))
    }
    
    class func  PSI_kPa(_ PSI: Double) -> Double {
        return UnitsConvertor.inHg_kPa(UnitsConvertor.PSI_inHg(PSI))
    }
    
    class func  PSI_mmHg(_ PSI: Double) -> Double {
        return UnitsConvertor.inHg_mmHg(UnitsConvertor.PSI_inHg(PSI))
    }
    
    class func  PSI_mBar(_ PSI: Double) -> Double {
        return UnitsConvertor.inHg_mBar(UnitsConvertor.PSI_inHg(PSI))
    }
    
    //
    class func  mBar_inHg(_ mBar: Double) -> Double {
        return mBar * 0.02953
    }
    
    class func  mBar_hPa(_ mBar: Double) -> Double {
        return UnitsConvertor.inHg_hPa(UnitsConvertor.mBar_inHg(mBar))
    }
    
    class func  mBar_pA(_ mBar: Double) -> Double {
        return UnitsConvertor.inHg_pA(UnitsConvertor.mBar_inHg(mBar))
    }
    
    class func  mBar_kPa(_ mBar: Double) -> Double {
        return UnitsConvertor.inHg_kPa(UnitsConvertor.mBar_inHg(mBar))
    }
    
    class func  mBar_mmHg(_ mBar: Double) -> Double {
        return UnitsConvertor.inHg_mmHg(UnitsConvertor.mBar_inHg(mBar))
    }
    
    class func  mBar_PSI(_ mBar: Double) -> Double {
        return UnitsConvertor.inHg_PSI(UnitsConvertor.mBar_inHg(mBar))
    }
}
