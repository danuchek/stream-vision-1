//
//  SetBulletBaseModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/9/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

class SetBallisticCalculatorBaseModel {
    var bullet: SetBulletModel = SetBulletModel()
    var rifle: SetRifleModel = SetRifleModel()
    var outdoor: SetOutdoorModel = SetOutdoorModel()
}
