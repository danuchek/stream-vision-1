//
//  BallisticBulletModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/13/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

extension SetBulletModel {
    struct CoefficientModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.RawUnits
        static var defaultValue: Double = 0.346
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 2, min: 0.001), pattern: (integer: 1, decimal: 3), units: .raw)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct ProfileModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Profile
        static var defaultValue: Double = 0
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .g1),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .g2),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .g5),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .g6),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .g7),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .g8),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .gs),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .gp)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct CaliberModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Measure
        static var defaultValue: Double = 7.62
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 20, min: 1), pattern: (integer: 2, decimal: 2), units: .mm),
             (limits: (max: 0.787, min: 0.039), pattern: (integer: 1, decimal: 3), units: .inch)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct WeightModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Weight
        static var defaultValue: Double = 100.0
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 900, min: 1), pattern: (integer: 3, decimal: 2), units: .gram),
             (limits: (max: 58.31, min: 0.06), pattern: (integer: 2, decimal: 2), units: .grain)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct LengthModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Measure
        static var defaultValue: Double = 10
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 80, min: 1), pattern: (integer: 2, decimal: 2), units: .mm),
             (limits: (max: 3.14, min: 0.039), pattern: (integer: 1, decimal: 3), units: .inch)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
}

struct SetBulletModel {
    var selectedBullet: SetBulletChooseBulletDataSource.Bullet?
    var isWithCustomBullet: Bool = false
    var isWithAmmunition: Bool = true
    var isWithGyroEffect: Bool = false
    
    var coefficient: CoefficientModel?
    var profile: ProfileModel?
    var caliber: CaliberModel?
    var weight:  WeightModel?
    var length:  LengthModel?
    
    var bulletCD: [Double] = []
}
