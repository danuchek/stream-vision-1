//
//  BallisticCalcPreset.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/28/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import RealmSwift

class RealmSelectedBulletModel: Object {
    @objc dynamic var index: Int = 0
    @objc dynamic var manufacture: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var calibre: String = ""
}

class DoubleObject: Object {
    @objc dynamic var value: Double = 0.0
}

class RealmBulletModel: Object {
    @objc dynamic var selectedBullet: RealmSelectedBulletModel? = nil
    @objc dynamic var bulletCD: String = ""
    @objc dynamic var muzzle: Double = 0.0; @objc dynamic var muzzleUnits: Int = 0
    @objc dynamic var isWithCustomBullet: Bool = false; @objc dynamic var isWithGyroEffect: Bool = false
    @objc dynamic var isWithAmmunition: Bool = false;
    
    let profile = RealmOptional<Int>()
    let coefficient = RealmOptional<Double>();
    let caliber = RealmOptional<Double>(); let caliberUnits = RealmOptional<Int>()
    let weight  = RealmOptional<Double>(); let weightUnits  = RealmOptional<Int>()
    let length  = RealmOptional<Double>(); let lengthUnits  = RealmOptional<Int>()
}

class RealmRifleModel: Object {
    @objc dynamic var sights: Double = 0.0; @objc dynamic var sightsUnits: Int = 0
    @objc dynamic var zero: Double = 0.0; @objc dynamic var zeroUnits: Int = 0
    @objc dynamic var clickValue: Int = 0;
    
    let twistDirection = RealmOptional<Int>()
    let twistValue = RealmOptional<Double>()
}

class RealmOutdoorModel: Object {
    let temperature = RealmOptional<Double>(); let temperatureUnits = RealmOptional<Int>();
    let pressure = RealmOptional<Double>(); let pressureUnits = RealmOptional<Int>();
    let humidity = RealmOptional<Double>(); let humidityUnits = RealmOptional<Int>();
}

extension BallisticCalcPreset {
    class func by(_ model: SetBallisticCalculatorBaseModel) -> BallisticCalcPreset {
        let resultModel = BallisticCalcPreset()
        
        // Setup bullet
        resultModel.bullet = RealmBulletModel()
        resultModel.bullet?.muzzle = model.rifle.muzzle!.value.value
        resultModel.bullet?.muzzleUnits = model.rifle.muzzle!.value.unit.rawValue
        resultModel.bullet?.profile.value = model.bullet.profile?.value.unit.rawValue
        
        // Optional
        resultModel.bullet?.coefficient.value = model.bullet.coefficient?.value.value
        resultModel.bullet?.caliber.value = model.bullet.caliber?.value.value
        resultModel.bullet?.caliberUnits.value = model.bullet.caliber?.value.unit.rawValue
        if let selectedBullet = model.bullet.selectedBullet {
            let bullet = RealmSelectedBulletModel()
            bullet.index = selectedBullet.index
            bullet.name = selectedBullet.name
            bullet.manufacture = selectedBullet.manufacture
            bullet.calibre = selectedBullet.calibreForDescripton
            resultModel.bullet?.selectedBullet = bullet
        }
        resultModel.bullet?.isWithCustomBullet = model.bullet.isWithCustomBullet
        resultModel.bullet?.isWithGyroEffect = model.bullet.isWithGyroEffect
        resultModel.bullet?.isWithAmmunition = model.bullet.isWithAmmunition
        
        resultModel.bullet?.weight.value = model.bullet.weight?.value.value
        resultModel.bullet?.weightUnits.value = model.bullet.weight?.value.unit.rawValue
        
        resultModel.bullet?.length.value = model.bullet.length?.value.value
        resultModel.bullet?.lengthUnits.value = model.bullet.length?.value.unit.rawValue
        
        resultModel.bullet?.bulletCD = model.bullet.bulletCD.description
        
        // Setup rifle
        resultModel.rifle = RealmRifleModel()
        resultModel.rifle?.sights = model.rifle.sights!.value.value
        resultModel.rifle?.sightsUnits = model.rifle.sights!.value.unit.rawValue
        
        resultModel.rifle?.zero = model.rifle.zero!.value.value
        resultModel.rifle?.zeroUnits = model.rifle.zero!.value.unit.rawValue
        
        resultModel.rifle?.clickValue = model.rifle.clickValue!.value.unit.rawValue
        
        resultModel.rifle?.twistDirection.value = model.rifle.twistDirection?.value.unit.rawValue
        resultModel.rifle?.twistValue.value = model.rifle.twistValue?.value.value
        
        // Setup outdoor
        resultModel.outdoor = RealmOutdoorModel()
        resultModel.outdoor?.temperature.value = model.outdoor.temperature?.value.value
        resultModel.outdoor?.temperatureUnits.value = model.outdoor.temperature?.value.unit.rawValue
        
        resultModel.outdoor?.pressure.value = model.outdoor.pressure?.value.value
        resultModel.outdoor?.pressureUnits.value = model.outdoor.pressure?.value.unit.rawValue
        
        resultModel.outdoor?.humidity.value = model.outdoor.humidity?.value.value
        resultModel.outdoor?.humidityUnits.value = model.outdoor.humidity?.value.unit.rawValue
        
        return resultModel
    }
    
    func setBallisticCalcBaseModel() -> SetBallisticCalculatorBaseModel {
        let resultModel = SetBallisticCalculatorBaseModel()
        
        // Bullet setup
        if let profile = bullet?.profile.value {
            resultModel.bullet.profile = SetBulletModel.ProfileModel(value: (value: 0.0, unit: SetBulletModel.ProfileModel.UnitsType(rawValue: profile)!))
        }
        
        let data = bullet?.bulletCD.data(using: .utf8, allowLossyConversion: false)
        if let jsonData = data {
            if let bulletCD = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) {
                resultModel.bullet.bulletCD = bulletCD as! [Double]
            }
        }
        
        if let coefficient = bullet?.coefficient.value {
            resultModel.bullet.coefficient =
                SetBulletModel.CoefficientModel(value: (value: coefficient, unit: .raw))
        }
        
        resultModel.rifle.muzzle = SetRifleModel.MuzzleModel(value:
            (value: bullet!.muzzle, unit: SetRifleModel.MuzzleModel.UnitsType(rawValue: bullet!.muzzleUnits)!))
        
        if let caliber = bullet?.caliber.value {
            resultModel.bullet.caliber = SetBulletModel.CaliberModel(value:
                (value: caliber, unit: SetBulletModel.CaliberModel.UnitsType(rawValue: bullet!.caliberUnits.value!)!))
        }
        if let weight = bullet?.weight.value {
            resultModel.bullet.weight = SetBulletModel.WeightModel(value:
                (value: weight, unit: SetBulletModel.WeightModel.UnitsType(rawValue: bullet!.weightUnits.value!)!))
        }
        if let length = bullet?.length.value {
            resultModel.bullet.length = SetBulletModel.LengthModel(value:
                (value: length, unit: SetBulletModel.LengthModel.UnitsType(rawValue: bullet!.lengthUnits.value!)!))
        }
        if let bullet = bullet?.selectedBullet {
            resultModel.bullet.selectedBullet =
            SetBulletChooseBulletDataSource.Bullet(name: bullet.name,
                                                   manufacture: bullet.manufacture,
                                                   calibreForDescripton: bullet.calibre,
                                                   index: bullet.index,
                                                   category: nil,
                                                   calibre: nil,
                                                   length: nil,
                                                   weight: nil,
                                                   bc: nil,
                                                   muzzle: nil,
                                                   profile: nil,
                                                   bulletCD: nil)
        }
        resultModel.bullet.isWithCustomBullet = bullet!.isWithCustomBullet
        resultModel.bullet.isWithGyroEffect = bullet!.isWithGyroEffect
        resultModel.bullet.isWithAmmunition = bullet!.isWithAmmunition
        
        // Rifle setup
        resultModel.rifle.sights = SetRifleModel.SightsModel(value:
            (value: rifle!.sights, unit: SetRifleModel.SightsModel.UnitsType(rawValue: rifle!.sightsUnits)!))
        
        resultModel.rifle.zero = SetRifleModel.ZeroModel(value:
            (value: rifle!.zero, unit: SetRifleModel.ZeroModel.UnitsType(rawValue: rifle!.zeroUnits)!))
        
        resultModel.rifle.clickValue = SetRifleModel.ClickValueModel(value:
            (value: 0.0, unit: SetRifleModel.ClickValueModel.UnitsType(rawValue: rifle!.clickValue)!))
        
        if let twistValue = rifle?.twistValue.value {
            resultModel.rifle.twistValue = SetRifleModel.TwistModel(value:(value: twistValue, unit: .raw))
        }
        
        if let twistDirection = rifle?.twistDirection.value {
            resultModel.rifle.twistDirection = SetRifleModel.TwistDirectionModel(value:
                (value: 0.0, unit: SetRifleModel.TwistDirectionModel.UnitsType(rawValue: twistDirection)!))
        }
        
        // Outdoor setup
        if let temperature = outdoor?.temperature.value {
            resultModel.outdoor.temperature = SetOutdoorModel.TemperatureModel(value:
                (value: temperature, unit: SetOutdoorModel.TemperatureModel.UnitsType(rawValue: outdoor!.temperatureUnits.value!)!))
        }
        
        if let pressure = outdoor?.pressure.value {
            resultModel.outdoor.pressure = SetOutdoorModel.PressureModel(value:
                (value: pressure, unit: SetOutdoorModel.PressureModel.UnitsType(rawValue: outdoor!.pressureUnits.value!)!))
        }
        
        if let humidity = outdoor?.humidity.value {
            resultModel.outdoor.humidity = SetOutdoorModel.HumidityModel(value:
                (value: humidity, unit: SetOutdoorModel.HumidityModel.UnitsType(rawValue: outdoor!.humidityUnits.value!)!))
        }
        
        return resultModel
    }
}

class BallisticCalcPreset: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var date: Date = Date()
    
    @objc dynamic var bullet: RealmBulletModel?
    @objc dynamic var rifle: RealmRifleModel?
    @objc dynamic var outdoor: RealmOutdoorModel?
    
    override static func primaryKey() -> String? {
        return "name"
    }
}
