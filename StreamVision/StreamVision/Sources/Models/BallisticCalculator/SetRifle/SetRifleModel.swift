//
//  SetRifleModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 3/16/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//


extension SetRifleModel {
    struct MuzzleModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Speed
        static var defaultValue: Double = 980
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 2000, min: 61), pattern: (integer: 4, decimal: 0), units: .ms),
             (limits: (max: 6561, min: 200), pattern: (integer: 4, decimal: 0), units: .fps)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct SightsModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Measure
        static var defaultValue: Double = 50
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 200, min: 0), pattern: (integer: 3, decimal: 0), units: .mm),
             (limits: (max: 7.87, min: 0), pattern: (integer: 1, decimal: 2), units: .inch)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct ZeroModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Distance
        static var defaultValue: Double = 100
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 10, min: 10), pattern: (integer: 4, decimal: 0), units: .m),
             (limits: (max: 10, min: 10), pattern: (integer: 4, decimal: 0), units: .yd)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct ClickValueModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.ClickValue
        static var defaultValue: Double = 0
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .halfMoa),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .oneQuarterMoa),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .oneEighthMoa),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .oneTenthMil),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .twoTenthMil)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct TwistDirectionModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.Direction
        static var defaultValue: Double = 0
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .left),
             (limits: (max: 0, min: 0), pattern: (integer: 0, decimal: 0), units: .right)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
    
    struct TwistModel: BallisticCalcParameterProtocol {
        typealias UnitsType = UnitsConvertor.RawUnits
        static var defaultValue: Double = 12
        
        var value: (value: Double, unit: UnitsType)
        var limits: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: UnitsType)] =
            [(limits: (max: 30, min: 3), pattern: (integer: 2, decimal: 0), units: .raw)]
        
        init(value: (value: Double, unit: UnitsType)) {
            self.value = value
        }
    }
}

struct SetRifleModel {
    var muzzle: MuzzleModel?
    var sights: SightsModel?
    var zero: ZeroModel?
    var clickValue: ClickValueModel?
    var twistDirection: TwistDirectionModel?
    var twistValue: TwistModel?
}
