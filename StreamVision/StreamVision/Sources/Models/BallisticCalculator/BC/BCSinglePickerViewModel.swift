//
//  BCSinglePickerViewModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/14/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

import UIKit

struct BCSinglePickerViewModel {
    typealias SelectValueCompletion = (_ value: Double, _ units: Int) -> Double?
    typealias EditFinishedCompletion = (_ canceled: Bool) -> ()
    typealias LimitsData = [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: String)]
    
    var value: Double
    var title: String
    var step: Double = 1
    var sepparator: String = "."
    var units: Int
    
    var isWithType: Bool
    
    var data: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: String)]
    var didSelectValue: SelectValueCompletion
    var editFinished: EditFinishedCompletion
    
    init(value: Double, title: String, sepparator: String = ".",
         step: Double = 1,
         units: Int, isWithType: Bool, data: LimitsData,
         didSelectValue: @escaping SelectValueCompletion,
         editFinished: @escaping EditFinishedCompletion) {
        
        self.value = value
        self.title = title
        self.step = step
        self.sepparator = sepparator
        self.units = units
        self.isWithType = isWithType
        self.data = data
        self.didSelectValue = didSelectValue
        self.editFinished = editFinished
    }
}
