//
//  BCRangePickerViewModel.swift
//  StreamVision
//
//  Created by Vladislav Chebotaryov on 8/15/17.
//  Copyright © 2017 Ciklum. All rights reserved.
//

struct BCRangePickerViewModel {
    var value: (Double, Double)
    var title: String
    var units: Int
    
    var isWithType: Bool
    
    var data: [(limits: (max: Double, min: Double), pattern: (integer: Int, decimal: Int), units: String)]
    var didSelectValue: (_ value: Double, _ units: Int) -> Double?
    var editFinished: (_ canceled: Bool) -> ()
}
