require 'xcodeproj'
require 'fileutils'

def generate(templateName, outputPath, name)
	gen = `sourcery --sources StreamVision --templates #{templateName} --output #{outputPath} --args name=\"#{name}\"`
	print gen

	out_file = "#{outputPath}.temp"
	in_file = "#{outputPath}"

	File.open(out_file, 'w') do |out_file|
  		File.foreach(in_file).with_index do |line,line_number|
			out_file.puts line unless line_number < 5
  		end
	end
	FileUtils.mv(out_file, in_file)
end

def gps_flow(name, base, path)
	destenationPath = "StreamVision/Sources/Presentation/Controllers/GPS/#{base}/"
	teplateBasePath = "Templates/GPSFlow"

	dirname = "#{destenationPath}#{name}"
	unless File.directory? dirname
  		FileUtils.mkdir_p dirname
	end

	files = Dir[ "#{teplateBasePath}/*" ].select { |f| File.file? f }
	files.each do |item|
		filename = File.basename(item, ".*") % [name]
		generate(item, "#{path}/#{filename}.swift", name)
		File.rename "#{path}/#{filename}.swift", "#{destenationPath}#{name}/#{filename}.swift"
	end

	#FileUtils.mv("#{path}", "#{destenationPath}#{name}")
	files = Dir[ "#{destenationPath}#{name}/*" ].select { |f| File.file? f }

	project_path = 'StreamVision.xcodeproj'
	project = Xcodeproj::Project.open(project_path)
	targets = project.targets
	devTarget = targets.select{ |item| item.name == "StreamVision-Dev" }[0]
	releaseTarget = targets.select{ |item| item.name == "StreamVision" }[0]
	
	group = project.main_group[destenationPath]
	newGroup = group.new_group(name)

	file_refs = []
	absPath = Dir.pwd
	files.each do |item|
		file_refs << newGroup.new_reference("#{absPath}/#{item}")
	end
	devTarget.add_file_references(file_refs)
	releaseTarget.add_file_references(file_refs)
	project.save
end

gps_flow("TrailDetails", "Scenes", "Generation/TrailDetails")