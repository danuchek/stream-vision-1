import os
import re
import io

os.chdir("StreamVision/Resources/Localization")
for x in os.listdir('.'):
    print x
    infoPlistPath = x + "/InfoPlist.strings"
    if os.path.isfile(infoPlistPath):
        os.remove(infoPlistPath)
    locoStrings = x + "/Localizable.strings"
    if os.path.isfile(locoStrings):
        with io.open(locoStrings, "r", encoding='UTF-16', errors='ignore') as f:
            with io.open(infoPlistPath, "w", encoding='UTF-16', errors='ignore') as f1:
                for line in f:
                    newLine = ""
                    for c in line:
                        if not ord(c) == 0:
                            newLine = newLine + c
                    if re.match(r'^.*\s*.*(NS)\w*(Description).*\s*.*$', newLine):
                        print line
                        f1.write(newLine)
                f1.close()
        f.close()