import os
import argparse
import localizable
import sys
from sets import Set
import requests

STUB_PROJECT_KEY = "5R-madvBOvGc_cndawDY6kpP_YGsh6yXk"#"BKXm2zw48NrbLJBCPIdVpFt1qnApqxVo2"
ORIGINAL_PROJECT_KEY = "-7scFuL6RnpYzb2T6W-eKZf7PQgzcMnf"

reload(sys)
sys.setdefaultencoding('utf-8')

parser = argparse.ArgumentParser()
parser.add_argument('--proj')
parser.add_argument('--loco')
parser.add_argument('--update_tags', action='store_true')
parser.add_argument('--fill_tags', action='store_true')
args = parser.parse_known_args()[0]

print args


def list(path, filter, exept):
    array = []
    for e in exept:
        if e in path:
            return []

    for x in os.listdir(path):
        if not os.path.isdir(path + "/" + x):
            print 'FILE -- ' + x

            for extension in filter:
                if x.endswith(extension):
                    array.append(path + "/" + x)
                    break
        else:
            print 'DIR --------' + x
            array = list(path + "/" + x, filter, exept) + array
    return array


all_files_paths = list(args.proj, ['.swift'], ['vendor', 'MobileVLCKit', 'git', 'imageset'])
# strings = localizable.parse_strings(filename=args.loco)
exepts = ['MarkerType', 'DeviceDetails.Description', 'Viewfinder.Parameters.MarkerColor', 'Viewfinder.Parameters']

r = requests.get('https://localise.biz:443/api/tags.json?key=%s' % ORIGINAL_PROJECT_KEY)
tags = r.json()
print "TAGS: " + ', '.join(tags)

keys_tags = {}
# Get original keys with tags
for tag in tags:
    r = requests.get(
        'https://localise.biz:443/api/export/locale/en.strings?filter=%s&key=%s' % (tag, ORIGINAL_PROJECT_KEY))
    loco_array = localizable.parse_strings(r.text)
    for loco in loco_array:
        key = str(loco['key'])

        if not keys_tags.get(key):
            keys_tags[key] = Set([str(tag)])
        else:
            keys_tags[key].add(str(tag))

print keys_tags


def update_tag_func(x):
    return requests.post('https://localise.biz:443/api/tags/%s.json?key=%s' % (x, STUB_PROJECT_KEY),
                         data=key).status_code


# Update tags from original project
if args.update_tags:
    for key in keys_tags.keys():
        tags = keys_tags[key]
        for tag in tags:
            while not update_tag_func(tag) == 200:
                r = requests.post('https://localise.biz:443/api/tags.json?key=%s' % STUB_PROJECT_KEY,
                                  data={"name": tag})
                print "Error update tag(%s) and key(%s) " % (tag, key)
                print "Created tag %s code: %d" % (tag, r.status_code)
                if not update_tag_func(tag) == 200:
                    break

            print "Updated tag=%s key=%s" % (tag, key)

found = {}
keys_without_exception = []
keys_exception = []
unused_keys = []
for key in keys_tags.keys():
    notExept = 0
    for e in exepts:
        if e in key:
            notExept += 1
    if notExept == 0:
        keys_without_exception.append(key)
        found[key] = 0
    else:
        keys_exception.append(key)

for path in all_files_paths:
    f = open(path)
    fileText = f.read()
    for key in keys_without_exception:
        if key in fileText:
            found[key] += 1
            print('FOUND KEY:' + key)
    f.close()

total = 0
file = open('output.txt', 'w')
for key in found:
    if found[key] == 0:
        total += 1
        print "Any usage detected - " + key
        file.write(key + "\n")
        unused_keys.append(key)

# Remove unused keys
for key in unused_keys:
    del found[key]

file.close()

print 'Total strings: ' + str(total)

# Fill used tags
if args.fill_tags:
    for key in found.keys() + keys_exception:
        tags = ["used_ios"]
        for tag in tags:
            while not update_tag_func(tag) == 200:
                r = requests.post('https://localise.biz:443/api/tags.json?key=%s' % STUB_PROJECT_KEY,
                                  data={"name": tag})
                print "Error update tag(%s) and key(%s) " % (tag, key)
                print "Created tag %s code: %d" % (tag, r.status_code)
                if not update_tag_func(tag) == 200:
                    break

            print "Updated tag=%s key=%s" % (tag, key)

    for key in unused_keys:
        tags = ["unused_ios"]
        for tag in tags:
            while not update_tag_func(tag) == 200:
                r = requests.post('https://localise.biz:443/api/tags.json?key=%s' % STUB_PROJECT_KEY,
                                  data={"name": tag})
                print "Error update tag(%s) and key(%s) " % (tag, key)
                print "Created tag %s code: %d" % (tag, r.status_code)
                if not update_tag_func(tag) == 200:
                    break

            print "Updated tag=%s key=%s" % (tag, key)
